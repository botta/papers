% -*-Latex-*-

%if False

> module Functors

> import Syntax.PreorderReasoning

> import About

> infix 6 <==>

> %default total
> %auto_implicits off
> %access public export

%endif

\section{Functors and extensional equality preservation}
\label{section:functors}

In category theory, a functor |F| is a structure-preserving mapping
between two categories $\mathcal{C}$ and $\mathcal{D}$.
%
A functor is both
%
a total function from the objects of $\mathcal{C}$ to the objects of
$\mathcal{D}$ and
%
a total function from the arrows of $\mathcal{C}$ to the arrows of
$\mathcal{D}$ (often both denoted by |F|) such that for each arrow |f
: A -> B| in $\mathcal{C}$ there is an arrow |F f : F A -> F B| in
$\mathcal{D}$.
%
For an introduction to category theory, see \citet{pierce_basic_1991}.
%
The arrow map preserves identity arrows and arrow composition.
%
In formulas:

< F id_A     = id_FA
<
< F (g . f)  = F g . F f

Here |A| denotes an object of $\mathcal{C}$, |F A| the corresponding
object of $\mathcal{D}$ under |F|, |id_A| and |id_FA| denote the
identity arrows of |A| and |F A| in $\mathcal{C}$ and $\mathcal{D}$,
respectively and $g$ and $f$ denote arrows between suitable objects in
$\mathcal{C}$.
%
In $\mathcal{D}$, |F id_A|, |F (g . f)|, |F g| and |F f| denote the
arrows corresponding to the $\mathcal{C}$-arrows |id_A|, |g . f|, |g|
and |f|.

\paragraph*{Level of formalization.}
When considering ADT specifications of functor (and of natural
transformation, monad, etc.) in dependently typed languages, one has to
distinguish between two related but different situations.

One in which the specification is put forward in the context of
formalizations of category theory, see for example \citep{UniMath,
CoqProofAssistant}. In this situation, one has to expect the notion of
category to be in place and that of functor to be predicated on that of
its source and target categories. A functor ADT in this situation is an
answer to the question ``What shall the notion of functor look like in
dependently typed formalizations of category theory?''

A different situation is the one in which, in a dependently typed
language, we consider the category whose objects are types (in Idris,
values of type |Type|), arrows are functions, and functors are
of type |Type -> Type|.
%
In this case, a functor ADT is an answer to the question ``What does
it mean for a value of type |Type -> Type| to be a functor?''  and
category theory plays the role of a meta-theory that we use to
motivate the specification.

The latter situation is the one considered in this paper.
%
More specifically, we consider the ADTs encoded in the Haskell type
classes |Functor| and |Monad| and ask ourselves what are meaningful
specifications for these ADTs in dependently typed languages.

\paragraph*{Towards an ADT for functors.}
In Idris, the notion of a functor that preserves identity and
composition can be specified as follows (but this is not our final version):

< interface Functor (F : Type -> Type) where
<   map          :  {A, B : Type}     ->  (A -> B) -> F A -> F B
<
<   mapPresId    :  {A : Type}        ->  map id <=> id {a = F A}
<   mapPresComp  :  {A, B, C : Type}  ->  (g : B -> C) -> (f : A -> B) ->
<                                         map (g . f) <=>  map g . map f

In |mapPresId| we have to help the type checker a little bit and give
the domain of the two functions that are posited to be extensionally
equal explicitly.
%
%For the sake of readability, we will sometimes omit such details in
%the rest of this paper.

Notice that the function |map| is required to preserve identity and
composition \emph{extensionally}.\label{text:MathEqExtEq}
%
In other words, |Functor| does not require |map id| and |id| (|map (g
. f)| and |map g . map f|) to be intensionally equal but only to be
equal extensionally.
%
This is for very good reasons!
%
If functors were required to preserve identity and composition
intensionally, the interface would be hardly implementable.
%
By contrast,
%%it is easy to verify that
|Identity|, |List|, |Maybe|,
|Vect n| and many other important type constructors are functors in the
sense specified by this |Functor| interface.
%% , see |mapList| and |mapListPresEE| for the list instance in the next
%% paragraph.

Does this |Functor| interface represent a suitable Idris implementation
of the notion of functor in dependently typed languages?
%
We argue that this is not the case and that beside requiring from |map|
preservation of identity and of composition, one should
additionally require preservation of extensional equality.
%
In other words, we argue that the above specification of |Functor| is
incomplete.
%
A more complete specification could look like

> interface VeriFunctor (F : Type -> Type) where
>   map          :  {A, B : Type}     ->  (A -> B) -> F A -> F B
>
>   mapPresEE    :  {A, B : Type}     ->  (f, g : A -> B) ->  f <=> g -> map f <=> map g -- New!
>
>   mapPresId    :  {A : Type}        ->  map id <=> id {a = F A}
>   mapPresComp  :  {A, B, C : Type}  ->  (g : B -> C) -> (f : A -> B) ->
>                                         map (g . f) <=>  map g . map f

The |Identity| functor, |List|, |Maybe|, |Vect n| and, more generally,
container-like functors built from algebraic datatypes, fulfil the
complete specification and the proofs for |mapPresEE| do not add
significant work.
%
But other prominent functors such as |Reader| do not fulfil the above
specification as we will explain below.

Note that it is quite possible to continue on the road towards full
generality (supporting a larger class of functors) by parameterising
over the equalities used, but this leads to quite a bit of book-keeping
(basically a setoid-based framework).
%
We instead stop at this point and show that it is a pragmatic
compromise between generality and convenient usage.

\paragraph*{Equality preservation examples.}
%
Let's first have a look at |map| and a proof of |mapPresEE| for
|List|, one of the functors that fulfil the above specification:

> mapList : {A, B : Type} -> (A -> B) -> (List A -> List B)
> mapList f []         = []
> mapList f (a :: as)  = f a  ::  mapList f as

Written out in equational reasoning style, the preservation of EE
proof looks as follows:
%PJ: useful for debugging lengths:
%  {-" above=\the\abovedisplayskip"-}
%  {-" below=\the\belowdisplayskip"-}

> mapListPresEE :  {A, B : Type} -> (f, g : A -> B) -> f <=> g -> mapList f <=> mapList g
> mapListPresEE f g fEEg []         =      Refl
> mapListPresEE f g fEEg (a :: as)  =  {-"\,"-}
>   ( mapList f (a :: as) )            ={  Refl }=
>   ( f a :: mapList f as )            ={  cong {f = (::{-"~"-} mapList f as)} (fEEg a) }=
>   ( g a :: mapList f as )            ={  cong (mapListPresEE f g fEEg as) }=
>   ( g a :: mapList g as )            ={  Refl }=
>   ( mapList g (a :: as) )            QED

In general the proofs have a very simple structure: they use the |f
<=> g| arguments at the ``leaves'', and otherwise only use the
induction hypotheses.
%
%They can also be written as dependent folds, but this results in less
%readable proofs.
%
With a suitable universe of codes for types, or a library for
parametricity proofs, these proofs can be automated using
datatype-generic programming.

%if False
We can write |mapListPresEE| in a more condensed form using Idris'
|rewrite|:

> mapListPresEE'  :  {A, B : Type} -> (f, g : A -> B) -> f <=> g -> mapList f <=> mapList g
> mapListPresEE' f g ee [] = Refl
> mapListPresEE' f g ee (a :: as) =
>          let ih = mapListPresEE' f g ee as in
>          rewrite ih in
>          rewrite ee a in Refl

In this form, proofs of preservation of EE do not become more
complicated for data types with more constructors or more recursive
arguments.

Similarly

> data Tree : Type -> Type where
>   Leaf : {A : Type} -> A -> Tree A
>   Node : {A : Type} -> (Tree A) ->  A -> (Tree A) -> Tree A

> mapTree : {A, B : Type} -> (A -> B) -> Tree A -> Tree B
> mapTree f (Leaf a) = Leaf (f a)
> mapTree f (Node l a r) = Node (mapTree f l) (f a) (mapTree f r)

> mapTreePresEE : {A, B : Type} -> (f, g : A -> B) -> f <=> g -> mapTree f <=> mapTree g
> mapTreePresEE f g ee (Leaf a) = rewrite ee a in Refl
> mapTreePresEE f g ee (Node tl a tr) =
>            let ihl = mapTreePresEE f g ee tl in
>            let ihr = mapTreePresEE f g ee tr in
>            rewrite ee a in
>            rewrite ihl in
>            rewrite ihr in Refl

The above proofs have a very simple structure: they use the |f <=> g|
arguments to transform the arguments of type |A| expected by the
constructors into arguments of type |B|, and otherwise only use the
induction hypotheses.
%
They can also be written as dependent folds, but this results in less
readable proofs.
%
This is a common pattern for proofs of |mapPresEE|.

%endif

Let's now turn to a type constructor that is not an instance of
our |VeriFunctor|, namely |Reader E| for some environment |E : Type|.

> Reader : Type -> Type -> Type
> Reader E A = E -> A
>
> mapR : {A, B, E : Type} -> (A -> B) -> (Reader E A -> Reader E B)
> mapR f r = f . r

If we try to implement preservation of extensional equality we end up with

%if False

> whatnow1 :  {E, A, B : Type} -> (r : E -> A) -> {f, g : A -> B} ->
>             ( f . r ) = ( g . r )
> whatnow1 = ?whatnowhole1

%endif

> mapRPresEE :  {A, B : Type} -> (f, g : A -> B) -> f <=> g -> mapR f <=> mapR g
> mapRPresEE f g fEEg r  =
>   ( mapR f r)             ={ Refl }=
>   ( f . r )               ={ whatnow1 r }=     -- here we need |f = g| to proceed
>   ( g . r )               ={ Refl }=
>   ( mapR g r)             QED

Notice the question mark in front of |whatnow1r|. This introduces an
unresolved proof step and allows us to ask Idris to help us implementing
this step, see ``Elaborator Reflection -- Holes'' in \citep{idrisdocs}.
Among other things, we can ask about the
type of |whatnow1r|. Perhaps not surprisingly, this turns out to be |f
. r = g . r|.

The problem is that, although we know that |(f . r) e = (g . r) e| for
all |e : E|, we cannot deduce |f . r = g . r| without extensionality.
%
Thus |Reader E| does not implement the |VeriFunctor| interface, but it
is ``very close''.
%if False

> (<==>) : {A, B, C : Type} -> (f, g : A -> B -> C) -> Type
> (<==>) = extify (<=>)

%endif
Using the 2-argument version of function extensionality |(<==>) =
extify (<=>)| it is easy to show

> mapRPresEE2 :  {E, A, B : Type} -> (f, g : A -> B) -> f <=> g  ->  mapR f <==> mapR g
> mapRPresEE2 f g fEEg r x = fEEg (r x)

Thus, |Reader E| is an example of a functor which does not preserve,
but rather \emph{transforms} the notion of equality.
%
By a similar argument, |mapPresEE| does not hold for the continuation
monad.
%

As we mentioned earlier, it is tempting to start adding equalities to
the interface (towards a setoid-based framework), but this is not a
path we take here.
%
As a small hint of the problems the setoid path leads to, consider
that we already have four different objects (|A|, |B|, |F A|, |F B|)
and two arrow types (|A -> B|, |F A -> F B|), all of which could be
allowed ``their own'' notion of equality.

%*PJ: Perhaps: At this point it is worth mentioning that wrapping a newtype constructor around a datatype does not change its Functor status.
%*PJ: Perhaps: But isomorphisms in general are not preserved, or at least do not behave in the way we might have thought {(Bool->) is not in, but (Vect 2) is}.

% Perhaps these ``functors'' could be called pre-functors.
%
% Or perhaps the functors that preserve extensional equality could be
% called ee-functors.

\paragraph*{Wrapping up.}
%
As stated in section \ref{section:about}, we argue that, for verified
generic programming, it is useful to distinguish between type
constructors whose |map| can be shown to preserve extensional equality
and type constructors for which this is not the case.
%
A discussion of what are appropriate names for the respective ADTs is
beyond the scope of this paper.
%
%*PJ: Some related definitions are available here:
%*https://github.com/agda/agda-stdlib/blob/master/src/Relation/Binary/Core.agda
%*Nuria: something with pre-categories etc.
%
In the next section we explore how functors with |mapPresEE| affect
the monad ADT design.
%
%% We are interested in ADTs that support well verified generic programming
%% and we discuss the advantages and the disadvantages of different
%% approaches from an DSL perspective. As in section \ref{section:example},
%% we discuss applications in dynamical systems and control theory.
