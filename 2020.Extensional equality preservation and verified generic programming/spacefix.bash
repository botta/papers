#!/bin/bash

# Intended replacements (fails on multiple characters needing escaping):
# sed 's/\mathrel{=}\{\mskip1.5mu /=\!\!\{\;/g'
# sed 's/\mskip1.5mu\}\mathrel{=}/\;\}\!\!=/g'

# Instead using https://stackoverflow.com/questions/29613304/is-it-possible-to-escape-regex-metacharacters-reliably-with-sed/29626460#29626460

# "\hspace{$pts}"  instead of "\;\;" in the replacements allows finetuning

pts='-3pt'

from1='\mathrel{=}\{\mskip1.5mu'
to1='=\hspace{'
to1+=$pts
to1+='}\{\;'
from1esc=$(sed 's/[^^]/[&]/g; s/\^/\\^/g' <<<"$from1")
# Check: sed -n "s/$from1esc/foo/p" <<<"$from1" # if ok, echoes 'foo'

to1esc=$(sed 's/[&/\]/\\&/g' <<<"$to1")
# Check: sed -n "s/\(.*\) \(.*\)/$to1esc/p" <<<"foo bar" # if ok, outputs $replace as is

from2='\mskip1.5mu\}\mathrel{=}'
to2='\;\}\hspace{'
to2+=$pts
to2+='}='
from2esc=$(sed 's/[^^]/[&]/g; s/\^/\\^/g' <<<"$from2")
# Check: sed -n "s/$from2esc/foo/p" <<<"$from2" # if ok, echoes 'foo'

to2esc=$(sed 's/[&/\]/\\&/g' <<<"$to2")
# Check: sed -n "s/\(.*\) \(.*\)/$to2esc/p" <<<"foo bar" # if ok, outputs $replace as is

sed -e "s/$from1esc/$to1esc/g" -e "s/$from2esc/$to2esc/g" "$1"
