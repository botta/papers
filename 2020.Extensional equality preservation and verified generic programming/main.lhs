% -*-Latex-*-
\RequirePackage{amsmath}
\documentclass{jfp}
%\documentclass{jfphack}
\usepackage{tikz-cd}
%% begin for submission
\pgfrealjobname{main}
%% end for submission
\usepackage{quoting}
\usepackage{bigfoot} % To handle url with special characters in footnotes!
\usepackage{hyperref}
\usepackage[utf8]{inputenc}

%include main.fmt

\input{macros.TeX}

\begin{document}

\journaltitle{JFP}
\cpr{The Author(s),}
\doival{10.1017/xxxxx}

\newcommand{\shorttitle}{Extensional equality preservation and verified generic programming}
%*PJ: perhaps "Extensional eq. pres. and verified gen. prog."
\lefttitle{Botta, Brede, Jansson and Richter}
\righttitle{\shorttitle}
\totalpg{\pageref{lastpage01}}
\jnlDoiYr{2021}


\title[\shorttitle]{Extensional equality preservation and\\verified generic programming}

% double "@" in email to please lhs2tex
\begin{authgrp}
\author{NICOLA BOTTA}
\affiliation{Potsdam Institute for Climate Impact Research, Potsdam, Germany, \\
  Chalmers University of Technology, Göteborg, Sweden.
  (\email{botta@@pik-potsdam.de})}
\author{NURIA BREDE}
\affiliation{Potsdam Institute for Climate Impact Research, Potsdam,
  Germany.
  (\email{nubrede@@pik-potsdam.de})}
\author{PATRIK JANSSON}
\affiliation{Chalmers University of Technology, Göteborg, Sweden.
  (\email{patrikj@@chalmers.se})}
\author{TIM RICHTER}
\affiliation{Potsdam University, Potsdam, Germany.
  (\email{tim.richter@@uni-potsdam.de})}
\end{authgrp}


\begin{abstract}
% Yet another attempt (NiBo):
%
In verified generic programming, one cannot exploit the structure of
concrete data types but has to rely on \emph{well chosen} sets of
specifications or abstract data types (ADTs).
%
Functors and monads are at the core of many applications of functional
programming. This raises the question of what useful ADTs for
verified functors and monads could look like.
%
The functorial map of many important monads preserves extensional
equality. For instance, if |f, g : A -> B| are extensionally equal, that
is, |∀ x ∈ A, ! f x = g x|, then |map f : List A -> List B| and |map g|
are also extensionally equal.
%
This suggests that \emph{preservation of extensional equality} could be
a useful principle in verified generic programming.
%
We explore this possibility with a minimalist approach: we deal with
(the lack of) extensional equality in Martin-Löf's intensional type
theories without extending the theories or using full-fledged setoids.
%
Perhaps surprisingly, this minimal approach turns out to be extremely
useful. It allows one to derive simple generic proofs of monadic laws
but also verified, generic results in dynamical systems and control
theory.
%
In turn, these results avoid tedious code duplication and ad-hoc
proofs. Thus, our work is a contribution towards pragmatic, verified
generic programming.
\end{abstract}

\maketitle

%% \begin{keyword}
%% dependent types \sep
%% policy advice \sep
%% avoidability \sep
%% formalization \sep
%% specification \sep
%% correctness \sep
%% functional languages \sep
%% Idris \sep
%% \end{keyword}


% \renewcommand{\hscodestyle}{}
\setlength{\mathindent}{1em}
%
%\renewcommand{\texfamily}{\fontfamily{cmtex}\selectfont\small}
%\renewcommand{\hsnewpar}[1]%
%  {{\parskip=0pt\parindent=0pt\par\vskip #1\noindent}}
%\renewcommand{\hscodestyle}{}
%\newcommand{\fixsmalldisplayskip}{\setlength{\belowdisplayskip}{6pt plus 0pt minus 0pt}}
\newcommand{\fixlengths}{\setlength{\abovedisplayskip}{6pt plus 1pt minus 1pt}\setlength{\belowdisplayskip}{6pt plus 1pt minus 1pt}}
\renewcommand{\hscodestyle}{\small\fixlengths}
\fixlengths
%\setlength{\belowdisplayskip}{6pt plus 0pt minus 0pt}    %% lhs2TeX uses this
%\setlength{\belowdisplayskip}{8.5pt plus 3pt minus 4pt}  %%\small
%\setlength{\belowdisplayskip}{10pt plus 2pt minus 5pt}   %%\normalsize

%include About.lidr
%include Example.lidr
%include Functors.lidr
%include Monads.lidr
%include Applications.lidr
%include RelatedWork.lidr
%include Conclusions.lidr

\section*{Acknowledgments}
The authors thank the JFP editors and reviewers, whose comments have
lead to significant improvements of the original manuscript.

%
The work presented in this paper heavily relies on free software, among others on Coq, Idris, Agda, GHC, git, vi, Emacs, \LaTeX\ and on the FreeBSD and Debian GNU/Linux operating systems.
%
It is our pleasure to thank all developers of these excellent products.
%
This is TiPES contribution No 38. This project has received funding from
the European Union’s Horizon 2020 research and innovation programme
under grant agreement No 820970.

\subsection*{Conflicts of Interest}
None.

\bibliographystyle{jfplike}
\bibliography{references}
\label{lastpage01}
\end{document}
