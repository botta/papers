% -*-Latex-*-

%if False

> module Monads

> import Syntax.PreorderReasoning

> import About
> import Functors

> %default total
> %auto_implicits off
> %access public export

> -- %hide Prelude.Functor.Functor

> infixr 1 >=>
> infixr 1 <=<

%endif

\section{Verified monad interfaces}
\label{section:monads}

In this section, we review two standard notions of monads.
%
We discuss their mathematical equivalence and consider ``thin'' and
``fat'' monad ADT formulations.
%
We discuss the role of extensional equality preservation for deriving
monad laws and for verifying the equivalence between the different ADT
formulations.
%
There are many possible ways of formulating Monad axioms. Here, we do not
argue for or against a specific formulation but rather discuss the most
popular ones and their trade-offs.

\subsection{The traditional view}\label{sec:MonadTradView}

In category theory, a monad is
%
an \emph{endofunctor} |M| on a category $\mathcal{C}$ together with
two \emph{natural transformations}
%
|etaN : Id -.> M| (the \emph{unit}) and
%
|muN : M . M -.> M| (the \emph{multiplication})
%
such that, for any object |A| of $\mathcal{C}$, the following diagrams commute:

\begin{minipage}{0.4\textwidth}
\beginpgfgraphicnamed{ExtEqPres-f2}
\begin{tikzcd}[row sep=large, column sep=large]
|M A| \arrow[r, "|eta (M A)|"] \arrow[dr, "|id_MA|"'] & |M (M A)| \arrow[d, "|mu A|"] & |M A| \arrow[l, "|M (eta A)|"'] \arrow[dl, "|id_MA|"] \\
                                                      & |M A|                             &
\end{tikzcd}
\endpgfgraphicnamed
\end{minipage}
\hfill
\begin{minipage}{0.4\textwidth}
\beginpgfgraphicnamed{ExtEqPres-f3}
\begin{tikzcd}[row sep=large, column sep=large]
|M (M (M A))| \arrow[r, "|M (mu A)|"] \arrow[d, "|mu (M A)|"'] & |M (M A)| \arrow[d, "|mu A|"] \\
      |M (M A)| \arrow[r, "|mu A|"']                                     & |M A|
\end{tikzcd}
\endpgfgraphicnamed
\end{minipage}

\noindent
The transformations |etaN| and |muN| are families of arrows, one for
each object |A|, with types |eta A : A -> M A| and |mu A : M (M A) -> M
A|.
%
That they are \emph{natural transformations} means that the following
diagrams commute for any arrow |f : A -> B| in $\mathcal{C}$:

%\begin{minipage}{0.3\textwidth}
%\begin{tikzcd}[row sep=large, column sep=large]
%F \, A \arrow[r, "F \, f"] \arrow[d, "\gamma \, A"'] & F \, B \arrow[d, "\gamma \, B"] \\
%G \, A \arrow[r, "G \, f"']                          & G \, B
%\end{tikzcd}
%\end{minipage}
\hfill
\begin{minipage}{0.3\textwidth}
\beginpgfgraphicnamed{ExtEqPres-f4}
\begin{tikzcd}[row sep=large, column sep=large]
    |A| \arrow[r, "|f|"] \arrow[d, "|eta A|"'] & |B| \arrow[d, "|eta B|"] \\
|M  A|  \arrow[r, "|M f|"']                    & |M B|
\end{tikzcd}
\endpgfgraphicnamed
\end{minipage}
\hfill
\begin{minipage}{0.4\textwidth}
\beginpgfgraphicnamed{ExtEqPres-f5}
\begin{tikzcd}[row sep=large, column sep=large]
|M (M A)| \arrow[r, "|M (M f)|"] \arrow[d, "|mu A|"'] & |M (M B)| \arrow[d, "|mu B|"] \\
   |M A|  \arrow[r, "|M f|"']                         & |M B|
\end{tikzcd}
\endpgfgraphicnamed
\end{minipage}

\noindent
From this perspective, a monad is a functor with additional structure,
namely families of maps |etaN| and |muN|, satisfying, for any %objects |A, B| and
arrow |f: A -> B|, the five properties:
%\newpage
\newcommand{\myeta}{|etaN|}
\newcommand{\mymu} {|muN|}

< {-"\text{T1. Triangle left:             }"-}  mu A . eta (M A)  = id_MA
< {-"\text{T2. Triangle right:            }"-}  mu A . M (eta A)  = id_MA
< {-"\text{T3. Square:                    }"-}  mu A . mu (M A)   = mu A . M (mu A)
< {-"\text{T4. Naturality of \myeta:\quad }"-}  M f . eta A       = eta B . f
< {-"\text{T5. Naturality of \mymu:       }"-}  M f . mu A        = mu B . M (M f)

In functional programming, |etaN| is traditionally denoted by |return| or by
|pure| and |muN| is traditionally called |join|.
%
Idris provides language support for interface \emph{refinement}.
%
Thus, we can leverage on the functor ADT |VeriFunctor| from section
\ref{section:functors} and define a monad to be a functor with
additional methods |pure| and |join| that satisfy the requirements
T1--T5:

%if False

> namespace Thin

%endif

>   interface VeriFunctor M => ThinVeriMonad (M : Type -> Type) where
>     pure                :  {A : Type}        -> A -> M A
>     join                :  {A : Type}        -> M (M A) -> M A
>
>     triangleLeft        :  {A : Type}        -> join . pure      <=> id {a = M A}
>     triangleRight       :  {A : Type}        -> join . map pure  <=> id {a = M A}
>     square              :  {A : Type}        -> join . join      <=> join . map {A = M (M A)} join
>
>     pureNatTrans        :  {A, B : Type}     -> (f : A -> B) ->    map f . pure  <=>  pure . f
>     joinNatTrans        :  {A, B : Type}     -> (f : A -> B) ->    map f . join  <=>  join . map (map f)

\paragraph*{Kleisli composition in the traditional view.}
In section \ref{section:example}, we have seen that monads are equipped
with a (Kleisli) composition |(>=>)| that we required to fulfil |kleisliSpec|:

>   (>=>)        :  {A, B, C : Type} -> {M : Type->Type} -> ThinVeriMonad M =>
>                   (A -> M B) -> (B -> M C) -> (A -> M C)
>
>   kleisliSpec  :  {A, B, C : Type} -> {M : Type -> Type} -> ThinVeriMonad M =>
>                   (f : A -> M B) -> (g : B -> M C) -> (f >=> g) <=> join . map g . f

One way of implementing |(>=>)| that satisfies |kleisliSpec| is to \emph{define}

>   f >=> g = join . map g . f

The extensional equality between |f >=> g| and |join . map g . f|
then follows directly:

>   kleisliSpec f g = \x => Refl

The same approach can be followed for implementing bind, another monad
combinator similar to Kleisli composition:

>   (>>=)  :  {A, B : Type} -> {M : Type -> Type} -> ThinVeriMonad M => M A -> (A -> M B) -> M B
>   ma >>= f = join (map f ma)

Starting from a \emph{thin} monad ADT as in the example above and
adding monadic operators that fulfil a specification
\emph{by-construction}, is a viable approach.
%
It leads to a rich structure entailing monad laws that can be
implemented generically.
%
Thus, one can show that |pure| is a left and a right identity of
Kleisli composition (we show only one side here)
%PJ: Anchors for reference from Example session.
\label{def:pureLeftIdKleisli}\label{def:pureRightIdKleisli}

>   pureLeftIdKleisli  :   {A, B : Type} -> {M : Type -> Type} -> ThinVeriMonad M =>
>                          (f : A -> M B) ->  (pure >=> f)  <=> f
>   pureLeftIdKleisli f a =
>     ( (pure >=> f) a )         ={ Refl }=
>     ( join (map f (pure a)) )  ={ cong {f = join} (pureNatTrans f a) }=
>     ( join (pure (f a)) )      ={ triangleLeft (f a) }=
>     ( f a )                    QED

%if False

>   pureRightIdKleisli  :  {A, B : Type} -> {M : Type -> Type} -> ThinVeriMonad M =>
>                          (f : A -> M B) -> (f >=> pure) <=> f
>   pureRightIdKleisli f a =
>     ( (f >=> pure) a )         ={ Refl }=
>     ( join (map pure (f a)) )  ={ triangleRight (f a) }=
>     ( f a )                    QED

%endif
%
and that Kleisli composition is associative as stated in section
\ref{section:example}, almost straightforwardly and without having to
invoke the |mapPresEE| axiom of the underlying functor.

>   kleisliAssoc  :  {A, B, C, D : Type} -> {M : Type -> Type} -> ThinVeriMonad M =>
>                    (f : A -> M B) -> (g : B -> M C) -> (h : C -> M D) ->
>                    ((f >=> g) >=> h) <=> (f >=> (g >=> h))
>   kleisliAssoc {M} {A} {C} f g h a =
>     ( ((f >=> g) >=> h) a )                            ={ Refl }=
>     ( (join . map h .  join  . map g . f) a )          ={ cong (joinNatTrans h (map g (f a))) }=
>     ( (join . join . map (map h) . map g . f) a )      ={ square _ }=
>     ( (join . map join . map (map h) . map g . f) a )  ={ cong  {f = join . map join}
>                                                                 (sym (mapPresComp _ _ _)) }=
>     ( (join . map join . map (map h . g) . f) a )      ={ cong  {f = join}
>                                                                 (sym (mapPresComp _ _ _)) }=
>     ( (join . map (join . map h . g) . f) a )          ={ Refl }=
>     ( (f >=> (g >=> h)) a )                            QED

Notice that in order to show that Kleisli composition preserves
extensional equality, one has to rely on the |mapPresEE| axiom of the
underlying functor, as one would expect.

%if False

>   kleisliLeapfrog  :  {A, B, C : Type} -> {M : Type -> Type} -> ThinVeriMonad M =>
>                       (f : A -> M B) -> (g : B -> M C) ->
>                       (f >=> g) <=> (id >=> g) . f
>   kleisliLeapfrog f g a = Refl

%endif

\begin{joincode}%

>   kleisliPresEE  :  {A, B, C : Type} -> {M : Type -> Type} -> ThinVeriMonad M =>
>                     (f, f' : A -> M B) -> (g, g' : B -> M C) ->
>                     f <=> f' -> g <=> g' -> (f >=> g) <=> (f' >=> g')

%if False

\TODO{Perhaps use short version (skipping |Refl|-steps) or even use a combinator to make it a one-liner as in \url{https://agda.github.io/agda-categories/Categories.Category.Construction.Kleisli.html}: |∘-resp-≈  = λ f≈h g≈i → ∘-resp-≈ (∘-resp-≈ʳ (F-resp-≈ f≈h)) g≈i|.}

> {-
>   kleisliPresEE f f' g g' fE gE a =
>     ( (f >=> g)             a   )  ={ cong (fE a) }=
>     ( (id >=> g)       (f'  a)  )  ={ cong (mapPresEE g g' gE (f' a)) }=
>     ( (join . map g')  (f'  a)  )  QED
> -}

%endif

>   kleisliPresEE f f' g g' fE gE a =
>     ( (f >=> g)             a   )  ={ kleisliLeapfrog f g a }=
>     ( (id >=> g)       (f   a)  )  ={ cong (fE a) }=
>     ( (id >=> g)       (f'  a)  )  ={ Refl }=
>     ( (join . map g)   (f'  a)  )  ={ cong (mapPresEE g g' gE (f' a)) }=
>     ( (join . map g')  (f'  a)  )  ={ Refl }=
>     ( (f' >=> g')           a   )  QED

\end{joincode}

In the implementation of |kleisliPresEE|, we have
applied the ``leapfrogging'' rule (compare \citep{bird2014thinking},
p. 250):

<   kleisliLeapfrog  :  {A, B, C : Type} -> {M : Type -> Type} -> ThinVeriMonad M =>
<                       (f : A -> M B) -> (g : B -> M C) -> (f >=> g) <=> (id >=> g) . f
<   kleisliLeapfrog f g a = Refl

% \subsection{Kleisli view}\label{sec:Kleisimonad}
%
% A third way to define a monad is via its \emph{Kleisli category}.
% %
% Suppose |M| is a monad on $\mathcal{C}$ with unit |etaN| and lifting
% operation |liftS| (either given as a primitive or defined using |map|
% and |muN|) and |f : A -> M B, g : B -> M C| are
% $\mathcal{C}$-arrows.
% %
% Then one can define the \emph{Kleisli composition} of $f$ and $g$ by
% |f >=> g := liftS g . f|, and the properties of |liftS| (and
% associativity of composition in $\mathcal{C}$) imply
%
% \begin{enumerate}
% \item Kleisli left identity:  |eta A >=> f = f|
% \item Kleisli right identity: |f >=> eta B = f|
% \item Kleisli associativity:  |(f >=> g) >=> h = f >=> (g >=> h)|
% \item Kleisli leapfrogging:   |f >=> g = (id_MB >=> g) . f|%
%   \footnote{The name is from \cite{bird2014thinking}, where (p.250)
%     the equivalent |(f >=> g) . h = (f . h) >=> g| is called
%     ``leapfrog rule''.}
% \end{enumerate}
% %
% for any objects |A, B, C, W| and arrows |f : A -> M B|,
% |g : B -> M C|, |h : C -> M W|.
% %
% One defines $\mathcal{C}_M$,
% the \emph{Kleisli category of |M|}, to have for objects just the objects of
% $\mathcal{C}$ and for arrows from |A| to |B| the $\mathcal{C}$-arrows
% from |A| to |M B|.
% %
% The first three Kleisli rules say that with |(>=>)| as composition and
% |etaN| as (the family of) identity arrows, $\mathcal{C}_M$ is indeed
% a category.
% %
%
% In turn, suppose |M| is an endofunction on the objects of a category
% $\mathcal{C}$, and there is a category $\mathcal{C}_M$ with objects
% and arrows as above, composition |(>=>)| and identity arrows |eta
% A| (in $\mathcal{C}(A,M A) \cong \mathcal{C}_M(A,A)$), satisfying
% Kleisli leapfrogging.
% %
% For objects |A, B| and arrow |f : A -> B|, define
% %
% |map f = id_MB >=> (eta B . f)|,
% %
% |mu A = id_MMA >=> id_MA| and
% %
% |liftS f = id_MB >=> f|.
% %
% Then |(M,eta,mu)| (with |M| extended to a functor using map) and
% |(M,eta,liftS)| can be proved to define a monad in the sense of the above
% definitions.
% %
% Note that Kleisli leapfrogging is the only one of the
% Kleisli properties that provides a means to ``rewrite'' Kleisli
% composition to composition in $\mathcal{C}$.
% %
% This is used in proofs of the properties of |map|, |etaN|, |muN| and
% |liftS| from the Kleisli rules.

\paragraph*{Fat ADTs.}

The main advantages of the approach outlined above -- a thin ADT and
explicit definitions of the monadic combinators -- are readability and
straightforwardness of proofs: thanks to the intensional equality
between |f >=> g| and |join . map g . f|, we were able to implement many
proof steps with just |Refl|.

The strength of thin ADT designs is also their weakness: in many
practical cases, one would like to be able to define |join| in terms
of bind and not the other way round.
%
In other words, one would like to weaken the requirements on, e.g.,
|join|, |map| and Kleisli composition and just require that |f >=> g|
and |join . map g . f| are extensionally equal.
%
If they happen to be intensionally equal for a specific instance, the
better.

This suggests that an alternative way of formalizing the traditional
notion of monads from category theory could be through a \emph{fat} ADT:
\pagebreak
%if False

> namespace Fat

%endif

>   interface VeriFunctor M => VeriMonadFat (M : Type -> Type) where
>
>     pure                :  {A : Type}        ->  A -> M A
>     join                :  {A : Type}        ->  M (M A) -> M A
>     (>>=)               :  {A, B : Type}     ->  M A -> (A -> M B) -> M B
>     (>=>)               :  {A, B, C : Type}  ->  (A -> M B) -> (B -> M C) -> (A -> M C)
>
>     bindJoinMapSpec     :  {A, B : Type}     ->  (f : A -> M B)  ->  (>>= f)    <=>  join . map f
>     kleisliJoinMapSpec  :  {A, B, C : Type}  ->  (f : A -> M B)  ->
>                                                  (g : B -> M C)  ->  (f >=> g)  <=>  join . map g . f
>
>     triangleLeft        :  {A : Type}    ->  join . pure      <=> id {a = M A}
>     triangleRight       :  {A : Type}    ->  join . map pure  <=> id {a = M A}
>     square              :  {A : Type}    ->  join . join      <=> join . map {A = M (M A)} join
>
>     pureNatTrans        :  {A, B : Type}   ->  (f : A -> B) ->  map f . pure  <=>  pure . f
>     joinNatTrans        :  {A, B : Type}   ->  (f : A -> B) ->  map f . join  <=>  join . map (map f)

One could go even further and add more combinators (and their axioms)
to the ADT, but for the purpose of this discussion the above example
will do.
%
(In any case, many of these methods could be filled in by defaults.)
%
What are the implications of having replaced definitions with
specifications?
%
A direct implication is that now, in implementing generic proofs of the monad
laws, we have to replace some |Refl| steps with suitable
specifications.
%
Thus, for instance |pureLeftIdKleisli| becomes

>   pureLeftIdKleisli  :  {A, B : Type} -> {M : Type -> Type} -> Fat.VeriMonadFat M =>
>                         (f : A -> M B) -> (pure >=> f) <=> f
>   pureLeftIdKleisli f a =  ( (pure >=> f) a )         ={ kleisliJoinMapSpec pure f a }=
>                            ( join (map f (pure a)) )  ={ cong {f = join} (Fat.pureNatTrans f a) }=
>                            ( join (pure (f a)) )      ={ Fat.triangleLeft (f a) }=
>                            ( f a )                    QED

where we have replaced the first proof step, |Refl|, with
|kleisliJoinMapSpec pure f a|.
%
Similar transformations have to be done for |pureRightIdKleisli|,
|kleisliAssoc|, etc.
%
However, completing the proof of the monad laws for the fat interface
is not just a matter of replacing definitions with
specifications.
%
Consider associativity:

%if False

>   kleisliLeapfrog  :  {A, B, C : Type} -> {M : Type -> Type} -> Fat.VeriMonadFat M =>
>                       (f : A -> M B) -> (g : B -> M C) ->
>                       (f >=> g) <=> (id >=> g) . f
>   kleisliLeapfrog f g a =
>     ( (f >=> g) a )
>       ={ kleisliJoinMapSpec f g a }=
>     ( (join . map g . f) a )
>       ={ Refl }=
>     ( (join . map g . id) (f a) )
>       ={ sym (kleisliJoinMapSpec id g (f a)) }=
>     ( (id >=> g) (f a) ) QED

>   kleisliPresEE  :  {A, B, C : Type} -> {M : Type -> Type} -> Fat.VeriMonadFat M =>
>                     (f, f' : A -> M B) -> (g, g' : B -> M C) ->
>                     f <=> f' -> g <=> g' -> (f >=> g) <=> (f' >=> g')
>   kleisliPresEE f f' g g' fE gE a =
>     ( (f >=> g) a )
>       ={ kleisliLeapfrog f g a }=
>     ( (id >=> g) (f a) )
>       ={ cong (fE a) }=
>     ( (id >=> g) (f' a) )
>       ={ kleisliJoinMapSpec id g (f' a) }=
>     ( (join . map g) (f' a) )
>       ={ cong (mapPresEE g g' gE (f' a)) }=
>     ( (join . map g') (f' a) )
>       ={ sym (kleisliJoinMapSpec f' g' a) }=
>     ( (f' >=> g') a) QED


%endif

>   kleisliAssoc  :  {A, B, C, D : Type} -> {M : Type -> Type} -> Fat.VeriMonadFat M =>
>                    (f : A -> M B) -> (g : B -> M C) -> (h : C -> M D) ->
>                    ((f >=> g) >=> h) <=> (f >=> (g >=> h))
>   kleisliAssoc {M} {A} {C} f g h a =
>     (((f >=> g) >=> h) a )                           ={ kleisliJoinMapSpec (f >=> g) h a }=
>     ((join . map h . (f >=> g)) a)                   ={ cong  {f = join . map h}
>                                                               (kleisliJoinMapSpec f g a) }=
>     ((join . map h . join . map g . f) a)            ={ cong (Fat.joinNatTrans h (map g (f a))) }=
>     ((join . join . map (map h) . map g . f) a)      ={ Fat.square _ }=
>     ((join . map join . map (map h) . map g . f) a)  ={ cong  {f = join . map join}
>                                                               (sym (mapPresComp _ _ _)) }=
>     ((join . map join . map (map h . g) . f) a)      ={ cong  {f = join}
>                                                               (sym (mapPresComp _ _ _)) }=
>     ((join . map (join . map h . g) . f) a) {-"\quad"-} ={ sym (kleisliJoinMapSpec f (join . map h . g) a) }=
>     ( (f >=> (join . map h . g)) a )
>       ={ kleisliPresEE      f f     (join . map h . g) (g >=> h)
>                             reflEE  (symEE  {f = g >=> h} {g = join . map h . g}
>                                             (kleisliJoinMapSpec g h))             a }=
>     ( (f >=> (g >=> h)) a ) QED

Here, in order to deduce |(f >=> (g >=> h)) a| from |(f >=> (join
. map h . g)) a| in the last step of the proof, we had to apply

<   kleisliPresEE  :  {A, B, C : Type} -> {M : Type -> Type} -> Fat.VeriMonadFat M =>
<                     (f, f' : A -> M B) -> (g, g' : B -> M C) ->
<                     f <=> f' -> g <=> g' -> (f >=> g) <=> (f' >=> g')

instead of just |Refl| as in the case of thin interfaces.
%
In other words: the specification |kleisliJoinMapSpec| alone is not
strong enough to grant the last step.
%
It allows one to deduce that |join . map h . g| and |g >=> h| are
extensionally equal.
%PJ: trivial:  and we know that |f| is (intensionally and thus also extensionally) equal to |f|
But this is not enough: we need a proof that Kleisli composition
preserves extensional equality.
%
This relies on the functorial |map| of |M| preserving extensional
equality, as in the case of thin ADTs.

%% <   kleisliPresEE f f' g g' fE gE a   =
%% <     ( (f >=> g) a )                 ={ kleisliLeapfrog f g a }=
%% <     ( (id >=> g) (f a) )            ={ cong (fE a) }=
%% <     ( (id >=> g) (f' a) )           ={ kleisliJoinMapSpec id g (f' a) }=
%% <     ( (join . map g) (f' a) )       ={ cong (mapPresEE g g' gE (f' a)) }=
%% <     ( (join . map g') (f' a) )      ={ sym (kleisliJoinMapSpec f' g' a) }=
%% <     ( (f' >=> g') a)                QED

The moral is that, when the relationships between the monadic operations
|pure|, |join| and |(>=>)| are specified rather then defined, preservation
of extensional equality plays a crucial role even in proofs of
straightforward properties like associativity.
%
The same situation occurs if we specify bind in terms of |pure| and
|join|.

%if False

>   pureRightIdKleisli  :  {A, B : Type} -> {M : Type -> Type} -> Fat.VeriMonadFat M =>
>                          (f : A -> M B) -> (f >=> pure) <=> f
>   pureRightIdKleisli f a =
>     ( (f >=> pure) a )           ={ kleisliJoinMapSpec f pure a }=
>     ( join (map pure (f a)) )    ={ Fat.triangleRight (f a) }=
>     ( f a )                      QED

>   mapJoinLemma  :  {M : Type -> Type} -> {A, B, C : Type} -> Fat.VeriMonadFat M =>
>                    (f : B -> C) -> (g : A -> M B) ->
>                    (<=>) {A = M A} {B = M C} (map f . (join . map g)) (join . map (map f . g))
>   mapJoinLemma f g ma =
>     ( map f (join (map g ma)) )
>       ={  Fat.joinNatTrans f (map g ma) }=
>     ( join (map (map f) (map g ma)) )
>       ={  Refl }=
>     ( join ((map (map f) . (map g)) ma) )
>       ={  cong (sym (mapPresComp (map f) g ma)) }=
>     ( join (map (map f . g) ma) ) QED

>   mapKleisliLemma  :  {A, B, C, D : Type} -> {M : Type -> Type} -> Fat.VeriMonadFat M =>
>                       (f : A -> M B) -> (g : B -> M C) -> (h : C -> D) ->
>                       (map h . (f >=> g)) <=> (f >=> map h . g)
>   mapKleisliLemma f g h ma  =
>     ( (map h . (f >=> g)) ma )
>       ={  cong (kleisliJoinMapSpec f g ma) }=
>     ( map h (join (map g (f ma))) )
>       ={  mapJoinLemma h g (f ma) }=
>     ( join (map (map h . g) (f ma)) )
>       ={  sym (kleisliJoinMapSpec f (map h . g) ma) }=
>     ( (f >=> map h . g) ma ) QED

%endif


\subsection{The Wadler view}\label{sec:MonadWadlerView}

A different perspective on monads goes back to
\citep{manes1976algebraic} and has been popularized by P.\
\citet{wadler1992essence}:
%
a monad on a category $\mathcal{C}$ can be defined by giving an
endo\-function |M| on the objects of $\mathcal{C}$, a family of
arrows |eta A : A -> M A| (like |etaN| above, but not required to be
natural), and a ``lifting'' operation that maps any
arrow |f : A -> M B| to an arrow |liftS f : M A -> M B|.
%
The lifting operation is required to satisfy W1--W3 for any objects
|A, B, C| and arrows |f : A -> M B| and |g : B -> M C|, see
\citep{streicher2003category}:

< {-"\text{W1.\quad}"-} liftS f . eta A    = f
< {-"\text{W2.\quad}"-} liftS (eta A)      = id_MA
< {-"\text{W3.\quad}"-} liftS g . liftS f  = liftS (liftS g . f)

\paragraph*{From tradition to Wadler and back.}
%
The two monad definitions can be seen as two views on the same mathematical
concept and we would like the corresponding ADT formulations to preserve
this relationship.

It turns out that, if (|M|, |etaN|, |muN|) fulfil the properties T1--T5
of the traditional view, then the object part of |M|, |etaN|, and the
lifting operation defined by |liftS f = mu (M B) . map f| satisfy W1--W3.

In turn, given |M|, |etaN| and |liftS| that satisfy W1--W3, one can define
|map f = liftS (eta B . f)| and |mu A = liftS id_MA| and prove that
|(M, map)| is a functor, and that T1--T5 are all satisfied.
% |etaN| and |muN| are natural transformations, and that the left and right triangle properties and the square property

This economical way to define a monad has become very popular in
functional programming, where |lift f = liftS f| is usually given in
infix form with flipped arguments and called bind:
%
|ma >>= f = liftS f ma|.
%
This suggests yet another ADT for monads:

%if False

> namespace MonadFromBind

%endif

>   interface VeriMonadBind (M : Type -> Type) where
>     pure             :  {A : Type}    -> A -> M A
>     (>>=)            :  {A, B : Type} -> M A -> (A -> M B) -> M B
>
>     pureLeftIdBind   :  {A, B : Type} -> (f : A -> M B) -> (\ a => pure a >>= f) <=> f
>     pureRightIdBind  :  {A : Type}    -> (>>= pure) <=> id {a = M A}
>     bindAssoc        :  {A, B, C : Type} -> (f : A -> M B) -> (g : B -> M C) ->
>                         (\ ma => (ma >>= f) >>= g) <=> (\ ma => ma >>= (\ a => f a >>= g))

%**PJ: Perhaps reformulate in terms of lift:
%
% <     lift             :  {A, B : Type} -> (A -> M B) -> (M A -> M B)
% <     liftAssoc        :  {A, B, C : Type} -> (f : A -> M B) -> (g : B -> M C) ->
% <                         (lift g) . (lift f)  <=>  lift (lift g . f)
%
The three axioms are formulations of the properties of |lift| W1--W3 in
terms of bind.
%
We can now \emph{define} |map|, |join| and Kleisli composition in
terms of bind and |pure|:

>   map :  {A, B : Type} -> {M : Type -> Type} -> VeriMonadBind M => (A -> B) -> (M A -> M B)
>   map f ma = ma >>= (pure . f)
>
>   join :  {A : Type} -> {M : Type -> Type} -> VeriMonadBind M => M (M A) -> M A
>   join mma = mma >>= id
>
>   (>=>) :  {A, B, C : Type} -> {M : Type -> Type} -> VeriMonadBind M =>
>            (A -> M B) -> (B -> M C) -> (A -> M C)
>   f >=> g = \ a => f a >>= g

The obligation is now to prove that |pure| and |join| fulfil the
properties T1--T5, for instance, that |pure| is a natural transformation.
%
In much the same way as for formalizations of the traditional view, some
of these proofs can be implemented straightforwardly.
%
But in some cases, one runs into trouble.
%
Consider the proof of T2. Triangle right: |mu A . M (eta A)  = id_MA|

%if False

>   whatnow2 :  {A : Type} -> {M : Type -> Type} -> VeriMonadBind M =>
>               (ma : M A) -> (ma >>= ((\ a => pure a >>= id) . pure)) = (ma >>= id . pure)
>   whatnow2 = ?whatnowhole2

%endif

>   triangleRightFromBind  :  {A : Type} -> {M : Type -> Type} -> VeriMonadBind M =>
>                             join . map pure <=> id {a = M A}
>   triangleRightFromBind {A} {M} ma =
>          ( join (map pure ma) )                       ={ Refl }=
>          ( (ma >>= (pure . pure)) >>= id )            ={ bindAssoc (pure . pure) id ma }=
>          ( ma >>= (\ a => pure (pure a) >>= id) )     ={ Refl }=
>          ( ma >>= ((\ a => pure a >>= id) . pure) )   ={ whatnow2 ma }=
>          ( ma >>= id . pure )                         ={ Refl }=
>          ( ma >>= pure )                              ={ pureRightIdBind ma }=
>          ( ma )                                       QED

\noindent
We know that |\ a => pure a >>= id| and |id| are \emph{extensionally}
equal by |pureLeftIdBind id|.
%
If this equality would hold \emph{intensionally}, we could fill the
hole by congruence.
%
But we cannot strengthen |(<=>)| to |(=)| in |pureLeftIdBind|.
%*PJ: It is not clear to me if this axiom is part of the Monad3 interface or not?
Instead, we extend our ADT with
%
%**PJ: Perhaps use |lift| to explain the name. (Perhaps note that bind does not preserve exteq.)

<     liftPresEE      :  {A, B : Type} -> (f, g : A -> M B) -> f <=> g -> (>>= f) <=> (>>= g)

and complete the proof by filling in |whatnow2 ma| by:

<            liftPresEE  ((\ a => pure a >>= id) . pure) (id . pure) (\ a => pureLeftIdBind id (pure a) ) ma

Notice that, in this approach, |map| is defined in terms of |pure| and
bind. Thus, we do not have at our disposal the axioms of the functor ADT
and thus we cannot leverage |mapPresEE| to \emph{derive} |liftPresEE|
as we have done in the traditional formulation for Kleisli
composition.

The moral is that, even if we adopt the Wadler view on monads and a more
economical specification, we have to require |lift| to preserve
extensional equality if we want our specification to be consistent with the
traditional one.

This completes the discussion on different notions of monads and on the
role of extensional equality preservation in generic proofs of monad
laws.
%
For the rest of the paper, we apply the traditional view on monads and the fat
monad interface.
%
%format Fat.VeriMonadFat = Monad

\subsection{More monad properties}\label{sec:MonadMoreResults}

As we have seen in section \ref{section:example} for |flowMonLemma|,
extensional equality preservation is crucially needed in inductive
proofs.
%
We discuss more examples of applications of the principle in the
context of DSLs for dynamical systems theory in section \ref{section:applications}.
%
In the rest of this section, we prepare by deriving two intermediate
properties. % in the context of the traditional view on monads.
%
The first is the extensional equality between |map f . join
. map g| and |join . map (map f . g)|:

<   mapJoinLemma  :  {M : Type -> Type} -> {A, B, C : Type} -> Fat.VeriMonadFat M =>
<                    (f : B -> C) -> (g : A -> M B) ->
<                    (<=>) {A = M A} {B = M C} (map f . join . map g) (join . map (map f . g))
<   mapJoinLemma f g ma =
<     ( map f (join (map g ma)) )             ={ Fat.joinNatTrans f (map g ma) }=
<     ( join (map (map f) (map g ma)) )       ={ Refl }=
<     ( join ((map (map f) . (map g)) ma) )   ={ cong (sym (mapPresComp (map f) g ma)) }=
<     ( join (map (map f . g) ma) )           QED

The second,

<   mapKleisliLemma  :  {A, B, C, D : Type} -> {M : Type -> Type} -> Fat.VeriMonadFat M =>
<                       (f : A -> M B) -> (g : B -> M C) -> (h : C -> D) ->
<                       (map h . (f >=> g)) <=> (f >=> map h . g)
<   mapKleisliLemma f g h ma =
<     ( (map h . (f >=> g)) ma )         ={ cong (kleisliJoinMapSpec f g ma) }=
<     ( map h (join (map g (f ma))) )    ={ mapJoinLemma h g (f ma) }=
<     ( join (map (map h . g) (f ma)) )  ={ sym (kleisliJoinMapSpec f (map h . g) ma) }=
<     ( (f >=> map h . g) ma )           QED

can be seen as an associativity law by rewriting it in terms of
|(<=<) = flip (>=>)|:

%if False

>   (<=<) :  {A, B, C : Type} -> {M : Type->Type} -> Fat.VeriMonadFat M =>
>            (B -> M C) -> (A -> M B) -> (A -> M C)
>   (<=<) = flip (>=>)

%endif

>   mapKleisliLemma'  :  {A, B, C, D : Type} -> {M : Type -> Type} -> Fat.VeriMonadFat M =>
>                        (f : A -> M B) -> (g : B -> M C) -> (h : C -> D) ->
>                        (map h . (g <=< f)) <=> ((map h . g) <=< f)

%*PJ: possible shortening: (not applied now 2020-07-16 because of page-breaks)
%
% >   mapKleisliLemma' f g h : (map h . (g <=< f)) <=> ((map h . g) <=< f)
