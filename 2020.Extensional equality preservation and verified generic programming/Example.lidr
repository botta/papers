% -*-Latex-*-

%if False

> module Example

> import Syntax.PreorderReasoning
> import Control.Monad.Syntax

> import About

> %default total
> %auto_implicits off
> %access public export

> Leibniz : {A : Type} -> {x, x' : A} -> x = x' -> (P : A -> Type) -> P x -> P x'
> Leibniz Refl P p = p

> compAssociative  :  {A, B, C, D : Type} -> (h : C -> D) -> (g : B -> C) -> (f : A -> B) ->
>                     (h . g) . f = h . (g . f)
> compAssociative h g f = Refl
>
> {-
>     ((h . g) . f)
>   ={ Refl }=
>     (\x=> ((h . g) . f) x)
>   ={ Refl }=
>     (\x=> h (g (f x)))
>   ={ Refl }=
>     (\x=> (h . (g . f)) x)
>   ={ Refl }=
>      (h . (g . f))
>    QED
> ---}

%endif

\section{Equality examples from dynamical systems theory}
\label{section:example}

In dynamical systems theory \citep{Kuznetsov:1998:EAB:289919,
  thomas2012catastrophe}, a prominent notion is that of the flow (or
iteration) of a system.
%
We start by discussing deterministic systems and then generalize to the
monadic case. A \emph{deterministic} dynamical system on a set |X| is an
endofunction on |X|.  The set |X| is often called the \emph{state space}
of the system.

Given a deterministic system |f : X -> X|, its $n$-th iterate or flow,
is typically denoted by |pow f n : X -> X| and is defined by induction
on $n$: the base case |pow f 0 = id| is the identity function and |pow f
(n+1)| is defined to be either |f . pow f n| or |pow f n . f|.
%
The two definitions are mathematically equivalent because of
associativity of function composition but what can one prove about |f
. pow f n| and |pow f n . f| in intensional type theory?
%
We define the two variants as |flowL| and |flowR|:

> flowL : {X : Type} -> (X -> X) -> Nat -> (X -> X)
> flowL f     Z     =  id
> flowL f  (  S n)  =  flowL f n . f
>
> flowR : {X : Type} -> (X -> X) -> Nat -> (X -> X)
> flowR f     Z     =  id
> flowR f  (  S n)  =  f . flowR f n

The flows |flowL f n| and |flowR f n| are intensionally equal:

> flowLemma : {X : Type} -> (f : X -> X) -> (n : Nat) -> flowL f n = flowR f n

%if False

> flowRLemma : {X : Type} -> (f : X -> X) -> (n : Nat) -> flowR f n . f = f . flowR f n

%endif
%
With |compPresIE| from section~\ref{section:about}, one can implement
|flowLemma| by induction on the number of iterations~|n|.
%
The base case is trivial

> flowLemma f     Z     =  Refl

For readability, we spell out the proof sketch for the induction step in
full:

> flowLemma f  (  S n)  =  ( flowL f (S n) )     ={ Refl }=
>                          ( flowL f n . f )     ={ compPresIE (flowLemma f n) Refl }=
>                          ( flowR f n . f )     ={ flowRLemma f n }=
>                          ( f . flowR f n )     ={ Refl }=
>                          ( flowR f (S n) )     QED

First, we apply the definition of |flowL| to deduce |flowL f (S n) =
flowL f n . f|.
%
Next, we apply |compPresIE| with the induction hypothesis |flowLemma f
n| and deduce |flowL f n . f = flowR f n . f|.
%
The (almost) final step is to show that |flowR f n . f = f . flowR f
n|.
%
This is obtained via the auxiliary |flowRLemma| where we use
associativity and preservation of intensional equality again.

\begin{joincode}% The first line was given inside %if False above

< flowRLemma : {X : Type} -> (f : X -> X) -> (n : Nat) -> flowR f n . f = f . flowR f n

> flowRLemma f     Z     =  Refl
> flowRLemma f  (  S n)  =  ( flowR f (S n) . f )       ={  Refl }=
>                           ( (f . flowR f n) . f )     ={  compAssociative f (flowR f n) f }=
>                           ( f . (flowR f n . f) )     ={  compPresIE Refl (flowRLemma f n) }=
>                           ( f . (f . flowR f n) )     ={  Refl }=
>                           ( f . flowR f (S n) )       QED

\end{joincode}
% > flowRLemma f     Z     =          Refl
% > flowRLemma f  (  S n)  =
% >   ( flowR f (S n) . f )       ={  Refl }=
% >   ( (f . flowR f n) . f )     ={  compAssociative f (flowR f n) f }=
% >   ( f . (flowR f n . f) )     ={  compPresIE Refl (flowRLemma f n) }=
% >   ( f . (f . flowR f n) )     ={  Refl }=
% >   ( f . flowR f (S n) )       QED
%

\noindent
Let's summarize: we have considered the special case of deterministic
dynamical systems, defined the flow (a higher order function) in two
different ways and shown that the two definitions are equivalent in
the sense that |e1 = flowR f n| and |e2 = flowL f n| are intensionally
equal for all |f| and |n|.
%
Before we move on to a more general setting, where intensional
equality does not hold, let's expand a bit on the different levels of
equality relevant for these two functions.
%
The two expressions |e1| and |e2| denote functions of type |X->X| and
thus for any |x : X| we also have |e1 x = e2 x|.
%
On the other hand, the quantification over all |n| can be absorbed
into the definition of extensional equality so that we have |flowR f
<=> flowL f| for all |f|.
%
And with the two-argument version of extensional equality we get |flowR
<==> flowL|.

\paragraph*{Monadic systems.}
What about non-deterministic systems, stochastic systems or perhaps
fuzzy systems?
%
Can we extend our results to the general case of \emph{monadic}
dynamical systems?
%
Monadic dynamical systems \citep{ionescu2009}
on a set |X| are functions of type |X -> M X| where |M| is a monad.
%
When |M| is the identity monad, one recovers the deterministic case.
%
When |M| is |List| one has non-deterministic systems, and finite
probability monads
%*TODO: perhaps cite (e.g. |Dist| \citep{10.1017/S0956796805005721} or |SimpleProb| \citep{ionescu2009, 2017_Botta_Jansson_Ionescu})
capture the notion of stochastic systems.
%
\nocite{10.1017/S0956796805005721,ionescu2009, 2017_Botta_Jansson_Ionescu}
%
%Other monads encode other notions of uncertainty, see \citep{ionescu2009,giry1981}.
\nocite{ionescu2009,giry1981}

One can extend the flow (and, as we will see in
section~\ref{section:applications}, other elementary operations) of
deterministic systems to the general, monadic case by replacing |id|
with |pure| and function composition with Kleisli composition (|>=>|):
%PJ: The repeated type signature is a bit disturbing. Perhaps use |using|?

> flowMonL  :  {X : Type} -> {M : Type -> Type} -> Monad M =>
>              (X -> M X) -> Nat -> (X -> M X)
> flowMonL {-"\,"-}  f     Z     =  pure
> flowMonL           f  (  S n)  =  flowMonL f n >=> f
>
> flowMonR  :  {X : Type} -> {M : Type -> Type} -> Monad M =>
>              (X -> M X) -> Nat -> (X -> M X)
> flowMonR           f     Z     =  pure
> flowMonR           f  (  S n)  =  f >=> flowMonR f n

Notice, however, that now the implementations of |flowMonL| and
|flowMonR| depend on |(>=>)|, which is a monad-specific operation.
%
This means that, in proving properties of the flow of monadic systems,
we can no longer rely on a specific \emph{definition} of |(>=>)|: we
have to derive our proofs on the basis of properties that we know (or
require) |(>=>)| to fulfil -- that is, on its \emph{specification}.

What do we know about Kleisli composition \emph{in general}?
%
We discuss this question in the next two sections but let us
anticipate that, if we require functors to preserve the extensional
equality of arrows (in addition to identity and composition) and
Kleisli composition to fulfil the specification
%
\vspace*{-2ex}
\begin{center}
%%***TODO replace with pdf include
%% See https://tex.stackexchange.com/questions/452/how-can-i-create-a-pdf-document-exactly-as-big-as-my-tikz-picture/514
\beginpgfgraphicnamed{ExtEqPres-f1}
\begin{tikzcd}[column sep=large]
  |M C|
& |M (M C)|  \arrow[l, "|join|"]
& |M B|      \arrow[l, "|map g|"]
& |A|        \arrow[l, "|f|"]\arrow[lll, bend right=30, "f
    \mathbin{> \!\! = \!\! >}
    g"]
\end{tikzcd}
\endpgfgraphicnamed
\end{center}

> kleisliSpec  :  {A, B, C : Type} -> {M : Type -> Type} -> Monad M =>
>                 (f : A -> M B) -> (g : B -> M C) -> (f >=> g) <=> join . map g . f

then we can derive preservation of extensional equality

> kleisliPresEE  :  {A, B, C : Type} -> {M : Type -> Type} -> Monad M =>
>                   (f, f' : A -> M B) -> (g, g' : B -> M C) ->
>                   f <=> f' -> g <=> g' -> (f >=> g) <=> (f' >=> g')

and associativity of Kleisli composition generically.

> kleisliAssoc  :  {A, B, C, D : Type} -> {M : Type -> Type} -> Monad M =>
>                  (f : A -> M B) -> (g : B -> M C) -> (h : C -> M D) ->
>                  ((f >=> g) >=> h) <=> (f >=> (g >=> h))

From these premises, we can prove the \emph{extensional} equality of
|flowMonL| and |flowMonR| using a similar lemma as in the deterministic case:
%

> flowMonRLem  :  {X : Type} -> {M : Type -> Type} -> Monad M =>
>                 (f : X -> M X) -> (n : Nat) -> (flowMonR f n >=> f) <=> (f >=> flowMonR f n)

First, notice that the base case of the lemma requires computing
evidence that |pure >=> f| is extensionally equal to |f >=> pure|.
%
This is a consequence of |pure| being a left and a right identity for
Kleisli composition: for |f : A -> M B| we have

< pureLeftIdKleisli   f  : (pure >=> f)  <=> f
< pureRightIdKleisli  f  : (f >=> pure)  <=> f

%if False

> pureLeftIdKleisli   :  {A, B : Type} -> {M : Type -> Type} -> Monad M =>
>                        (f : A -> M B) ->  (pure >=> f)  <=> f
>
> pureRightIdKleisli  :  {A, B : Type} -> {M : Type -> Type} -> Monad M =>
>                        (f : A -> M B) ->  (f >=> pure)  <=> f

%endif

> flowMonRLem f Z x =
>   ( (flowMonR f Z >=> f) x )    ={ Refl }=
>   ( (pure >=> f) x )            ={ pureLeftIdKleisli f x }=
>   ( f x )                       ={ sym (pureRightIdKleisli f x) }=
>   ( (f >=> pure) x )            ={ Refl }=
>   ( (f >=> flowMonR f Z) x )    QED

As we will see in subsection \ref{def:pureLeftIdKleisli},
|pureLeftIdKleisli| and |pureRightIdKleisli| are either ADT axioms or
theorems, depending of the formulation of the monad ADT.
%
The induction step of |flowMonRLem| relies on preservation of
extensional equality and on associativity of Kleisli composition:

> flowMonRLem f  (  S n) x =
>  let rest = flowMonR f n in
>   ( (flowMonR f (S n) >=> f) x )      ={ Refl }=
>   ( ((f >=> rest) >=> f) x )          ={ kleisliAssoc f rest f x }=
>   ( (f >=> (rest >=> f)) x )          ={ kleisliPresEE  f f            (rest >=> f) (f >=> rest)
>                                                         (\ v => Refl)  (flowMonRLem f n) x     }=
>   ( (f >=> (f >=> rest)) x )          ={ Refl }=
>   ( (f >=> flowMonR f (S n)) x )      QED

Finally, the extensional equality of |flowMonL| and |flowMonR|

> flowMonLemma  :  {X : Type} -> {M : Type -> Type} -> Monad M =>
>                  (f : X -> M X) -> (n : Nat) -> flowMonL f n <=> flowMonR f n
>
> flowMonLemma f Z x = Refl
>
> flowMonLemma f (S n) x =
>  let  fLn  = flowMonL  f n
>       fRn  = flowMonR  f n  in
>   ( flowMonL f (S n) x )       ={ Refl }=
>   ( (fLn >=> f) x )            ={ kleisliPresEE  fLn fRn             f f
>                                                  (flowMonLemma f n)  (\v => Refl) x }=
>   ( (fRn >=> f) x )            ={ flowMonRLem f n x }=
>   ( (f >=> fRn) x )            ={ Refl }=
>   ( flowMonR f (S n) x )       QED

follows from |flowMonRLem| and, again, preservation of extensional equality.

\paragraph*{Discussion.} Before we turn to the next section, let us discuss one
objection to what we have just done.
%
Why have we not tried to prove that |flowMonL f n| and |flowMonR f n|
are \emph{intensionally} equal as we did for the deterministic flows?
%
If we managed to show the intensional equality of the two flow
computations, their extensional equality would follow.% from |IEqImplEE|.

The problem with that approach is that it would require much stronger
assumptions: |pureLeftIdKleisli|, |pureRightIdKleisli|, |kleisliAssoc|
and |kleisliSpec| would need to hold intensionally.
%
For example, it would require |f >=> g| to be intensionally equal to
|join . map g . f|.
%
In section \ref{section:monads} we will see that, in some abstract
data types for monads this is indeed the case, but to require all of
these would make our monad interface impossible (or at least very
hard) to implement.
%
In general, we cannot rely on |f >=> g| to be intensionally equal to
|join . map g . f|.
% we haven't introduced bind yet...
%(or, for that matter, to be intensionally equal to |\a => f a >>= g|).

In designing ADTs and formulating generic results, we have to be careful
not to impose too strong proof obligations on implementors.
%
%% Verified generic programming would become straightforward if we required
%% every functor implementation to exhibit an element of the empty type!
%% %
%% Or if we postulated intensional equality to follow from extensional
%% equality.
%% %
%% Unfortunately, this would make our ADTs hardly implementable.
%% %
%% %% While unimplementable ADTs are a legitimate approach towards building
%% %% (unimplementable) theories, the focus of this paper is on design
%% %% patterns that are implementable.
%
Requiring the monad operations to fulfil intensional equalities would
perhaps not be as bad as pretending that function extensionality holds
in general, but would still imply unnecessary restrictions.
%
By contrast, requiring proper functors to preserve the extensional
equality of arrows is a natural, minimal invasive specification:
%
it allows one to leverage on what |List|, |Maybe|, |Dist|
\citep{10.1017/S0956796805005721}, |SimpleProb|
\citep{2017_Botta_Jansson_Ionescu} and many other monads that are
relevant for applications are known to fulfil, derive generic verified
implementations, avoid boilerplate code and improve the
understandability of proofs.
