## Extensional equality preservation and verified generic programming

* To type check About.lidr (or other literate Idris file) with idris
  (version 1.3.2) in your path:

    + Download IdrisLibs2 from [1].

    + enter *idris -i $IDRISLIBS --sourcepath $IDRISLIBS --allow-capitalized-pattern-variables -p contrib About.lidr*

  where *IDRISLIBS* is the path of IdrisLibs2.

* To generate a PostScript version of the paper enter *make* in the command line.

* If you encounter any issue, please get in touch (botta@pik-potsdam.de).


* [1] https://gitlab.pik-potsdam.de/botta/IdrisLibs2
