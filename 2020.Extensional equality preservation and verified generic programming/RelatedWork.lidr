% -*-Latex-*-

\section{Related work}\label{section:relatedwork}

As already mentioned in section \ref{section:about}, there is a large
body of literature that relates in some form to (the treatment of)
equality in intensional type theory.
%
Most of that work, however, is concerned with the theoretical study of
the Martin-L\"of identity type or with the implementation of variants
of type theory and thus very different in nature from the present
paper which takes a pragmatic user-level approach.
%

Closest to our approach from the theoretical point of view are perhaps
works on formalization in type theory using \emph{setoids}.
%
These were originally introduced by Bishop \citep{Bishop1967-BISFOC-3}
for his development of constructive mathematics, and studied in
\citep{hofmann1995extensional} for the treatment of weaker notions of
equality in intensional type theory.
%
Setoids are sets equipped with an equivalence relation and mappings
between setoids have to take equivalent arguments to equivalent
results.
%
The focus of our paper can thus be seen as one special case with
extensional equality of functions as the equivalence relation of
interest and thus its preservation as coherence condition on
mappings.
%
The price to pay when using a full-fledged setoid approach is the
presence of a potentially huge amount of additional proof obligations,
needed to coherently deal with sets (types) and their equivalence
relations -- this often is pointedly referred to as \emph{setoid hell}
(for instance in \citet{altenkirch_setoidhell}, but it seems to have
been used colloquially in the community for much longer).

Still, there are some large developments using setoids, e.g. the CoRN
library (formalizing constructive mathematics) by \citet{CoRN_library}
and the CoLoR library (for rewriting and termination) by
\citet{CoLoR_library} in Coq where the proof assistant provides the
user with some convenient tools for dealing with setoids
\citep{sozeau2010new}.
%
% There is recent work on extending Observational Type Theory to a type system supporting
% reflection-free extensional equality \citep{DBLP:journals/corr/abs-1904-08562}.
%
Setoids are also used in a number of formalizations of category
theory,
e.g.~\citep{huet_saibi,megasz_coq,wiegley_coq,carette_agda,hu2021formalizing}.


\emph{Homotopy Type Theory} with \emph{univalence} \citep{hottbook}
provides function extensionality as a byproduct.
%
However, in most languages (notably in Coq in which the Univalent
Foundations library \cite[UniMath]{UniMath} is developed), univalence
is still an axiom and thus blocks computation.
%
Moreover, univalence is incompatible with the principle of
\emph{Uniqueness of Identity Proofs} which e.g.\ in Idris is built in,
and in Agda has to be disabled using a special flag.
%

%
Finally, in \emph{Cubical Type Theory} \citep{cohenetal18:cubical}
function extensionality is provable because of the presence of the
\emph{interval primitive} and thus has computational content.
%
Cubical type theory has recently been implemented as a special version
of Agda \citep{cubicalagda2}.
%
Another (similar) version of homotopy type theory is implemented in
the theorem prover Arend \citep{arend_prover}.
%
However, it is not clear at the present stage how long it will take
for these advances in type theory to become available in mainstream
functional programming.

On the topic of interfaces (type classes) and their laws there is
related work in specifying \citep{janssonjeuring-dataconv}, rewriting
\citep{peytonjones2001playing}, testing
\citep{jeuringHaskell12ClassLaws} and proving
\citep{arvidssonetal19:typeclasslaws} type class laws in Haskell.
%
The equality challenges here are often related to the semantics of
non-termination as described in the Fast and Loose Reasoning paper
\citep{danielssonetal06:fastandloose}.
%
In a dependently typed setting there is related work on contrasting
the power of testing and proving, including Agda code for the Functor
interface with extensional equality for the identity and composition
preservation but not preservation of extensional equality
\citep{ionescujansson:LIPIcs:2013:3899}.
%

\citet{Carette_2014} nicely abstract the ideas about different
(minimal) interfaces that we only exemplified using verified monads.
%
Regarding the relation between different representations of monads, the reader
might contrast the approach in the UniMath
\citep[\texttt{CategoryTheory.Monads.KTriplesEquiv}]{UniMath} library with our approach
in section~\ref{section:monads}. The UniMath development is part of a
full-fledged formalization of category theory and relates ``the traditional view''
with the ``Wadler view'' of a monad (as we called them) via a weak equivalence of
categories. This approach is very satisfactory from an abstract mathematical
perspective.
%
Our equivalence result is much less general but still practically
relevant and more lightweight: although it requires considerations
about preservation of extensional equality, it does not require
stronger axioms like univalence or a larger conceptual framework.
