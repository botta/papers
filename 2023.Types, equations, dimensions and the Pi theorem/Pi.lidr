% -*-Latex-*-

%if False

> module Pi

> %default total
> %auto_implicits off
> %access public export

%endif

\section{Similarity theory and the Pi theorem}
\label{section:pi}

The notion of dimension function informally introduced in the last
section is closely related with a fundamental principle in physical
sciences and with a very pragmatic question in physical modeling.

We start with the latter. At the turn of the 20th century, no computers
were available for approximating numerical solutions of mathematical
models of physical systems, typically in the form of partial
differential equations.
%
Thus, models of physical systems, say of a ship cruising in shallow
waters or of an airplane, were themselves physical systems, for
convenience often at a reduced \emph{scale}.
%
This raised the obvious question of under which conditions careful
measurements made on the scaled model could be ``scaled up'' to the real
system and how this scaling up should be done.

For example, if the drag on a 1:50 model of a ship cruising in a water
channel was found to be |x| Newton, what would be the drag of the
real ship? And under which conditions is it possible to give a definite
answer to this question?

At first glance, the problem seems to be one of engineering. But the
theory that was developed to answer this question, similarity theory or
dimensional analysis (DA), turned out to be a logical consequence of a
fundamental principle: ``physical laws do not depend on arbitrarily
chosen basic units of measurement'', see \citep[section 0.1]{barenblatt1996scaling}.
This is, in turn, an instance of Galileo's principle of
relativity, see \citep[page 3]{arnold1989mathematical}.

The core results of DA can be summarized in Buckingham's Pi theorem and
this theorem is also an answer to the question(s) of model similarity
raised above.
%
We do not need to be concerned with these answers and with specific
applications of the Pi theorem in the physical sciences here.
%
But we need to understand the notions that are at core of this theorem
in order to develop a DSL that is suitable for mathematical physics and
for modeling.

We introduce Buckingham's Pi theorem as it is stated in
\citep{barenblatt1996scaling}. This formulation is consistent with
textbook presentations and raises a number of questions.
%
We flag these questions here and then tackle them from a functional
programming perspective in \cref{section:dsl1} where we build a minimal
DSL for dimensional consistent programming .


\subsection{Buckingham's Pi theorem}
\label{subsection:pi}

The theorem is stated at page 42 of \citep{barenblatt1996scaling}:

\begin{quotation}
A physical relationship between some dimensional (generally speaking)
quantity and several dimensional governing parameters can be rewritten
as a relationship between some dimensionless parameter and several
dimensionless products of the governing parameters; the number of
dimensionless products is equal to the total number of governing
parameters minus the number of governing parameters with independent
dimensions.
\end{quotation}

\noindent
This formulation comes at the end of a derivation that starts at page 39
by positing a ``physical relationship'' between a ``dimensional
quantity'' $a$ and $k + m$ ``dimensional governing parameters''
$a_1,\dots,a_k$ and $b_1,\dots,b_m$
%
\begin{equation}
a = f(a_1,\dots,a_k,b_1,\dots,b_m)
\label{eq:pih0}
\end{equation}
%
such that $a_1,\dots,a_k$ ``have independent dimensions, while the
dimensions of parameters $b_1,\dots,b_m$ can be expressed as products of
powers of the dimensions of the parameters $a_1,\dots,a_k$'':
%
\begin{equation}
[b_i] = [a_1]^{p_{i1}} \dots [a_k]^{p_{ik}} \quad i = 1,\dots,m
\label{eq:pih1}
\end{equation}
%
With these premises, the conclusions are then 1) that ``the dimension of
$a$ must be expressible in terms of the dimensions of the governing
parameters $a_1,\dots,a_k$'':
%
\begin{equation}
[a] = [a_1]^{p_1} \dots [a_k]^{p_k}
\label{eq:pi0}
\end{equation}
%
and 2) that the function $f$ ``possesses the property of generalized
homogeneity or symmetry, i.e., it can be written in terms of a function
of a smaller number of variables, and is of the following special
form'':
%
\begin{equation}
  f(a_1, \dots, a_k, b_1, \dots, b_m) = a_1^{p_1} \dots a_k^{p_k} \ \Phi(\Pi_1, \dots, \Pi_m)
\label{eq:pi1}
\end{equation}
%
where $\Pi_i = b_i / (a_1^{p_{i1}} \dots a_k^{p_{ik}})$ for $i = 1, \dots,
m$. On page 42ff, the author comments that the term ``physical
relationship'' for $f$ ``is used to emphasize that it should obey the
covariance principle'' and, further, that the $\Pi$-theorem is
``completely obvious at an intuitive level'' and that ``it is clear that
physical laws should not depend on the choice of units'' and that this
``was realized long ago, and concepts from dimensional analysis were in
use long before the $\Pi$-theorem had been explicitly recognized,
formulated and proved formally'' among others, by ``Galileo, Newton,
Fourier, Maxwell, Reynolds and Rayleigh''.
%
Indeed, one of the most successful applications of the theorem was
Reynolds' scaling law for fluid flows in pipes in 1883, well before
Buckingham's seminal papers \citep{Buckingham1914, Buckingham1915}.

Here we do not discuss applications of the $\Pi$-theorem, but its
relevance for data analysis, parameter identification, and sensitivity
analysis is obvious: the computational cost of these algorithms is
typically exponential in the number of parameters. The theorem allows
cost reductions that are exponential in the number of parameters with
independent dimensions. In the case of $f$, for example, the theorem
allows to reduce the cost from $N^{k + m}$ to $N^m$ where $N$ denotes
a sampling size.
%
In data-based modelling and machine learning,
%
this can make the difference between being able to solve a problem in
principle and being able to solve it in practice.

The bottom line is that, even though DA and the $\Pi$-theorem were
formulated at a time in which computer based modeling was not available
and were mainly motivated by the questions of model similarity mentioned
above, they are still very relevant today.

For us, the challenge is to understand how to apply dependent types to
1) systematically check the dimensional consistency of expressions and
2) assist DA.

In \cref{section:dimensions}, we have seen that what in mathematical
physics and modeling are called physical quantities are equipped with
a dimension function.
%
In analogy with the judgment |e : t| (or, |typeOf e = t|), we have
informally introduced the judgment |[e] = d| to denote that expression
|e| has dimension function |d|. With the $\Pi$-theorem, we have seen
that physical quantities may have ``independent dimensions'' or be
dimensionally dependent.
%
In \cref{eq:pih1,eq:pi0} we have encountered (systems of) equations
between dimension functions whose solutions play a crucial role in the
$\Pi$-theorem \cref{eq:pi1}.
%
In the next section we come back to the notion of dimension function of
a physical quantity |[x] : RealPlusN -> RealPlus|, discuss its relationship
with units of measurement, and argue that |[x]| is a power-law
monomial. This property is at the core of the $\Pi$-theorem and of the
dependently typed formalization of DA outlined in
\cref{section:piexplained1,section:dsl1}.

%if False

\begin{PLAN}

We introduce Buckingham's Pi theorem \citep{Buckingham1914}, raise the
questions that will be answered in the next section, give standard
examples (pendulum, pressure drop in pipes, etc.) of applications (on
the basis of the informal notion of dimension introduced in the previous
section) and discuss the potential applications to data-based modelling.

We start by giving a bit of historical context by remarking that, at the
beginning of the last century, no computer-based numerical experiments
were available to the physical sciences. Thus, all experiments were done
on \emph{scaled} models (of engines, ships, cars, airplanes, etc.) in
reproducible laboratory setups (wind and water channels, etc.) We
formulate explicitly the two fundamental questions in modelling:

1) Under which conditions are we entitled to extrapolate the results of
measurements performed on a scaled model to the real object?

2) What are the rules for such extrapolations?

We make a few simple examples (pendulum, lever, ...) and then the not so
trivial example of the Reynolds problem (pressure drop in pipes).

We remark that the Pi theorem is a powerful answer to both questions but
that its content is not immediately obvious not only to computer
scientists but, we argue, also to today modelers in domains (climate
science) in which modelling is basically limited to computer-based
modelling!

We spell out the Pi theorem, raise the question (from page
\pageref{question:invariant} of the notes) and show its application to
the Reynolds problem and point to the more authoritative examples
discussed in the textbooks and to its generalizations in theoretical
physics, fractal geometry, etc.

We argue that, if we understand how to formalize the Pi theorem in TT,
we can provide modelers, data analysts and practitioners with (Quick*
like) tools for checking the plausibility of assumptions about functional
dependencies and for suggesting dimension reductions in data analysis.

\end{PLAN}

%endif
