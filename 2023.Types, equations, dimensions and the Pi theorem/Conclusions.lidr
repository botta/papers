% -*-Latex-*-

%if False

> module Conclusions

> %default total
> %auto_implicits off
> %access public export

%endif

\section{Conclusions}
\label{section:conclusions}

Specialization and the pervasive usage of computer-based modelling and
simulation in the physical sciences have widened the gap between the
languages of mathematical physics and modelling and those of mathematics
and functional programming.
%
This gap is a major obstacle to fruitful communication and to
interdisciplinary collaborations: computer-based modelling badly needs
precise specifications and dependently typed programming languages have
enough expressive power to support formulating such specifications.
%
But dependently typed programming languages are not (yet) well equipped
for encoding the ``grammar of dimensions'' which rules the languages of
mathematical physics and modelling.
%
Our contribution is a first step towards making FP more suitable for
developing applications in these domains.

We have studied the role of equations, laws and dimensions on the basis
of established examples from the physical sciences and of seminal works
in modelling.
%
We have analyzed the notions of dimension function, physical quantity
and units of measurement and we have provided an account of the theory
of physical similarity and of Buckingham's Pi theorem from the point of
view of computer science and FP.
%
Finally, we have proposed a small DSL that encodes these notions in
Idris, supports dimensional judgments, and leverages the type system
of the host language to provide tests of dimensional consistency,
dependence, independence and to ensure the consistency of expressions
involving physical quantities.

The DSL also supports dependently typed formulations of Buckingham's Pi
theorem and we have discussed and type checked one such formulation.
%
From this perspective, our work is also a contribution towards
understanding relativity principles through formalization.
In the physical sciences these principles are well understood and appreciated.
They have lead to important applications in engineering and data science.
% but also to surprising conceptual breakthroughs.% \citep{pauli2013theory}.
% % Maybe this one? \cite{pauli2013theory}
%
But it is not clear how relativity principles could be formulated in the
economic sciences, in the biological sciences, and thus also in climate
science.
%
We believe that type theory and FP can contribute towards answering this
question.

%if False

\begin{PLAN}

We wrap-up, explain that this is a contribution towards bridging the gap
between climate modelling and computer science and perhaps conclude by
explaining the limitations of DA (already recognized in Bridgman22 if I
am not mistaken and well discussed in more recent accounts of DA).

At the same time, point out that out contribution can also be seen as
one towards understanding relativity principles through formalization.
While in physics such principles are well understood and appreciated and
have lead to surprising breakthrough (as far as I understand, the
special theory of relativity can be seen as a way of rescuing the
principle of relativity of observations [expressed in the new theory
  through the Lorentz transformations and in classical mechanics by the
  Galilean transformations] while at the same time accounting for the
constancy of the speed of light) it is not clear how such principles
could be formulated in the economic sciences in the biological sciences
and thus also in climate science.

\end{PLAN}

%endif
