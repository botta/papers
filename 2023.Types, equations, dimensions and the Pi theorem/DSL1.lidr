% -*-Latex-*-

%if False

\begin{code}
module DSL1

import Data.Vect
-- import Data.Vect.Quantifiers
import Data.Primitives.Views
import Syntax.PreorderReasoning

%default total
%auto_implicits off
%access public export

namespace Vect

  (+)  :  {n : Nat} -> {T : Type} -> Num T =>
          Vect n T -> Vect n T -> Vect n T
  (+)  =  zipWith (+)

  (-)  :  {n : Nat} -> {T : Type} -> Neg T =>
          Vect n T -> Vect n T -> Vect n T
  (-)  =  zipWith (-)

  infixl 5 <#

  (<#)  :  {n : Nat} -> {T : Type} -> Num T => T -> Vect n T -> Vect n T
  x <# v =  map (x *) v

  const : {n : Nat} -> {A : Type} -> A -> Vect n A
  const {n} a = replicate n a

infix 6 <--->

Real : Type
Real = Double

RealPlus : Type
RealPlus = Real

RealPlus3 : Type
RealPlus3 = Vect 3 Real

%hide Exists
%hide getWitness
%hide getProof

data Exists : (X : Type) -> (P : X -> Type) -> Type where
  Evidence : {X : Type} -> {P : X -> Type} -> (x : X) -> (px : P x) -> Exists X P

getWitness : {X : Type} -> {P : X -> Type} -> Exists X P -> X
getWitness (Evidence x px) = x

getProof : {X : Type} -> {P : X -> Type} -> (evi : Exists X P) -> P (getWitness evi)
getProof (Evidence x px) = px
\end{code}
%endif

%%%%%%%

\section{A minimal DSL for dimensionally consistent programming}
\label{section:dsl1}

In this section we introduce a minimal domain-specific language for
dimensionally consistent programming in Idris and in the context of
classical mechanics.
%
We formalize the notions of dimension function, physical quantity,
measurement and units of measurement from \cref{section:dimensions} and
the notion of dimensional (in)dependence which are at the core of the Pi
theorem from \cref{section:pi}.

The DSL supports expressing dimensional judgments and dimensionally
consistent programming in the domain of classical mechanics. It can be
straightforwardly extended to higher dimensional domains (for example,
thermodynamics) or restricted to subdomains of mechanics like kinematics
or non-fractal geometry.

At the core of the DSL is the idea that any physical quantity can be
associated with a dimension function. We follow a concrete, minimalist
approach and encode dimension functions in terms of vectors of integers.

%

\subsection{Dimension function}
\label{subsection:df}

In \cref{section:dimensions}, we said that the dimension function
of a physical quantity |x|, |[x] : RealPlusN -> RealPlus| encodes the idea
that |x| can be measured in a system of |n| fundamental units of
measurement and that the number |[x] (L1, ..., Ln)| denotes the factor
by which the measure of |x| increases (is multiplied) when the |n| units
are decreased (divided) by |L1, ..., Ln|.

We can give a precise meaning to this idea by denoting the measure of
|x| in the units of measurement |u1, ..., un| by |meas (u1, ..., un) x|.
%
We will sometimes write |u| for the tuple |(u1, ..., un)| and shorten
the notation to |measu x| for |meas u x|.
%
For the time being, we posit |measu x : Real| but defer the
specification of the types of |x| and |u| to
\cref{subsection:quantities,subsection:units}.
%
The reader should keep in mind that |x| could represent a velocity or a
stress tensor and the result of measuring |x| could be a vector or a
tensor of real numbers.

%
With these premises, we can make precise the notion of dimension
function through (for any non-zero |u1|, \dots, |un|):
%
% x : Q d;   dimfun : Q d -> R^n -> R;   meas : Q d -> SysUnit -> R
% SysUnit1 = Vec R n
% SysUnit2 = (i : Fin n) -> Q (basedims i)  -- basedims : Fin n -> Dim
% SysUnit2LTM  = (Q L, Q T, Q M)
% If we only provide this way to "get out of" Q:
%   extract : Q Dimless -> R
% then meas must apply the dimfun to get a scalar out.
% _/su_ : SysUnit -> R^n -> SysUnit -- this requires more than just tags for system of units
%
\begin{equation}
\text{|[x] (L1, ..., Ln)|} = \frac{\text{|meas (u1 / L1, ..., un / Ln) x|}}{\text{|meas (u1, ..., un) x|}}
\label{eq:df0}
\end{equation}
%
The specification suggests that the dimension function of |x| does not
depend on the units of measurement. It formalizes the principle (of
covariance, relativity of measurements) that there is no privileged
system of units of measurement or, in other words, that all systems are
equally good:
%
\begin{equation}
  \frac{\text{|meas (u1 / L1, ..., un / Ln) x|}}{\text{|meas (u1, ..., un) x|}}
  =
  \frac{\text{|meas (u1' / L1, ..., un' / Ln) x|}}{\text{|meas (u1', ..., un') x|}}
\label{eq:cov0}
\end{equation}
%
for any physical quantity |x|, systems of units |u1, ..., un| and |u1',
..., un'| and scaling factors |L1, ..., Ln|. It is easy to see that the
principle \ref{eq:cov0} implies that the dimension function fulfils
%
\begin{equation}
  \text{|[x] (L1 / L1', ..., Ln / Ln')|}
  =
  \frac{\text{|[x] (L1, ..., Ln)|}}{\text{|[x] (L1', ..., Ln')|}}
\label{eq:fun0}
\end{equation}
%
by equational reasoning

< [x] (L1, ..., Ln) / [x] (L1', ..., Ln')
<
<   = -- use \cref{eq:df0}: def. of |[x]| for units |(u1, ..., un)|
<
< meas (u1 / L1, ..., un / Ln) x / meas (u1 / L1', ..., un / Ln') x
<
<   = -- let |u1' = u1 / L1'|, \dots, |un' = un / Ln'|
<
< meas (u1' / (L1 / L1'), ..., un' / (Ln / Ln')) x / meas (u1', ..., un') x
<
<   = -- use \cref{eq:df0}: def. of |[x]| for units |(u1', ..., un')|
<
< [x] (L1 / L1', ..., Ln / Ln')

\noindent
From \cref{eq:fun0} it follows that
%
\begin{equation}
\text{|[x] (1, ..., 1) = 1|}
\label{eq:fun1}
\end{equation}
%
and that dimension functions have the form of power-law monomials
%
\begin{equation}
\text{|[x] (L1, ..., Ln)|} = L_1^{|d1 x|} |*| \dots |*| L_n^{d_n \; x}
\label{eq:fun2}
\end{equation}
%
The derivation is not straightforward and involves solving the
functional equation \cref{eq:fun0}, see \citep[section 1.1.4]{barenblatt1996scaling}.
%
The exponents $d_1 \; x$, \dots $d_n \; x$ are sometimes called (perhaps
confusingly) the ``dimensions'' of |x| and |x| is said to ``have
dimensions'' $d_1 \; x$, \dots $d_n \; x$. Their value can be obtained by
recalling the specification \cref{eq:df0}.

For concreteness, consider the case of mechanics already discussed in
\cref{section:dimensions}. Here |n = 3| and, in a system of units
of measurements for lengths, times and masses, the scaling factors
$L_1$, $L_2$ and $L_3$ are denoted by |L|, |M| and |T|. For consistency,
we denote the exponents $d_1 \; x$, $d_2 \; x$ and $d_3 \; x$ by $d_L \; x$,
$d_M \; x$ and $d_T \; x$, respectively.

Thus, in mechanics, \cref{eq:df0} tells us that $L^{d_L \; x} |*|
M^{d_M \; x} |*| T^{d_T \; x}$ is the factor by which the measure
of |x| gets multiplied when the units of measurement for lengths, masses
and times are divided by |L|, |M| and |T|.
%
Therefore, when |x| represents a length (for example, the distance
between two points or a space coordinate) we have $d_L \ x = 1$ and $d_M
\ x = d_T \ x = 0$. Similarly, when |x| represents a mass we have $d_M
\ x = 1$ and $d_L \ x = d_T \ x = 0$ and when |x| represents a time we
have $d_T \ x = 1$ and $d_L \ x = d_M \ x = 0$. And when |x| represents
a velocity (a distance divided by a time), the factor by which
the measure of |x| gets multiplied when the units of measurement for
lengths, masses and times are divided by |L|, |M| and |T| shall be $L /
T$ and thus $d_L \ x = 1$, $d_M \ x = 0$ and $d_T \ x = -1$.

These judgments are a consequence of the notion of (direct or indirect)
measurement as \emph{counting}: when we say that the length of a pen is
20 centimeters we mean that we have to add 20 times the length of a
centimeter to obtain the length of that pen.

The above analysis suggests that, in classical mechanics, the type of
dimension functions is isomorphic to |Integer3|, see also page 3 of
\citep{doi:10.1142/9789811242380_0020}. Thus, in this domain, we can
represent a dimension function with a vector of integers of length 3:
%Agda code in: agda-dimensional/filename.agda: 57 (rough line number)
% PiExplained.lagda

\begin{joincode}%
\begin{code}
namespace Mechanics

  namespace LTM

    data Units = SI | CGS

    D : Type
    D = Vect 3 Integer
\end{code}
%if False
\begin{code}
    fs : Units -> Units -> RealPlus3
    fs SI   SI = [1, 1, 1]
    fs SI  CGS = [100, 1, 1000]
    fs CGS  SI = [0.01, 1, 0.001]
    fs CGS CGS = [1, 1, 1]
\end{code}
%endif
\end{joincode}
%
\noindent
%
Here, we have embedded the datatypes |Units| (with just two codes for
the |SI| and |CGS| systems of units for simplicity) and |D| in the
namespaces |Mechanics| and |LTM|, the latter representing the class of
units for lengths, times and masses, see \cref{section:dimensions}.
%
Following our analysis, we can model the syntax of dimensional
expressions in terms of a |DimLess| vector for dimensionless quantities,
of |Length|, |Time| and |Mass| vectors for lengths, times and masses
(the dimensions associated with the fundamental units of measurement in
|LTM|)

\begin{code}
    DimLess  :  D;                    Length  :  D;                    Time  :  D;                    Mass  :  D
    DimLess  =  [0,0,0]; {-"\quad"-}  Length  =  [1,0,0]; {-"\quad"-}  Time  =  [0,1,0]; {-"\quad"-}  Mass  =  [0,0,1]
\end{code}
and of two combinators |Times| and |Over|:
\begin{code}
    Times    :  D -> D -> D;  {-"\quad"-}  Over     :  D -> D -> D
    Times    =  (+);                       Over     =  (-)
\end{code}
where |(+)| and |(-)| are the canonical addition and subtraction between
vectors of integer numbers.
%
%if False
\begin{code}
    Pow      :  D -> Integer -> D
    -- Pow d n = n <# d
    Pow d n  =  pow d (integerRec n) where
      pow : {n : Integer} -> D -> IntegerRec n -> D
      pow D       IntegerZ  = DimLess
      pow D (IntegerSucc m) = pow D m `Times` D
      pow D (IntegerPred m) = pow D m  `Over` D

    ProdPows : {n : Nat} -> Vect n D -> Vect n Integer -> D
    --ProdPows ds ps = foldr (+) DimLess (zipWith (<#) ps ds)
    ProdPows      Nil       Nil   =  DimLess
    ProdPows (d :: ds) (p :: ps)  =  Pow d p `Times` ProdPows ds ps

    postulate dodIsDimLess : {d : D} -> d `Over` d = DimLess
\end{code}
%endif
%
These correspond to the idea that the dimensions of derived units of
measurement (for example, for velocities or for energies) are obtained
by multiplying or by dividing the dimensions of the fundamental units:

\begin{code}
    Velocity : D;                                   Acceleration : D
    Velocity = Length `Over` Time; {-"\quad"-}      Acceleration = Velocity `Over` Time

    Force : D;                                      Work : D
    Force = Mass `Times` Acceleration; {-"\quad"-}  Work = Force `Times` Length

    Energy : D
    Energy = Mass `Times` (Velocity `Times` Velocity)
\end{code}
One can easily check that energy and mechanical work are equivalent, as
one would expect
\begin{code}
    check1 : Energy = Work
    check1 = Refl
\end{code}
that force and energy are different notions
\begin{code}
    check2 : Not (Force = Energy)
    check2 Refl impossible
\end{code}
and, we can compute the dimension functions of |D|-values:
\begin{code}
    df : D -> RealPlus3 -> RealPlus
    df [d1, d2, d3] [l1, l2, l3]  =  Prelude.Doubles.pow l1 (fromInteger d1) * Prelude.Doubles.pow l2 (fromInteger d2) * Prelude.Doubles.pow l3 (fromInteger d3)
\end{code}
%if False
\begin{code}
    {-
    df d ls = foldr (*) 1.0 (zipWith pow ls ds) where
      ds : Vect 3 Real
      ds = map fromInteger d
    -}
\end{code}
%endif
%
For both |D| and |Units| we use datatypes of codes (just syntactic
expressions) and that |df| here and |fs| later (\cref{subsection:units}) are used to translate
these codes to their intended semantics.
%
The use of integers for the exponents of the dimension function makes
dimensional judgments decidable, which would not hold for real
numbered exponents, or functions.

We come back to the choice of types in \cref{section:generalization}
where we put forward specifications for data types that implement
dimension functions in terms of type classes. For the rest of this
section, we stick to the example of mechanics and to the
representation of dimension functions in terms of vectors of three
integer exponents.

%

\subsection{Physical quantities}
\label{subsection:quantities}

We are now ready to formalize the notion of physical quantity informally
introduced in \cref{section:dimensions}. There, we posited that a
parameter (e.g., the parameter |c| of the Stommel model discussed in
\cref{subsection:laws}) represents a \emph{dimensional} physical
quantity when 1) that parameter can be measured in a system of units of
a given class and 2) one can define another system of units in the same
class that gives a different measurement for the parameter.
%
By contrast, measurements of \emph{dimensionless} physical quantities
do not change when the units of measurement are re-scaled.

This suggests that a (dimensional or dimensionless) physical quantity
can be represented by a value of type
%
\begin{code}
    data Q : D -> Type where
      Val : {d : D} -> (u : Units) -> Real -> Q d
\end{code}
\noindent
effectively annotating |Real| values with different dimensions and
systems of units, for example
\begin{code}
    x  :  Q Length;  {-"\quad"-}  t  :  Q Time;     {-"\quad"-}  m  :  Q Mass
    x  =  Val SI 3;  {-"\quad"-}  t  =  Val CGS 1;  {-"\quad"-}  m  =  Val SI 2
\end{code}
\noindent
What kind of combinators shall be defined for physical quantities? As a
minimum, one wants to be able to ``compute'' the |D|-value of a physical
quantity
%if False
\begin{code}
    units : {d : D} -> Q d -> Units
    units (Val u x) = u
\end{code}
%endif
\begin{code}
    dim : {d : D} -> Q d -> D
    dim {d} _ = d
\end{code}
and its dimension function |df . dim| and to measure physical quantities
in different units of measurement
\begin{code}
    meas : {d : D} -> Units -> Q d -> Real
    meas {d} u' (Val u x) = x * df d (fs u u')
\end{code}
%
In the definition of |meas|, |fs| is the table that returns the scaling
factors between units of measurement. Further, it is useful to define
elementary binary |(+)|, |(-)|, |(*)| and |(/)| operations
%
\begin{code}
    (+)  :  {d : D} -> Q d -> Q d -> Q d
    (+) q1 q2 = Val SI (meas SI q1 + meas SI q2)

    (*)  :  {d1, d2 : D} -> Q d1 -> Q d2 -> Q (d1 `Times` d2)
    (*) q1 q2 = Val SI (meas SI q1 * meas SI q2)
\end{code}
%
%if False
\begin{code}
    (-)  :  {d : D} -> Q d -> Q d -> Q d
    (-) q1 q2 = Val SI (meas SI q1 - meas SI q2)

    (/)  :  {d1, d2 : D} -> Q d1 -> Q d2 -> Q (d1 `Over` d2)
    (/) q1 q2 = Val SI (meas SI q1 / meas SI q2)

    pow  :  {d : D} -> Q d -> (n : Integer) ->  Q (d `Pow` n)
    pow (Val u x) n = Val u (Prelude.Doubles.pow x (fromInteger n))

    data All : {n : Nat} -> {A : Type} -> (P : A -> Type) -> Vect n A -> Type where
      Nil :   {A : Type} -> {P : A -> Type} -> All P Nil
      (::) :  {n : Nat} -> {A : Type} -> {P : A -> Type} -> {x : A} -> {xs : Vect n A} ->
              P x -> All P xs -> All P (x :: xs)
    
    QVect : (n : Nat) -> Vect n D -> Type
    QVect n = All Q  -- All is defined in Idris-dev/libs/base/Data/Vect/Quantifiers.idr

    mapQ  :  {n : Nat} ->  {ds : Vect n D} -> {B : Type} ->
             ({d : D} -> Q d -> B) -> QVect n ds -> Vect n B
    mapQ f      Nil  =  Nil
    mapQ f (q :: qs) =  f q :: mapQ f qs

    prodPows : {n : Nat} -> {ds : Vect n D} ->
               QVect n ds -> (ps : Vect n Integer) -> Q (ProdPows ds ps)
    prodPows      Nil       Nil   =  Val SI 1.0
    prodPows (q :: qs) (p :: ps)  =  (pow q p) * (prodPows qs ps)

    tau  :  Q Time
\end{code}
%endif
%
with similar definitions for subtraction and division. Notice that
addition (subtraction) is only defined for quantities of the same
dimensions. This helps avoiding ``adding apples and oranges'' in
expressions involving physical quantities. When such additions make
sense, as in the example at page 6 of \citep{barenblatt1996scaling},
the computations can be implemented by pattern matching, as done in the
definition of |meas|.

\paragraph*{Physical quantities and ``phantom'' types.} \ We have
introduced physical quantities through the ``phantom\footnote{See \url{https://wiki.haskell.org/Phantom_type}.}'' data type |Q : D ->
Type|. The type parameter |D| allows one to \emph{specify} physical
quantities of given dimensions as in |x : Q Length|. When
\emph{defining} |x| one has to provide its measure in a system of
units as in |x = Val SI 3|.
%
An alternative design could be to make the units of measurement visible
in the type of physical quantities:
%
\begin{code}
    data Q2 : D -> Units -> Type where
      Val2 : {d : D} -> (u : Units) -> Real -> Q2 d u
\end{code}
%
This would allow one to avoid the introduction of an apparently
distinguished system of units in the definition of the elementary binary
operations, |SI| in our implementation. With |Q : D -> Units -> Type|, addition would read
%
%if False
\begin{code}
    meas2 : {d : D} -> {u : Units} -> Units -> Q2 d u -> Real
    meas2 {d} u' (Val2 u x) = x * df d (fs u u')
\end{code}
%endif
\begin{code}
    plus2  :  {d : D} -> {u1, u2, u3 : Units} -> Q2 d u1 -> Q2 d u2 -> Q2 d u3
    plus2 q1 q2 = Val2 u3 (meas2 u3 q1 + meas2 u3 q2)
\end{code}
%
A practical drawback of that approach is the proliferation of implicit
parameters. More importantly, exposing the units of measurement in the
type of physical quantities is conceptually unsatisfactory: whether |x|
has been defined to be a length of one meter or 1.093613 yards does not
really matter and we argue that this choice shall not be visible in its
type.

\paragraph*{The covariance principle for elementary operations.} \
Notice that |meas| and |(+)| fulfil the specification
%
%{
%format meas u = "\mu_{" u "}"
\begin{code}
    measHomPlus  :  {d : D} -> (u : Units) -> (q1, q2 : Q d) -> meas u (q1 + q2) = meas u q1 + meas u q2
\end{code}
%}
%
Similarly, |measu| is a homomorphism from |(-)|, |(*)| and |(/)| on
physical quantities and the corresponding binary operations in |Real|.
Proving |measHomPlus| requires postulating associativity of |(*)|,
distributivity of |(*)| over |(+)| in |Real| and
\begin{spec}
    dfSpec  :  {d : D} -> {u1, u2, u3 : Units} -> df d (fs u1 u2) * df d (fs u2 u3) = df d (fs u1 u3)
\end{spec}
The latter follows from the definition of the dimension function and, again,
from standard properties of exponentiation in |Real|, see the literate
Idris code that generates this document at
\url{https://gitlab.pik-potsdam.de/botta/papers}.

%if False
\begin{code}
    postulate multDistrPlusReal : {x, y, z : Real} -> (x + y) * z = x * z + y * z
    postulate multAssocReal : {x, y, z : Real} -> (x * y) * z = x * (y * z)
    postulate dfSpec : {d : D} -> {u1, u2, u3 : Units} -> df d (fs u1 u2) * df d (fs u2 u3) = df d (fs u1 u3)

    measHomPlus {d} u (Val u1 x1) (Val u2 x2) =
      let q1 = Val u1 x1
          q2 = Val u2 x2
      in  ( meas u (q1 + q2) )
            ={ Refl }=
          ( meas u (Val SI (meas SI q1 + meas SI q2)) )
            ={ Refl }=
          ( (meas SI q1 + meas SI q2) * df d (fs SI u) )
            ={ multDistrPlusReal }=
          ( (meas SI q1) * df d (fs SI u) + (meas SI q2) * df d (fs SI u) )
            ={ Refl }=
          ( (meas SI (Val u1 x1)) * df d (fs SI u) + (meas SI (Val u2 x2)) * df d (fs SI u) )
            ={ Refl }=
          ( (x1 * df d (fs u1 SI)) * df d (fs SI u) + (x2 * df d (fs u2 SI)) * df d (fs SI u) )
            ={ cong {f = \ x => x + (x2 * df d (fs u2 SI)) * df d (fs SI u)} multAssocReal }=
          ( x1 * (df d (fs u1 SI) * df d (fs SI u)) + (x2 * df d (fs u2 SI)) * df d (fs SI u) )
            ={ cong {f = \ x => x1 * (df d (fs u1 SI) * df d (fs SI u)) + x} multAssocReal }=
          ( x1 * (df d (fs u1 SI) * df d (fs SI u)) + x2 * (df d (fs u2 SI) * df d (fs SI u)) )
            ={ cong {f = \ x => x1 * x + x2 * (df d (fs u2 SI) * df d (fs SI u))} dfSpec }=
          ( x1 * df d (fs u1 u) + x2 * (df d (fs u2 SI) * df d (fs SI u)) )
            ={ cong {f = \ x => x1 * df d (fs u1 u) + x2 * x} dfSpec }=
          ( x1 * df d (fs u1 u) + x2 * df d (fs u2 u) )
            ={ Refl }=
          ( meas u (Val u1 x1) + meas u (Val u2 x2) )
            ={ Refl }=
          ( meas u q1 + meas u q2 )
            QED
\end{code}
%endif
%
That fact that |measu| is a homomorphism between elementary binary
operations on physical quantities and the corresponding operations in
|Real| is a special form of the covariance principle. We discuss a
general form of this principle in \cref{section:piexplained1}.

\subsection{Dimensional judgments, dimensional consistent programming}
\label{subsection:judgments}

The infrastructure introduced so far allows one to define new
physical quantities from existing ones
\begin{code}
    v  :  Q Velocity
    v  =  (x + x) / t
\end{code}
and implement dimensional judgments for verified programming like
\begin{code}
    check3 : dim (x / (t * t)) = Acceleration
    check3 = Refl
\end{code}
%
Here, values of type |dim (x / (t * t)) = Acceleration| witness that |x
/ (t * t)| is an acceleration. We have seen many examples of this kind
of dimensional judgments in previous sections: |f| is a force, |m| is a
mass, |T| is a temperature, |p| is a pressure, etc. We can express these
judgments through the idiom
\begin{code}
    Is : {d : D} -> Q d -> D -> Type
    Is q d = dim q = d
\end{code}
and write |Is q d| instead of |dim q = d|:
\begin{code}
    check4 : Is (m * x / (t * t)) Force
    check4 = Refl
\end{code}
%
Similarly, we can assess whether a physical quantity is dimensionless or
not
\begin{code}
    IsDimLess : {d : D} -> Q d -> Type
    IsDimLess q = dim q = DimLess

    check5 : IsDimLess ((x + x) / x)
    check5 = Refl
\end{code}
%
As one would expect, dimensionless quantities are invariant under
re-scaling of the units of measurement
\begin{spec}
IRP meas SI ((x + x) / x)
2.0 : Double

IRP meas CGS ((x + x) / x)
2.0 : Double
\end{spec}
and the dimension function fulfils the specification \cref{eq:df0}: by
the definition of |meas|, measurements only depend on the scaling
factors between the units of measurement, not on the units themselves.

%if False
\begin{spec}
IRP meas CGS (m * x / (t * t))
600000.0000000001 : Double

IRP meas SI (m * x / (t * t))
6.0 : Double

IRP dimf (m * x / (t * t)) [100, 1, 1000]
100000.00000000001 : Double
\end{spec}
%endif

Notice, however, that for the (perhaps most discussed application of
dimensional analysis) computation of the period |tau| of ''small''
oscillation of a simple pendulum of length |l| in a gravitational field
of strength |g|
\begin{code}
    l  :  Q Length;    {-"\quad"-}  g  :  Q Acceleration;  {-"\quad"-}  pi  :  Q DimLess
    l  =  Val SI 0.5;  {-"\quad"-}  g  =  Val SI 9.81;     {-"\quad"-}  pi  =  Val SI 3.14
\end{code}
the definition
\begin{spec}
    tau  :  Q Time
    tau  =  2 * pi * sqrt (l / g)
\end{spec}
does not type check. On one hand, this is because we have not defined
rules for the multiplication between numerical literals and physical
quantities and for computing the square root of physical
quantities. This can easily be fixed.
%
On the other hand, the definition of |tau| does not type check because
we have represented dimension functions through vectors of
\emph{integer} rather than \emph{rational} numbers. This is what makes
our DSL ``minimal''. We motivate this choice in \cref{subsection:dind}
and discuss possible generalizations in
\cref{section:generalization}.

%

\subsection{Units of measurement}
\label{subsection:units}

We have introduced the notion of units of measurement through a datatype
|Units| with just two data constructors: |SI| and |CGS|. This is also
the approach followed by \citep{barenblatt1996scaling} and reflects the
idea that units of measurement are just annotations.
%
There is nothing wrong with this approach and |Units| can readily be
extended to accommodate more systems of units by enumeration.

Notice, however, that this requires defining |fs|, the table that
returns the scaling factors between units of measurement, for all
possible pairs of data constructors. For the definition of |Units| above,
for example, |fs| was defined as:

\begin{spec}
    fs : Units -> Units -> RealPlus3
    fs SI   SI   = [1,     1,  1      ]
    fs SI   CGS  = [100,   1,  1000   ]
    fs CGS  SI   = [0.01,  1,  0.001  ]
    fs CGS  CGS  = [1,     1,  1      ]
\end{spec}

The table has to fulfil the specification |fs u1 u2 = map inv (fs u2
u1)| (which, in turn, guarantees |df d (fs u1 u2) * df d (fs u2 u3) = df
d (fs u1 u3)| for arbitrary |d|, |u1|, |u2| and |u3|, as discussed
above) which can be exploited to reduce the amount of boiler-plate code
in the definition of |fs|.

Still, defining |Units| through enumeration is perhaps not completely
satisfactory from a conceptual point of view: first, it does not
explicitly encode the idea that units of measurement are distinguished
properties of \emph{reference} physical quantities.

For example, the standard metre is defined as the length of the path
traveled by light in vacuum during a time interval of 1/299792458 of a
second, see \url{https://en.wikipedia.org/wiki/Metre}.

Second, a datatype with only two (or even a countable number of)
inhabitants is perhaps a bit too small to encode a principle (that there
is no privileged system of units of measurement) that shall hold for an
uncountable number of systems of units.

One can avoid these shortcomings by introducing a data constructor for
systems of units that takes as arguments strictly positive but otherwise
arbitrary reference lengths, times and masses. For example
%
\begin{code}
    mutual

      data Units' : Type where
        SI'  :  Units'
        Ref' :  (l  : Q' Length)  ->  {auto p  : (0.0 < val' l)  = True} ->
                (t  : Q' Time)    ->  {auto q  : (0.0 < val' t)  = True} ->
                (m  : Q' Mass)    ->  {auto r  : (0.0 < val' m)  = True} -> Units'

      data Q' : D -> Type where
        Val' : {d : D} -> (u : Units') -> Real -> Q' d

      val' : {d : D} -> Q' d -> Real
      val' (Val' u x) = x
\end{code}
%
Perhaps not surprisingly, this approach requires a mutual definition of
units of measurement and physical quantities but otherwise presents no
difficulties, see the literate Idris code that generates this document
at \url{https://gitlab.pik-potsdam.de/botta/papers}.

As one would expect from the notion of measurement as counting, negative
values and the neutral elements of |(+)| for lengths, times and masses
are not suitable reference quantities: in building an arbitrary system
of units in the LTM class with the |Ref'| constructor, these conditions
are checked with the ``internal'' |val'| helper.

A variation on the mutual approach sketched above could be to avoid
introducing |SI| as an outstanding system of units altogether and
instead equip the data type of physical quantities with three reference
constructors for a length, a time and a mass. For the rest of this and
of the next section, we stick to the minimal DSL with just two codes for systems of units.

%if False
\begin{code}
      partial fs' : Units' -> Units' -> RealPlus3
      fs' SI'   SI' = [1, 1, 1]
      fs' SI' (Ref' l t m) = [1.0 / (meas' SI' l), 1.0 / (meas' SI' t), 1.0 / (meas' SI' m)]
      fs' (Ref' l t m) SI' = [       meas' SI' l,        meas' SI' t ,        meas' SI' m ]
      fs' (Ref' l1 t1 m1) (Ref' l2 t2 m2)  =
        [meas' SI' l1 / meas' SI' l2, meas' SI' t1 / meas' SI' t2 , meas' SI' m1 / meas' SI' m2]

      partial meas' : {d : D} -> Units' -> Q' d -> Real
      meas' {d} u' (Val' u x) = x * df d (fs' u u')

    dim' : {d : D} -> Q' d -> D
    dim' {d} _ = d

    metre :  Q' Length
    metre =  Val' SI' 1
    cm    :  Q' Length
    cm    =  Val' SI' 0.01
    km    :  Q' Length
    km    =  Val' SI' 1000
    hour  :  Q' Time
    hour  =  Val' SI' 3600
    sec   :  Q' Time
    sec   =  Val' SI' 1
    gram  :  Q' Mass
    gram  =  Val' SI' 0.001

    cgs   : Units'
    cgs   = Ref' cm sec gram

    kgh   : Units'
    kgh   = Ref' km hour gram

    u     : Q' Velocity
    u     = Val' cgs 100

\end{code}
%endif

%

\subsection{Dimensional (in)dependence}
\label{subsection:dind}

\textbf{Dependence.}
Remember that the Pi theorem is about two properties of a generic
``physical relationship'' |f| between a ``dimensional quantity'' $a$
and $k + m$ ``dimensional governing parameters'' $a_1,\dots,a_k$ and
$b_1,\dots,b_m$.

One property (conclusion of the theorem) is that ``the dimension of $a$
can be expressed as a product of the powers of the dimensions of the
parameters $a_1,\dots,a_k$'' as formulated in \cref{eq:pi0}.
%
We have just seen examples of physical quantities whose dimension
functions can be expressed as products of powers of the dimension
functions of other physical quantities:
\begin{equation*}
  [l]     = [g] [\tau]^{2}, \quad
  [g]     = [l] [\tau]^{-2}, \quad \mathrm{or} \quad
  [\tau]  = [l]^{1/2} [g]^{-1/2}
\end{equation*}
In the |D|-language, we can express and assert the first
equality with
\begin{code}
    check6 : Is l (dim g `Times` (dim tau `Times` dim tau))
    check6 = Refl
\end{code}
or, equivalently
\begin{code}
    check7 : Is l (dim g `Times` Pow (dim tau) 2)
    check7 = Refl
\end{code}
and similarly for the second equality. In the definition of |check7| we
have used the integer exponentiation function |Pow : D -> Integer ->
D|. This fulfils the specification

\begin{spec}
    Pow d      0   =  DimLess
    Pow d (n + 1)  =  Pow d n `Times` d
    Pow d (n - 1)  =  Pow d n `Over` d
\end{spec}

\noindent
As already mentioned, formulating $[\tau] = [l]^{1/2} [g]^{-1/2}$ would
require fractional exponents and extending our representation of
dimension functions to vectors of rational numbers.
%
In other words: the exponents in \cref{eq:pih1} (and those in
\cref{eq:pi0}) are, in general, rational numbers and
representing dimension functions in terms of vectors of rational numbers
is perhaps the most natural setting for formulating the Pi theorem in
type theory.

The drawback of this approach is that it requires implementing rational
numbers in type theory. This is not a problem in principle, but integers
have a simpler algebraic structure (initial ring) and more efficient
implementations.
%
%% for example in Idris, we do not have implementations of rational numbers
%% in the standard library.
%
Also, formulating the Pi theorem in terms of rational exponents does not
seem strictly necessary: if dimensional analysis with rational exponents
allows one to deduce that the period $\tau$ of a simple pendulum scales
with $(l/g)^{\frac{1}{2}}$, integer-based dimensional analysis should be
enough to deduce that $\tau^2$ scales with $l/g$!

\paragraph*{Formalizing dependence.} \ We explore this possibility in the next section. To this end, let's
formalize the notion that |d : D| is dependent on |ds : Vect k D| iff a
non-zero integer power of |d| can be expresses as a product of integer
powers of |ds|:
%
%{
%format => = "\to"
\begin{code}
    IsDep  :  {k : Nat} -> (d : D) -> (ds : Vect k D) -> Type
    IsDep {k} d ds = Exists  (Integer, Vect k Integer) (\ (p, ps) => (Not (p = 0), Pow d p = ProdPows ds ps))
\end{code}
%}
where the function |ProdPows| implements the computation of products of
integer powers of vectors of |D| values. It fulfils the specification
%
\begin{spec}
    ProdPows : {n : Nat} -> Vect n D -> Vect n Integer -> D
    ProdPows      Nil       Nil   =  DimLess
    ProdPows (d :: ds) (p :: ps)  =  Pow d p `Times` ProdPows ds ps
\end{spec}
%
The dimension computation is mirrored in the corresponding computation
on quantities:
\begin{spec}
    prodPows :  {n : Nat} -> {ds : Vect n D} ->
                QVect n ds -> (ps : Vect n Integer) -> Q (ProdPows ds ps)
    prodPows  Nil        Nil        =  Val SI 1.0
    prodPows  (q :: qs)  (p :: ps)  =  (pow q p) * (prodPows qs ps)
\end{spec}
Note that the definition of |prodPows| has the same structure as the
definition of its type (index) |ProdPows|.
%
This is an example of type-driven development~\citep{idrisbook}.

Because of the definition of |DimLess|, |Times| and |Over|
from \cref{subsection:df}, |Pow| and |ProdPows| also fulfil
%
\begin{spec}
    Pow d n = n <# d
    ProdPows ds ps = foldr (+) DimLess (zipWith (<#) ps ds)
\end{spec}
%
where |(<#)| is generic scaling for vectors of numerical types:
%
%format => = "\Rightarrow"
\begin{spec}
    (<#)  :  {n : Nat} -> {T : Type} -> Num T => T -> Vect n T -> Vect n T
    x  <# v =  map (x *) v
\end{spec}
%
Therefore, in the |D|-language, dependence between dimension functions
boils down to linear dependence between their representations, as one
would expect.
%
%if False
\begin{code}
    namespace AreDep

      AreDep : {m, k: Nat} -> (ds' : Vect m D) -> (ds : Vect k D) -> Type
      AreDep ds' ds = All (\d' => IsDep d' ds) ds'
{-
      data AreDep  :  {m, k: Nat} -> (ds' : Vect m D) -> (ds : Vect k D) -> Type where
        Nil  : {k : Nat} -> {ds : Vect k D} -> AreDep Nil ds
        (::) : {d' : D} -> {k : Nat} -> {ds : Vect k D} -> {m : Nat} -> {ds' : Vect m D} ->
               IsDep d' ds -> AreDep ds' ds -> AreDep (d' :: ds') ds
-}
\end{code}
%endif
%
By extension, we say that a physical quantity |q| is \emph{dimensionally
dependent} on a vector of physical quantities |qs| if |dim q| is
dependent on the dimensions of |qs|:
\begin{code}
    IsDimDep  :  {d : D} -> {k : Nat} -> {ds : Vect k D} -> Q d -> QVect k ds -> Type
    IsDimDep {d} {ds} q qs  =  IsDep d ds
\end{code}

\noindent
In the definition of |IsDimDep| we have applied the data type |QVect|
for vectors of physical quantities (of different dimensions):
\begin{spec}
    data All : {n : Nat} -> {A : Type} -> (P : A -> Type) -> Vect n A -> Type where
      Nil :   {A : Type} -> {P : A -> Type} -> All P Nil
      (::) :  {n : Nat} -> {A : Type} -> {P : A -> Type} -> {x : A} -> {xs : Vect n A} ->
              P x -> All P xs -> All P (x :: xs)
    
    QVect : (n : Nat) -> Vect n D -> Type
    QVect n ds = All Q ds
\end{spec}

\noindent
Notice that |IsDimDep a as| is an existential type. In order to assess
that |a| is dimensionally dependent on |as|, one has to provide suitable
integer exponents and an equality proof. For the simple pendulum, for
example:
%if False
\begin{code}
    not2eq0 : Not (2 = 0)
    not2eq0 Refl impossible
\end{code}
%endif
\begin{code}
    check8 : IsDimDep tau [l, g]
    check8 = Evidence (2, [1, -1]) (not2eq0, Refl)
\end{code}
where |not2eq0| is a proof that 2 is not equal to 0. This evidence is
just another way of asserting the equality
\begin{code}
    check9 : Pow (dim tau) 2 = Pow (dim l) 1 `Times` Pow (dim g) (-1)
    check9 = Refl
\end{code}
For the simple pendulum example, it allows one to deduce that, under
quite general assumptions\footnote{The assumptions are
that the period of oscillation of the pendulum only depends on its mass,
its length, the acceleration of gravity and the initial angle, see for
example the introduction of \citep{barenblatt1996scaling}.}, the period
of oscillation $\tau$ is proportional to the square root of $l/g$.

\paragraph*{Forming dimensionless products.} \ Given a physical quantity which is dimensionally dependent on other
physical quantities, one can make it dimensionless like the ``$\Pi$''
quantities of the Pi theorem:
%if False
\begin{code}
    dimMakeDimLessComp : {k : Nat} -> (d : D) -> (ds : Vect k D) -> IsDep d ds -> D
    dimMakeDimLessComp d ds (Evidence (p, ps) _) = Pow d p `Over` ProdPows ds ps

    dimMakeDimLessCompIsDimLess  :  {k : Nat} ->
                                    (d : D) -> (ds : Vect k D) -> (e : IsDep d ds) ->
                                    dimMakeDimLessComp d ds e = DimLess
    dimMakeDimLessCompIsDimLess d ds (Evidence (p, ps) (c, prf)) =
        ( dimMakeDimLessComp d ds (Evidence (p, ps) (c, prf)) )
      ={ Refl }=
        ( Pow d p `Over` ProdPows ds ps )
      ={ cong {f = \ X => X `Over` ProdPows ds ps} prf }=
        ( ProdPows ds ps `Over` ProdPows ds ps )
      ={ dodIsDimLess }=
        ( DimLess )
      QED

    dimMakeAllDimLess : {m, k : Nat} -> (ds' : Vect m D) -> (ds : Vect k D) -> AreDep ds' ds -> Vect m D
    dimMakeAllDimLess        Nil  ds        Nil   =  Nil
    dimMakeAllDimLess (d' :: ds') ds (e' :: es')  =  DimLess :: dimMakeAllDimLess ds' ds es'
\end{code}
%endif
\begin{code}      
    makeDimLess  :  {d : D} -> {k : Nat} -> {ds : Vect k D} ->
                    (q : Q d) -> (qs : QVect k ds) -> IsDimDep q qs -> Q DimLess
    makeDimLess {d} {ds} q qs (Evidence (p, ps) prf) = 
      let comp = pow q p / prodPows qs ps
      in  replace (dimMakeDimLessCompIsDimLess d ds (Evidence (p, ps) prf)) comp
\end{code}
%if False
\begin{code}      

    makeAllDimLess  :  {m : Nat} -> {ds' : Vect m D} ->
                       {k : Nat} -> {ds : Vect k D} ->
                       (qs' : QVect m ds') -> (qs : QVect k ds) ->
                       (es' : AreDep ds' ds) ->
                       QVect m (dimMakeAllDimLess ds' ds es')
    makeAllDimLess        Nil  qs      Nil     =  Nil
    makeAllDimLess (q' :: qs') qs (e' :: es')  =  makeDimLess q' qs e' :: makeAllDimLess qs' qs es'

    makeDimLessIsDimLess  :  {d : D} -> {k : Nat} -> {ds : Vect k D} ->
                             (q : Q d) -> (qs : QVect k ds) ->
                             (e : IsDep d ds) ->
                             IsDimLess (makeDimLess q qs e)
    makeDimLessIsDimLess {d} {ds} q qs e = Refl
\end{code}
%endif
\noindent
In |makeDimLess|, we have applied the function
|dimMakeDimLessCompIsDimLess|. This takes a dimension |d|, a vector of
dimensions |ds| and an evidence |e : IsDep d ds|. It returns a proof
that |comp = pow q p / prodPows qs ps| is indeed
dimensionless. Implementing |dimMakeDimLessCompIsDimLess| requires
proving that |d `Over` d| is equal to |DimLess| for arbitrary |d|:
\begin{spec}
    dodIsDimLess : {d : D} -> d `Over` d = DimLess
\end{spec}
This is not a problem and suggests that |D| is a group, see
\citep{doi:10.1142/9789811242380_0020} and \cref{section:generalization}
for a discussion of the algebraic structure of |D|.

\paragraph*{Independence.} \ Remember that a condition for the function $f$ of the Pi theorem
to be reducible to the form of \cref{eq:pi1} is that the parameters
$a_1,\dots,a_k$ ``have independent dimensions''.
%
Perhaps not surprisingly, the idea is that |qs : QVect k ds| are
dimensionally independent iff expressing |DimLess| as a product of
powers of their dimension functions requires all exponents to be zero.
%
This is equivalent to say the vectors associated with their dimensions
are linearly independent, see \citep[section 1.1.5]{barenblatt1996scaling}:
%if False
\begin{code}
    dot   : Vect 3 Integer -> Vect 3 Integer -> Integer
    dot [a0, a1, a2] [b0, b1, b2] = a0 * b0 + a1 * b1 + a2 * b2

    cross : Vect 3 Integer -> Vect 3 Integer -> Vect 3 Integer
    cross [a0, a1, a2]
          [b0, b1, b2] =
          [a1 * b2 - a2 * b1,
           a2 * b0 - a0 * b2,
           a0 * b1 - a1 * b0]

    det : Vect 3 Integer -> Vect 3 Integer -> Vect 3 Integer -> Integer
    det a b c = dot a (cross b c)

    areCollinear : Vect 3 Integer -> Vect 3 Integer -> Bool
    areCollinear a b = cross a b == [0,0,0]

    areLinIndep : {n : Nat} -> Vect n (Vect 3 Integer) -> Bool
    areLinIndep        Nil                   = True
    areLinIndep (v1 :: Nil)                  = not (v1 == [0,0,0])
    areLinIndep (v1 :: v2 :: Nil)            = not (areCollinear v1 v2)
    areLinIndep (v1 :: v2 :: v3 :: Nil)      = not (det v1 v2 v3 == 0)
    areLinIndep (v1 :: v2 :: v3 :: v4 :: vs) = False

    AreIndep : {n : Nat} -> Vect n (Vect 3 Integer) -> Type
    AreIndep vs = areLinIndep vs = True

    AreDimIndep  :  {k : Nat} -> {ds : Vect k D} -> QVect k ds -> Type
    AreDimIndep {ds} _ = AreIndep ds
\end{code}
%endif
\begin{spec}
    AreDimIndep  :  {k : Nat} -> {ds : Vect k D} -> QVect k ds -> Type
    AreDimIndep {ds} _ = AreIndep ds
\end{spec}
In the definition of |AreDimIndep| we have applied the predicate
|AreIndep|. In mechanics, |n = 3| and |D = Vect 3 Integer|, so that |AreIndep| is decidable
\begin{spec}
    AreIndep : {k : Nat} -> Vect k (Vect 3 Integer) -> Type
    AreIndep vs = areLinIndep vs = True
\end{spec}
and a decision procedure can be implemented by pattern matching on the the length |vs|
\begin{spec}
    areLinIndep : {k : Nat} -> Vect k (Vect 3 Integer) -> Bool
    areLinIndep        Nil                    =  True
    areLinIndep (v1 :: Nil)                   =  not (v1 == [0,0,0])
    areLinIndep (v1 :: v2 :: Nil)             =  not (areCollinear v1 v2)
    areLinIndep (v1 :: v2 :: v3 :: Nil)       =  not (det v1 v2 v3 == 0)
    areLinIndep (v1 :: v2 :: v3 :: v4 :: vs)  =  False
\end{spec}
where |areCollinear| and |det| are defined in terms of the standard dot
and cross products in |Vect 3 Integer|. Thus, |AreDimIndep| can be
applied to assess the dimensional independence of physical quantities:
\begin{code}
    check11 : AreDimIndep [l, g]
    check11 = Refl

    check12 : Not (AreDimIndep [tau, l, g])
    check12 Refl impossible
\end{code}

We have encoded the notions of dimension function, physical quantity,
measurement, units of measurement and dimensional (in)dependence for
classical mechanics (and its subdomains) in Idris. With this minimal
DSL, we can express dimensional judgments and implement dimensional
consistent programs.
%
In the next section, we apply the DSL to formulate the Pi theorem in
type theory and discuss what is left to do to actually prove it.
