% -*-Latex-*-
%\RequirePackage{amsmath}
\documentclass{jfp}
%\documentclass[nolinenum]{jfp} % for arXiv
%\documentclass{jfphack}
%\usepackage{amsmath}
\usepackage{tikz-cd}
%% begin for submission
\pgfrealjobname{main}
%% end for submission
\usepackage{quoting}
\usepackage{bigfoot} % To handle url with special characters in footnotes!
\usepackage{hyperref}
%\usepackage{cleveref}
\usepackage[utf8]{inputenc}

%include main.fmt

\input{macros.TeX}

\begin{document}

\journaltitle{JFP}
\cpr{The Author(s),}
\doival{10.1017/xxxxx}

\newcommand{\shorttitle}{Types, equations, dimensions and the Pi theorem}
\lefttitle{Botta, Jansson, da Silva}
\righttitle{\shorttitle}
\totalpg{\pageref{lastpage01}}
\jnlDoiYr{2023}


\title{Types, equations, dimensions and the Pi theorem}

% double "@" in email to please lhs2tex
\begin{authgrp}
\author{NICOLA BOTTA}
\affiliation{Potsdam Institute for Climate Impact Research, Potsdam, Germany, \\
  Chalmers University of Technology, Göteborg, Sweden.
  (\email{botta@@pik-potsdam.de})}
%
\author{PATRIK JANSSON}
\affiliation{Chalmers University of Technology and University of Gothenburg, Göteborg, Sweden.
  (\email{patrikj@@chalmers.se})}
%
\author{GUILHERME HORTA ALVARES DA SILVA}
\affiliation{Chalmers University of Technology and University of Gothenburg, Göteborg, Sweden.
  (\email{alvares@@chalmers.se})}
%
\end{authgrp}


\begin{abstract}
  The languages of mathematical physics and modelling are endowed with
  a rich ``grammar of dimensions'' that common abstractions of
  programming languages fail to represent. We propose a dependently
  typed domain-specific language (embedded in Idris) that captures
  this grammar. We apply it to explain basic notions of dimensional
  analysis and Buckingham's Pi theorem. We hope that the language
  makes mathematical physics more accessible to computer scientists
  and functional programming more palatable to modelers and
  physicists.
\end{abstract}

\maketitle%[E]

%% \begin{keyword}
%% types \sep
%% equations \sep
%% dimensions \sep
%% \end{keyword}


% \renewcommand{\hscodestyle}{}
\setlength{\mathindent}{1em}
%
%\renewcommand{\texfamily}{\fontfamily{cmtex}\selectfont\small}
%\renewcommand{\hsnewpar}[1]%
%  {{\parskip=0pt\parindent=0pt\par\vskip #1\noindent}}
%\renewcommand{\hscodestyle}{}
%\newcommand{\fixsmalldisplayskip}{\setlength{\belowdisplayskip}{6pt plus 0pt minus 0pt}}
\newcommand{\fixlengths}{\setlength{\abovedisplayskip}{6pt plus 1pt minus 1pt}\setlength{\belowdisplayskip}{6pt plus 1pt minus 1pt}}
\renewcommand{\hscodestyle}{\small\fixlengths}
\fixlengths
%\setlength{\belowdisplayskip}{6pt plus 0pt minus 0pt}    %% lhs2TeX uses this
%\setlength{\belowdisplayskip}{8.5pt plus 3pt minus 4pt}  %%\small
%\setlength{\belowdisplayskip}{10pt plus 2pt minus 5pt}   %%\normalsize

%include Introduction.lidr
%include Equations.lidr
%include Dimensions.lidr
%include Pi.lidr
%%include PiExplained.lidr
%%include DSL.lidr
%include DSL1.lidr
%include PiExplained1.lidr
%include Generalization.lidr
%include Conclusions.lidr
%%include Appendix.lidr

\section*{Acknowledgments}
We are grateful to Prof.\ Jeremy Gibbons, Dr.\ Julian Newman, the
JFP editors and the anonymous reviewers whose comments and quenstions
have lead to significant improvements of the original manuscript.
%
The work presented in this paper heavily relies on free software, among others on Coq, Idris, Agda, GHC, git, vi, Emacs, \LaTeX\ and on the FreeBSD and Debian GNU/Linux operating systems.
%
It is our pleasure to thank all developers of these excellent products.
%
This is TiPES contribution No 231. This project has received funding from
the European Union’s Horizon 2020 research and innovation programme
under grant agreement No 820970.

\subsection*{Conflicts of Interest}
None.

\bibliographystyle{jfplike}
\bibliography{references}
\label{lastpage01}
\end{document}
