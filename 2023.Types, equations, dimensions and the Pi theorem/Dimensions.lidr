% -*-Latex-*-

%if False

> module Dimensions

> %default total
> %auto_implicits off
> %access public export

%endif

\section{Dimensions, physical quantities, units of measurement}
\label{section:dimensions}

In the last section we have discussed simple examples of equations and
implicit problem specifications in the context of mathematical physics
and modelling.
%
We have seen that the variables that appear in equations like Newton's
second law \cref{eq:newton1} or in the ideal gas relationship are endowed
with properties, like being a force or a temperature, that we do not
know how to represent through the type system.

In the case of the Stommel model, we have encountered variables that
were said to ``have a dimension'', for example |c|. Other expressions
were said to be ``non-dimensional''. But what does it mean for |c| to
have a dimension?  In a nutshell, it means two things:

\begin{enumerate}
  \item That |c| represents a \emph{physical quantity} that can be
    measured in a system of \emph{units} of measurement of a given
    \emph{class}.
  \item That, given a class, a system of units in that class and a
    measurement of |c| in that system, one can define another system of
    units in the same class that gives a different measurement.
%
%    \TODO{This seems very weak: can the new measurement be whatever,
%      as long as it not equal?}
\end{enumerate}

\noindent
An example will help illustrating the idea. Consider the sheet of paper
on which this article is printed. Assume its width to be 20 centimeters
and its height to be 30 centimeters.

If we measure \emph{lengths} in centimeters, the measures of width and
height will be 20 and 30, respectively. In meters, these measures will
instead be 0.2 and 0.3. A change in the units of measurement of lengths
has resulted in a change in the measures of the width and of the height
of the paper: we say that the width and the height have a dimension or,
equivalently, that they are dimensional quantities.

By contrast, the ratio between the height and the width of the paper is
3/2 no matter whether we measure lengths in centimeters, meters or in
other units: we say that the ratio is a non-dimensional quantity.

Notice that the distinction between dimensional and non-dimensional
quantities crucially relies on the (implicit) assumption of measuring
\emph{both} the height and the width of the paper (more generally, all
lengths) with the same units.

%% In this case we only have to do with units for lengths but if we were to
%% measure the paper height in centimeters and the width in meters first,
%% and then measure both in meters, we would obtain ratios of 150 and 3/2,
%% respectively, and thus come to the conclusion that the ratio between the
%% height and the width of the paper is a dimensional quantity!

In strongly typed languages like Idris and Agda, the judgment |e : t|
means that expression |e| has type |t|.
%
In the physical sciences, the judgment |[e] = d| means that expression
|e| (representing a physical quantity) has dimension |d|. At this point,
we do not know how to formalize this judgment in type theory (we
discuss how to do so in \cref{section:dsl1}) but the idea
is that |d| is a function of type $\Real_{+}^n \to \Real_{+}$ where the
number |n : Nat| is domain-specific and $\Real_{+}$ denotes the set of
positive real numbers.

For example, in mechanics |n = 3|. In this domain, the judgment |[e] =
\ (L,T,M) => L * invT| means that |e| is a quantity that can be measured
in a system of units of measurements, for example SI (international) or
CGS (centimeter-gram-second), for \emph{lengths}, \emph{times} and
\emph{masses}, and that the measure of |e| \emph{increases} by a
factor |L * invT| when the units for lengths, times and masses are
\emph{decreased} by factors |L|, |T| and |M|, respectively. Another way
to express the same judgment is to say that ``|e| is a velocity'' or
that ``|e| has the dimension of a velocity''. The notation was
originally introduced by Maxwell, see \citep[section 1.1.3]{barenblatt1996scaling},
and in DA (dimensional analysis) it is common to write
|[e] = L invT| as an abbreviation for |[e] = \ (L,T,M) => L * invT|.

In mechanics, physical quantities can alternatively be measured in a system of
units for \emph{lengths}, \emph{times} and \emph{forces}. This defines a
different \emph{class} of units in the same domain (thus |n = 3|). In plain geometry |n
= 1| and in classical mechanics with heat transfer |n = 4|: beside units
for lengths, times and forces, in this domain we also need units for
\emph{temperatures}.

A physical quantity |phi| is called dimensionless if |[phi] = const 1|
and dimensional otherwise.
%\TODO{Do all ``expressions'' represent physical quantities? (or only the ``leaves'' of base type?)}

For the reader who finds all this very confusing and suspiciously far
away from the clear and deep waters of type theory: it is!
%
As we have seen in \cref{section:equations}, the standard arsenal
of functional programming abstractions is not yet ready to encode the
grammar of dimensions that informs the language of mathematical physics
and modeling.

If we want to develop DSLs that are palatable to mathematicians,
physicists and modelers, we need to spend some time wading in shallow
and muddy waters. The ideas summarized in this section are discussed
more authoritatively and to a greater extent in the introduction and in
section 1 of \citep{barenblatt1996scaling}.

We will give a precise specification of the higher order function |[!]|
and formalize the notion of physical quantity in type theory in
\cref{section:dsl1}.
%
To get there, however, we need to first get an idea of the problems
addressed by the theory of \emph{similarity} and by Buckingham's Pi
theorem. This is done in the next two sections. We conclude this one
with three remarks:

%\paragraph*{Remark 1.}
The first remark is that equations like Newton's second principle
\cref{eq:newton1} or the ideal gas law \cref{eq:gas0} represent
relationships between physical quantities.
%
The idea is that these equations summarize empirical facts about
measurements (or put forward axioms about such measurements) of these
quantities.
%% \TODO{Do we need to involve measurements?}\REMARK{Nicola}{I think so,
%%   Tim also finds it clarifying}
%
These facts (or axioms) about measurements are understood to hold under
a number of assumptions, typically implicit. A crucial one is that all
measurements are done in the same class and system of units.

For example, \cref{eq:gas0} maintains that measurements of the
pressure |p| of an ideal gas are proportional to the product of
measurements of density |rho| and measurements of temperature |T|,
the factor of proportionality being a gas-specific constant |R|.
%
We come back to the idea that equations like Newton's second principle
represent relationships between physical quantities and we present a more
consistent interpretation of such equations in
\cref{subsection:covariance}.

%\paragraph*{Remark 2.}
The second remark is that the result of such measurements (and thus the
type of the variables entering the equations) depends on a context that
is not visible in the equations themselves.
%
For example, when the measurements of pressure, density and temperature
are pertinent to a homogeneous gas, \cref{eq:gas0} can be
interpreted as the specification

< pSpec          :  meas p = meas rho * meas R * meas T

where |meas| is the measure function (which will talk more about later).
%
In a context in which |p|, |rho| and |T| represent the pressure, the
density and the temperature of a gas in local thermodynamical
equilibrium, the same equation can be interpreted as the specification

< pSpec          :  meas p <=> \ (x, t) => meas (rho (x,t)) * meas R * meas (T (x,t))

This is typically the case when a symbol that represents the pressure
appears in the right hand side of a system of partial differential
equations like the Euler or the Navier-Stokes equations of fluid
mechanics \citep{chorin2000mathematical}.
%
In yet another context, |p|, |rho| and |T| could represent
probability density functions or other higher order functions.
%
But what could be the types of |p|, |rho|, |R| and |T| in these
contexts?  We answer this question in \cref{subsection:quantities}.

The last remark is that, when |p| is a function taking values in, e.g.,
|(Real, CalTime)|, the judgment ``|p| is a pressure'' (or ``|p| has the
dimension of a pressure'' or, |[p] = M invL invT2|) is just an
abbreviation for ``|forall (x,t) : (Real, CalTime)|, \ |p(x,t)| is a
pressure''.
%
If |p| is differentiable with respect to both space and time and if the
space and the time coordinates are dimensional (that is, |forall (x,t) :
(Real, CalTime)|, |[x] = L| and |[t] = T|) then the partial derivatives of
|p| with respect to space and time have the dimensions |M invL2 invT2|
and |M invL invT3|, respectively.
%
More generally, if |p| is a function from a dimensional space-time set
into real numbers and |p| has dimension |d|, the partial derivatives of
|p| with respect to time and space are functions of the same type as |p|
but with dimensions |d invT| and |d invL|. Again, these are shortcuts
for |\ (L,T,M) => d (L,T,M) * invT| and |\ (L,T,M) => d (L,T,M) *
invL|, respectively.

As already mentioned in \cref{section:equations}, the last
specification for |p| could be written more concisely as |p <=> rho * R
* T| by introducing canonical abstractions for functions that return
numerical values.
%
To the best of our knowledge, no standard Idris library provides such
abstractions although, as discussed in the introduction, there have been
proposals for making types in FP languages more aware of dimensions and
physical quantities, for example \citep{IdrisQuantities}.
%but see \citep{IdrisQuantities}.
%\TODO{It seems there could be an ``annotated simple type system'' with dimensions in the ``leaves''?}
%\TODO{We need to relate to the earlier accounts of ``DA meets types.''}

%if False

\begin{PLAN}

We argue that the understanding achieved in the previous section is
incomplete and introduce the notions of \emph{measurement} and of
\emph{dimension} informally, following \citep{barenblatt1996scaling}.

We start by remarking that physical laws like

  \[F = ma\]

or

  \[p = \rho R T\]

summarize empirical facts (observations), entail implicit quantification
(at any point on the surface of the earth/anywhere, at any time, for any
\dots) and that the symbols $F$, $m$, $a$, $\rho$, $R$, $T$,
etc. represent, among other things, outcomes of \emph{measurements}.

We give a plain natural language interpretation of what $F = ma$ is
meant to express by putting forward all necessary conditions for
measurements: a \emph{frame of reference} for accelerations and
\emph{units} for all quantities.

\label{question:invariant}
We observe that (1) is invariant w.r.t. the choice of the frame of
reference and also w.r.t. the choice of the units as long as these
choices are the same for all variables.

We remark that this is not by chance and re-interpret (1) as a solution
to the problem of finding a functional relationship between measurements
of forces, masses and accelerations such that the invariances hold.

Introduce the notions of measurement and of dimension informally,
following and citing textbooks. Introduce the next section.

\end{PLAN}

%endif
