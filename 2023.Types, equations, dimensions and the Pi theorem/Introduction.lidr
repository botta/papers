% -*-Latex-*-

%if False

> module Introduction1

> %default total
> %auto_implicits off
> %access public export

%endif

\section{Introduction}
\label{section:intro}

%if False

In this paper we look at the languages of mathematical physics and of
physical modelling from the viewpoint of Functional Programming (FP).
%
We argue that these languages are endowed with a rich ``grammar of
dimensions'' that common abstractions of FP languages fail to represent.
%
We propose a small dependently typed domain-specific language (DSL) that
captures this grammar and apply it to explain basic notions of
dimensional analysis and Buckingham's Pi theorem.
%
Our main motivation is to make the languages of mathematical physics and
physical modelling more accessible to computer scientists but also to
make FP more palatable to modelers and physicists.

%endif

\paragraph*{Motivation.}

\ The main motivation for this work comes from a failure. During more
than one decade, two of the authors have been been advocating
mathematical specifications, type-driven analysis and functional
programming (FP) as methodologies to better understand, specify and
solve problems in climate impact research, climate policy advice and,
by large, global systems science %(GSS)
\citep{botta+al2011a, ionescujansson:LIPIcs:2013:3899,
  2017_Botta_Jansson_Ionescu, esd-9-525-2018, ijb:ISoLA:2018,
  Botta2023MatterMost}.

Alas, after ten years of proselytism and intense collaborations, we have
hardly been able to convince any climate modeler of the usefulness of
FP, let apart convert them to ``thinking functionally'' with Haskell
\citep{bird2014thinking}, Agda~\citep{norell2007thesis},
Idris~\citep{idrisbook}, or Coq~\citep{CoqProofAssistant}.
%
Have we just done a bad job or is this failure a symptom of a deeper
problem?

\paragraph*{There is no need for FP in mathematical physics, is there?}

\ Physicists and modelers are well trained in exploiting established
numerical libraries \citep{NAG, GSL, NumPy} and frameworks
\citep{OpenFOAM, AMReX_JOSS}
% -- Libraries
% NAG https://www.nag.com/content/nag-library
% NumPy https://numpy.org/
% GSL https://www.gnu.org/software/gsl/
% Unimath for Chemistry in Agda
% A Formalization of Elements of Special Relativity in Coq -- https://dash.harvard.edu/handle/1/38811518
% Isabelle Marries Dirac: A Library for Quantum Computation and Quantum Information -- https://dash.harvard.edu/handle/1/38811518
%
% -- frameworks
% OpenFOAM https://www.openfoam.com/
%
%
for approximating solutions of (ordinary, partial, stochastic)
differential equations efficiently, implementing large computer-based
models in FORTRAN, C, C++, Java, Python, or Julia and
% -- models
% FORTRAN:
% Weather Research and Forecasting (WRF) Model
% C:
% The Community Earth System Model (CESM)
% The Fast Multipole Method (FMM)
testing model predictions against analytical solutions or observations.

In engineering and in many physical sciences, this methodology has lead
to reliable computations and to computer-based modelling almost fully
replacing physical modelling: for many applications, running computer
programs is more flexible and much cheaper than running wind tunnels or
full-scale experiments.

But there are important research areas in which empirical validations
are beyond reach and the predictive capability of computer-based models
is poorly known and needs to be questioned.
%
In climate science but also in plasma physics, for example, it is
simply impossible (or just too dangerous or too expensive) to test the
correctness of computations empirically.
%
It is not possible to study the effectiveness of a policy designed to
reduce greenhouse gas (GHG) emissions without implementing that policy
or, as argued by \citet{lucarini2004}, ``the usual Galilean scientific
validation criteria do not apply to climate science''. In much the same
way, plasma physicists and engineers cannot afford to damage hundreds of
experimental tokamak fusion reactors to assess and validate optimal
control options for such devices \citep{HOPPE2021108098,
  Pusztai2023BayesOptMMI}.

In these domains, scientists need methodologies that bring confidence
that their computations are correct well before such computations can
actually be applied to real world systems.
%
Formal specifications is the key both for testing programs and
for showing the \emph{absence} of errors in computations
\citep{ionescujansson:LIPIcs:2013:3899} and dependently typed FP
languages have reached enough expressive power to support formulating
very precise specifications.
%
And yet climate modelers and physicists have, by large,
stayed away from FP languages. Why so?

\paragraph*{Educational gaps.}

\ It is probably fair to say that most physicists and modelers have never
heard about mathematical program specifications, not to mention FP and
dependently typed languages.
%
In much the same way, most computer scientist have hardly been exposed
to the language of mathematical physics, say, for example, that of
\citet{courant89, arnold1989mathematical, barenblatt1996scaling,
  Kuznetsov:1998:EAB:289919}.

Originally very close to elementary set theory and calculus, this
language has evolved over the last decades and fragmented into a
multitude of dialects or DSLs,
%% perhaps under the pressure of
%% specialization and of the pervasive usage of imperative programming and
%% computer-based modelling in the physical sciences.
perhaps through the pervasive usage of imperative programming and
computer-based modelling.
%
Common traits of these dialects are the limited usage of currying and
higher order functions, the overloading of the equality sign, the lack
of referential transparency and explicit type information (although
mnemonic rules are often introduced for encoding such information like
in $x_{[0,T]}$ instead of |x : [0,T] -> Real|) and the usage of
parentheses to denote both function application and function composition
as in $\dot{x} = f(x)$ instead of $\forall t, \ \dot{x}(t) = f(x(t))$ or, in
point-free notation, |dot x = f . x|.
%
\DONE{The examples can be hard to read at this stage (but that may be OK with a forward pointer).}

These DSLs represent a major difficulty for computer scientists and
systematic efforts have been undertaken by one of the authors to make
them more accessible to computer science students \citep{Ionescu_2016,
  JanssonIonescuBernardyDSLsofMathBook2022} and
improve the dialogue between the computational sciences and the physical
sciences. Our paper is also a contribution to such dialogue.

We argue that the
DSLs of mathematical physics are endowed with a rich but hidden
``grammar of dimensions'' that (functional) programming languages have
failed to exploit or even recognize.
%
This grammar informs important notions of consistency and of correctness
which, in turn, are the bread and butter of program analysis, testing
and derivation.

From this perspective, it is not very surprising that physicists and
modelers have hardly been interested in FP.
%
Standard FP abstractions emphasize type annotations that do not matter
to physicists and modelers (for the climate scientist, all functions
are, bluntly speaking, of type $\mathbb{R}^m \to \mathbb{R}^n$ for some
natural numbers $m$ and $n$), while at the same time failing to highlight
differences that do matter like the one between a
\emph{length} and a \emph{time}.
%
We hope that this work will also help making FP a bit more palatable to
physicists and modelers.

\paragraph*{Outline.}

\ In \cref{section:equations} we briefly revise the role of equations,
laws, types and \emph{dimensions} in computer science, mathematical
physics and modelling.

In \cref{section:dimensions} we discuss the ideas of dimension,
\emph{physical quantity}, and \emph{units of measurement}
informally. This is mainly meant to guide the computer scientist out of
her comfort zone but also to answer a question that should be of
interest also to readers who are familiar with modelling: ``what does it
mean for a parameter or for a variable to have a dimension?''

\Cref{section:pi} is a short account of similarity theory
\citep{Buckingham1914, Rayleigh1915, bridgman1922} and of Buckingham's
Pi theorem, mainly following
\citet[Section 1]{barenblatt1996scaling}. This is going to be new ground for most
computer scientists but, again, we hope to also provide a new angle to
modelers who are young and have therefore mainly been imprinted with
computer-based modelling.

\DONE{It looks a bit strange that the section title is ``The Pi Theorem
  in Type Theory'' (which sounds pretty abstract!) while the explanation
  here talks about ``a concrete DSL''.}

In \cref{section:dsl1} we introduce a minimal domain specific language
for dimensionally consistent programming in Idris. We formalize the
notions of dimension function, physical quantity, measurement and units
of measurement and the notion of dimensional (in)dependence which are at
the core of the Pi theorem.

In \cref{section:piexplained1}, we apply the DSL to formulate the
covariance principle (principle of relativity of measurements that is,
there is no privileged system of units of measurement) and the Pi
theorem in type theory.
%
Formulating the theorem in type theory is, on the one hand a benchmark
for the DSL from \cref{section:dsl1}. On the other hand it is a way to
understand the theorem and to make it more accessible to computer
scientists. Perhaps not surprisingly, implementing the theorem would
requires formulating at least some fragments of linear algebra. At the
end of \cref{section:piexplained1}, we sketch the proof of the theorem
given in Section 1 of \citet{barenblatt1996scaling} and discuss the role
of the covariance principle in the proof.

In \cref{section:generalization} we discuss possible extensions and
generalizations of the DSL from \cref{section:dsl1} and give a meaning
to the equations and physical laws from \cref{section:equations}, while
%
\cref{section:conclusions} wraps up and discusses links between
dimensional analysis and more general relativity principles.

\paragraph*{Related work.}

\ Dimensional analysis (DA) \citep{bridgman1922,
  barenblatt1996scaling, alma991010162519703414,jonsson2014dimensional},
is closely connected with the theory
of physical similarity: under which conditions is it possible to
translate observations made on a scaled model of a physical system to
the system itself. %, see \cref{section:pi}.
%
This is captured in the fundamental principles of invariance and
relativity (of units of measurement), as described by
\citet{arnold1989mathematical}.

The theory of physical similarity was formulated well before the advent
of digital computers and programming languages \citep{Buckingham1914,
  Buckingham1915, Rayleigh1915, bridgman1922} and its formalizations
have been rare \citep{quade1961, hassler1, hassler2}.
%
With the advent of digital computers and massive numerical
simulations, physical modeling has been almost completely replaced by
computer-based modeling, mainly for economic reasons, and the theory
of physical similarity and DA are not any longer an integral component
of the education of physicists, modelers and data analysts%
\footnote{But Bridgman's work has been republished in 2007 by
  Kessinger Publishing and in 2018 by Forgotten Books and DA still
  plays a crucial role in the analysis of computer-based models in climate
  science \citep{esd-11-281-2020, esd-12-63-2021, esd-13-879-2022,
    cp-2023-30}.}.

The notions of invariance (with respect to a group of transformations) and
relativity (e.g., of units of measurement) in physics are similar to the
notions of parametricity and polymorphism in computer science and thus
it is perhaps not surprising that, as programming languages have gained
more and more expressive power, mathematicians and computer scientists
have started ``rediscovering'' DA, see \citep{10.1145/263699.263761,
  dimension-models, 10.48550/arxiv.1107.4520}.
%
More recently the idea that dependent types can be applied to enforce
the dimensional consistency of expressions involving scalar physical
quantities has been generalized to expressions with vectors, tensors
and other derived quantities \citep{doi:10.1142/9789811242380_0020}.
%
We return to these papers with a bit more details in
\cref{subsection:relatedwork}.

Dependent types can certainly be applied to enforce the dimensional
consistency of expressions and libraries for annotating values of
standard types with dimensional information are available in most
programming languages \citep{units, dimensional, Unitful, pint, astropy,
  Boost.Units} and since quite some time \citep{10.1093/comjnl/26.4.366}.
% -- Haskell
% units: A domain-specific type system for dimensional analysis -- https://hackage.haskell.org/package/units
% dimensional: Statically checked physical dimensions -- https://hackage.haskell.org/package/dimensional
% -- Julia
% Unitful.jl: https://github.com/PainterQubits/Unitful.jl
% -- Python
% Pint: https://pint.readthedocs.io/en/stable/
% astropy: https://docs.astropy.org/en/stable/units/index.html
% -- C++
% Boost.Units: https://www.boost.org/doc/libs/1_77_0/doc/html/boost_units.html

But dependently typed languages can do more. The type checker of Idris,
for example, can effectively assist the implementation of verified
programs by interactively resolving the types of holes, suggesting
tactics and recommending type consistent implementations. Similar
support is available in other languages based on
intensional type theory.

A DSL built on top of a dependently typed language that supports
expressing Buckingham's Pi theorem should in principle be able to assist
the interactive implementation of dimensionally consistent programs.
%
It should support the programmer formulating the question of how a
function that computes a force, say |F|, may depend on arguments |m| and
|a| representing masses and accelerations, leverage on the type system
of the host language and automatically derive |F m a = alpha * m * a|.
%
\DONE{Add a constant factor? (that is what the Pi theorem would give)}
%
The work presented here is a first step in this direction. We are not
yet there and in \cref{section:conclusions} we discuss which steps are
left.
