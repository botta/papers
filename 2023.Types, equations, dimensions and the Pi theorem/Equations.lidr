% -*-Latex-*-

%{
%format D = "\Varid{D}"
%if False

> module Equations

> import Data.List
> import Data.List.Quantifiers

> import Syntax.PreorderReasoning

> %default total
> %auto_implicits off
> %access public export

> infix 6 <=>

> (<=>) : {A, B : Type} -> (A -> B) -> (A -> B) -> Type
> (<=>) {A} f g = (x : A) -> f x = g x

> infix 6 <-->

> (<-->) : (A : Type) -> (B : Type) -> Type
> (<-->) A B = (A -> B, B -> A)

> Real : Type
> Real = Double

> infixr 10 ^

< (^) : Real -> Nat -> Real
< x ^ Z      = 1
< x ^ (S n)  = x * (x^n)

> (^) : {A : Type} -> (A -> A) -> Nat -> (A -> A)
> x ^ Z      = id
> x ^ (S n)  = x . (x^n)

%endif

\section{Equations, physical laws and types}
\label{section:equations}

In the preface to the second edition of ``Programming from
specifications'' \citep{DBLP:books/daglib/0073499}, Carroll Morgan
starts with the observation that, in mathematics, $x^2 = 1$ is an
equation and that |x = 1| and |x = -1| are equations too.
%
He then goes on pointing out that, because of the relationships between
these three equations (the implications |x = 1| $\Rightarrow$ $x^2 = 1$ and
|x = -1| $\Rightarrow$ $x^2 = 1$) and because |x = 1| and |x = -1|
define the value of |x| ``without further calculation'', these two
equations are called \emph{solutions} of $x^2 = 1$.

Thus equations in mathematics sometimes represent \emph{problems}. In
dependently typed languages, these problems can be formulated explicitly
and the resulting expressions can be checked for consistency.
%
For example, in Idris \citep{idrisbook} one can specify the problem of
finding a real number $x$ whose square is $1$ as

< x      :  Real
< xSpec  :  x^2 = 1

\noindent
where

< (^) : Real -> Nat -> Real
< x ^ Z      = 1
< x ^ (S n)  = x * (x ^ n)

\noindent
It is worth pointing out that 1) it is a context that is \emph{not}
immediately deducible from $x^2 = 1$ that determines the meaning of the
equality sign in this equation and 2) that it is the type of |x| and
that of the ``to the power of 2'' function that make such context
clear.
%
For example, with |x : Nat| and |(^) : Nat -> Nat -> Nat| there is
just one solution, and in |Double|, |x^2 = 2| has no (exact) solutions.

In this paper we will often use equations between functions.
%
In such equations, we use extensional equality implemented as follow

%format <=> =  "\ee"

< (<=>) : {A, B : Type} -> (A -> B) -> (A -> B) -> Type
< (<=>) {A} f g = (x : A) -> f x = g x

%format <=> =  "\;\ee\;"
Because of the equivalence between logical propositions and types
\citep{DBLP:journals/cacm/Wadler15}, the type |f <=> g| means $\forall
x,$ |f x = g x| and values of this type are proofs of this equality.
%
While it would be possible to work with setoid equality, or homotopy
type theory, we make the pragmatic choice to use extensional equality
(we explored the consequences in
\citep{botta_brede_jansson_richter_2021}).

\subsection{Generic differential equations}
\label{subsection:de}

In mathematical physics but also in the social sciences and in modelling
it is common to specify problems implicitly in terms of equations.

Typically, such specifications are \emph{generic} and come in the form
of systems of differential equations. In this context, generic means
that the problem equations are given in terms of functions which are
not defined explicitly\footnote{Thus, one has a family of systems in a
  function parameter. The idea is then to study how properties of these
  systems, for example that of having stationary solutions, depend on
  properties of the parameter.}.
%
The focus is on the semantics and the syntax can be confusing. For
example, the ordinary differential Equation (1.4) at page 18 of
\citep{Kuznetsov:1998:EAB:289919}

\begin{equation}
\dot{x} = f(x)
\label{eq:ode0}
\end{equation}

\noindent
is said to define a continuous-time \emph{dynamical system} |(CalTime,
X, phi)|. In this context, |CalTime| is the \emph{time} set (a real
interval), |X| is the \emph{state space} of the system, |x| is a
function of type |CalTime -> X|, |dot x| (also of type |CalTime -> X|)
is the first derivative of |x|, and |f| is a function of type |X -> X|
smooth enough to grant existence and uniqueness of solutions. Thus,
\cref{eq:ode0} contains a type error. The twist here is that the
equation is just an abbreviation for the specification

< x      :  CalTime -> X
< xSpec  :  D x <=> f . x

\noindent
where we adopt the notation of \citet{JanssonIonescuBernardyDSLsofMathBook2022} and use |D x|
to denote the derivative of |x|. When not otherwise stated, |D x| has
the type of |x|. When quoting textbooks verbatim, we also denote |D x|
by |dot x| (and |D (D x)| by |ddot x|), $dx/dt$ or similar.
%
The third element of the dynamical system associated to
\cref{eq:ode0}, |phi|, is a function of type |CalTime -> X -> X|. The idea
is that |phi t x_0| is the value of \emph{the} solution
of \cref{eq:ode0} for initial condition |x_0| at time |t|. Thus |phi|
does depend on |f| although this is not immediately visible from its
type.
%
\DONE{Perhaps not important here, but our intro would suggest that a
  solution would be a value of the same type as that of |x|, thus
  |CalTime -> X|.  Then with the initial condition |x 0 = x_0| for a
  parameter |x_0| we get a family of unique solutions |psi : X ->
  CalTime -> X|. This is |flip phi|, but I would find the type of
  |psi| more natural then that of |phi|.}

In mathematical physics, it is very common to use the same notation
for function application and function composition as in
\cref{eq:ode0}. For example, Newton's principle of
determinacy\footnote{Newton's principle of determinacy maintains that
  the initial state of a mechanical systems (the positions and the
  velocities of its points at an initial time) uniquely determines its
  motion, see \citep{arnold1989mathematical}, page 4} is formalized in
Equation (1) of \citep{arnold1989mathematical} as

\begin{equation}
\ddot{\mathbf{x}} = F(\mathbf{x}, \dot{\mathbf{x}}, t)
\label{eq:newton0}
\end{equation}

\noindent
where
$\mathbf{x}, \dot{\mathbf{x}}, \ddot{\mathbf{x}} : \CalTime \to
\Real^N$ and $F : (\Real^N, \Real^N, \CalTime) \to \Real^N$. Again,
the equation is to be understood as an abbreviation for the
composition between $F$ and a suitably curried function (taking $t$ to
$(\mathbf{x}(t), \dot{\mathbf{x}}(t), t)$).
%
The function $F$ only has access to a state triple at the current
instant in time, not to the full time evolution of the state.

Keeping in mind that $f(x)$ often just means |f . x| and with a little
bit of knowledge of the specific domain, it is often not difficult to
understand the problem that equations represent. For example, in
bifurcation theory, a slightly more general form of \cref{eq:ode0}

\begin{equation}
\dot{x} = f(x, p)
\label{eq:ode3}
\end{equation}

\noindent
with parameter |p : P| and |f : (X, P) -> X| (again, with some abuse of
notation) is often put forward to discuss three different but closely
related problems:

\begin{itemize}

\item The problem of predicting how the system evolves in time from an
  initial condition |x_0 : X| and for a given |p : P|.

\item The problem of finding \emph{stationary} points that is, values of
  |x : X| such that \(f(x,p) = 0\) for a given |p : P| and to study
  their \emph{stability}.

\item The problem of finding \emph{critical} parameters that is, values
  of |p : P| at which the set of stationary points $\{x \mid x : X,
  f(x, p) = 0\}$ (or a more general \emph{invariant} set associated with
  $f$) exhibits structural changes\footnote{In the context of climate
    research, such critical values are often called ``tipping
    points''.}.

\end{itemize}

\noindent
Mathematical models of physical systems often involve \emph{functional}
equations and systems of partial differential equations.
%
Understanding the problems associated with these equations from a FP
perspective requires some more domain-specific expertise.
%
But even these more advanced problems can often be understood by
applying ``type-driven'' analysis, see for example the discussion of the
Lagrangian in Chapter 3 ``Types in Mathematics'' of
\citep{JanssonIonescuBernardyDSLsofMathBook2022}.

\subsection{Physical laws, specific models}
\label{subsection:laws}

Perhaps surprisingly, understanding basic physical principles but also
mathematical models of specific systems in terms of well typed
expressions is often tricky.
%
\DONE{More difficult than what?}
%
Consider, for example, Newton's second law as often stated in
elementary textbooks

\begin{equation}
F = m a
\label{eq:newton1}
\end{equation}

\noindent
or the relationship between pressure, density and temperature in an
ideal gas

\begin{equation}
p = \rho R T
\label{eq:gas0}
\end{equation}

\noindent
In these equations |F| denotes a \emph{force}, |m| a \emph{mass}, |a| an
\emph{acceleration}, |p| a \emph{pressure}, |rho| a \emph{density}, |T|
a \emph{temperature} and |R| a gas-specific constant. But what are the
types of these quantities? What kind of equalities do these types imply?

In climate science, ``conceptual'' models of specific components of the
climate system often consist of simple, low-dimensional systems of
ordinary differential equations.
%
%format Te = "T_e"
%format Se = "S_e"
%
For example, Stommel's seminal 1961 paper starts by describing a simple
system consisting of a vessel of water with ``temperature |T| and
salinity |S| (in general variable in time) separated by porous walls
from an outside vessel whose temperature |Te| and salinity |Se| are
maintained at constant values'', see Fig. I at page 1 of
\citep{doi:10.3402/tellusa.v13i2.9491}.

The evolution of the temperature and of the salinity in the vessel are
then modeled by two uncoupled linear differential equations:

%% \begin{equation}
%% \begin{array}{lcl}
%%   \frac{dT}{dt} & = & c (T_e - T) \\
%%   \frac{dS}{dt} & = & d (S_e - S)
%% \end{array}
%% \label{eq:stommel0}
%% \end{equation}

\begin{equation}
  \frac{dT}{dt} = c (T_e - T) \quad \mathrm{and} \quad \frac{dS}{dt} = d (S_e - S)
\label{eq:stommel0}
\end{equation}

\noindent
In this context, |T| and |S| represent functions of type |Real -> Real|
and |Te|, |Se|, the ``temperature transfer coefficient'' |c|, and the
``salinity transfer coefficient'' |d| are real numbers.
%
%% \footnote{This is not the celebrated Stommel model of thermohaline
%% convection. The latter is a more interesting non-linear coupled system
%% which is introduced at page 2 of the same paper. For the purpose of our
%% discussion, however, \cref{eq:stommel0} will do.}
%
We can formulate the problem of computing solutions of
\cref{eq:stommel0} through the specification:

%if False

> c     :  Real
> d     :  Real
> Te    :  Real
> Se    :  Real
> T     :  Real -> Real
> S     :  Real -> Real
> D     :  (Real -> Real) -> (Real -> Real)

> infixr 10 $
> ($) : {A, B : Type} -> (A->B) -> A -> B
> f$x = f x

%endif
%An "invisible application operator"
%format $ = " "
%format => = "\to"

> TSpec  :  D T  <=> \ t => c * (Te - T t); {-"\qquad"-} SSpec  :  D S  <=> \ t => d * (Se - S t)

The $\lambda$-expression in |TSpec| denotes the function that maps |t|
to |c * (Te - T t)|. Thus (again, because of the equivalence between
types and logical propositions) any (total) \emph{definition} of |TSpec|
is (equivalent to) a proof that, $\forall t, \ dT(t)/dt = c (T_e -
T(t))$ and its \emph{declaration} specifies the task\footnote{\emph{Aufgabe},
\citep{Kolmogoroff1932}.} of providing one such proof.
%

The next step at the end of page 1 of Stommel's paper is to make
\cref{eq:stommel0} ``non-dimensional'' by introducing

\begin{equation}
  \tau = c \ t, \quad \delta = \frac{d}{c}, \quad y = T/T_e, \quad x = S/S_e
\label{eq:stommel1}
\end{equation}

\noindent
This yields

%% \begin{equation}
%% \begin{array}{lcl}
%%   \frac{dy}{d\tau} & = & 1 - y \\
%%   \frac{dx}{d\tau} & = & \delta * (1 - x)
%% \end{array}
%% \label{eq:stommel2}
%% \end{equation}

\begin{equation}
  \frac{dy}{d\tau} = 1 - y \quad \mathrm{and} \quad \frac{dx}{d\tau} = \delta (1 - x)
\label{eq:stommel2}
\end{equation}

\noindent
at the top of page 2. From there, Stommel then goes on discussing how a
\emph{density} of the vessel (a function of its temperature and salinity
and, thus, of |x| and |y|) evolves in time as the solution of
\cref{eq:stommel2} for arbitrary initial conditions |x_0|, |y_0|

%% \begin{equation}
%% \begin{array}{lcl}
%%   y(\tau) & = & 1 + (y_0 - 1)e^{-\tau} \\
%%   x(\tau) & = & 1 + (x_0 - 1)e^{-\delta\tau}
%% \end{array}
%% \label{eq:stommel3}
%% \end{equation}

\begin{equation}
  y(\tau) = 1 + (y_0 - 1)e^{-\tau} \quad \mathrm{and} \quad x(\tau) = 1 + (x_0 - 1)e^{-\delta\tau}
\label{eq:stommel3}
\end{equation}

\noindent
approaches 1 as |tau| increases. Although Stommel's discussion
is in many ways interesting, we do not need to be concerned with it
here.
%
What we need to understand, however, are the types of |tau|, |delta|,
|x| and |y| and how \cref{eq:stommel2} can be derived from
\cref{eq:stommel0} given \cref{eq:stommel1}. The first two equations of
\cref{eq:stommel1} are definitions of |tau| and |delta|:

> tau : Real -> Real;    {-"\qquad "-}      delta  :  Real
> tau t  =  c * t;                          delta  =  d / c

\DONE{It may be helpful to use different type names such that we get |tau : TTime -> TauTime|. (A bit unfortunate that Tau is now the type of t instead of the type of tau.)}%
However, the last two equations are not \emph{explicit} definitions of
|y| and |x|. Instead, they are \emph{implicit} definitions,
corresponding to the specifications
\DONE{I think the type of y should be |TauTime -> Real|, not |TTime -> Real|. Only the composition |y . tau| has type |TTime -> Real|.}

> y      :  Real -> Real;                                 x      :  Real -> Real
> ySpec  :  y . tau <=> \ t => T t / Te;  {-"\qquad "-}   xSpec  :  x . tau <=> \ t => S t / Se

and it is easy to see that the definitions
\DONE{Then it would be useful to have |tau| as a variable name here. We need to think about what that would mean for the name of the conversion function (called |tau| above) which clashes. Perhaps |norm| for normalize? or scale? or conv? Or live with the clash?}

> y sigma = T (sigma / c) / Te;    {-"\qquad "-}       x sigma = S (sigma / c) / Se

fulfil |ySpec| and |xSpec|:

%if False

> whatnow  :  {t : Real} -> T (c * t / c) / Te  =  T t / Te

> zuzu : {t : Real} -> c * t / c =  t

%endif

> ySpec t  =  ( (y . tau) t ) ={ Refl }= ( T (c * t / c) / Te ) ={ whatnow }= ( T t / Te ) QED

and similarly for |xSpec|. Filling the |whatnow| hole in the last step
requires invoking congruence and a proof of |c * t / c = t|. Here, the
types of |c| and |t| are aliases for double precision floating point
numbers for which such an equality needs to be postulated. The
differential equations \cref{eq:stommel2} for |y| and |x| follow then
from the definitions of |tau| and |delta|, from the specifications |TSpec|, |ySpec|, |SSpec|, |xSpec| and from the
rules for derivation. Informally:

< y . tau <=> \ t => T t / Te
<
<   ⇒ -- congruence
<
< D (y . tau) <=> D (\ t => T t / Te)
<
<   = -- D-composition, \dots
<
< \ t => (D y) (tau t) * (D tau) t <=> \ t => (D T) t / Te
<
<   = -- |(D tau) t = (const c) t = c|, |TSpec|
<
< \ t => (D y) (tau t) * c <=> \ t => c * (Te - T t) / Te
<
<   = -- arithmetics
<
< \ t => (D y) (tau t) <=> \ t => 1 - T (c * t / c) / Te
<
<   = -- |ySpec|, def. composition
<
< \ t => ((D y) . tau) t <=> \ t => 1 - y (tau t)
<
<   = -- |beta|-reduction
<
< (D y) . tau <=> \ t => 1 - (y . tau) t
<
<   = -- factoring on the RHS
<
< (D y) . tau <=> (\ sigma => 1 - y sigma) . tau
<
<   = -- cancel the composition on both sides (|tau| is invertible)
<
< D y <=> \ sigma => 1 - y sigma

\DONE{I think two steps are missing:}
\DONE{Possibly a last step with 1 standing for const 1}

\noindent
We can turn this into a valid Idris proof but the point here is that the
computation looks significantly more contrived than the straightforward
(again, informal) derivation

< y = T / Te
<
<   ⇒ -- chain rule
<
< frac dy dtau ! frac dtau dt = frac dT dt ! frac 1 Te
<
<   = -- def. of |tau|, \cref{eq:stommel2}
<
< frac dy dtau ! c = c (Te - T) / Te
<
<   = -- def. of |y|
<
< frac dy dtau = 1 - y

that is taken for granted and not even mentioned in Stommel's
paper. This complication is a consequence of having insisted on explicit
types and on consistent typing rules in a language that lacks important
abstractions. Namely, the rules for lifting the multiplication
(division, addition, subtraction, etc.) between real numbers to the
multiplication between real numbers and functions (and between functions
and real numbers) with matching co-domain.

Such complication may partly explain why modelers and mathematicians
have not found much added value in dependent types and functional
languages: even for derivations as simple as those of the Stommel model,
these languages add a formal layer that seems to unnecessarily obfuscate
computations that are nearly self-evident.

It is not difficult to make the Idris (or Agda, Coq, etc.) type checker
digest definitions like \cref{eq:stommel1} and simplify derivations like
those of |ySpec1| and |xSpec1| but, unfortunately, there are more bad
news: the ``dimensionality'' of the expressions involved in Stommel's
derivation are not visible in their types!
%
Stommel shows that by ``making the equations non-dimensional'' the
number of parameters of \cref{eq:stommel0} has been reduced to just one:
the ``non-dimensional'' ratio |delta|.

This is an important result. It implies that the solution of
\cref{eq:stommel2} for a given value of |delta| allows one to recover a
whole family of solutions of \cref{eq:stommel0}, namely, all those
corresponding to values of |c| and |d| such that $d / c = \delta$.
%
Or, the other way round, it shows that the problem defined by
\cref{eq:stommel0} is \emph{inflated}.

In mathematical modeling but also in data analysis, inflated problems
are ubiquitous. Recognizing that the solution of a problem can be
reduced to the solution of a much simpler problem is often crucial to
avoid the ``curse of dimensionality'', e.g. when solving large systems
of partial differential equations or in training deep neural networks.

But how can one recognize that a problem is inflated? What does it mean
to say that |delta| is \emph{non-dimensional}? And why are
\cref{eq:stommel0} dimensional and \cref{eq:stommel2}
non-dimensional? The types of the expressions involved do not
provide us with any clue.

Like in the case of Newton's second law and of the equation for ideal
gases, we have to do with a \emph{grammar} of properties (being a force,
being a mass, being non-dimensional) that we do not grasp.
%
As a consequence, our types are not well suited to that grammar: |T| and
|y|, |S| and |x|, |m| and |rho| have different properties and these
properties are important in the domain of discourse. Yet, they all have
the same types!
%
\DONE{In a natural (DSLsofMath-inspired) ``encoding'' of the problem, we would have separate types for t, T, S, and then let the ``non-dimensional'' versions by just REALs. But that is perhaps the point here. It is a bit hidden behind other notational differences, though.}

If we want to take mathematical physics and modeling seriously (and if
we want experts in these domains to take FP seriously) we have to encode
that grammar through suitable types.
%
We discuss a minimal DSL of dimensional quantities in
\cref{section:dsl1}. Before getting there, however, we need to build a
better understanding of the questions raised in this section. In the
next one, we start with the question of what it means for a physical
quantity to \emph{have a dimension}.

%if False

\begin{PLAN}

We introduce equations (algebraic, differential, etc. following Carroll
Morgan preface to ``Programming from specifications'') as problem
specifications. Then we introduce physical laws like

  \[F = ma\]

We explain how such laws are instantiated, and give as an example the
derivation of the second order ODE for the oscillations of a pendulum
(we also give its solution and the period of oscillation).  We also
introduce the ideal gas law,

  \[p = \rho R T\]

give an example of its instantiation (like in the Euler equations) and
then we start asking what kind of equality is meant in these equations
and what could possibly be suitable types for $f$, $m$ and $a$:

< F, m, a : Real ?

< F, m, a : Type ?

We notice that it is not completely obvious how to answer this question
(thus perhaps it is not surprising that types from FP are not very
interesting for physicists!)

We formulate Newton's second principle and the ideal gas law as axioms
and ask what are the types of the implicit arguments:

< Axiom2 : (F : Force, m : Mass, a : Acceleration) => F = m * a ?

We start reasoning that |Mass| and |Acceleration| must be refinements of
some |Multiplyable| two arguments base class and work our way (through
more examples, e.g. of possible implementation of |Mass|) toward a (for
the time being) halfway satisfactory type-based (or type-driven)
understanding of physical laws as postulates about equations.

We announce that in the next section we will disappoint the reader and
argue that this understanding is wrong (or perhaps just incomplete).

\end{PLAN}

%endif
%}
