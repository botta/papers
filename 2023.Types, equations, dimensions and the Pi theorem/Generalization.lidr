% -*-Latex-*-

%if False

\begin{code}
module Generalization

import Data.Vect
import Data.Primitives.Views
import Syntax.PreorderReasoning

import DSL1
import PiExplained1

%default total
%auto_implicits off --on
%access public export


infixl 9 `Times`, `LTE'.Times`, `Over`, `LTE'.Over`
infix 6 <=>

(<=>) : {A, B : Type} -> (A -> B) -> (A -> B) -> Type
(<=>) {A} f g = (x : A) -> f x = g x

IsCommutative : {X : Type} -> (X -> X -> X) -> Type
IsCommutative {X} op = {x1, x2 : X} -> x1 `op` x2 = x2 `op` x1

IsAssociative : {X : Type} -> (X -> X -> X) -> Type
IsAssociative {X} op = {x1, x2, x3 : X} -> (x1 `op` x2) `op` x3 = x1 `op` (x2 `op` x3)

IsLeftIdentity : {X : Type} -> (X -> X -> X) -> X -> Type
IsLeftIdentity {X} op e = {x : X} -> e `op` x = x

IsRightIdentity : {X : Type} -> (X -> X -> X) -> X -> Type
IsRightIdentity {X} op e = (x : X) -> x `op` e = x

IsIdentity : {X : Type} -> (X -> X -> X) -> X -> Type
IsIdentity op e = (IsLeftIdentity op e, IsRightIdentity op e)

IdentityExists : {X : Type} -> (X -> X -> X) -> Type
IdentityExists {X} op = Exists X (IsIdentity op)

IdentityIsUnique : {X : Type} -> (X -> X -> X) -> Type
IdentityIsUnique {X} op = (e1, e2 : X) -> IsIdentity op e1 -> IsIdentity op e2 -> e1 = e2

IsLeftInverseElem : {X : Type} -> (X -> X -> X) -> X -> X -> Type
IsLeftInverseElem op x invx = DPair (IdentityExists op) (\ (Evidence e p) => invx `op` x = e)

IsRightInverseElem : {X : Type} -> (X -> X -> X) -> X -> X -> Type
IsRightInverseElem op x invx = DPair (IdentityExists op) (\ (Evidence e p) => x `op` invx = e)

IsInverseElem : {X : Type} -> (X -> X -> X) -> X -> X -> Type
IsInverseElem op x invx = (IsLeftInverseElem op x invx, IsRightInverseElem op x invx)

IsInverse : {X : Type} -> (X -> X -> X) -> (X -> X) -> Type
IsInverse {X} op inv = (x : X) -> IsInverseElem op x (inv x)

InverseExists : {X : Type} -> (X -> X -> X) -> Type
InverseExists {X} op = Exists (X -> X) (IsInverse op)

InverseIsUnique : {X : Type} -> (X -> X -> X) -> Type
InverseIsUnique {X} op = (inv1, inv2 : X -> X) -> IsInverse op inv1 -> IsInverse op inv2 -> inv1 <=> inv2

typeOf : {T : Type} -> T -> Type
typeOf {T} _ = T

Domain : {A, B : Type} -> (A -> B) -> Type
Domain {A} _ = A

Codomain : {A, B : Type} -> (A -> B) -> Type
Codomain {B} _ = B
\end{code}

%endif

\section{Possible generalizations and extension}
\label{section:generalization}

We discuss possible generalizations and extensions of the DSL for for
dimensionally consistent programming presented in \cref{section:dsl1}.
%
Some of these extensions (\cref{subsection:fun}) can be implemented
straightforwardly, some (\cref{subsection:dpq}) come with substantial
disadvantages and are probably not worth being pursued. Other extensions
(\cref{subsection:adv}) go beyond the scope of this paper.

\subsection{Dimensions and physical quantities}
\label{subsection:dpq}

In \cref{section:dsl1} we have introduced the concrete data types |D|
and |Q| and encoded the notions of dimensions and of physical quantities
in the domain of mechanics and for the |LTE| (lengths, times and masses)
class of units of measurement.
%
This has allowed us to formalize basic notions of DA (among others,
Buckingham's Pi theorem) in type theory and to apply dependent types to
ensure the dimensional consistency of expressions involving physical
quantities.
%
In doing so, we have exploited a number of properties that values of
type |D| fulfilled by definition. Most prominently, that equality of
dimensions is decidable. In \cref{subsection:dind} we also suggested
that |D| together with the binary operation |Times| form a group.
%
\paragraph*{The algebraic structure of dimensions.} \ It seems natural
to generalize the approach of \cref{section:dsl1} by putting forward the
algebraic structure of dimensions. As we will see, this has both
advantages and disadvantages. Again, we first discuss the generalization
in the domain of mechanics and for the |LTE| class of units of
measurement.

As done in \citep{botta_brede_jansson_richter_2021} for the notions of
functor and monad, we discuss the operations required for a type to be a
dimension as well as their laws through an Idris type class.
%
Encoding the algebraic structure of dimensions through type classes and
attempting generic implementations of |df| and of dimensional judgments
requires introducing a few language-specific details but is a well
established method for discovering the potential drawbacks of more
abstract approaches than the one proposed in \cref{section:dsl1}.
%
In Idris, type classes are introduced through the |interface|
keyword. For example

< interface DecEq t where
<   decEq : (x1 : t) -> (x2 : t) -> Dec (x1 = x2)

explains what it means for a type |t| to be in |DecEq|, the class of
types for which propositional equality is decidable. The data
constructor |Dec| in the definition of |DecEq| is defined as

< data Dec : Type -> Type where
<   Yes  :  (prf : prop) -> Dec prop
<   No   :  (contra : prop -> Void) -> Dec prop

A value of type |Dec prop| can only be constructed in two ways: either
by providing a proof of |prop| (a value of type |prop|) or by providing
a proof of |Not prop| (a function that maps values of type |prop| to
values of the empty type, that is, a contradiction). Thus, a value of
type |Dec (x1 = x2)| is either a proof of |x1 = x2| or a proof of |Not
(x1 = x2)| which is what it means for the equality to be decidable.

We can explain what it means for a type |D| to encode the notion of
dimension through a |Dim| interface. As discussed in
\cref{subsection:df}, we need dimensional judgments or, more precisely,
equality in |D|, to be decidable.
%% \REMARK{Tim}{Maybe explain somewhere that ``dimensional judgments'' are
%% in fact about equality in |D|?}.
This can be expressed by introducing |Dim| as a \emph{refinement} of
|DecEq|:
%
%if False
\begin{code}
namespace Mechanics'

  namespace LTM'
\end{code}
%endif
%format => = "\Rightarrow"
\savecolumns
\begin{code}
    interface DecEq D => Dimension D where
\end{code}
%
Perhaps confusingly, this says that |Dimension D| implies |DecEq D| or, in
other words, that being in |DecEq| is a necessary condition for being in
|Dimension|.
%
This condition is certainly not sufficient. We have seen in
\cref{subsection:df} that, as a minimum, we need to be able to define
dimensionless physical quantities and the 3 fundamental dimensions of
the |LTE| class:
\restorecolumns
\begin{code}
      DimLess : D; {-"\quad"-} Length : D; {-"\quad"-} Time : D; {-"\quad"-} Mass : D
\end{code}
Further, we need the |Times| and |Over| combinators
\restorecolumns
\begin{code}
      Times    :  D -> D -> D
      Over     :  D -> D -> D
\end{code}
%if False

<       df       :  D -> (RealPlus3 -> RealPlus)

\begin{code}
      ds       :  D -> Vect 3 Integer
\end{code}

%endif
%
It is time to put forward some axioms. In \cref{subsection:dind} we
mentioned that |d `Over` d| has to be equal to |DimLess| (for any |d :
D|) and that |D| is a group. The idea is that |D| together with the
|Times| operation is the free Abelian group generated by the
fundamental dimensions (which are also required to be not equal).
Thus, writing |(*)| for |Times|, |(/)| for |Over|, and |1| for
|DimLess| we have
%
\DONE{I think these are easier to read with 0 for DimLess, * for `Times`,
  etc. Here is one way of doing that.}
%{
%format DimLess = "1"
%format `Times` = *
%format `Over` = "/"
%if False

<       isCommutativeTimes    :  IsCommutative Times
<       isAssociativeTimes    :  IsAssociative Times
<       isIdentityDimLess     :  IsIdentity Times DimLess
<       identityIsUnique      :  IdentityIsUnique Times
<       isInverseDimLessOver  :  IsInverse Times (DimLess `Over`)
<       inverseIsUnique       :  InverseIsUnique Times

%endif

\begin{code}
      isCommutativeTimes           :  {d1, d2 : D} -> d1 `Times` d2 = d2 `Times` d1

      isAssociativeTimes           :  {d1, d2, d3 : D} -> (d1 `Times` d2) `Times` d3 = d1 `Times` (d2 `Times` d3)

      isLeftIdentityDimLess        :  {d : D} -> DimLess `Times` d = d

      isRightIdentityDimLess       :  {d : D} -> d `Times` DimLess = d

      isLeftInverseDimLessOver     :  {d : D} -> (DimLess `Over` d) `Times` d = DimLess

      isRightInverseDimLessOver    :  {d : D} -> d `Times` (DimLess `Over` d) = DimLess
\end{code}


\noindent
In order to derive |d `Over` d = DimLess| one also needs
|Times| to associate with |Over| \citep{gibbonsPhD1991}:

\begin{code}
      noPrec : {d1, d2, d3 : D} -> (d1 `Times` d2) `Over` d3 = d1 `Times` (d2 `Over` d3)
\end{code}

%}
\DONE{We may even use this shorter notation further down.}%
%{
%format DimLess = "1"
%format `Times` = "*"
%format `Over` = "/"
%if False

\begin{code}
    dodIsDimLess  :  {D : Type} -> Dimension D => {d : D} -> d `LTM'.Over` d = LTM'.DimLess
    dodIsDimLess {d}  =  let P1 = \ X => d `LTM'.Over` d = X `LTM'.Over` d in
                         ( d `LTM'.Over` d )
                      ={ replace {P = P1} (sym isRightIdentityDimLess) Refl }=
                         ( (d `LTM'.Times` DimLess) `LTM'.Over` d )
                      ={ noPrec {d1 = d} {d2 = DimLess} {d3 = d} }=
                         ( d `LTM'.Times` (DimLess `LTM'.Over` d) )
                      ={ isRightInverseDimLessOver }=
                         ( LTM'.DimLess )
                      QED
\end{code}

%endif
%}

\noindent
With |DimLess|, |Times| and |Over| one can implement the functions |Pow|
and |ProdPows| from \cref{subsection:dind} generically

%{
%format LTM'.DimLess = "1"
%format `LTM'.Times` = *
%format `LTM'.Over` = "/"
\begin{code}
    Pow      :  {D : Type} -> Dimension D => D -> Integer -> D
    Pow d n  =  pow d (integerRec n) where
      pow : {n : Integer} -> D -> IntegerRec n -> D
      pow d       IntegerZ   =  LTM'.DimLess
      pow d (IntegerSucc m)  =  pow d m `LTM'.Times` d
      pow d (IntegerPred m)  =  pow d m  `LTM'.Over` d

    ProdPows : {n : Nat} -> {D : Type} -> Dimension D => Vect n D -> Vect n Integer -> D
    ProdPows      Nil       Nil   =  LTM'.DimLess
    ProdPows (d :: ds) (p :: ps)  =  LTM'.Pow d p `LTM'.Times` LTM'.ProdPows ds ps
\end{code}
%}
%
and define derived dimensions as we did in \cref{subsection:df}:
%
%{
%format DimLess = "1"
%format `Times` = *
%format `Over` = "/"
\begin{code}
    Velocity : {D : Type} -> Dimension D => D
    Velocity = Length `Over` Time

    Acceleration : {D : Type} -> Dimension D => D
    Acceleration = Velocity `Over` Time

    Force : {D : Type} -> Dimension D => D
    Force = Mass `Times` Acceleration

    Energy : {D : Type} -> Dimension D => D
    Energy = Mass `Times` (Velocity `Times` Velocity)

    Work : {D : Type} -> Dimension D => D
    Work = Force `Times` Length
\end{code}
%}

\noindent
Notice, however, that the type of |Velocity|, |Acceleration|, etc. is
generic rather than specific. As a consequence, proving elementary
dimensional equalities requires some more work, as one would expect. For
example, in \cref{subsection:df}, we could assess the equivalence
between energy and mechanical work simply by

< check1 : Energy = Work
< check1 = Refl

because the type of |Energy| and |Work| was fully \emph{defined}. A
similar proof based on the \emph{specification} |Dimension D| would look like
\DONE{Hard to read with the implicit type arguments to (=).}

%{
%format LTM'.DimLess = "1"
%format `LTM'.Times` = *
%format `LTM'.Over` = "/"
%if False

\begin{code}
    whatnow  :  {D : Type} -> Dimension D =>
                (=) {A = D} {B = D}
                (LTM'.Mass `LTM'.Times` ((LTM'.Length `LTM'.Over` LTM'.Time) `LTM'.Times` (LTM'.Length `LTM'.Over` LTM'.Time)))
                ((LTM'.Mass `LTM'.Times` ((LTM'.Length `LTM'.Over` LTM'.Time) `LTM'.Over` LTM'.Time)) `LTM'.Times` LTM'.Length)
    whatnow {D} =  let e : D  =  LTM'.DimLess in
                   let L : D  =  LTM'.Length in
                   let T : D  =  LTM'.Time in
                   let M : D  =  LTM'.Mass in
                   let times : (D -> D -> D) = LTM'.Times in
                   let over  : (D -> D -> D) = LTM'.Over in
                   let P1 = \ X => M `times` ((L `over` T) `times` (L `over` T)) =
                                   M `times` ((L `over` T) `times` (X `over` T)) in
                   let P2 = \ X => M `times` ((L `over` T) `times` ((L `times` e) `over` T)) =
                                   M `times` ((L `over` T) `times` X) in
                   let P3 = \ X => M `times` ((L `over` T) `times` (L `times` (e `over` T))) =
                                   M `times` ((L `over` T) `times` X) in
                   let P4 = \ X => M `times` ((L `over` T) `times` ((e `over` T) `times` L)) =
                                   M `times` X in
                   let P5 = \ X => M `times` (((L `over` T) `times` (e `over` T)) `times` L) =
                                   M `times` (X `times` L) in
                   let P6 = \ X => M `times` ((((L `over` T) `times` e) `over` T) `times` L) =
                                   M `times` ((X `over` T) `times` L) in
                   ( M `times` ((L `over` T) `times` (L `over` T)) )
                ={ replace {P = P1} (sym isRightIdentityDimLess) Refl }=
                   ( M `times` ((L `over` T) `times` ((L `times` e) `over` T)) )
                ={ replace {P = P2} noPrec Refl }=
                   ( M `times` ((L `over` T) `times` (L `times` (e `over` T))) )
                ={ replace {P = P3} isCommutativeTimes Refl }=
                   ( M `times` ((L `over` T) `times` ((e `over` T) `times` L)) )
                ={ replace {P = P4} (sym isAssociativeTimes) Refl }=
                   ( M `times` (((L `over` T) `times` (e `over` T)) `times` L) )
                ={ replace {P = P5} (sym noPrec) Refl }=
                   ( M `times` ((((L `over` T) `times` e) `over` T) `times` L) )
                ={ replace {P = P6} isRightIdentityDimLess Refl }=
                   ( M `times` (((L `over` T) `over` T) `times` L) )
                ={ sym isAssociativeTimes }=
                   ( (M `times` ((L `over` T) `over` T)) `times` L )
                QED
\end{code}

%endif
%}

%{
%format LTM'.DimLess = "1"
%format `LTM'.Times` = *
%format `LTM'.Over` = "/"
\begin{code}
    check1 : {D : Type} -> Dimension D => (=) {A = D} {B = D} LTM'.Energy LTM'.Work
    check1  =                          ( LTM'.Energy )
            ={  Refl }= {-"\quad"-}    ( LTM'.Mass `LTM'.Times` (LTM'.Velocity `LTM'.Times` LTM'.Velocity) )
            ={  Refl }=                ( LTM'.Mass `LTM'.Times` ((LTM'.Length `LTM'.Over` LTM'.Time) `LTM'.Times` (LTM'.Length `LTM'.Over` LTM'.Time)) )
            ={  whatnow }=             ( (LTM'.Mass `LTM'.Times` ((LTM'.Length `LTM'.Over` LTM'.Time) `LTM'.Over` LTM'.Time)) `LTM'.Times` LTM'.Length )
            ={  Refl }=                ( (LTM'.Mass `LTM'.Times` (LTM'.Velocity `LTM'.Over` LTM'.Time)) `LTM'.Times` LTM'.Length )
            ={  Refl }=                ( (LTM'.Mass `LTM'.Times` LTM'.Acceleration) `LTM'.Times` LTM'.Length )
            ={  Refl }=                ( LTM'.Force `LTM'.Times` LTM'.Length )
            ={  Refl }=                ( LTM'.Work )
            QED
\end{code}
%}
%
We can omit all |Refl| steps, which are only there to guide the reader
(not the type checker) and perhaps make the type of |check1| more
readable.  However, filling in the |whatnow| hole and completing the
proof requires invoking the axioms of |Dimension|, see the literate Idris code
that generates this document at
\url{https://gitlab.pik-potsdam.de/botta/papers}. Alas, we know that
implementing generic proofs can be awkward! \DONE{Add link to public
repo and name 'DSL.lidr'.}

Thus, a DSL for dimensionally consistent programming that does not rely
on a concrete representation of |D| like the one put forward in
\cref{section:dsl1}, would have to provide a library of proofs of
elementary equalities like the one between energy and work. Perhaps more
importantly, it would also have to provide proofs of elementary
inequalities, for example that |Not (Force = Energy)|. In
\cref{subsection:df}, we could assess this inequality by

< check2 : Not (Force = Energy)
< check2 Refl impossible

Implementing a generic proof on the only basis that the type of |Force|
is equal to the type of |Energy| and that such type is in |Dimension| would
not be as easy. As a minimum, it would require extending the
|Dimension| interface with axioms that guarantee that the generators are not
equal.

Beside providing the basic grammar of the |D|-language, a data type in
|Dimension| also needs to provide a dimension function.
%
There are (at least) two ways of encoding this requirement. One
is to require |Dimension| to be equipped with a dimension function method

<       df       :  D -> (RealPlus3 -> RealPlus)

that fulfils the specifications (\ref{eq:fun0}) and (\ref{eq:fun1}):

<       dfSpec1  :  {d : D} -> df d [1.0, 1.0, 1.0] = 1.0
<
<       dfSpec2  :  {d : D} -> {L, L', T, T', M, M' : RealPlus} ->
<                   df d [L / L', T / T', M / M'] = df d [L, T, M] / df d [L', T', M']

In \cref{subsection:quantities} we have seen that our dimension function
indeed fulfilled these requirements up to floating point accuracy.
%
\DONE{No, it is not clear. Explanation needed.}
%
But, with |dfSpec1| and |dfSpec2|, implementing |Dimension| would have to rely
on non-implementable assumptions (if |RealPlus| is just an alias for
floating-point numbers) or on a formalization of real numbers. One way
to circumvent this difficulty would be to restrict the type of |df d| to
|RationalPlus3 -> RationalPlus|. This is awkward and conceptually
unsatisfactory.

Another way of making |Dimension| support the definition of a dimension
function would to require it to provide the integer exponents of the
dimension function of \cref{eq:fun2}:

<       ds       :  D -> Vect 3 Integer

One could then define the dimension function associated with a |D| type
in |Dimension| on the basis of such exponents, as done in
\cref{subsection:quantities}. For example
%
\begin{code}
    df : {D : Type} -> Dimension D => D -> Vect 3 RealPlus -> RealPlus
    df d ls = foldr (*) 1.0 (zipWith pow ls rds) where
      rds : Vect 3 Real
      rds = map fromInteger (ds d)
\end{code}
%
The discussion above suggests that a DSL for DA and dimensionally
consistent programming should be based on a concrete implementation of
|D| like the one discussed in \cref{subsection:df}.
%if False

1) type checking this requires to set |auto_implicits| to |on|, line 12

2) type variables have to be small-cap to type check

< interface Group g where
<   op               :  g -> g -> g
<   e                :  g
<   inv              :  g -> g
<   opIsAssociative  :  IsAssociative op
<   eIsIdentity      :  IsIdentity op e
<   eIsUnique        :  IdentityIsUnique op
<   invIsInverse     :  IsInverse op inv
<   invIsUnique      :  InverseIsUnique op

< interface (DecEq d, Group d) => Dim' d where
<   DimLess'  :  d
<   Length'   :  d
<   Time'     :  d
<   Mass'     :  d
<   Times'    :  d -> d -> d
<   Over'     :  d -> d -> d
<
<   DimLess'  =  e
<   Times'    =  op
<   Over'     =  \ d1 => \ d2 => d1 `Times'` (inv d2)

%endif
%
We argue that this conclusion holds even if we define |Dimension| as a
refinement of a |Group| type class. By a similar token, we argue that a
DSL for DA and dimensionally consistent programming should also be based
on a concrete implementation of the data type |Q| for physical
quantities, as proposed in \cref{subsection:quantities}.

\paragraph*{Beyond mechanics and the |bfLTE| class.} \ Other parameters in
which it is natural to generalize the DSL from \cref{section:dsl1} are
the number of fundamental dimensions and the class of units of
measurement: we have introduced data types for dimensions, physical
quantities, etc. in the specific domain of mechanics (|n = 3|
fundamental dimensions) and for the |LTE| (lengths, times and masses)
class of units of measurement. But the Pi theorem holds for an arbitrary
number of fundamental dimensions and, perhaps more importantly, for
arbitrary classes of units. It would be nice to have a general theory
that is parameterized on |n| and on the class of units and that can be
easily instantiated to other domains. The major obstacle towards such a
generalization is, as already mentioned, the need to formalize a
significant fraction of linear algebra. In \cref{section:dsl1} we have
defined the predicates |IsDep|, |AreDep|, |AreIndep| on |D|-values (and
the corresponding ones for physical quantities) for the specific case |n
= 3|. This is straightforward but implementing these predicated for a
generic |n| can only be done on the top of a library the formalizes the
basic notion of linear algebra. Building such a library is certainly not
trivial but it is probably a necessary step for making FP more palatable
to modelers and physicists.

At the same time, it is probably worth keeping in mind the potential
danger associated with generalizations of DA: while the theory can be a
powerful methodology to reduce complexity and, up to a certain extent,
obtain physical laws "for free", it is not a "generic" tool. As Bridgman
has made very clear in Chapter 5 of his 1931 "Dimensional Analysis"
book, applying DA to a domain whose fundamental laws have not yet been
formulated in a form independent of the size of the fundamental units
can be potentially very dangerous. At this early stage, we feel that the
best that we can do is to build small prototypes of consistent
"grammars" of dimensions for specific domains and test how they work.

\medskip

Thus, our preliminary conclusion is that generalizing the approach of
\cref{section:dsl1} is useful to understand the algebraic structure of
dimension function and the role of the number of fundamental dimension
in the notions of dimensional (in)dependence that are at the core of the
Pi theorem but also comes with a number of practical disadvantages. This
is not really surprising if one keeps in mind that we still do not have
an established methodology for encoding fragments of well understood
theories (for example, game theory, optimal control, linear algebra) in
dependently typed languages.

%

\subsection{Functions and their dimensions}
\label{subsection:fun}

As we have seen in
\cref{section:equations,section:dimensions,section:pi}, most
computations in mathematical physics involve operations on functions
between physical quantities, for example
%
\begin{code}
pos : Q Time -> Q Length
pos t = v * t
\end{code}
%
for a function that describes the position of a body moving at constant
speed. Standard arithmetic operations between such functions can
be defined straightforwardly by lifting the corresponding operations on
|Q|-values. For example:
%
%{
%format => = "\to"
\begin{code}
(+)  :  {d1, d2 : D} -> (Q d1 -> Q d2) -> (Q d1 -> Q d2) -> (Q d1 -> Q d2)
(+) f1 f2 = \ q => f1 q + f2 q
\end{code}
%}
Other operations, however, require some more care. Let |pos'| represent
the first derivative of |pos|. What shall be the type of |pos'|? The
discussion at the end of \cref{section:dimensions} suggests that this
has to be |Q Time -> Q Velocity|. We can build on the the DSL of
\cref{section:dsl1} and specify types for dimensionally consistent
differentiation, for example
%
%if False
\begin{code}
dimDomain : {d0, d1 : D} -> (Q d0 -> Q d1) -> D
dimDomain {d0} f = d0

dimCodomain : {d0, d1 : D} -> (Q d0 -> Q d1) -> D
dimCodomain {d1} f = d1
\end{code}
%endif
\begin{code}
derivative  :  {d0, d1 : D} -> (Q d0 -> Q d1) -> Q d0 -> Q (d1 `Over` d0)
\end{code}
%
This is enough to support elementary dimensional judgments
%
\begin{code}
pos'' : typeOf (derivative (derivative pos))
pos'' t = v / t

check13 : dimCodomain pos'' = Acceleration
check13 = Refl
\end{code}
%
and reject definitions that are dimensionally inconsistent like  |pos'' t = x / t|.
%
As one would expect, actually implementing |derivative| (and
dimensionally consistent operations for partial differentiation,
integration, ``nabla'' operators etc.) requires developing a small DSL
of elementary calculus for functions of |Q|-variables (for example, as
discussed in
\citep[Chapter 3]{JanssonIonescuBernardyDSLsofMathBook2022} for functions of real
variables) and thus a number of non-trivial decisions.
%

\subsection{More advanced features: DA driven program derivation and
data analysis}
\label{subsection:adv}

Beside supporting program specification and verified programming,
dependently typed languages are also powerful tools for type-driven
program development \citep{idrisbook}. For example, the Idris system can
be queried interactively and asked to assist filling in holes like
|whatnow|.
%
This suggests that, in principle, one should be able to exploit this
system to derive implementations of physical relationships that fulfil
the Pi theorem. For example, coming back to the simple pendulum example
from \cref{subsection:quantities}, implementing a function that computes
the length |penLen alpha g m tau| of the pendulum that
obtains oscillations of period |tau|
given its mass |m|,
the acceleration of gravity |g|
and the amplitude |alpha| of the oscillations:
%
\begin{code}
penLen  :  Q DimLess -> Q Acceleration -> Q Mass -> Q Time -> Q Length
\end{code}
%
As a first step, we assess that |Acceleration|, |Mass| and |Time| are
independent and that |DimLess| depends on these three dimensions. This can
be done straightforwardly:
%if False

\begin{code}
not1eq0 : Not (1 = 0)
not1eq0 Refl impossible
\end{code}

%endif
\begin{code}
check14  :  AreIndep [Acceleration, Mass, Time]
check14  =  Refl

check15  :  IsDep DimLess [Acceleration, Mass, Time]
check15  =  Evidence (1, [0,0,0]) (not1eq0, Refl)
\end{code}
%
Then we define |penLen alpha g m tau| as a product of powers of |g|,
|m| and |tau|, consistently with the Pi theorem
%if False
\begin{code}
whatnow1  :  Integer
whatnow2  :  Integer
whatnow3  :  Integer
Psi       :  Q DimLess -> Q DimLess
\end{code}
%endif
\DONE{I suggest to use much shorter names for the metavariables - perhaps h1, h2, h3 (for ``hole 1'', etc.), or m1, m2, m3, or ...}

< penLen alpha g m tau = pow g whatnow1 * pow m whatnow2 * pow tau whatnow3 * Psi alpha

and fill in the holes with exponents that match the type of |penLen|: 1,
0 and 2.
%
%if False
\begin{code}
penLen alpha g m tau = pow g 1 * pow m 0 * pow tau 2 * Psi alpha
\end{code}
%endif
%
The function |Psi : Q DimLess SI -> Q DimLess SI| remains undefined,
but is the only part left to deduce from experiments. The type checker
will not accept other implementations of |penLen| but notice that we
are solving the system of equations |whatnow1 = 1|; |-2 * whatnow1 +
whatnow3 = 0|; and |whatnow2 = 0| by hand, with little help from the
type checker!

A better approach would be to ask the type checker to solve the system
for us, e.g. by searching for suitable values of |whatnow1|, |whatnow2| and |whatnow3|
in certain ranges.
%
Perhaps more importantly, we would like the type checker to detect
situations in which the system has no solutions and recommend possible
decompositions of the arguments of physical relationships into lists of
dimensionally independent and dimensionally dependent components: the
|as| and the |bs| parameters of the Pi theorem.
%
This requires formulating a fragment of linear algebra in type theory
and goes well beyond the scope of this paper. But we think that it is an
effort that would be worth pursuing: it would provide domain experts
with alternative, dimensionally consistent views of data sets and help
practitioners reducing the complexity of data-based studies.

%

\subsection{Related and future work}
\label{subsection:relatedwork}

The best account we have found of the Pi theorem from a linear algebra
perspective is by \citet{CURTIS1982117}. In a compact 10 page paper
they both explain informally, and prove more formally, the Pi theorem
in a classical mathematical style. Unfortunately, there is no
connection to programming languages or types, only vector spaces,
linear transformations, and how to model dimension analysis in this
setting.

In the key reference on the programming languages side,
\citet{10.1145/263699.263761} describes one way of combining
relational parametricity and units of measure.
%
Our paper shares some of the main ideas: we also use a typed
functional programming language for the formalization, we also use
integers as exponents of the base dimensions, and we also use
parametric polymorphism. But there are also differences: where their
types are indexed by units, ours are indexed by dimensions; and where
they prove results using parametricity, we formulate the Pi theorem
using dependent types.
%
An interesting avenue for future work could be to combine these
approaches and perhaps use parametricity for dependent types
\citep{DBLP:journals/jfp/BernardyJP12} to gain further understanding
of the interplay between dimension analysis and strongly types
functional programming.

\Citet{dimension-models} provide a categorical framework for semantic
models of a type theory that has special types for physical
quantities. Their starting point is \citep{10.1145/263699.263761} and
they provide a general notion programming language with physical
dimension types. They provide dimension polymorphism, but not regular
System-F-style polymorphism. The paper provides an impressive
collection of definitions, theorems, and examples which explain how
Abelian groups, fibrations, and groupoid actions can be used to build
models for languages with dimensions. Even though the setting is
different (and much more general), their key type |Quantity(X)| is
basically the same as our |Q d|. They briefly mention an instance of
the Pi theorem, but do not formalize it.

The most recent related work is that by
\citet{doi:10.1142/9789811242380_0020} which uses dependent types to
extend type systems for units of measure from scalars to matrices.
%
Their approach is more algebraic, with graded semirings over a group
of physical dimensions, and it would be interesting to further extend
this to tensor calculus where we are also developing DSLs
\citep{bernardy2023domainspecifictensorlanguages}.



\subsection{Physical laws revisited}
\label{subsection:covariance}

We conclude this section by going back to \cref{section:dimensions}
where we have argued that equations like Newton's second principle,
\cref{eq:newton1}, or the ideal gas law, \cref{eq:gas0}, summarize
empirical facts about measurements (or put forward axioms about such
measurements) of physical quantities.
%
Specifically, we have argued that \cref{eq:newton1} posits that
measurements of $F$ (force) are equal to the product of measurements of
$m$ (mass) and measurements of $a$ (acceleration).
%
In \cref{subsection:quantities} we have formalized the notions of
physical quantity and measurement and in \cref{section:piexplained1} we
have applied these notions to formulate covariance principle for a
generic function between physical quantities, see \cref{figure:covariance}.

With this understanding, we can now give a clearer meaning to equations
\cref{eq:newton1,eq:gas0} from \cref{section:equations} and, more
generally, to equations that represent physical laws.
%
The idea is that these equations represent both functions between
physical quantities, for example
%
\begin{code}
F : Q Mass -> Q Acceleration -> Q Force
F m a = m * a
\end{code}
%
and also instances of the covariance principle as encoded generically in
\cref{figure:covariance}
%
\begin{equation*}
|measu| \ (m \ensuremath{\cdot} a) = |measu| \ m |*| |measu| \ a
\end{equation*}
%
or, with |rhoF : Real -> Real -> Real|, \ $\rho_F = (\ensuremath{\cdot})$
%
\begin{equation*}
|measu| \ (F \ m \ a) = \rho_F \ (|measu| \ m) \ (|measu| \ a)
\end{equation*}
%
In this special form the covariance principle can actually be proved, as
discussed in \cref{subsection:quantities}.

%if False

\begin{PLAN}

  \begin{itemize}

  \item Fulfil the obligations from previous sections, specifically:

      \begin{itemize}

        \item Put forward specifications for data types that implement
          dimension function in terms of type classes (Obligation from
          \cref{subsection:df}). [Done]

        \item Specifically, require |D| to be a group (Obligation from
         \cref{subsection:dind}). [Done]

        \item Discuss the notion of covariance for a generic function
        between dimensional quantities (Obligation from
        \cref{subsection:piagain}).

      \end{itemize}

  \item Discuss (further) desiderata that a DSL for DA and dimensionally
    consistent programming shall fulfil, among others:

      \begin{itemize}

      \item Dimensionally consistent operations adding, multiplying,
        etc. functions. [Done]

      \item Dimensionally consistent types for (symbolic, approximate)
        differentiation, integration, etc. [Done]

      \item Perhaps prove that |makeDimLess| does indeed return
        dimensionless quantities, see |dimMakeDimLessIsDimLess| from
        \cref{subsection:dind}.

      \item Exploit type checking (the proof assistants, Idris' support
        for hole resolving, etc.) to accept/reject/suggest models of
        functional dependencies (recall the sentence ``For us, the
        challenge is to understand how to apply dependent types to 1)
        systematically check the dimensional consistency of expressions
        and 2) assist DA.'' from \cref{subsection:pi}!) [partially done]

      \end{itemize}

  \end{itemize}

\end{PLAN}

%endif
