% -*-Latex-*-

%if False

\begin{code}
module PiExplained1

import Data.Vect
-- import Data.Vect.Quantifiers
import Data.Primitives.Views
import Syntax.PreorderReasoning

import DSL1

%default total
%auto_implicits off
%access public export

\end{code}
%endif

%%%%%%%

\section{The Pi theorem in Type Theory}
\label{section:piexplained1}

%if False
\begin{code}
    IsCovariant : {k, m : Nat} -> {ds  : Vect k D} -> {ds' : Vect m D} -> {d : D} ->
                  (f : QVect k ds -> QVect m ds' -> Q d) -> Type
    IsCovariant {k} {m} {ds} {ds'} {d} f =
      DPair (Vect k Real -> Vect m Real -> Real)
            (\ rhof =>
               (as : QVect k ds) -> (bs : QVect m ds') -> (u : Units) ->
               meas u (f as bs) = rhof (mapQ (meas u) as) (mapQ (meas u) bs)
            )

    exponents   :  {k : Nat} -> {d : D} -> {ds  : Vect k D} -> IsDep d ds -> (Integer, Vect k Integer)
    exponents = getWitness

    Pi   :  Type
    Pi   =  {k : Nat} -> {ds  : Vect k D} ->
            {m : Nat} -> {ds' : Vect m D} ->
            {d : D} ->
            (f : QVect k ds -> QVect m ds' -> Q d) ->
            IsCovariant f ->
            AreIndep ds ->
            (es' : AreDep ds' ds) ->
            DPair (IsDep d ds)
                  (\ evi
                   =>
                   Exists (QVect m ((dimMakeAllDimLess ds' ds es')) -> Q DimLess)
                          (
                            \ Phi =>
                            (as : QVect k ds) -> (bs : QVect m ds') ->
                            makeDimLess (f as bs) as evi
                            =
                            Phi (makeAllDimLess bs as es')
                          )
                  )

    Pi0  :  Type
    Pi1  :  Type
\end{code}
%endif

With the notions of dimension function, physical quantity and
dimensional independence in place, we can formulate the Pi theorem of
\cref{section:pi} in type-theory.
%
Following the principle that types can encode logical propositions
\citep{DBLP:journals/cacm/Wadler15}, our goal is to define a type, say
|Pi|, such that values of type |Pi| are proofs of the Pi theorem.

In \cref{subsection:pi} we saw that the Pi theorem asserts that
a ``physical relationship'' |f| between a ``dimensional
quantity'' |a| and |k + m| ``dimensional governing parameters''
$a_1,\dots,a_k$ and $b_1,\dots,b_m$ that fulfil \cref{eq:pih0,eq:pih1}
satisfies \cref{eq:pi0,eq:pi1}.
%
Thus the theorem entails two conclusions, both quantified over functions
between physical quantities. We account for this through two
higher-order function types |Pi0, Pi1 : Type|
%
\begin{spec}
    Pi0  =  (f : QVect k ds -> QVect m ds' -> Q d) -> ...
\end{spec}
%
and similarly for |Pi1| with implicit parameters |k, m : Nat|, |ds :
Vect k D|, |ds' : Vect m D| and |d : D|. As discussed in
\cref{subsection:pi}, the term ``physical relationship'' is used in
\citep{barenblatt1996scaling} to denote a function that fulfils the
``covariance principle''.

We have seen in \cref{subsection:df} that the covariance
principle (or principle of relativity of measurements) posits that that
there is no privileged system of units of measurement or, equivalently,
that all systems are equally good.
%
So far, we have formalized the notion of covariance for dimension
functions (through the specification \ref{eq:df0}) and we have seen
that, for elementary binary operations on physical quantities, the
covariance principle boils down to the requirement that |meas| is a
homomorphism, see \cref{subsection:quantities}.

%
\begin{figure}[h]
\begin{center}
\begin{tikzcd}[row sep=large, column sep = huge]
|QVect m ds| \arrow[r, "|mapQ (meas u)|"] \arrow[d, "|f|"]
& |Vect m Real| \arrow[d, "|rhof|"] \\
|Q d| \arrow[r, "|meas u|"]
& |Real|
\end{tikzcd}
\end{center}
\caption{The the covariance principle (or principle of relativity of
  measurements) for a function between physical quantities.}
\label{figure:covariance}
\end{figure}
%
It is now time to discuss this notion for a generic function between
physical quantities. The idea is that a function |f : QVect m ds -> Q
d| fulfils the covariance principle iff there exists a representation
|rhof : Vect m Real -> Real| such that the diagram in
\cref{figure:covariance} commutes.
%
Here |mapQ (meas u)| is the function that applies |meas u| to
the physical quantities of a |QVect| and |u| is an arbitrary system of
units of measurement. One can encode the requirement that the physical
relationship |f| of the Pi theorem fulfils the covariance principle in
terms of a predicate

%\pagebreak
%{
%format => = "\to"
\begin{spec}
    IsCovariant  :  {k, m : Nat} -> {ds  : Vect k D} -> {ds' : Vect m D} -> {d : D} ->
                    (f : QVect k ds -> QVect m ds' -> Q d) -> Type
    IsCovariant {k} {m} {ds} {ds'} {d} f =
      DPair  (Vect k Real -> Vect m Real -> Real)
             (\  rhof =>
                 (as : QVect k ds) -> (bs : QVect m ds') -> (u : Units) ->
                 meas u (f as bs) = rhof (mapQ (meas u) as) (mapQ (meas u) bs)
             )
\end{spec}
%}
and use |IsCovariant| to encode the first assumption of the theorem:
%
\begin{spec}
    Pi0  =  (f : QVect k ds -> QVect m ds' -> Q d) -> (h1 : IsCovariant f) -> ...
\end{spec}

\noindent
Next, we need to formalize the two assumptions \cref{eq:pih0,eq:pih1}
about the arguments of |f|. The first one states that $a_1,\dots,a_k$
``have independent dimensions''. We have seen how to formalize this
assumption in \cref{subsection:dind}:
\begin{spec}
    Pi0  =  (f : QVect k ds -> QVect m ds' -> Q d) -> (h1 : IsCovariant f) ->
            (h2 : AreIndep ds) -> ...
\end{spec}

\noindent
The second assumption of the Pi theorem, \cref{eq:pih1}, specifies |m|
equalities between dimension functions. We have seen that equality
between dimension functions boils down to equality in |IntegerN| (in
mechanics |n = 3|) and is thus decidable.

In \cref{subsection:dind}, we have also seen that the exponents
in \cref{eq:pih1} are rational numbers and that we can rewrite these
equalities as
\begin{equation}
[b_i]^{p_{i}} = [a_1]^{p_{i1}} \dots [a_k]^{p_{ik}} \quad i = 1,\dots,m
\label{eq:pih1rev}
\end{equation}
with integers $p_{i},p_{i,1},\dots,p_{i,k}$ as we have done for the
simple pendulum example. This states that the dimension functions of the
physical quantities of the second argument |bs| of |f| can be expressed
as products of powers of the dimension functions of the physical
quantities of the first argument
\begin{spec}
    Pi0  =  (f : QVect k ds -> QVect m ds' -> Q d) -> (h1 : IsCovariant f) ->
            (h2 : AreIndep ds) -> (h3 : AreDep ds' ds) -> ...
\end{spec}
where |h3 : AreDep ds' ds| is a vector of |IsDep| proofs, one for each
element of |ds'|:
\begin{spec}
      AreDep : {m, k: Nat} -> (ds' : Vect m D) -> (ds : Vect k D) -> Type
      AreDep ds' ds = All (\d' => IsDep d' ds) ds'
\end{spec}
In the traditional formulation of the |Pi| theorem, the parameters
that are turned into dimensionless quantities are $b_1,\dots,b_m$
(with dimensions |ds'|).
%
They are dimensionally dependent on the dimensionally independent ones
$a_1,\dots,a_k$ (with dimensions |ds|).
%
From this angle, the two hypotheses |h2 : AreIndep ds| and |h3 :
AreDep ds' ds'| completely determine how the |k+m| parameters are
split.
%
In general there can be several choices, and that choice is up to the
user of the |Pi| theorem.

With these premises, the Pi theorem warrants the existence of
exponents $p_1,\dots,p_k$ and of a function $\Phi$ such that the
equalities (\ref{eq:pi0}) and (\ref{eq:pi1}) do hold. As for
\cref{eq:pih1rev}, these are rational numbers but we can reformulate
\cref{eq:pi0} as:
\begin{equation}
[a]^{p} = [a_1]^{p_1} \dots [a_k]^{p_k}
\label{eq:pi0rev}
\end{equation}
%
with integers exponents $p,p_1,\dots,p_k$ and the first conclusion of
the Pi theorem (with all its implicit arguments) as
\begin{code}
    Pi0  =  {k, m : Nat} -> {ds  : Vect k D} -> {ds' : Vect m D} -> {d : D} ->
            (f : QVect k ds -> QVect m ds' -> Q d) -> (h1 : IsCovariant f) ->
            (h2 : AreIndep ds) -> (h3 : AreDep ds' ds) -> IsDep d ds
\end{code}

\noindent
The second conclusion of the Pi theorem is \cref{eq:pi1}. This states
the existence of a function |Phi| that allows one to express |f as bs|
to the power of |p| as a product of powers of the |as| times |Phi|
applied to the non-dimensional ``$\Pi$'' fractions of \cref{eq:pi1}:
%if False
\begin{code}
    pi0  :  Pi0
\end{code}
%endif
%
\begin{code}
    Pi1  =  {k, m : Nat} -> {ds  : Vect k D} -> {ds' : Vect m D} -> {d : D} ->
            (f : QVect k ds -> QVect m ds' -> Q d) -> (h1 : IsCovariant f) ->
            (h2 : AreIndep ds) -> (h3 : AreDep ds' ds) ->
            Exists  (  QVect m (dimMakeAllDimLess ds' ds h3) -> Q DimLess )
                    (  \ Phi  =>  (as : QVect k ds) -> (bs : QVect m ds') ->
                                  let  (p, ps)  =  exponents (pi0 f h1 h2 h3)
                                       Pis      =  makeAllDimLess bs as h3
                                  in   pow (f as bs) p = prodPows as ps * Phi Pis )
\end{code}

\noindent
Thus, the second conclusion of the Pi theorem is an existential type
which depends on the first conclusion through the integer exponents
$p,p_1,\dots,p_k$ and one needs to postulate |pi0 : Pi0| in order to
define |Pi1|. Alternatively, one could formulate the two
conclusions as a dependent pair.
%
Notice that, in order to define |Pi1|, we need to ``compute'' the type
of |Phi|, the exponents |p|, |ps| and the ``$\Pi$'' fractions |Pis|. We
compute the domain of |Phi| by applying the function
|dimMakeAllDimLess|. This is an extension of |dimMakeDimLess| from
\cref{subsection:dind}. Similarly, the ``$\Pi$'' fractions |Pis| are
computed with an extension of |makeDimLess| also from
\cref{subsection:dind}.
%% In the definition of |dimMakeAllDimLess|, we
%% %
%% \begin{spec}
%%     dimMakeAllDimLess : {m, k : Nat} -> (ds' : Vect m D) -> (ds : Vect k D) -> AreDep ds' ds -> Vect m D
%%     dimMakeAllDimLess        Nil  ds        Nil   =  Nil
%%     dimMakeAllDimLess (d' :: ds') ds (e' :: es')  =  dimMakeDimLess d' ds e' :: dimMakeAllDimLess ds' ds es'
%% \end{spec}
%% %
%% to |ds'|, |ds|, and |es'|. In the definition of |dimMakeAllDimLess|, we
%% have used the function |dimMakeDimLess| from
%% \cref{subsection:dind}. Notice that |Phi| has to return dimensionless
%% physical quantities for \cref{eq:pi1} to be consistent. Similarly, we
%% compute the ``$\Pi$'' fractions |Pis| by generalizing the function
%% |makeDimLess| discussed in \cref{subsection:dind}:
%% \begin{spec}
%%     makeAllDimLess  :  {m : Nat} -> {ds' : Vect m D} -> {k : Nat} -> {ds : Vect k D} ->
%%                        (qs' : QVect m ds') -> (qs : QVect k ds) -> (es' : AreDep ds' ds) ->
%%                        QVect m (dimMakeAllDimLess ds' ds es') us'
%%     makeAllDimLess        Nil  qs      Nil     =  Nil
%%     makeAllDimLess (q' :: qs') qs (e' :: es')  =  makeDimLess q' qs e' :: makeAllDimLess qs' qs es'
%% \end{spec}

We have applied the minimal DSL from section \cref{section:dsl1} to
formulate the covariance principle and the Pi theorem in Idris.
%
To the best of our knowledge, this is the first formulation of these
notions in type theory and an obvious question is whether the theorem in
this form can actually be proven that is, whether |Pi0| and |Pi1| can
be implemented.

A glance at section 1 of \citep{barenblatt1996scaling} suggests that, in
principle, |Pi0| should be provable by contradiction and on the basis of
a lemma. Informally
%
\begin{lemma}
  \label{lemma1}
Let $as = a_1,\dots,a_k$ have independent dimensions. Then for any system of
units $u$, any positive real number $r$ and any $1 \leq j \leq k$ there
exists a system of units $u_j$ such that $\mu \ u_j \ a_j = r \cdot \mu \ u
\ a_j$ and $\mu \ u_j \ a_i = \mu \ u \ a_i$ for all $1 \leq i \leq k$,
$i \neq j$.
\end{lemma}
%
Let |bs| = $b_1,\dots,b_m$.  From the lemma and from the assumption
that |f| fulfils the covariance principle it follows that
$a_0 = f(as, bs)$ and |as| cannot be dimensionally independent by
contradiction. Thus, $a_0$ is dimensionally dependent on |as| that is
|IsDep d ds|.
%
Informally, assume |a0| and |as| are dimensionally independent and
take $r \neq 1$. Then one has
%format u0
\begin{equation*}
|meas u0 (f(as, bs)) = r * meas u (f(as,bs))|
\end{equation*}
by \cref{lemma1} and also
\begin{equation*}
|meas u0 (f(as,bs)) = rhof (mapQ (meas u0) as, mapQ (meas u0) bs)|
\end{equation*}
by the covariance principle. But by \cref{lemma1} one also has
|mapQ (meas u0) as = mapQ (meas u) as|, and
also |mapQ (meas u0) bs = mapQ (meas u) bs|
(because |bs| are dimensionally dependent on |as|)
and thus
\begin{equation*}
|meas u0 (f(as,bs)) = rhof (mapQ (meas u) as, mapQ (meas u) bs)|
\end{equation*}
by congruence and, again by the covariance principle |meas u0
(f(as,bs)) = meas u (f(as,bs))| and thus $r = 1$. Thus |as| are
dimensionally dependent.

A similar argument
% applied to the function (exponents given by |Pi0|!)
% \begin{equation*}
% \frac{1}{a_1^{p_1} \dots a_k^{p_k}} f
% \end{equation*}
allows one to ``prove'' the second part of the Pi theorem |Pi1|, see
pages 41-42 of \citep{barenblatt1996scaling}.

It is perhaps worth noticing that implementing \cref{lemma1}
requires formulating and solving a linear system of equations and thus
formalizing at least a fragment of linear algebra.
%
Indeed, many works present dimensional analysis and the Pi theorem as
corollaries of linear algebra (see
\cref{subsection:relatedwork}). This is a slightly different
perspective from the one presented here which strictly follows
\citep{barenblatt1996scaling} and emphasizes the role of the first of
the three assumptions at the core of the theorem: that the ``physical
relationship'' |f| fulfils the covariance principle.

It is perhaps also worth pointing out that, while the types of |Pi0| and
|Pi1| suggest that the physical relationship |f| is given, this is far
from actually being the case.
%
In practice, the Pi theorem is applied to reduce the complexity of
data-based modelling: instead of learning a function of $k + m$
variables from empirical data, one only needs to learn a function of $m$
variables.
%
In other words, |f| is unknown and |Phi| has to be identified via data
analysis or through a theory, as discussed in \cref{section:pi} and, in
greater detail, in section one of \citep{barenblatt1996scaling}.
%
For example, the constant $\Phi = (2 |*| \pi)^2$ in the equation for the
period of a simple pendulum $\tau^2 = l / g |*| \Phi$ can be estimated
empirically or obtained by solving the second order differential
equation obtained by applying Newton's second law in the limit of small
amplitudes.

The minimal DSL presented in \cref{section:dsl1} and the formulation of
the covariance principle and of the Pi theorem discussed in this section
are the main contributions of this paper towards making mathematical
physics (functional programming) more accessible to computer scientists
(modelers and physicists). In the next section we discuss possible
generalizations and desiderata that go beyond the scope of this
manuscript.
