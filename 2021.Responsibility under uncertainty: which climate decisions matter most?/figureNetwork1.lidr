
\begin{tikzpicture}[shorten >=1pt,node distance=3cm,auto]
 \tikzset{
  	default/.style={color=black,
                    top color=white,
                    bottom color=PIKorangeLight1,
                    minimum width=1.3cm},
  	tab/.style={color=black,
                    top color=white,
                    bottom color=PIKgreenLight3,
                    minimum width=1.3cm}
                    }
                    
    

  \node[state, default, ellipse]  (SD0)    []                {$x_1$};
  \node[state, default, ellipse]  (LH0)    [below of=SD0,
                          node distance=3.5cm]   {$x_2$};
  \node[state, default, ellipse]  (UC0)    [above of=SD0]   {$x_3$};
  \node[state, default, ellipse]  (LH1)  [right of=LH0,
                          node distance=6cm]   {$x_2^{\prime}$};
  \node[state, default, ellipse]  (SD1)    [right of=SD0,
                          node distance=6cm]   {$x_1^{\prime}$};
  \node[state, default, ellipse]  (Ctrl)   [left of=SD1,
                          node distance=2.5cm]   {$y$};
  \node[state, default, ellipse]  (UC1)  [right of=UC0,
                          node distance=6cm]   {$x_3^{\prime}$};
  \node[state, default, ellipse]  (Step)  [above of=UC1,
                          node distance=2cm]   {$t$};
                          
  % \node[state, tab, rectangle]  (SD1table)  [right of=SD1,
  %                         node distance=6.5cm]   
  %                         {\parbox{4.75cm}{$
  %                         \begin{array}{l | l l}
  %                              & \qquad S  & \qquad D\\\hline&&\\[-0.7em]
  %                          Start & pS\_Start & pD\_Start \\
  %                          Delay & pS\_Delay & pD\_Delay \\
  %                          ()  & 1       & 0       \\
  %                         \end{array}$}
  %                         };
                          
  %  \node[state, tab, rectangle]  (LH1table) [right of=LH1,
  %                         node distance=6.5cm]   
  %                         {\parbox{5.5cm}{$
  %                         \begin{array}{c c c | l l}
  %                            &   &       & \qquad L  & \qquad H\\\hline&&&\\[-0.7em]
  %                         S  & D & H     & pL\_S\_DH & pH\_S\_DH \\
  %                         S  & D & L     & pL\_S\_DL & pH\_S\_DL \\
  %                         S  & S & H     & pL\_S\_SH & pH\_S\_SH \\
  %                         S  & S & L     & pL\_S\_SL & pH\_S\_SL \\
  %                         D  & D & H     & pL\_D\_DH & pH\_D\_DH \\
  %                         D  & D & L     & pL\_D\_DL & pH\_D\_DL \\
  %                         D  & S & *     & \alpha  & 1 - \alpha \\
  %                         \end{array}$}
  %                         };  
                          
  %  \node[state, tab, rectangle]  (UC1table)  [right of=UC1,
  %                         node distance=6.5cm]   
  %                         {\parbox{5.05cm}{$
  %                         \begin{array}{c c c | l l}
  %                            &       && \qquad U & \qquad C\\\hline&&&\\[-0.7em]
  %                         U  & S    & 0 & pU\_S\_0 & pC\_S\_0 \\
  %                         U  & S    & 1... & pU\_S    & pC\_S    \\
  %                         U  & D    & 0 & pU\_D\_0 & pC\_D\_0 \\
  %                         U  & D    & 1... & pU\_D    & pC\_D    \\
  %                         C  & * & *  & 0   & 1   \\
  %                         \end{array}$}
  %                     };


   \node[state, tab, rectangle]  (SD1table)  [right of=SD1,
                          node distance=6.5cm]   
                          {\parbox{4cm}{$
                          \begin{array}{p{1cm} || l l}
                                          & \ \quad S  & \quad D\\\hline&&\\[-0.7em]
                           \textit{Start} &\ |pS_Start| & |pD_Start| \\
                           \textit{Delay} &\ |pD_Delay| & |pD_Delay| \\
                           ()             &\ 1       & 0       \\
                          \end{array}$}
                          };
                          
   \node[state, tab, rectangle]  (LH1table) [right of=LH1,
                          node distance=6.5cm]   
                          {\parbox{4.5cm}{$
                          \begin{array}{l l l @@{\hspace*{0.2cm}} || l l}
                             &   &       & \qquad L  & \quad H\\\hline&&&\\[-0.7em]
                          S  & D & H     &\ |pL_S_DH| & |pH_S_DH| \\
                          S  & D & L     &\ |pL_S_DL| & |pH_S_DL| \\
                          S  & S & H     &\ |pL_S_SH| & |pH_S_SH| \\
                          S  & S & L     &\ |pL_S_SL| & |pH_S_SL| \\
                          D  & D & H     &\ |pL_D_DH| & |pH_D_DH| \\
                          D  & D & L     &\ |pL_D_DL| & |pH_D_DL| \\
                          D  & S & *     &\ \alpha  & 1 - \alpha \\
                          \end{array}$}
                          };  
                          
   \node[state, tab, rectangle]  (UC1table)  [right of=UC1,
                          node distance=6.5cm]   
                          {\parbox{4cm}{$
                          \begin{array}{l l p{0.55cm} || l l}
                             &      &      &\ \quad U & \quad C\\\hline&&&\\[-0.7em]
                          U  & S    & 0    &\ |pU_S_0|  & |pC_S_0| \\
                          U  & S    & 1... &\ |pU_S|    & |pC_S|   \\
                          U  & D    & 0    &\ |pU_D_0|  & |pC_D_0| \\
                          U  & D    & 1... &\ |pU_D|    & |pC_D|   \\
                          C  & *    & *    &\ 0            & 1     \\
                          \end{array}$}
                          };                       
                      
  
   \path[->]
  (SD0) edge [bend right, line width=0.5mm] 
        node[pos=.8] (I2C) {}
       (LH1)
        edge [dashed, line width=0.4mm] 
        node[pos=.8] (I2C) {}
       (Ctrl)
  (SD1)  edge [bend left, line width=0.5mm]
         node[pos=.7] (I2A) 
         {\parbox{7cm}{}}
       (LH1)
       edge [-, dashed, color=PIKgreenLight3, line width=0.7mm]
         node[pos=.7] (I2A) 
         {\parbox{7cm}{}}
         (SD1table.west)
       edge [bend right, line width=0.5mm] 
        node[pos=.8] (I2C) {}
        (UC1)
  (LH0)  edge [bend right, line width=0.5mm]
         node[pos=.7] (I2A) 
         {\parbox{7cm}{}}
       (LH1)
  (LH1) edge [-, dashed, color=PIKgreenLight3, line width=0.8mm]
         node[pos=.7] (I2A) 
         {\parbox{7cm}{}}
       (LH1table.west)
  (Ctrl) edge [line width=0.5mm]
         node[pos=.7] (I2A) 
         {\parbox{7cm}{}}
       (SD1)
  (UC0) edge [bend left, line width=0.5mm]
         node[pos=.7] (I2A) 
         {\parbox{7cm}{}}
       (UC1)
  (UC1) edge [-, dashed, color=PIKgreenLight3, line width=0.8mm]
         node[pos=.7] (I2A) 
         {\parbox{7cm}{}}
       (UC1table.west)
  (Step) edge [line width=0.5mm]
         node[pos=.7] (I2A) 
         {\parbox{7cm}{}}
       (UC1)
      
       ;
 
\end{tikzpicture}

