% -*-Latex-*-
\documentclass[sn-mathphys]{sn-jnl-h1}

\usepackage{amsmath}
\usepackage{quoting}
\usepackage{bigfoot} % To handle url with special characters in footnotes!
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{tikz-cd}
\usetikzlibrary{cd, automata, arrows, positioning, backgrounds}
\usetikzlibrary{arrows,positioning,shapes.callouts,decorations.pathmorphing,
 shapes, fadings}
\usepackage{enumitem}
%\setlist[itemize,1]{leftmargin=10pt}
%\setlist[enumerate,1]{leftmargin=10pt}

%include main.fmt

\input{macros.TeX}

\input{pik_colors}

\jyear{2021}


\begin{document}

\title[Responsibility under uncertainty: which climate decisions matter
  most?]{Responsibility under uncertainty: which climate decisions
  matter most?}

\author*[1,2]{\fnm{Nicola} \sur{Botta}}\email{botta@@pik-potsdam.de}
\author[1,6]{\fnm{Nuria} \sur{Brede}}%%\email{nubrede@@pik-potsdam.de}
\author[3]{\fnm{Michel} \sur{Crucifix}}%%\email{michel.crucifix@@uclouvain.be}
\author[4]{\fnm{Cezar} \sur{Ionescu}}%%\email{cezar.ionescu@@th-deg.de}
\author[2]{\fnm{Patrik} \sur{Jansson}}%%\email{patrikj@@chalmers.se}
\author[5]{\fnm{Zheng} \sur{Li}}%%\email{winston@@arimadata.com}
\author[3]{\fnm{Marina} \sur{Martínez}}%%\email{marina.martinez@@uclouvain.be}
\author[6]{\fnm{Tim} \sur{Richter}}%%\email{tim.richter@@uni-potsdam.de}

% double "@" in email to please lhs2tex

\affil[1]{\orgname{Potsdam Institute for Climate Impact Research},
           \orgaddress{\city{Potsdam}, \country{Germany}}}
\affil[2]{\orgname{Chalmers University of Technology},
           \orgaddress{\city{Göteborg}, \country{Sweden}}}
\affil[3]{\orgname{Université catholique de Louvain},
           \orgaddress{\city{Ottignies-Louvain-la-Neuve}, \country{Belgium}}}
\affil[4]{\orgname{Technische Hochschule Deggendorf},
           \orgaddress{\city{Deggendorf}, \country{Germany}}}
\affil[5]{\orgname{Northeastern University \& Arima Inc.},
           \orgaddress{\country{Canada}}}
\affil[6]{\orgname{Potsdam University},
           \orgaddress{\city{Potsdam}, \country{Germany}}}

\abstract{
We propose a new method for estimating how much decisions under monadic
uncertainty matter.
%
The method is generic and suitable for measuring responsibility in
finite horizon sequential decision processes. It fulfills ``fairness''
requirements and three natural conditions for responsibility measures:
agency, avoidance and causal relevance.
%
We apply the method to study how much decisions matter in a stylized
greenhouse gas emissions process in which a decision maker repeatedly
faces two options: start a ``green'' transition to a decarbonized
society or further delay such a transition.
%
We account for the fact that climate decisions are rarely implemented
with certainty and that their consequences on the climate and on the
global economy are uncertain.
%
We discover that a ``moral'' approach towards decision making -- doing
the right thing even though the probability of success becomes
increasingly small -- is rational over a wide range of uncertainties.  }

\keywords{climate policy, responsibility measures, uncertainty, GHG
  emissions processes, verified decision making}

\maketitle

\setlength{\mathindent}{1em}
%
\newcommand{\fixlengths}{\setlength{\abovedisplayskip}{6pt plus 1pt minus 1pt}\setlength{\belowdisplayskip}{6pt plus 1pt minus 1pt}}
%%\renewcommand{\hscodestyle}{\footnotesize\fixlengths}
\renewcommand{\hscodestyle}{\codesize\fixlengths}
\fixlengths
%\setlength{\belowdisplayskip}{6pt plus 0pt minus 0pt}    %% lhs2TeX uses this
%\setlength{\belowdisplayskip}{8.5pt plus 3pt minus 4pt}  %%\small
%\setlength{\belowdisplayskip}{10pt plus 2pt minus 5pt}   %%\normalsize

%include Introduction1.lidr
%%include Notation.lidr
%include Theory.lidr
%include Specification.lidr
%include Responsibility.lidr
%include Generic.lidr
%include Application.lidr
%include Conclusion.lidr

\section*{Acknowledgments}
The authors thank the editors and reviewers, whose comments have
lead to significant improvements of the original manuscript.
%
The work presented in this paper heavily relies on free software, among
others on Coq, Idris, Agda, GHC, git, vi, Emacs, \LaTeX\ and on the
FreeBSD and Debian GNU/Linux operating systems.
%
It is our pleasure to thank all developers of these excellent products.

\section*{Author Declarations}

\paragraph*{Funding}
This is TiPES contribution No 138. This project has received funding
from the European Union’s Horizon 2020 research and innovation programme
under grant agreement No 820970.

\paragraph*{Conflict of interest/Competing interests}
None.

\paragraph*{Ethics approval}
Not applicable.

\paragraph*{Consent to participate}
Not applicable.

\paragraph*{Consent for publication}
Not applicable.

\paragraph*{Availability of data and materials}
This manuscript has been written as literate Idris programs, one program
per section. The programs/sections have been processed with lhs2\TeX
\citep{lhs2tex} and with latexmk to generate a PostScript manuscript.
All files are publicly available at \citep{papers} in folder
``2021.Responsibility under uncertainty: which climate decisions matter
most?''

\paragraph*{Code availability}
At \citep{papers}, see above.

\paragraph*{Authors' contributions}
NiBo drafted the initial sketch, performed the inlined computations and
managed the preparation and the submission of the manuscript.
%
NuBr drafted parts of section \ref{section:specification} and
implemented the DSLs of section \ref{section:generic}.
%
MM and NuBr performed the sensitivity analysis of the responsibility
measures.
%
MC, CI and ZL revised the introduction, sections \ref{section:notation}
and \ref{section:theory} and the specification of the stylized GHG
emissions decision problem.
%
PJ drafted section \ref{section:notation}. TR improved the formulation
of the sequential decision problems of section
\ref{section:responsibility}.
%
All authors read, commented and approved the final manuscript.

\bibliography{references}
\label{lastpage01}
%include Appendix.lidr

\end{document}
