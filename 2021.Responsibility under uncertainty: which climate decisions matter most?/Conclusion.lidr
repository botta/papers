% -*-Latex-*-

%if False

> module Conclusion

%endif

\section{Conclusion}
\label{section:conclusion}

In this paper, we have studied the notion of responsibility under
uncertainty in sequential decision processes in the context of
global climate policy.
%
Specifically, we have extended the verified theory of policy advice and
avoidability \citep{2017_Botta_Jansson_Ionescu} with a family of methods
for measuring how much decisions under uncertainty do matter and the
degree of responsibility associated with such decisions.

We have also introduced a small domain specific language for specifying
sustainability goals in GHG emissions decision processes.
%
We have applied the DSL to formalize a stylized decision process in
which a decision maker repeatedly faces two options over a finite number
of decision steps: start a ``green'' transition to a decarbonized
society or delay such transition. We have studied how uncertainties (on
the capability of decision makers to actually implement their decisions
and on the consequences of starting or delaying green transitions)
affect how much decisions at specific points in time do matter and the
degree of responsibility associated with these decisions.

Some of the results presented in sections \ref{section:responsibility},
\ref{section:generic} and \ref{section:application} are consistent with
common intuitions on how responsibility changes when the capability of
decision makers to actually impose their decisions increases or
decreases.

Perhaps more surprisingly, we have also found that the measures of
responsibility discussed in section \ref{section:responsibility} suggest
that a ``moral'' approach towards decision making -- doing the right
thing even though the probability of success becomes increasingly small
-- is perfectly rational over a wide range of uncertainties.

The fact that these results are based on verified methods for computing
optimal policies is crucial for their interpretation: they are a logical
consequence of the assumptions about the decision process specified in
section \ref{section:specification} and of the goals of decision making
(avoiding short term economic downturns and long term negative impacts
from climate change) explicitly stated in section \ref{section:generic}
and not the result of programming errors.
%
%% \REMARK{Nuria}{Change to ``and we can be sure that they are not the
%%   result...''?  Not using ``verified methods'' does not necessarily mean
%%   that the results are ``the result of programming errors''.}
%
%% Nicola: the statement seems perfectly fine to me. It does not suggest
%% that not using verified methods necessarily leads to programming
%% errors.
%

The fact that ``best'' decisions are stable with respect to both
decision horizons (the number of decision steps to look forward in order
to define measures of responsibility) and to the amount of uncertainty
suggest that our results could be valid for more realistic decision
processes than the one studied here.

In the last section, we have also shown that the measures of
responsibility introduced in section \ref{section:responsibility}
fulfill two of the three natural conditions put forward in
\citep{10.1111/ecoj.12507}. For the third condition, see footnote 14 on
page 41.
%
%%\REMARK{Nuria}{Maybe rather spell this out?}
%
%% Nicola: suggestion?

Also in section \ref{section:application}, we have shown that, in DHU
(green transition delayed, economic welfare high, uncommitted to
negative impacts from climate change) the importance of taking the right
decision (starting a green transition) at decision step 0 systematically
\emph{decreases} (as compared to the importance of taking the right
decision -- also starting a green transition -- at decision step 1) as
the probability of severe impacts from climate change \emph{increases}.

It is important to point out that this result is only in apparent
contradiction with the intuition (that inspires, among others, the
``Fridays for future'' movement) that current climate decisions matter
more than decisions to be taken in the upcoming decades. This is because
of two reasons.

The first one is that the probability of facing the decision to either
start or to delay a green transition in DHU at decision step 1 is less
than one. In other words: it is true that, if the next generation will
happen to be in DHU, they will face a decision that matters more than
the current one. But the probability that the next generation
\emph{will be} in DHU is relatively low, especially if the current
decision is to further delay a green transition!

The second reason why the results discussed in section
\ref{section:application} are not in contradiction with the notion that
current climate decisions matter more than decisions to be taken in the
upcoming decades is more subtle and needs to be discussed with some
care.

In the introduction, we have pointed out a fundamental difficulty of
climate policy advice: the lack of agreement on how to account for the
chances and the risks of climate change.

In the language introduced in section \ref{section:generic}, lack of
agreement on how to account for the chances and the risks of climate
change means lack of agreement on how to define \cs{|goal|}. Remember that
the results discussed in section \ref{section:application} have been
obtained with

< goal = Avoid isCommitted <&&> Avoid isDisrupted

In other words, we have measured how much decisions matter and how to
attribute responsibility to specific decisions with respect to the goal
of avoiding negative impacts from climate change and economic
downturns.

This encodes notions of sustainability but not necessarily of
\emph{fairness} (balanced share of responsibility between generations),
not to mention \emph{justice}: there is nothing in the above definition
of \cs{|goal|} that prevents optimal decisions to lead to states in which the
set of options available to upcoming generations has shrunk or to states
in which decision makers have to face more crucial decisions than the
current one.

By contrast, the idea that current climate decisions matter more than
decisions to be taken in the upcoming decades is based on notions of
fairness and justice that are not encoded in \cs{|goal|} and thus are not
accounted for in the analysis presented in section
\ref{section:application}.
%
As far as one can define predicates on states that encode notions of
fairness and justice, one can apply the measures of responsibility from
section \ref{section:generic}.

The problem to agree on what is to be considered fair and just 
limits the applicability of rigorous decision theories to climate
policy. But it is a problem that, to quote Hardin \citep{hardin1968},
``has no technical solution'' and cannot be avoided -- neither by
verified decision making \citep{2017_Botta_Jansson_Ionescu}, nor by
generalizations of cost-benefit analysis \citep{shape2021},
multi-objective optimal control \citep{CARLINO202016593} or storylines
\citep{shepherd2019}.

%% At the same time, it is crucial that both advisors
%% and decision makers fully understand the implications of this problem
%% for policy advice.

From this perspective, this paper can also be seen as a contribution
from verified decision theory towards understanding the limits of
applicability of decision theories to policy advice. 
