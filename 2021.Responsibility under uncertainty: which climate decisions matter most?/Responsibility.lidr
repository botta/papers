% -*-Latex-*-

%if False

> module Responsibility

> import Data.So
> import Effects

> import Effect.Exception
> import Effect.StdIO

> import Basic.Predicates
> -- import FastSimpleProb.SimpleProb
> -- import FastSimpleProb.BasicOperations
> import FastSimpleProb.Measures
> import FastSimpleProb.Functor
> import NonNegDouble.NonNegDouble
> import NonNegDouble.Constants
> import NonNegDouble.BasicOperations
> import NonNegDouble.Operations
> import NonNegDouble.Predicates
> import NonNegDouble.Properties
> import NonNegDouble.LTEProperties
> import Double.Predicates
> -- import List.Operations
> import LocalEffect.Exception
> import LocalEffect.StdIO
> import Fun.Operations


> import Theory
> import Specification

> %default total
> %access public export
> %auto_implicits off

%endif

\section{Responsibility measures}
\label{section:responsibility}

We formulate and answer three questions that we raised, informally, in
the introduction:

\begin{itemize}

\item What does it mean precisely for decisions to matter?

\item Are there general ways to measure how much decisions matter when
  these have to be taken under uncertainty?

\item Is there a natural way of comparing similar decisions at different
  times?

\end{itemize}
%
%% \REMARK{Nuria}{Maybe rather use itemize here, to avoid an implicit (wrong)
%%   association with the numbering for the three steps below?}
%
%% Nicola: done
%

\noindent
We extend the theory of section \ref{section:theory} with a
\emph{responsibility} measure for sequential decision processes under
monadic uncertainty.
%
The measure is obtained, for a given decision process, in three steps.

% Nuria: for fine-tuning the indentation of itemize:
\setlist[itemize,1]{leftmargin=30pt}

\begin{itemize}

%TR 19.06.22: I find it important to say right here at the start that
%  the specification of the goal (i.e. the extension to a decision problem)
%  is done to encode for what we want to measure responsibility. Before,
%  this was said in similar words a little later (where I removed it now).  
\item[S1] First, we need to define \emph{the goal for which} we seek to measure
  responsibility, e.g. ``saving the child'' or ``avoiding states that are
  committed to severe climate change impacts''. We do this by extending our
  decision process to a full-fledged decision problem (compare section \ref{section:theory}).
%\item[S1] The decision process is extended to a full-fledged
%  decision problem. This encodes a goal of decision making. For example,
%  ``saving the child'' or ``avoiding states that are committed to severe
%  climate change impacts''.

  
\item[S2] Verified ``best'' and ``conditional worst'' decisions are
  compared at the specific state at which we want to measure how much
  decisions matter for the goal encoded in S1.

\item[S3] We define a degree of responsibility consistent with this measure.
  %% \REMARK{Nuria}{I'd rather not use ``step'' here to avoid confusion
  %%   with the steps of the decision problem. Maybe ``stage'' or
  %%   ``phase''?}
  %%
  %% Nicola: we'll do this if reviewers turn out to be confused.

\end{itemize}


\noindent
This is how the theory of section \ref{section:theory} is applied to
implement the idea outlined in section \ref{subsection:about} for
measuring how much decisions under uncertainty matter.
%
For concreteness, we illustrate S1--S3 for the decision problem of
section \ref{section:specification}. The extensions of the theory
discussed in this section, however, are fully generic and can be applied
to arbitrary decision processes.
%
% TR, 19.06.22: I think that the next subsection discusses preliminaries and
%   does not yet belong to S1...
First, however, let's discuss a general condition any measure of how
much decisions matter (for whatever goal) should satisfy.
%We tackle step one by first discussing conditions under which decisions
%shall not matter.

\subsection{When decisions shall not matter}
\label{subsection:not_matter}

%TR, 19.06.22
A responsibility measure has to attribute a non-negative number to
the states of a decision process (e.g. the GHG emissions decision process
specified in section \ref{section:specification}):
%Consider the problem of attributing a non-negative number to the states
%of the GHG emissions decision process specified in section
%\ref{section:specification}:

< mMeas : (t : Nat) -> X t -> NonNegDouble

The idea is that \cs{|mMeas t x|} represents how much decisions in state \cs{|x|}
(at step~\cs{|t|}) do matter: the larger, the more the decisions in \cs{|x|} matter.
%
For the time being, assume that \cs{|mMeas t x|} takes values between zero
and one. Under which conditions shall we require it to be zero?
Certainly, we would like \cs{|mMeas t x|} to be zero whenever only
one option is available to the decision maker in \cs{|x|}:

< mMeasSpec1 : (t : Nat) -> (x : X t) -> Singleton (Y t x) -> mMeas t x = zero

Here, we have formalized the condition that only one option is available
to the decision maker in \cs{|x|} with the predicate \cs{|Singleton (Y t x)|}. We do
not need to be concerned with the exact definition of \cs{|Singleton|}:
it is a component of our language and \cs{|Singleton A|} posits that there is
only one value of type \cs{|A|} in a concise and precise way.

The specification \cs{|mMeasSpec1|} is consistent with \emph{avoidance}, one
of the three conditions put forwards in \citep{10.1111/ecoj.12507} under
which ``a person can be ascribed responsibility for a given
outcome''. The other two conditions are \emph{agency} (the capability to
act intentionally, to plan, and to distinguish between desirable and
undesirable outcomes) and \emph{causal relevance}.

The notion of causality is not uncontroversial \citep{carnap1995} and
its role in formalizations of responsibility has been addressed, among
others by \citep{10.5555/1622487.1622491, 10.1109/QEST.2006.9} and
\citep{halpern2014cause}. In the next section we show that, at least for
sequential decision processes, it is possible to define ``meaningful''
measures of how much decisions matter without having to deal with
causality. In section \ref{subsection:rmeas}, we discuss the relation
between these measures and responsibility measures.


\subsection{S1: Encoding goals of decision making}
\label{subsection:em}

%TR, 19.06.22: hopefully clearer:
To measure how much decisions matter \emph{with respect to a specific goal},
we extend our decision process to a decision problem by encoding this goal into
definitions for the components \cs{|Val|}, \cs{|reward|}, \cs{|meas|}, \cs{|<+>|},
\cs{|<=|} and \cs{|zero|} discussed in section \ref{section:theory}. The theory
then allows the computation of \emph{best} (and, as will be described below,
\emph{conditionally worst}) decisions for attaining \emph{that given goal}. From
these we will then define the measure of responsibility.

Note, however, that the decision problem thus specified is just a means to enable
the definition of our measure of responsibility, and does \emph{not} depend on
the aims and preferences of an actual decision maker. 

%To measure how much decisions matter, we have to extend a decision
%process to a full-fledged decision problem. First and foremost, this
%requires specifying which goals shall inform decision making.
%

%This value judgment does \emph{not} depend on the aims or on the
%preferences of the decision maker and, as discussed in section
%\ref{section:theory}, has to be specified by providing the definitions of
%%\cs{|Val|}, \cs{|reward|}, \cs{|meas|}, \cs{|<+>|}, \cs{|<=|} and \cs{|zero|} from sections.
%\cs{|Val|}, \cs{|reward|}, \cs{|meas|}, \cs{|<+>|}, \cs{|<=|} and \cs{|zero|}.
%
%
%TR 19.06.22: moved down: example to "For example...", generic goal functions
% to the end of the section
%In the following, we define these components for our stylized decision
%process. In section \ref{section:generic}, we discuss generic goal
%functions and show how to automate the definition of \cs{|Val|}, \cs{|reward|},
%etc. for such functions.

% TR, 19.06.22 : moved this up to (S1)
%As a starting point, we need to define the goal for which we
%seek responsibility measures. In short, we have to say \emph{for what}
%we want to measure how much decisions matter.

For example, in our stylized GHG emissions decision process, we might be
interested in measuring how much decisions matter for avoiding states that
are committed to severe impacts from climate change. Or
perhaps we want to measure how much decisions matter for avoiding
climate change impacts but also economic downturns. This
can be done by defining

%if False

We need to define \cs{|Val|}, \cs{|<+>|}, \cs{|zero|}, \cs{|<=|} and \cs{|lteTP|} for actually
computing optimal policies, below.

> Theory.Val = NonNegDouble

> Theory.(<+>) = (+)

> Theory.zero = cast 0.0

> Theory.(<=) = LTE

> Theory.lteTP = totalPreorderLTE

%endif

< Theory.reward t x y x'  =  if isCommitted (S t) x' then (cast 0.0) else (cast 1.0)

or

> Theory.reward t x y x'  =  if isCommitted (S t) x' || isDisrupted (S t) x' then (cast 0.0) else (cast 1.0)

with \cs{|Val = NonNegDouble|} and \cs{|<+>|}, \cs{|<=|} and \cs{|zero|} set to their
canonical values for non-negative double precision floating point
numbers. A special attention has to be taken in defining the measure
function \cs{|meas|}. Here, we follow standard decision theory and take
\cs{|meas|} to be the expected value measure

> Theory.meas = expectedValue

but see section \ref{subsection:caveats} for alternative formulations.

In section \ref{section:generic}, we discuss generic goal
functions and show how to automate the definition of \cs{|Val|}, \cs{|reward|},
etc. for such functions.

\subsection{S2: Measuring how much decisions matter}
\label{subsection:mmeas}

With a goal (avoiding climate change impacts but also economic
downturns) encoded via the reward function, we have now extended the
decision process of section \ref{section:specification} to a decision
problem.
%
This allows us to tackle the problem of measuring how much decisions in
a state do matter for that goal. For concreteness, let's consider the
initial state \cs{|DHU|} of our decision problem. In this state, the decision
maker has two options: start a green transition or further delay it.
%
Remember that our decision maker is effective only to a certain
extent. As shown in figure \ref{fig:network}, a decision to start a
green transition may well yield a next state in which the transition has
been delayed. According to section \ref{section:specification}, the
probability of this event is \cs{|pD_Start|}, that is, 10\%.

What does this uncertainty imply for the decision to be taken in the
initial state \cs{|DHU|}? Answering this question rigorously requires fixing
a decision \emph{horizon}. This is the number of decision steps of our
decision process that we look ahead in order to measure how much
decisions matter.
%
Remember from section \ref{section:theory} that the value of taking zero
decision steps is always \cs{|zero : Val|}, a problem-specific reference
value that holds for every decision step and state at that step.
%
Thus, if we look forward zero steps, no decision matters,
%
%% \REMARK{Nuria}{``matter nothing'' sounds off... maybe rather ``no
%%   decision matters, independently of the decision step and state''?}
%
%% Nicola: done
%
independently of the decision step and state. But, for a strictly
positive number of decision steps, we can formulate and rigorously
answer the following two questions

\begin{enumerate}

\item Is it better, in \cs{|DHU|} to (decide to) start or to delay a green
transition?

\item How much does this decision matter (for avoiding climate change
impacts but also economic downturns)?

\end{enumerate}

\noindent
To do so, we first apply generic backward induction from section
\ref{section:theory} and compute an optimal sequence of policies \cs{|ps|}
over the horizon.

Remember that \cs{|bi|} fulfills \cs{|biLemma|}\footnote{If \cs{|<=|}, \cs{|<+>|}, \cs{|meas|},
  etc. fulfill the specifications from section
  \ref{subsection:solution_components}, see \citep{idrislibs2} for full
  machine-checked proofs.}. This means that no other policy sequence
entails better decisions (again, for the goal of avoiding climate change
impacts but also economic downturns) than \cs{|ps|}.
%
Thus, we can compute a best decision and the (expected) value (of the
sum of the rewards associated with avoiding climate change impacts and
economic downturns) over a horizon of \cs{|n|} steps for arbitrary states:

%if False

> {-
> best : (t, n : Nat) -> X t -> IO ()
> best t    Z     x  =  do  putStrLn ("The horizon must be greater than zero!")
> best t (  S m)  x  =  do  ps <- pure (bi t (S m))
>                           bestDec <- pure ((head ps) x)
>                           bestVal <- pure (val ps x)
>                           putStrLn (  "Horizon, best decision in " ++
>                                       showX {t} x ++ ", its value:  " ++
>                                       show (S m) ++ ",  " ++
>                                       showY {t} {x} bestDec ++ ",  " ++
>                                       show bestVal)
> -}

%endif

> best : (t, n : Nat) -> X t -> String
> best t    Z     x  =  "The horizon must be greater than zero!"
> best t (  S m)  x  =
>   let ps  =  bi (S t) m in
>   let p   =  bestExt ps in
>   let b   =  p x in
>   let vb  =  val (p :: ps) x in
>   "Horizon, best, value:  " ++ show (S m) ++ ",  " ++ show b ++ ",  " ++ show vb

\noindent
What is a best decision in \cs{|DHU|} for an horizon of only one step?

< *Responsibility> :exec best 0 1 DHU
< Horizon, best, value:  1,  Delay,  0.468

This is not very surprising: from the definition of \cs{|next 0 DHU Start|}
from section \ref{section:specification}, the probability of entering
states that are either economically disrupted or committed to severe
impacts from climate change is 0.708. Thus, the expected value of
deciding to start a green transition is only

< 1 - 0.708 = 0.292

By contrast, the expected value of deciding to delay a green transition
is 0.468, as seen above. As it turns out, one has to look forward at
least over three decision steps (or, in our interpretation, about three
decades) for the decision to start a green transition to become a best
decision in \cs{|DHU|}. We can apply the computation

> bests : (t : Nat) -> List Nat -> X t -> IO ()
> bests  t       Nil   x  =  putStrLn "done!"
> bests  t  (n :: ns)  x  =  do  putStrLn (best t n x)
>                                bests  t  ns  x

to study how best decisions vary with the horizon. Again, for \cs{|x = DHU|}
one obtains:

< *Responsibility> :exec bests 0 [1..8] DHU
< Horizon, best, value: 1, Delay, 0.468
< Horizon, best, value: 2, Delay, 0.635454
< Horizon, best, value: 3, Start, 0.940669612
< Horizon, best, value: 4, Start, 1.250012318344
< Horizon, best, value: 5, Start, 1.533635393558128
< Horizon, best, value: 6, Start, 1.790773853744118
< Horizon, best, value: 7, Start, 2.022874449805313

As anticipated, the decision to start a green transition at the first
decision step becomes a best decision for horizons of three or more
decisions. The other way round: our decision maker would have to be very
myopic (or, equivalently very much discount future benefits) to conclude
that delaying a green transition is a best decision in \cs{|DHU|}. 

But how much does this decision actually matter? To answer this
question, we need to compare a best decision in \cs{|DHU|} \emph{for a
given time horizon} to a conditional worst decision. What does
``conditional worst'' mean in this context? Again, for concreteness,
let's for the moment fix the horizon to 7 decision steps.

What is the value (again, in terms of the sum of the rewards associated
with avoiding climate change impacts and economic downturns) of deciding
to delay a green transition in \cs{|DHU|}? There are different ways of
answering this question, but a canonical one\footnote{We discuss
  alternative approaches in section \ref{subsection:caveats}.} is to
consider the consequences of deciding to delay a green transition at the
first decision step in \cs{|DHU|} \emph{and} take later decisions
optimally.
%
The approach is canonical because it corresponds to a well-established
notion: that of stability with respect to local, not necessarily small,
perturbations. In game theory, the notion is often called ``trembling
hands'' (see \citep{sep-game-theory}, section 2.8) and was originally
put forward by R.~Selten in 1975 \citep{seltenreexamination}. In our
specific problem, it corresponds to considering the impact of a mistake
(trembling hands) at the decision point at stake (\cs{|DHU|}) under the
assumption that future generations will act rationally to avoid negative
impacts from climate change and economic downturns.

%% or, in other words, that later decision makers share our values and
%% goals. Notice, however, that, in a competitive situation with multiple
%% decision makers, the same assumption has a different interpretation.
%
%
%% \REMARK{Nuria}{It seems to me that this footnote would be sufficiently 
%% important to be discussed the main text.}
%
%% Nicola : done

If we denote our optimal policy sequence for an horizon of 7 steps by
\cs{|ps|}, we can compute the consequences of deciding to delay at the first
decision step in \cs{|DHU|} and then take later decisions optimally by
replacing the first policy of \cs{|ps|} with one that recommends \cs{|Delay|} in
\cs{|DHU|}:

> ps  :  PolicySeq 0 7
> ps  =  bi 0 7

> ps' : PolicySeq 0 7
> ps' = (setInTo (head ps) DHU Delay ) :: tail ps

The function \cs{|setInTo|} in the definition of \cs{|ps'|} is a higher-order
primitive: it takes a function (in this case the first policy of \cs{|ps|}),
a value in its domain and one in its codomain, and returns a function of
the same type that fulfills the specification

< (setInTo f a b) a = b ! && ! Not (a = a') -> (setInTo f a b) a' = f a'

for all \cs{|f|}, \cs{|a|}, \cs{|a'|} and \cs{|b|} of appropriate type. With \cs{|ps'|}, we can
compute the value of deciding to delay a green transition
at the first decision step in \cs{|DHU|}:

< *Responsibility> :exec show [val ps DHU, val ps' DHU]
< "[2.022874449805313, 1.672795254555656]"

The difference between the value of \cs{|ps|} and the value of \cs{|ps'|} in \cs{|DHU|}
then is a measure of \emph{how much decisions in \cs{|DHU|} matter for
avoiding climate change impacts and economic downturns over a time
horizon of 7 decision steps}: the bigger this difference, the more the
decision matters.

%

\subsection{S3: Responsibility measures}
\label{subsection:rmeas}

%
%% \REMARK{Nuria}{Below is where the "Step i" seem a bit unfortunate when
%%   used together with "n decision steps".}
%
%% Nicola: I think it's acceptable, we have the Steps of our
%% construction and we have the decision steps of the process.
%
We have argued that the difference between the value of \cs{|ps|} and the
value of \cs{|ps'|} in \cs{|DHU|}, is a measure of how much decisions in \cs{|DHU|}
matter for avoiding climate change impacts and economic downturns over a
time horizon of 7 decision steps. This argument is justified because:

\begin{itemize}

\item We have defined optimal policy sequences to be policy sequences
that avoid (as well as it gets) climate change impacts and economic
downturns (S1).

\item Over 7 decision steps, \cs{|ps|} is a \emph{verified
optimal} policy sequence.

\item The best decision in \cs{|DHU|} is to start a green
  transition\footnote{Remember from section \ref{subsection:FAC} that
    \cs{|head ps x = (head ps) x|} and that \cs{|head|} is a function that returns
    the first element of a vector. Thus, \cs{|head ps|} is a policy and we
    can apply it to a state to obtain a control.}:

< *Responsibility> :exec show (head ps DHU)
< "Start"

\item \cs{|ps'|} is a sequence of policies identical to \cs{|ps|} except for
recommending \cs{|Delay|} instead of \cs{|Start|} in \cs{|DHU|} and for the first
decision step:

< *Responsibility> :exec show (head ps' DHU)
< "Delay"

\end{itemize}

\noindent
These facts are sufficient to guarantee that the difference between the
value of \cs{|ps|} and the value of \cs{|ps'|} in \cs{|DHU|} is actually the difference
between the value (in terms of avoided climate change impacts and
economic downturns over 7 decision steps) of the best and of the worst
decisions that can be taken in \cs{|DHU|}.

The computation and the definitions of \cs{|ps|} and |ps'| suggest a
refinement and an implementation of the measure of how much decisions
matter \cs{|mMeas|} put forward in the beginning of this section. First, we
want \cs{|mMeas|} to depend on a time horizon \cs{|n|}. Second,
%% for the sake of simplicity, 
we want \cs{|mMeas|} to return plain double precision
floating point numbers%\footnote{Because of \cs{|biLemma|}, we know that \cs{|v - v'|} cannot be negative!}
:
%
%% \REMARK{Nuria}{I would drop this footnote -- it rather seems to raise
%%   the question why one would not want to use the more precise type
%%   instead of motivating the use of plain doubles now.}
%
%% Nicola: done

< mMeas : (t : Nat) -> (n : Nat) -> X t -> Double
< mMeas t  Z    x  =  0.0
< mMeas t (S m) x  =  let ps  =  bi (S t) m in
<                     let v   =  toDouble (val (bestExt ps :: ps) x) in
<                     let v'  =  toDouble (val (worstExt ps :: ps) x) in
<                     v - v'

Remember that, in S1 \ref{subsection:em}, we have encoded the goal
of avoiding severe climate change impacts and economic downturns for
which we compute \cs{|mMeas|} through a function

< reward t x y : X (S t) -> NonNegDouble

that returns 0 for next states that are committed to severe climate
change impacts and economically disrupted and 1 otherwise. In this
formulation, the value 1 is completely arbitrary: it could be replaced
by any other positive number and perhaps discounted. This suggests that
measures of how much decisions matter should be normalized

> mMeas : (t : Nat) -> (n : Nat) -> X t -> Double
> mMeas t  Z    x  =  0.0
> mMeas t (S m) x  =  let ps  =  bi (S t) m in
>                     let v   =  toDouble (val (bestExt ps :: ps) x) in
>                     let v'  =  toDouble (val (worstExt ps :: ps) x) in
>                     if v == 0 then 0 else (v - v') / v

%if False

> -- if we also want to plot also the best and worst outcomes
> mMeas' : Monad M => (t : Nat) -> (n : Nat) -> X t ->
>          (Double, Double, Double, M (StateCtrlSeq t (S n)), M (StateCtrlSeq t (S n)))
> mMeas' t  Z    x  =  (0.0, 0.0, 0.0, pure (Last x), pure (Last x))
> mMeas' t (S m) x  =  let ps  =  bi (S t) m in
>                      let be  =  bestExt ps in
>                      let we  =  worstExt ps in
>                      let v   =  toDouble (val (be :: ps) x) in
>                      let v'  =  toDouble (val (we :: ps) x) in
>                      if v == 0 then (v, v, v', trj (be :: ps) x, trj (we :: ps) x)
>                      else ((v - v') / v, v, v', trj (be :: ps) x, trj (we :: ps) x)

> bestmMeas : (t, n : Nat) -> X t -> String
> bestmMeas t    Z     x  =  "The horizon must be greater than zero!"
> bestmMeas t (  S m)  x  =
>   let ps  =  bi (S t) m in
>   let p   =  bestExt ps in
>   let b   =  p x in
>   "Horizon, best, mMeas:  " ++
>   show (S m) ++ ",  " ++ show b ++ ",  " ++ show (mMeas t (S m) x)

> bestmMeass : (t : Nat) -> List Nat -> X t -> IO ()
> bestmMeass  t       Nil   x  =  putStrLn "done!"
> bestmMeass  t  (n :: ns)  x  =  do  putStrLn (bestmMeas t n x)
>                                     bestmMeass  t  ns  x

%endif

\noindent
Notice that, in states in which the control set is a singleton, any
policy has to return the same control. In particular, the best
extension and the worst extension of any policy sequence have to return
the same control. Therefore, \cs{|mMeas|} fulfills the avoidance condition
from \citep{10.1111/ecoj.12507} discussed in S1 \emph{per
construction}.
%
As a consequence, in \cs{|S|}-states, the measure is identically zero,
independently of the time horizon:

< *Responsibility> :exec show (mMeas 0 4 SHU)
< "0"

< *Responsibility> :exec show (mMeas 0 6 SLC)
< "0"

Notice also that \cs{|mMeas|} can be applied to estimate how much decisions
matter at later steps of a decision process. For example, we can assess
that, for our decision process and under a fixed time horizon, decisions
in \cs{|DHU|} at decision step 0 matter less than decisions in \cs{|DHU|} at later
steps:

< *Responsibility> :exec show (mMeas 0 7 DHU)
< "0.1730602684132721"

< *Responsibility> :exec show (mMeas 1 7 DHU)
< "0.5673067719100584"

< *Responsibility> :exec show (mMeas 3 7 DHU)
< "0.5673067719100584"

This is not surprising given that the best decision, in \cs{|DHU|} and for a
time horizon of 7 decision steps, is to start a green transition and
that, as stipulated in the introduction and specified in section
\ref{subsection:next} through

< pSpec9 : pC_D_0 `LTE` pC_D

the probability of entering states in which the world is committed to
future severe impacts from climate change is higher in states in which a
green transition has not already been started as compared to states in
which a green transition has been started.

\subsection{Wrap-up}
\label{subsection:wrapup}

Through S1, S2 and S3, we have introduced a measure of how much
decisions under uncertainty matter that fulfills the requirements for
responsibility measures put forward in the introduction.
%
%%\REMARK{Nuria}{Maybe use itemize for the following?}
%
%% Nicola: I think it is fine for the wrap-up to be colloquial.
%
It accounts for all the knowledge which is encoded in the specification
of a decision process, it is independent of the aims of a (real or
hypothetical) decision maker and it is \emph{fair} in the sense that all
decisions (decision makers) are measured in the same way.

Thus, we introduce \cs{|mMeas|} as a first example of responsibility
measure. In the next section, we generalize it by introducing a small
DSL for the specification of goals of sequential decision processes
under uncertainty and discuss alternative definitions.


%if False

> {-

> ---}

%endif
