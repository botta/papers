% -*-Latex-*-

%if False

> module Specification

> import Data.So
> import Data.Fin

> import FastSimpleProb.SimpleProb
> import FastSimpleProb.BasicOperations
> import NonNegDouble.NonNegDouble
> import NonNegDouble.Constants
> import NonNegDouble.BasicOperations
> import NonNegDouble.Operations
> import NonNegDouble.Predicates
> import NonNegDouble.Properties
> import Double.Predicates
> import List.Operations
> import Unit.Properties
> import Finite.Predicates

> import Theory

> %default total
> %access public export
> %auto_implicits off

%endif

\section{Specification of the stylized decision process}
\label{section:specification}

We specify the stylized GHG emissions decision process of the
introduction in the theory summarized in section \ref{section:theory}.
%
As a first step, we have to define the uncertainty monad \cs{|M|}. Our
decision process is a \emph{stochastic} process and thus

> Theory.M = SimpleProb

Here, \cs{|SimpleProb|} is a finite probability monad: for an arbitrary
type \cs{|A|}, a value of type \cs{|SimpleProb A|} is a list of pairs
\cs{|(A, NonNegDouble)|} together with a proof that the sum of the
\cs{|NonNegDouble|} elements of the pairs is positive. These are double
precision floating point numbers with the additional restriction
(remember section \ref{subsection:DT}) of being non-negative.

%

\subsection{States, controls}
\label{subsection:XY}

Second, we have to specify the states of the decision
process. Consistently with section \ref{subsection:stylized} and with
the notation introduced in section \ref{section:notation} we define:
%
%% \TODO{Nicola: compare to the representation via triples in
%%   |Specification1.lidr|.}
%%
%% Done (2021-10-04)
%

> data State = DHU | DHC | DLU | DLC | SHU | SHC | SLU | SLC
>
> Theory.X t = State

Third, we have to specify the controls of the decision process.  In the
introduction, we said that in states in which a green transition has not
already been started (that is, in \cs{|D|}-states), the decision maker has the
option of either starting or further delaying the
transition\footnote{Notice that we are using the term \emph{transition}
  to denote two different notions: the green transition of the decision
  process and the function \cs{|next|} of the theory discussed in section
  \ref{section:theory} in which we now specify such a process!}

> data StartDelay = Start | Delay

%if False

> Theory.Y t DHU = StartDelay
> Theory.Y t DHC = StartDelay
> Theory.Y t DLU = StartDelay
> Theory.Y t DLC = StartDelay

> Theory.Y t SHU = Unit
> Theory.Y t SHC = Unit
> Theory.Y t SLU = Unit
> Theory.Y t SLC = Unit

%endif

\noindent
However, if a a green transition has already been started, the decision
maker has no alternatives. We formalize this idea by defining the set of
controls in \cs{|S|}-states to be a singleton.
%
It will be useful to have two functions that test if a state is
committed to impacts from climate change and if the economic wealth
has taken a downturn:

< isCommitted, isDisrupted : (t : Nat) -> X t -> Bool

%if False

> isCommitted  :  (t : Nat) -> X t -> Bool
> isDisrupted  :  (t : Nat) -> X t -> Bool

%endif

\noindent
The idea is that \cs{|isCommitted|} (\cs{|isDisrupted|}) returns \cs{|True|} in
\cs{|C|}-states (\cs{|L|}-states) and \cs{|False|} in \cs{|U|}-states (\cs{|H|}-states). 

%

\subsection{The transition function}
\label{subsection:next}

Finally, we have to specify the transition function of the process. As
discussed in the introduction, this is defined in terms of transition
probabilities. 

\paragraph{The probabilities of starting a green transition}

Let's first specify the probability that a green transition is started,
conditional to the decision taken by the decision maker. Let
%
%%\TODO{Tim: improve the rendering of underlines in names.}
%% Nicola: done (2021-11-08)
%

> pS_Start : NonNegDouble

denote the probability that a green transition is started (during the
time interval between the current and the next decision step) given that
the decision maker has decided to start it. For a perfectly effective
decision maker, \cs{|pS_Start|} would be one. 
%
%% \REMARK{Nuria}{Maybe recall the type \cs{|NonNegDouble|} of probabilities and
%%   suppress the type declarations for the subsequent ones from the
%%   visible text.}
%
%% Nicola: too complicated interleaving of text and code!
%
Let's assume a 10\% chance that a decision to start a green transition
fails to be implemented, perhaps because of inertia of legislations, as
discussed in section \ref{subsection:stylized}:

> pS_Start = cast 0.9

Consistently, the probability that a green transition is delayed even if
the decision maker has chosen to start it is 

> pD_Start : NonNegDouble
> pD_Start = cast 1.0 - pS_Start

Similarly, we denote with \cs{|pD_Delay|} and \cs{|pS_Delay|} the probabilities that a green
transition is delayed (started) given that the decision maker has
decided to delay it. As a first step, we take \cs{|pS_Delay|} to be equal to \cs{|pD_Start|}

> pD_Delay  :  NonNegDouble
> pD_Delay  =  cast 0.9

> pS_Delay  :  NonNegDouble
> pS_Delay  =  cast 1.0 - pD_Delay

but we will come back to this choice in section \ref{section:application}.
%
%%\REMARK{Nuria}{This seems to be a missing TODO?}
%
%% Nicola: done
%
%% We want to make sure that the values of \cs{|pS_Start|}, \cs{|pD_Start|},
%% \cs{|pD_Delay|} and \cs{|pS_Delay|} are consistent with the assumption (remember
%% the informal description of our decision process from the introduction)
%% that our decision maker is, up to a certain degree, effective and
%% require them to fulfill the inequalities
%
%% \TODO{Nuria: add a brief remark on the application of type thery for
%%   sanity checking parameter values?}
%
%% Nicola: I do not think this is the right point to discuss the merits
%% and the drawbacks of TT.
%

%% < pSpec1 : pD_Start `LTE` pS_Start 
%% <
%% < pSpec2 : pS_Delay `LTE` pD_Delay 


\paragraph*{The probabilities of economic downturns.}

In the informal description of the decision process from section
\ref{subsection:stylized}, we said that an essential trait of the
decision process is that
%
\begin{quote}
\dots decisions to start a green transition, if implemented, are more
likely to yield states with a low level of economic wealth (\cs{|L|}-states)
than states with high economic wealth. This assumption reflects the fact
that starting a green transition requires more investments and costs
than just moving to states in which most of the work towards a globally
decarbonized society remains to be done.
\end{quote}
%

\noindent
We need to formulate this idea in terms of transition probabilities. Let
\cs{|pL_S_DH|} denote the probability of transitions to states with a low level of
economic wealth (\cs{|L|}) given that a green transition has been started
(\cs{|S|}) from delayed states (\cs{|D|}) with a high level of economic wealth
(\cs{|H|}).
%
Similar interpretations hold for \cs{|pL_S_DL|}, \cs{|pL_S_SH|}, \cs{|pL_S_SL|} and
their counterparts for the cases in which a green transition has been
delayed, \cs{|pL_D_DH|} and \cs{|pL_D_DL|}. Remember that in our decision process
%
\begin{quote}
\dots once a transition has been started, it cannot be halted or
reversed by later decisions or events.
\end{quote}
%
In terms of transition probabilities, this means that we do not need to
specify \cs{|pL_D_SH|} and \cs{|pL_D_SL|} because the probability of transitions
from \cs{|S|}-states to \cs{|D|}-states is zero.
%
We encode the requirement that ``decisions to start a green transition,
if implemented, are more likely to yield states with a low level of
economic wealth (\cs{|L|}-states) than states with high economic wealth'' by
the specification

%if False

> pL_S_DH  :  NonNegDouble
> pL_S_DL  :  NonNegDouble
> pL_S_SH  :  NonNegDouble
> pL_S_SL  :  NonNegDouble
> pL_D_DH  :  NonNegDouble
> pL_D_DL  :  NonNegDouble

%endif

< pSpec3 : pH_S_DH `LTE` pL_S_DH

Because \cs{|pH_S_DH = 1 - pL_S_DH|}, this requires \cs{|pL_S_DH|} to be greater
or equal to 50\%. Let's say that

> pL_S_DH = cast 0.7

We also want to express the idea that starting a green transition in a
weak economy (perhaps a suboptimal decision?) is more likely to yield a
weak economy than starting a green transition in a strong economy

< pSpec4 : pL_S_DH `LTE` pL_S_DL

which requires specifying a value of \cs{|pL_S_DL|} between 0.7 and 1.0, say

> pL_S_DL = cast 0.9

This fixes the values of \cs{|pL_S_DH|} and \cs{|pL_S_DL|} for our decision
process in the ranges imposed by the ``semantic'' constraints \cs{|pSpec3|}
and \cs{|pSpec4|}.
%
We discuss how these (and other) transition probabilities would have to
be estimated in a more realistic (as opposed to stylized) GHG emissions
decision process in section \ref{subsection:realistic}. 

Next, we have to specify the remaining transition probabilities
\cs{|pL_S_SH|}, \cs{|pL_S_SL|}, \cs{|pL_D_DH|} and \cs{|pL_D_DL|}. What are meaningful
constraints for these? Remember that \cs{|pL_S_SH|} and \cs{|pL_S_SL|} represent
the probabilities of transitions to low wealth states (\cs{|L|}-states) from \cs{|H|}
and \cs{|L|}-states, respectively, while an already started green transition is
accomplished.
%
In this situation, and again because of the inertia of economic systems,
it is reasonable to assume that transitions from \cs{|H|}-states (booming
economy) to \cs{|H|}-states are more likely than transitions from \cs{|H|}-states to
\cs{|L|}-states and, of course, the other way round. In formulas:

< pSpec5 : pL_S_SH `LTE` pH_S_SH
<
< pSpec6 : pH_S_SL `LTE` pL_S_SL

Again, because \cs{|pH_S_SH = 1 - pL_S_SH|} (and \cs{|pH_S_SL = 1 - pL_S_SL|}),
this requires \cs{|pL_S_SH|} and \cs{|pL_S_SL|} to be below and above 50\%,
respectively. 

In our decision process, a high value of \cs{|pL_S_SL|} implies a low
probability of recovering from economic downturns in states in which a
transition towards a globally decarbonized society has been started or
has been accomplished.
%
%% \TODO{Explain that, in a realistic GHG process, one might want to
%%   distinguish between these situations and that this is not done here
%%   because the purpose of our process is to illustrate a DSL for
%%   influence, responsibility and commitment, not \dots.}
%
%% Nicola: done
%
In more realistic specifications of GHG emission processes, one may want
to distinguish between these two cases, or even to keep track of the
time elapsed since a green transition was started and define the
probability of recovering from economic downturns
accordingly\footnote{As explained in the introduction, the main purpose
  of this paper is to present a novel approach towards measuring
  responsibility when decisions are to be taken under uncertainty. To
  this end, considering more realistic emission processes would be an
  unnecessary distraction.}.
%
%% \REMARK{Nuria, Tim}{I think the content of the footnote should be stated very
%%   clearly in the introduction (about/not about), and here does not need to
%%   be addressed again.}
%%
%% Nicola: done (2021-10-04)
%

Conversely, a low value of \cs{|pL_S_SH|} means high \emph{resilience}
against economic downturns in states in which a transition towards a
globally decarbonized society has been started or has been
accomplished.
%
In such states, we assume a moderate likelihood of fast recovering from
economic downturns:

> pL_S_SL  =  cast 0.7

and also a moderate resilience

> pL_S_SH  =  cast 0.3

Let's turn the attention to the last two transition probabilities that
need to be specified in order to complete the description of the
transitions leading to economic downturns or recoveries. These are
\cs{|pL_D_DH|} and \cs{|pL_D_DL|}.

The semantics of \cs{|pL_D_DH|} and \cs{|pL_D_DL|} should meanwhile be clear:
\cs{|pL_D_DH|} represents the probability of economic downturns and \cs{|1 -
pL_D_DL|} the probability of recovering (from economic downturns) in
states in which a green transition has not already been started. As for
their counterparts discussed above, we have the semantic requirements

< pSpec7  :  pL_D_DH `LTE` pH_D_DH
<
< pSpec8  :  pH_D_DL `LTE` pL_D_DL

with \cs{|pH_D_DH = 1 - pL_D_DH|} and \cs{|pH_D_DL = 1 - pL_D_DL|} and thus, by
the same argument as for \cs{|pL_S_SH|} and \cs{|pL_S_SL|}, \cs{|pL_D_DH|} and
\cs{|pL_D_DL|} below and above 50\%, respectively. 

How should \cs{|pL_D_DH|} and \cs{|pL_D_DL|} compare to \cs{|pL_S_SH|} and \cs{|pL_S_SL|}? Is
the likelihood of economic downturns in states in which a green
transition has not already been started higher or lower than the
likelihood of economic downturns in states in which a transition towards
a globally decarbonized society has been started or has been
accomplished? 
%
Realistic answers to this question are likely to depend on the decision
step and on the time elapsed since the green transition has been
started, see \ref{subsection:realistic}. As a first approximation, here
we just assume that these probabilities are the same:

> pL_D_DL  =  pL_S_SL
>
> pL_D_DH  =  pL_S_SH

This completes the discussion of the probabilities of economic
downturns and recoveries.

\paragraph*{The probabilities of commitment to severe impacts from
  climate change.}

The last ingredient that we need to fully specify the transition
function of our decision process are the probabilities of transitions to
states that are committed to severe impacts from climate change. In the
introduction, we have stipulated that

%
\begin{quote}
\dots we assume that the probability of entering states in which the
world is committed to future severe impacts from climate change is
higher in states in which a green transition has not already been
started as compared to states in which a green transition has been
started.
\end{quote}
%

\noindent
We account for this assumption with four transition probabilities:
\cs{|pU_S_0|}, \cs{|pU_D_0|}, \cs{|pU_S|} and \cs{|pU_D|}.
%
The first two represent the probabilities of transitions (from
uncommitted states) to uncommitted states at decision step zero for the
cases in which a transitions to a decarbonized economy has been
implemented and delayed, respectively. 
%
%% Remember that decisions are not implemented with certainty and that the
%% probability of entering states that are committed (uncommitted) only
%% depends on whether a green transition has been started or not, no matter
%% what the decision was.
%
Similarly, \cs{|pU_S|} and \cs{|pU_D|} represent the probabilities of transitions
from \cs{|U|}-states to \cs{|U|}-states at later decision steps. We take the informal
specification from section \ref{subsection:stylized} of the introduction
%
\begin{quote}
\dots delaying transitions to decarbonized
economies increases the likelihood of entering states in which the world
is committed to future severe impacts from climate change.
\end{quote}
%

%if False

> pU_S_0  :  NonNegDouble
>
> pU_D_0  :  NonNegDouble
>
> pU_S    :  NonNegDouble
>
> pU_D    :  NonNegDouble

%endif

\noindent
by the letter and, for the sake of simplicity, assume that the whole
increase in the likelihood of entering committed states takes place
during the first step of our decision process. This is a very crude
assumption and we will come back to it when we discuss the results of
measures of responsibility in section \ref{subsection:rmeas}.
%
With these premises (and keeping in mind that \cs{|pC_S_0 = 1 - pU_S_0|},
\cs{|pC_D_0 = 1 - pU_D_0|}, etc.) our informal specification translates into
the constraints:

< pSpec9   :  pC_S_0 `LTE` pU_S_0
<
< pSpec10  :  pC_S_0 `LTE` pC_D_0
<
< pSpec11  :  pC_S `LTE` pU_S
<
< pSpec12  :  pC_S `LTE` pC_D
<
< pSpec13  :  pC_D_0 `LTE` pC_D

%if False

> pU_S_0  =  cast 0.9
>
> pU_D_0  =  cast 0.7
>
> pU_S    =  cast 0.9
>
> pU_D    =  cast 0.3

%endif

\noindent
For the time being, we set \cs{|pU_S_0|}, \cs{|pU_D_0|}, \cs{|pU_S|} and \cs{|pU_D_0|} to
0.9, 0.7, 0.9 and 0.3, respectively.
%
In words, we assume a 30\% chance of committing to future severe impacts
from climate change if we fail to start a green transition at the first
decision step. We assume this chance to increase to 70\% at later
decision steps.
%
We also assume a 10\% chance of severe climate change impacts if
we start a green transition at the first decision step or later. We will
come back to these numbers in section \ref{subsection:impacts3}.
%
%% \TODO{Nuria: make the comments invisible}
%% Nicola: done (2021-10-27)
%

%if False

> pH_S_DH  :  NonNegDouble
> pH_S_DH  =  cast 1.0 - pL_S_DH

> pH_S_SH  :  NonNegDouble
> pH_S_SH  =  cast 1.0 - pL_S_SH

> pH_S_DL  :  NonNegDouble
> pH_S_DL  =  cast 1.0 - pL_S_DL

> pH_S_SL  :  NonNegDouble
> pH_S_SL  =  cast 1.0 - pL_S_SL

> pH_D_DH  :  NonNegDouble
> pH_D_DH  =  cast 1.0 - pL_D_DH

> pH_D_DL  :  NonNegDouble
> pH_D_DL  =  cast 1.0 - pL_D_DL

> pC_S_0   :  NonNegDouble
> pC_S_0   =  cast 1.0 - pU_S_0

> pC_D_0   :  NonNegDouble
> pC_D_0   =  cast 1.0 - pU_D_0

> pC_S     :  NonNegDouble
> pC_S     =  cast 1.0 - pU_S

> pC_D     :  NonNegDouble
> pC_D     =  cast 1.0 - pU_D


> -- pSpec1   :  pD_Start `LTE` pS_Start 
> -- pSpec1   =  MkLTE Oh

> -- pSpec2   :  pS_Delay `LTE` pD_Delay 
> -- pSpec2   =  MkLTE Oh

> pSpec3   :  pH_S_DH `LTE` pL_S_DH
> pSpec3   =  MkLTE Oh

> pSpec4   :  pL_S_DH `LTE` pL_S_DL
> pSpec4   =  MkLTE Oh

> pSpec5   :  pL_S_SH `LTE` pH_S_SH
> pSpec5   =  MkLTE Oh

> pSpec6   :  pH_S_SL `LTE` pL_S_SL
> pSpec6   =  MkLTE Oh

> pSpec7   :  pL_D_DH `LTE` pH_D_DH
> pSpec7   =  MkLTE Oh

> pSpec8   :  pH_D_DL `LTE` pL_D_DL
> pSpec8   =  MkLTE Oh

> pSpec9   :  pC_S_0 `LTE` pU_S_0
> pSpec9   =  MkLTE Oh

> pSpec10  :  pC_S_0 `LTE` pC_D_0
> pSpec10  =  MkLTE Oh

> pSpec11  :  pC_S `LTE` pU_S
> pSpec11  =  MkLTE Oh

> pSpec12  :  pC_S `LTE` pC_D
> pSpec12  =  MkLTE Oh

%endif

\paragraph*{The transition function.}

With the transition probabilities in place, we can now specify the
transition function of the decision process.
%
We proceed by cases, starting from transitions at step zero. The first
case is the one in which the initial state is \cs{|DHU|} and the decision
was to start a green transition:

> Theory.next Z DHU Start = mkSimpleProb 
>   
>   [  (DHU,  pD_Start  *  pH_D_DH  *  pU_D_0), 
>     
>      (DHC,  pD_Start  *  pH_D_DH  *  pC_D_0),
>        
>      (DLU,  pD_Start  *  pL_D_DH  *  pU_D_0), 
>        
>      (DLC,  pD_Start  *  pL_D_DH  *  pC_D_0),
>        
>      (SHU,  pS_Start  *  pH_S_DH  *  pU_S_0), 
>        
>      (SHC,  pS_Start  *  pH_S_DH  *  pC_S_0),
>        
>      (SLU,  pS_Start  *  pL_S_DH  *  pU_S_0), 
>        
>      (SLC,  pS_Start  *  pL_S_DH  *  pC_S_0)]

In the above definition, \cs{|mkSimpleProb|} is a function that (for an
arbitrary type \cs{|A|}) takes a list of pairs \cs{|(A, NonNegDouble)|} and
returns a value of type \cs{|M A = SimpleProb A|} that is, a finite
probability distribution on \cs{|A|}. The sum of the probabilities of the
list elements has to be strictly positive, thus the resulting
probability distributions are sound per construction.

The interpretation of \cs{|next Z DHU Start|} is straightforward given the
transition probabilities introduced in the previous paragraphs.
%
We only comment the definition of the probability of \cs{|SHU|}, the state in
which a green transition has been started, the economy is in a wealthy
state and the world is not committed to future severe impacts from
climate change.

This probability is defined by the product of three transition
probabilities: the probability that a green transition is actually
implemented, given that the decision was to do so \cs{|pS_Start|}; the
probability that the economy is in a good state (an \cs{|H|}-state) given that
a green transition has been started from an \cs{|H|}-state \cs{|pH_S_DH|}; and the
probability of entering states that are not committed to severe impacts
from climate change, again given that a transition to a decarbonized
economy has been started \cs{|pU_S_0|}.

Notice that \cs{|pC_D_0 + pU_D_0|} and \cs{|pC_S_0 + pU_S_0|} are equal to one by
definition of \cs{|pC_D_0|} and \cs{|pC_S_0|}. The same holds for \cs{|pH_D_DH +
pL_D_DH|} and \cs{|pH_S_DH + pL_S_DH|} (by definition of \cs{|pH_D_DH|}, \cs{|pH_S_DH|})
and for \cs{|pD_Start + pS_Start|} (by definition of \cs{|pD_Start|}). 
%
It follows that the sum of the probabilities of \cs{|next Z DHU Start|} is
one, as one would expect.

We can derive the probability of \cs{|SHU|} (and of all other possible
next states) given the decision to \cs{|Start|} a green transition in
\cs{|DHU|}:

< pS_Start  *  pH_S_DH  *  pU_S_0

rigorously if we represent our decision process as a Bayesian belief
network. To this end, it is useful to introduce some notation from
elementary probability theory. Different textbooks adopt slightly
different notations; here, we follow \citep{10.5555/541177} and denote
the conditional probability of entering \cs{|SHU|} given the decision to
\cs{|Start|} a green transition in \cs{|DHU|} with \cs{|PSHU_StartDHU|}.
Thus, our obligation is to show

< PSHU_StartDHU = pS_Start  *  pH_S_DH  *  pU_S_0

Let \cs{|x1|}, \cs{|x2|}, \cs{|x3|} denote the ``components'' of the \emph{current}
state \cs{|x : X t|} and \cs{|x1'|}, \cs{|x2'|}, \cs{|x3'|} the components of the
\emph{next} state. Thus, for \cs{|x = DHU|}, we have \cs{|x1 = D|}, \cs{|x2 = H|} and
\cs{|x3 = U|}. As usual, we denote a decision in \cs{|x|} at step \cs{|t|} with \cs{|y : Y
t x|}.

The \emph{variables} \cs{|x1|}, \cs{|x2|}, \cs{|x3|}, \cs{|y|}, \cs{|x1'|}, \cs{|x2'|}, \cs{|x3'|} and the
decision step \cs{|t|} are associated with the \emph{nodes} of the Bayesian
network of figure \ref{fig:network}. The \emph{edges} of the network
encode the notion of conditional dependency: the arrow between \cs{|x1|} and
\cs{|x2'|} posits that the probability of transitions to states with a low
(high) economic wealth depends on whether a green transition is
currently underway or has been delayed\footnote{Because of the arrows
  from \cs{|x2|} and \cs{|x1'|} to \cs{|x2'|}, such probability also depends on
  whether the current state of the economy is low or high and on whether
  a green transition gets started or not. }.

\begin{figure}
\begin{center}
\hspace*{1.5cm}\scalebox{0.65}{
%include figureNetwork1.lidr
%\input{figureNetwork1.tex}
}
\end{center}
\caption{Stylized decision process as a Bayesian network.}
\label{fig:network}
\end{figure}

The conditional probability tables associated with the nodes encode such
probabilities.
%% \TODO{Nuria: transpose the tables to support the natural interpretation:
%%   \cs{|t1 S Start = P(S mid Start)|}.}.
Thus, for example, the
table associated with \cs{|x1'|} posits that the conditional probability of
entering \cs{|S|}-states given that the decision (variable \cs{|y|}) was to \cs{|Start|}
a green transition is \cs{|pS_Start|} as discussed above.
%
Similarly, the table associated with \cs{|x2'|} encodes the specification
that the probability of entering an \cs{|L|}-state given that an \cs{|S|}-state was
entered from a current \cs{|D|}- and \cs{|H|}-state is \cs{|pL_S_DH|}\footnote{Notice that
  the conditional probability table associated with \cs{|x2'|} contains an
  undefined value $\alpha$. This is because the probability of entering
  \cs{|L|} (or \cs{|H|}) states given that a \cs{|D|}-state was entered starting from an
  \cs{|S|}-state is irrelevant: the probability of transitions from \cs{|S|}-states to
  \cs{|D|}-states is zero (remember that we have assumed that green transitions
  cannot be halted or reversed by later decisions), as encoded in the
  third row of the table associated with \cs{|x1'|}.}.
%
We can now derive \cs{|PSHU_StartDHU|} from the Bayesian network
representation of our decision process by equational reasoning. The
computation is straightforward but we spell out each single step for
clarity:

< PSHU_StartDHU
<
<   = -- definition of \cs{|x1'|} ... \cs{|y|} ... \cs{|x3|}
<
< P(x1' = S, x2' = H, x3' = U mid y = Start, x1 = D, x2 = H, x3 = U)
<
<   = -- definition of conditional probability, set theory
<
< P(x2' = H, x3' = U, x1' = S mid y = Start, x1 = D, x2 = H, x3 = U)
<
<   = -- chain rule
<
< P(x2' = H mid x3' = U, x1' = S, y = Start, x1 = D, x2 = H, x3 = U) *
< P(x3' = U, x1' = S mid y = Start, x1 = D, x2 = H, x3 = U)
<
<   = -- chain rule
<
< P(x2' = H mid x3' = U, x1' = S, y = Start, x1 = D, x2 = H, x3 = U) *
< P(x3' = U mid x1' = S, y = Start, x1 = D, x2 = H, x3 = U) *
< P(x1' = S mid y = Start, x1 = D, x2 = H, x3 = U)
<
<   = -- Bayesian network (conditional independence)
<
< P(x2' = H mid x1' = S, x1 = D, x2 = H) *
< P(x3' = U mid x1' = S, x3 = U) *
< P(x1' = S mid y = Start)
<
<   = -- Bayesian network (tables)
<
< pH_S_DH * pU_S_0 * pS_Start

Similar derivations can be obtained, in terms of the network of figure
\ref{fig:network}, for the other transition probabilities that define
\cs{|next Z DHU Start|} and, in fact, for all the transition probabilities
that define \cs{|next|}. Thus, figure \ref{fig:network} is in fact a compact
representation of the transition function \cs{|next|} of our decision process. 
%
Notice that the causal networks at the core of the storyline approach
\citep{shepherd2019} are also Bayesian belief networks, albeit without
a clearcut distinction between state and control spaces.

%
%% \REMARK{Nicola}{Tim suggests (in ruu\_draft\_remarks\_TiRi\_210804.txt)
%%   to cut the next two pages. Perhaps give the complete specification
%%   of \cs{|next|} in an appendix?}
%
%% Nicola: done (2021-10-04)
%

The case in which the initial state is \cs{|DHU|} and the decision was to
delay a green transition is similar to the first case with \cs{|pD_Start|} and
\cs{|pS_Start|} replaced by \cs{|pD_Delay|} and \cs{|pS_Delay|}, respectively:

> Theory.next Z DHU Delay = mkSimpleProb 
>   
>   [  (DHU,  pD_Delay  *  pH_D_DH  *  pU_D_0), 
>     
>      (DHC,  pD_Delay  *  pH_D_DH  *  pC_D_0),
>        
>      (DLU,  pD_Delay  *  pL_D_DH  *  pU_D_0), 
>        
>      (DLC,  pD_Delay  *  pL_D_DH  *  pC_D_0),
>        
>      (SHU,  pS_Delay  *  pH_S_DH  *  pU_S_0), 
>        
>      (SHC,  pS_Delay  *  pH_S_DH  *  pC_S_0),
>        
>      (SLU,  pS_Delay  *  pL_S_DH  *  pU_S_0), 
>        
>      (SLC,  pS_Delay  *  pL_S_DH  *  pC_S_0)]

The cases in which the initial states are \cs{|DHC|}, \cs{|DLU|},
\cs{|DLC|}, \cs{|SHU|}, \cs{|SHC|}, \cs{|SLU|} and \cs{|SLC|} are
analogous to the \cs{|DHU|} case and complete the specification of the
transition function at decision step zero. The transition function at
step one or greater is perfectly analogous with \cs{|pU_D|},
\cs{|pC_D|}, \cs{|pU_S|} and \cs{|pC_S|} in place of \cs{|pU_D_0|},
\cs{|pC_D_0|}, \cs{|pU_S_0|} and \cs{|pC_S_0|}, respectively. Interested
readers can find the full specification of the transition function
\citep{papers}, see file ``Specification.lidr'' in folder
``2021.Responsibility under uncertainty: which climate decisions matter
most?''
  
%if False

The case in which the initial state is \cs{|DHC|} is simpler because the
probability of transitions to \cs{|U|}-states is zero:

> Theory.next Z DHC Start = mkSimpleProb 
>   
>   [  (DHC,  pD_Start  *  pH_D_DH),
>        
>      (DLC,  pD_Start  *  pL_D_DH),
>        
>      (SHC,  pS_Start  *  pH_S_DH),
>        
>      (SLC,  pS_Start  *  pL_S_DH)]

> Theory.next Z DHC Delay = mkSimpleProb 
>   
>   [  (DHC,  pD_Delay  *  pH_D_DH),
>        
>      (DLC,  pD_Delay  *  pL_D_DH),
>        
>      (SHC,  pS_Delay  *  pH_S_DH),
>        
>      (SLC,  pS_Delay  *  pL_S_DH)]

The cases in which the initial states are \cs{|DLU|} and \cs{|DLC|} are, mutatis
mutandis, equivalent to the \cs{|DHU|} and \cs{|DHC|} cases:

> Theory.next Z DLU Start = mkSimpleProb 
>   
>   [  (DHU,  pD_Start  *  pH_D_DL  *  pU_D_0), 
>     
>      (DHC,  pD_Start  *  pH_D_DL  *  pC_D_0),
>        
>      (DLU,  pD_Start  *  pL_D_DL  *  pU_D_0), 
>        
>      (DLC,  pD_Start  *  pL_D_DL  *  pC_D_0),
>        
>      (SHU,  pS_Start  *  pH_S_DL  *  pU_S_0), 
>        
>      (SHC,  pS_Start  *  pH_S_DL  *  pC_S_0),
>        
>      (SLU,  pS_Start  *  pL_S_DL  *  pU_S_0), 
>        
>      (SLC,  pS_Start  *  pL_S_DL  *  pC_S_0)]

> Theory.next Z DLU Delay = mkSimpleProb 
>   
>   [  (DHU,  pD_Delay  *  pH_D_DL  *  pU_D_0), 
>     
>      (DHC,  pD_Delay  *  pH_D_DL  *  pC_D_0),
>        
>      (DLU,  pD_Delay  *  pL_D_DL  *  pU_D_0), 
>        
>      (DLC,  pD_Delay  *  pL_D_DL  *  pC_D_0),
>        
>      (SHU,  pS_Delay  *  pH_S_DL  *  pU_S_0), 
>        
>      (SHC,  pS_Delay  *  pH_S_DL  *  pC_S_0),
>        
>      (SLU,  pS_Delay  *  pL_S_DL  *  pU_S_0), 
>        
>      (SLC,  pS_Delay  *  pL_S_DL  *  pC_S_0)]

> Theory.next Z DLC Start = mkSimpleProb 
>   
>   [  (DHC,  pD_Start  *  pH_D_DL),
>        
>      (DLC,  pD_Start  *  pL_D_DL),
>        
>      (SHC,  pS_Start  *  pH_S_DL),
>        
>      (SLC,  pS_Start  *  pL_S_DL)]

> Theory.next Z DLC Delay = mkSimpleProb 
>   
>   [  (DHC,  pD_Delay  *  pH_D_DL),
>        
>      (DLC,  pD_Delay  *  pL_D_DL),
>        
>      (SHC,  pS_Delay  *  pH_S_DL),
>        
>      (SLC,  pS_Delay  *  pL_S_DL)]

The cases in which the initial states are \cs{|SHU|} and \cs{|SHC|} are more
interesting: in these cases the decision maker has no alternatives, the
control set is a singleton and the probability of transitions to
\cs{|D|}-states is zero:

> Theory.next Z SHU () = mkSimpleProb 
>   
>   [  (SHU,  pH_S_SH  *  pU_S_0), 
>        
>      (SHC,  pH_S_SH  *  pC_S_0),
>        
>      (SLU,  pL_S_SH  *  pU_S_0), 
>        
>      (SLC,  pL_S_SH  *  pC_S_0)]

> Theory.next Z SHC () = mkSimpleProb 
>   
>   [  (SHC,  pH_S_SH),
>        
>      (SLC,  pL_S_SH)]

A similar situation holds when the initial states are \cs{|SLU|} and \cs{|SLC|}:

> Theory.next Z SLU () = mkSimpleProb 
>   
>   [  (SHU,  pH_S_SL  *  pU_S_0), 
>        
>      (SHC,  pH_S_SL  *  pC_S_0),
>        
>      (SLU,  pL_S_SL  *  pU_S_0), 
>        
>      (SLC,  pL_S_SL  *  pC_S_0)]

> Theory.next Z SLC () = mkSimpleProb 
>   
>   [  (SHC,  pH_S_SL),
>        
>      (SLC,  pL_S_SL)]

This completes the specification of the transition function at decision
step zero. The transition function at step one or greater is perfectly
analogous with \cs{|pU_D|}, \cs{|pC_D|}, \cs{|pU_S|} and \cs{|pC_S|} in place of
\cs{|pU_D_0|}, \cs{|pC_D_0|}, \cs{|pU_S_0|} and \cs{|pC_S_0|}, respectively. 

> Theory.next (S n) DHU Start = mkSimpleProb 
>   
>   [  (DHU,  pD_Start  *  pH_D_DH  *  pU_D), 
>     
>      (DHC,  pD_Start  *  pH_D_DH  *  pC_D),
>        
>      (DLU,  pD_Start  *  pL_D_DH  *  pU_D), 
>        
>      (DLC,  pD_Start  *  pL_D_DH  *  pC_D),
>        
>      (SHU,  pS_Start  *  pH_S_DH  *  pU_S), 
>        
>      (SHC,  pS_Start  *  pH_S_DH  *  pC_S),
>        
>      (SLU,  pS_Start  *  pL_S_DH  *  pU_S), 
>        
>      (SLC,  pS_Start  *  pL_S_DH  *  pC_S)]

> Theory.next (S n) DHU Delay = mkSimpleProb 
>   
>   [  (DHU,  pD_Delay  *  pH_D_DH  *  pU_D), 
>     
>      (DHC,  pD_Delay  *  pH_D_DH  *  pC_D),
>        
>      (DLU,  pD_Delay  *  pL_D_DH  *  pU_D), 
>        
>      (DLC,  pD_Delay  *  pL_D_DH  *  pC_D),
>        
>      (SHU,  pS_Delay  *  pH_S_DH  *  pU_S), 
>        
>      (SHC,  pS_Delay  *  pH_S_DH  *  pC_S),
>        
>      (SLU,  pS_Delay  *  pL_S_DH  *  pU_S), 
>        
>      (SLC,  pS_Delay  *  pL_S_DH  *  pC_S)]

> Theory.next (S n) DHC Start = mkSimpleProb 
>   
>   [  (DHC,  pD_Start  *  pH_D_DH),
>        
>      (DLC,  pD_Start  *  pL_D_DH),
>        
>      (SHC,  pS_Start  *  pH_S_DH),
>        
>      (SLC,  pS_Start  *  pL_S_DH)]

> Theory.next (S n) DHC Delay = mkSimpleProb 
>   
>   [  (DHC,  pD_Delay  *  pH_D_DH),
>        
>      (DLC,  pD_Delay  *  pL_D_DH),
>        
>      (SHC,  pS_Delay  *  pH_S_DH),
>        
>      (SLC,  pS_Delay  *  pL_S_DH)]

> Theory.next (S n) DLU Start = mkSimpleProb 
>   
>   [  (DHU,  pD_Start  *  pH_D_DL  *  pU_D), 
>     
>      (DHC,  pD_Start  *  pH_D_DL  *  pC_D),
>        
>      (DLU,  pD_Start  *  pL_D_DL  *  pU_D), 
>        
>      (DLC,  pD_Start  *  pL_D_DL  *  pC_D),
>        
>      (SHU,  pS_Start  *  pH_S_DL  *  pU_S), 
>        
>      (SHC,  pS_Start  *  pH_S_DL  *  pC_S),
>        
>      (SLU,  pS_Start  *  pL_S_DL  *  pU_S), 
>        
>      (SLC,  pS_Start  *  pL_S_DL  *  pC_S)]

> Theory.next (S n) DLU Delay = mkSimpleProb 
>   
>   [  (DHU,  pD_Delay  *  pH_D_DL  *  pU_D), 
>     
>      (DHC,  pD_Delay  *  pH_D_DL  *  pC_D),
>        
>      (DLU,  pD_Delay  *  pL_D_DL  *  pU_D), 
>        
>      (DLC,  pD_Delay  *  pL_D_DL  *  pC_D),
>        
>      (SHU,  pS_Delay  *  pH_S_DL  *  pU_S), 
>        
>      (SHC,  pS_Delay  *  pH_S_DL  *  pC_S),
>        
>      (SLU,  pS_Delay  *  pL_S_DL  *  pU_S), 
>        
>      (SLC,  pS_Delay  *  pL_S_DL  *  pC_S)]

> Theory.next (S n) DLC Start = mkSimpleProb 
>   
>   [  (DHC,  pD_Start  *  pH_D_DL),
>        
>      (DLC,  pD_Start  *  pL_D_DL),
>        
>      (SHC,  pS_Start  *  pH_S_DL),
>        
>      (SLC,  pS_Start  *  pL_S_DL)]

> Theory.next (S n) DLC Delay = mkSimpleProb 
>   
>   [  (DHC,  pD_Delay  *  pH_D_DL),
>        
>      (DLC,  pD_Delay  *  pL_D_DL),
>        
>      (SHC,  pS_Delay  *  pH_S_DL),
>        
>      (SLC,  pS_Delay  *  pL_S_DL)]

> Theory.next (S n) SHU () = mkSimpleProb 
>   
>   [  (SHU,  pH_S_SH  *  pU_S), 
>        
>      (SHC,  pH_S_SH  *  pC_S),
>        
>      (SLU,  pL_S_SH  *  pU_S), 
>        
>      (SLC,  pL_S_SH  *  pC_S)]

> Theory.next (S n) SHC () = mkSimpleProb 
>   
>   [  (SHC,  pH_S_SH),
>        
>      (SLC,  pL_S_SH)]

> Theory.next (S n) SLU () = mkSimpleProb 
>   
>   [  (SHU,  pH_S_SL  *  pU_S), 
>        
>      (SHC,  pH_S_SL  *  pC_S),
>        
>      (SLU,  pL_S_SL  *  pU_S), 
>        
>      (SLC,  pL_S_SL  *  pC_S)]

> Theory.next (S n) SLC () = mkSimpleProb 
>   
>   [  (SHC,  pH_S_SL),
>        
>      (SLC,  pL_S_SL)]

> {-

> ---}

%endif

%

\subsection{Realistic and stylized decision processes}
\label{subsection:realistic}

Before defining how much decisions under uncertainty matter in the next
section, let us clarify the notion of \emph{stylized} decision process.
%
As mentioned in the introduction, this notion was originally introduced
in \citep{esd-9-525-2018} to contrast the one of \emph{realistic}
decision process. This is also the sense in which it has been used in
this work.

For example, in discussing the probability of economic downturns, we
have argued that, in the specification of more realistic GHG emissions
decision processes, one might want to distinguish between states in
which a transition towards a globally decarbonized society is ongoing
and states in which the transition has already been accomplished.

In the case of ongoing green transitions, one may want to consider
different transition probabilities, perhaps depending on the degree to
which the transition has been accomplished or the time since it was
started.

From this angle, more realistic essentially means a larger number of
states (remember that, as discussed in the introduction, the states of a
decision process typically represent sets of micro-states with the
latter being detailed descriptions of physical, economic and social
conditions), perhaps also of control options (for example, fast or slow
green transitions) and hence more complex transition functions.

This reductionist approach towards ``realism'' is paradigmatic of
so-called \emph{modelling} approaches. In climate policy advice, it has
lead to (integrated assessment) models of decision processes based on
high-dimensional state and control spaces and a large number of model
parameters \citep{Nordhaus2018, esd-10-453-2019}.

While this is popular in climate policy assessment and advice, the usage
of ``realistic'' integrated assessment models (IAM) has also been
criticized, among others, because of their poor understandability and
limited predictive capability.
%
%%\TODO{Summarize Pindyck's critique and observations.}
%
%% Nicola: done.
%
For example, in \citep{pindyck2017}, it was found that very different
estimates of the ``right'' social cost of carbon can be ``obtained'' by
setting the values of certain IAM parameters (for example, discount
factors and climate sensitivities) to specific, arbitrary but
``plausible'' values and Pindyck even argued that

\begin{quote}
IAM-based analyses of climate policy create a perception of knowledge
and precision that is illusory and can fool policymakers into thinking
that the forecasts the models generate have some kind of scientific
legitimacy \citep{pindyck2017}.
\end{quote}

\noindent
Similar concerns and the problem that a too strong focus on
\emph{reliability} may be unsuitable for climate decision making at
regional scales, have been discussed in \citep{shepherd2019}.  

Another weakness of IAMs for climate policy is their strong bias towards
deterministic modelling.  With very few exceptions, these models assume
that decisions (e.g. of starting a global green transition) are
implemented with certainty, that crucial parameterizations of climate
processes (like the \emph{equilibrium climate sensitivity}) can be
estimated accurately and that the costs and the benefits of future
climate changes can be accounted for in suitable ``terminal'' (salvage,
scrap, see \citep{puterman2014markov} section 2.1.3) rewards.

Is there a way of specifying decision processes that are useful for
pragmatic climate decision making and that avoid the drawbacks of
deterministic modelling approaches based on high-dimensional state
spaces?

%% More concretely: is there a way to make the specification of sections
%% \ref{subsection:M}, \ref{subsection:XY} and \ref{subsection:next} more
%% realistic \emph{without increasing the number of states, the number of
%%   controls and the computational complexity of the transition function?}

%% The answer is yes and the key for improving the specification are
%% the twelve probabilities that fully determine the
%% transition function \cs{|next|}.

We believe that this is the case and that, rather than neglecting
uncertainty, the way to address this challenge is to
%
0) specify low-dimensional state and control spaces that are logically
consistent with the informal description of the specific decision
process at stake;
%
1) explicitly account for the uncertainties that are known to affect
best decisions for that process,
%
2) exploit the knowledge available (from past experience, data, model
simulation, etc.) to specify trustable transition probabilities with
interpretations that are consistent with that process.

This is the essence of the approach that we have demonstrated
in this section: starting from the informal description of section
\ref{subsection:stylized}, we have introduced formal specifications of
state and control spaces that are logically consistent with that
description.
%
We have accounted for all the uncertainties of the informal description
in terms of 12 transition probability parameters. For each parameter,
we have provided an \emph{interpretation} together with a range of
values compatible with that interpretation.
%
Within these ranges, we have then chosen certain values and defined the
transition function in terms of those values.  For example, we have
postulated a 10\% chance that a decision to start a green transition
fails to be implemented.

In a (more) realistic specification, this figure could perhaps have been
obtained by asking a pool of experts, perhaps political scientists,
historians, etc.
%
Similarly, in more realistic specifications, the probabilities of
recovering from economic downturns might be obtained from climate
economists. These, in turn, might rely on model simulations, expert
elicitation or perhaps statistical data.
%
Finally, climate models (general circulation models, intermediate
complexity models, low-dimensional systems of ordinary differential
equations representing global mass and energy budgets) might be applied
to representative micro-states samples of a given (macro) state (for
example, our initial state \cs{|DHU|}) to compute more realistic estimates
(for example via Monte Carlo simulations) of transition probabilities,
for instance, to committed states.

From this angle, the approach of ``stylized'' decision processes is
similar to the \emph{storyline} approach -- the ``identification of
physically self-consistent, plausible pathways'' -- proposed in
\citep{shepherd2019}. The focus, there on physical consistency and
causal networks, is here on logical consistency and decision networks.
%
Common to both approaches is the need to integrate contributions from
very different disciplines, ranging from theoretical computer science to
the social sciences \citep{shepherd2019, shape2021}.

In this enterprise, the theory of section \ref{section:theory} and the
language extensions to be discussed in sections
\ref{section:responsibility} and \ref{section:generic} play a twofold
role.
%
On the one hand, they help ensuring that results of model simulations,
expert opinions, and statistical data are applied consistently. On the
other hand, they make it possible to reason about pragmatic decision
processes in a formal and rigorous way. We demonstrate this second
aspect in section \ref{section:responsibility}.

%if False

* Extensions (\ref{section:responsibility}):

We will need to show and compare states for equality:

> implementation Show State where
>   show DHU = "DHU"
>   show DHC = "DHC"
>   show DLU = "DLU"
>   show DLC = "DLC"
>   show SHU = "SHU"
>   show SHC = "SHC"
>   show SLU = "SLU"
>   show SLC = "SLC"

> showX : {t : Nat} -> X t -> String
> showX x = show x

> implementation DecEq State where
>   decEq DHU DHU = Yes Refl
>   decEq DHU DHC = let contra = \ Refl impossible in No contra
>   decEq DHU DLU = let contra = \ Refl impossible in No contra
>   decEq DHU DLC = let contra = \ Refl impossible in No contra
>   decEq DHU SHU = let contra = \ Refl impossible in No contra
>   decEq DHU SHC = let contra = \ Refl impossible in No contra
>   decEq DHU SLU = let contra = \ Refl impossible in No contra
>   decEq DHU SLC = let contra = \ Refl impossible in No contra
>
>   decEq DHC DHU = let contra = \ Refl impossible in No contra
>   decEq DHC DHC = Yes Refl
>   decEq DHC DLU = let contra = \ Refl impossible in No contra
>   decEq DHC DLC = let contra = \ Refl impossible in No contra
>   decEq DHC SHU = let contra = \ Refl impossible in No contra
>   decEq DHC SHC = let contra = \ Refl impossible in No contra
>   decEq DHC SLU = let contra = \ Refl impossible in No contra
>   decEq DHC SLC = let contra = \ Refl impossible in No contra
>
>   decEq DLU DHU = let contra = \ Refl impossible in No contra
>   decEq DLU DHC = let contra = \ Refl impossible in No contra
>   decEq DLU DLU = Yes Refl
>   decEq DLU DLC = let contra = \ Refl impossible in No contra
>   decEq DLU SHU = let contra = \ Refl impossible in No contra
>   decEq DLU SHC = let contra = \ Refl impossible in No contra
>   decEq DLU SLU = let contra = \ Refl impossible in No contra
>   decEq DLU SLC = let contra = \ Refl impossible in No contra
>
>   decEq DLC DHU = let contra = \ Refl impossible in No contra
>   decEq DLC DHC = let contra = \ Refl impossible in No contra
>   decEq DLC DLU = let contra = \ Refl impossible in No contra
>   decEq DLC DLC = Yes Refl
>   decEq DLC SHU = let contra = \ Refl impossible in No contra
>   decEq DLC SHC = let contra = \ Refl impossible in No contra
>   decEq DLC SLU = let contra = \ Refl impossible in No contra
>   decEq DLC SLC = let contra = \ Refl impossible in No contra
>   
>   decEq SHU DHU = let contra = \ Refl impossible in No contra
>   decEq SHU DHC = let contra = \ Refl impossible in No contra
>   decEq SHU DLU = let contra = \ Refl impossible in No contra
>   decEq SHU DLC = let contra = \ Refl impossible in No contra
>   decEq SHU SHU = Yes Refl
>   decEq SHU SHC = let contra = \ Refl impossible in No contra
>   decEq SHU SLU = let contra = \ Refl impossible in No contra
>   decEq SHU SLC = let contra = \ Refl impossible in No contra
>
>   decEq SHC DHU = let contra = \ Refl impossible in No contra
>   decEq SHC DHC = let contra = \ Refl impossible in No contra
>   decEq SHC DLU = let contra = \ Refl impossible in No contra
>   decEq SHC DLC = let contra = \ Refl impossible in No contra
>   decEq SHC SHU = let contra = \ Refl impossible in No contra
>   decEq SHC SHC = Yes Refl
>   decEq SHC SLU = let contra = \ Refl impossible in No contra
>   decEq SHC SLC = let contra = \ Refl impossible in No contra
>
>   decEq SLU DHU = let contra = \ Refl impossible in No contra
>   decEq SLU DHC = let contra = \ Refl impossible in No contra
>   decEq SLU DLU = let contra = \ Refl impossible in No contra
>   decEq SLU DLC = let contra = \ Refl impossible in No contra
>   decEq SLU SHU = let contra = \ Refl impossible in No contra
>   decEq SLU SHC = let contra = \ Refl impossible in No contra
>   decEq SLU SLU = Yes Refl
>   decEq SLU SLC = let contra = \ Refl impossible in No contra
>
>   decEq SLC DHU = let contra = \ Refl impossible in No contra
>   decEq SLC DHC = let contra = \ Refl impossible in No contra
>   decEq SLC DLU = let contra = \ Refl impossible in No contra
>   decEq SLC DLC = let contra = \ Refl impossible in No contra
>   decEq SLC SHU = let contra = \ Refl impossible in No contra
>   decEq SLC SHC = let contra = \ Refl impossible in No contra
>   decEq SLC SLU = let contra = \ Refl impossible in No contra
>   decEq SLC SLC = Yes Refl

We will need to show controls and take advantage of the finiteness of
controls for actually computing optimal policies.

> implementation Show StartDelay where
>   show Start = "Start"
>   show Delay = "Delay"

> show : {t : Nat} -> {x : X t} -> Y t x -> String
> show {x = DHU} y = show y
> show {x = DHC} y = show y
> show {x = DLU} y = show y
> show {x = DLC} y = show y
> show {x = SHU} y = show y
> show {x = SHC} y = show y
> show {x = SLU} y = show y
> show {x = SLC} y = show y

> to : StartDelay -> Fin 2
> to Start  =     FZ
> to Delay  =  FS FZ

> from : Fin 2 -> StartDelay
> from             FZ   = Start
> from         (FS FZ)  = Delay
> from     (FS (FS  k)) = absurd k

> toFrom : (k : Fin 2) -> to (from k) = k
> toFrom             FZ   = Refl
> toFrom         (FS FZ)  = Refl
> toFrom     (FS (FS  k)) = absurd k

> fromTo : (a : StartDelay) -> from (to a) = a
> fromTo Start  =  Refl
> fromTo Delay  =  Refl

> finiteStartDelay : Finite StartDelay
> finiteStartDelay = MkSigma 2 (MkIso to from toFrom fromTo)

We need to implement \cs{|isCommitted|} and \cs{|isDisrupted|} and prove that
controls are finite and not empty for actually computing optimal
policies:

> isCommitted t DHU  =  False
> isCommitted t DHC  =  True
> isCommitted t DLU  =  False
> isCommitted t DLC  =  True
> isCommitted t SHU  =  False
> isCommitted t SHC  =  True
> isCommitted t SLU  =  False
> isCommitted t SLC  =  True

> isDisrupted t DHU  =  False
> isDisrupted t DHC  =  False
> isDisrupted t DLU  =  True
> isDisrupted t DLC  =  True
> isDisrupted t SHU  =  False
> isDisrupted t SHC  =  False
> isDisrupted t SLU  =  True
> isDisrupted t SLC  =  True

> Theory.notEmptyY t DHU = Start
> Theory.notEmptyY t DHC = Start
> Theory.notEmptyY t DLU = Start
> Theory.notEmptyY t DLC = Start
> Theory.notEmptyY t SHU = ()
> Theory.notEmptyY t SHC = ()
> Theory.notEmptyY t SLU = ()
> Theory.notEmptyY t SLC = ()

> Theory.finiteY t DHU = finiteStartDelay
> Theory.finiteY t DHC = finiteStartDelay
> Theory.finiteY t DLU = finiteStartDelay
> Theory.finiteY t DLC = finiteStartDelay
> Theory.finiteY t SHU = finiteUnit
> Theory.finiteY t SHC = finiteUnit
> Theory.finiteY t SLU = finiteUnit
> Theory.finiteY t SLC = finiteUnit

%endif

