% -*-Latex-*-

%if False

> module Theory

> import Data.Vect

> import Rel.TotalPreorder
> import Rel.TotalPreorderOperations
> import Finite.Predicates
> import Finite.Operations
> import Finite.Properties
> import Opt.Operations

> %default total
> %access public export
> %auto_implicits off

> infixr 7 <++>
> infixr 7 ##

%endif

\section{The theory in a nutshell}
\label{section:theory}

In this section, we overview the elements of the
\citep{2017_Botta_Jansson_Ionescu} theory\footnote{hereafter simply
  referred to as the \emph{theory}} that we apply in sections
\ref{section:specification} to \ref{section:application}. For
motivations, comparisons with alternative formulations and details,
please see \citep{2017_Botta_Jansson_Ionescu,
  brede2021monadic}.
%
For a summary of the notation, see Appendix~\ref{section:notation}.
% 

In a nutshell, the theory consists of two sets of components: one for
the \emph{specification} of sequential decision problems (SDPs) and one
for their \emph{solution} with verified backward induction. For
informal introductions to SDPs, see
\citep{2017_Botta_Jansson_Ionescu}. Reference mathematical introductions
to SDP are given in sections 1.2 and 2.1 of \citep{bertsekas1995} and
\citep{puterman2014markov}, respectively. For an application of the
theory to GHG emissions problems, see \citep{esd-9-525-2018}.

%% \TODO{Perhaps distinguish between proper specification components and
%%   components that are needed for the varification of backward
%%   induction.}
%
%% Nicola: no.

%% The components for the specification of SDPs are implemented as global
%% forward Idris declarations. An important subset of these components are
%% those that specify the sequential decision process that underlies a
%% decision \emph{problem}. This is defined in terms of 4 components. The
%% first component

The components for the specification of SDPs are global
declarations. Four of these describe the sequential decision
\emph{process} that underlies a decision \emph{problem}. The first
declaration

> M : Type -> Type

specifies the uncertainly \emph{monad} \cs{|M|}. Discussing the notion of
monad here would go well beyond the scope of this manuscript, and we
refer interested readers to \citep{DBLP:conf/nato/Wadler92} and
\citep{botta_brede_jansson_richter_2021}.
%
The idea is that \cs{|M|} accounts for the uncertainties that affect the
decision process. In the stylized GHG emissions process outlined in the
introduction, \cs{|M|} represents stochastic uncertainty. For this process,
values of type \cs{|M A|} are finite probability distributions on \cs{|A|}, see
section \ref{section:specification}.
%
%% It is crucial for the theory that \cs{|M A|} represents finite probability
%% distributions on \cs{|A|} that is, probability distributions in which only a
%% finite number of values of type \cs{|A|} have non-zero
%% probabilities. However, \cs{|A|} itself does not need to be finite.
%% For a minimal formalisation of elementary probability theory in Idris,
%% see appendix \ref{appendix:probability}.

Remember that, as shown in section \ref{subsection:stylized}, sequential
decision processes are defined in terms of the states, of the options
available to the decision maker (in a given state and at a given
decision step) and of the state transitions that take place between two
subsequent decisions.

In control theory, the options available to the decision maker are
called \emph{controls} and the theory supports the specification of the
\emph{states}, of the \emph{controls} and the state \emph{transition
function} of a decision process in terms of three declarations:

> X : (t : Nat) -> Type
> 
> Y : (t : Nat) -> X t -> Type 
> 
> next : (t : Nat) -> (x : X t) -> Y t x -> M (X (S t))

The interpretation is that \cs{|X t|} is the type (set) of states at decision
step \cs{|t|}. For example, the states \cs{|DHU|}, \cs{|DHC|}, \dots, \cs{|SLC|} of our
stylized GHG emission process.
%
Similarly, \cs{|Y t x|} represents the controls available at decision step
\cs{|t|} and in state \cs{|x|} and \cs{|next t x y|} is an \cs{|M|}-structure of the states
that can be obtained by selecting control \cs{|y|} in state \cs{|x|} at decision
step \cs{|t|}. In the decision process of section \ref{subsection:stylized}, \cs{|Y 0
DHU|} (the set of controls available to the decision maker at decision
step 0 and in state \cs{|DHU|) only contains two alternatives: \cs{|Start|} and
|Delay|}.

The uncertainty monad \cs{|M|}, the states \cs{|X|}, the controls \cs{|Y|} and the
transition function \cs{|next|} completely specify a decision process: if we
were given a rule for selecting controls for a given decision process
(that is, a function that gives us a control for every possible state)
and an initial state (or, in case of epistemic uncertainty
\citep{shepherd2019}, a probability distribution of initial states) we
could compute all possible trajectories compatible with that initial
state (or with that probability distribution) together with their
probabilities\footnote{This is not a trivial result. It holds because we
have required \cs{|M|} to be a monad.}.

Indeed, a sequential decision \emph{problem} for \cs{|n|} steps consists of
finding a sequence of \cs{|n|} \emph{policies} (in control theory, functions
that map states to controls are called policies) that, for a given
decision process, maximises the \emph{value} of taking \cs{|n|} decision
steps according to those policies, one after the other.

Here, the value of taking \cs{|n|} decision steps according to a sequence of
\cs{|n|} policies is defined through a measure (in stochastic problems often
the expected-value measure) of a sum of rewards obtained along the
trajectories.
%
It follows that, in order to fully specify a decision problem, one has
to define the rewards obtained at each decision step, the sum that the
decision maker seeks to maximize and the measure function.
%
In the \citep{2017_Botta_Jansson_Ionescu} theory, this is done in terms
of 6 problem specification components. These are summarized in the next
section.


\subsection{Problem specification components} 
\label{subsection:specification_components}


> Val       :  Type
> 
> reward    :  (t : Nat) -> (x : X t) -> Y t x -> X (S t) -> Val
> 
> (<+>)     :  Val -> Val -> Val

Here, \cs{|Val|} is the type of rewards, \cs{|reward t x y x'|} is the reward
obtained by selecting control \cs{|y|} in state \cs{|x|} when the next state is
\cs{|x'|} and the infix operator \cs{|<+>|} is the rule for adding rewards. A few
remarks are at place here.

\begin{enumerate}

\item In many applications, \cs{|Val|} is a numerical type and controls are
  actions that consume certain amounts of resources: fuel, water,
  etc. In these cases, the reward function encodes the value (cost) of
  these resources (and perhaps also the benefits achieved by using them)
  over a decision step. Often, the latter also depends on the
  ``current'' state \cs{|x|} and on the next state \cs{|x'|}. For example, in the
  stylized decision problem of section \ref{subsection:stylized},
  \cs{|reward t x y x'|} would possibly be higher than \cs{|reward t x y x''|} if
  \cs{|x'|} is an H-state (a state with a high level of economic wealth) and
  \cs{|x''|} is an L-state. The theory nicely copes with all these
  situations.

\item When \cs{|Val|} is a numerical type, \cs{|<+>|} is often the canonical
  addition associated with that type. However, in many applications more
  flexibility is needed, e.g., to account for the fact that later
  rewards are often valued less than earlier ones. Again, formulating
  the theory in terms of a generic addition rule nicely covers all these
  applications.

\item Mapping \cs{|reward t x y|} onto \cs{|next t x y|}\footnote{Because \cs{|M|} is a
  monad, functions of type \cs{|A -> B|} can be mapped on values of type \cs{|M
  A|}, obtaining values of type \cs{|M B|} for arbitrary \cs{|A, B : Type|}} yields
  a value of type \cs{|M Val|}. These are the \emph{possible} rewards
  obtained by selecting control \cs{|y|} in state \cs{|x|} at decision step \cs{|t|}.
  %
  A sequential decision problem for \cs{|n|} steps consists of finding a
  sequence of \cs{|n|} policies that maximises a measure of a sum of the
  rewards along possible \emph{trajectories}. We introduce a \emph{value
    function} that computes such a measure in section
  \ref{subsection:solution_components}: as it turns out, comparing two
  policy sequences for a fixed initial state essentially means comparing
  two \cs{|M Val|} values.

  In mathematical theories of optimal control, the implicit assumptions
  are often that \cs{|Val|} is equal to \cs{|Real|}, values of type \cs{|M Val|} are
  probability distributions on real numbers and such values are compared
  in terms of their \emph{expected value} measures.
  %
  Measuring uncertainties in terms of expected value measures subsumes a
  neutral attitude towards risks. This is not always adequate and the
  theory supports alternative (e.g., worst-case) measures via the
  declaration:

> meas             :  M Val -> Val

  In much the same way, the framework allows users to compare \cs{|Val|}
  values in terms of a problem specific total preorder

> (<=)             :  Val -> Val -> Type
> lteTP            :  TotalPreorder (<=)

  This allows, among others, to specify multi-objective optimal control
  \cite{CARLINO202016593} problems.  Here \cs{|<=|} and
  \cs{|TotalPreorder : (A -> A -> Type) -> Type|} are predicates like those discussed in section
  \ref{subsection:PAT} and \cs{|TotalPreorder R|} encodes the notion that \cs{|R|}
  is a total preorder.

  %%and \cs{|TotalPreorder|} is the data type:
  %
  %% \REMARK{Nuria}{I think it suffices to just say ``Here
  %% \cs{|TotalPreorder R|} encodes the notion that \cs{|R|} is a total preorder''
  %% and omit the
  %%   definition.}
  %
  %% Nicola: done

  %% < data TotalPreorder : {A : Type} -> (A -> A -> Type) -> Type where
  %% < MkTotalPreorder : {A : Type} ->
  %% <                        (R : A -> A -> Type) ->
  %% <                        (reflexive : (x : A) -> R x x) ->
  %% <                        (transitive : (x : A) -> (y : A) -> (z : A) -> R x y -> R y z -> R x z) ->
  %% <                        (totalPre : (x : A) -> (y : A) -> Either (R x y) (R y x)) ->
  %% <                        TotalPreorder R

\end{enumerate}

%% \noindent
%% A few more components need to be defined in order to fully specify a
%% sequential decision problem. We discuss these components together with
%% those that support the solution of SDPs in the next paragraph.


\subsection{Problem solution components} 
\label{subsection:solution_components}

The second set of theory components formalizes classical optimal control
theory. Here, we only provide a concise, simplified overview.
Motivation and details can be found in \cite{2014_Botta_et_al},
\cite{2017_Botta_Jansson_Ionescu} and \cite{esd-9-525-2018}. For an
introduction to the mathematical theory of optimal control, we recommend
\cite{puterman2014markov} and \cite{bertsekas1995}.
%
As mentioned, policies (decision rules) are functions from states to
controls:

> Policy : (t : Nat) -> Type
> Policy t = (x : X t) -> Y t x

Policy sequences of length \cs{|n : Nat|} are then just vectors (remember
section \ref{subsection:DT}) of \cs{|n|} policies:

> data PolicySeq : (t : Nat) -> (n : Nat) -> Type where
>   Nil   :  {t : Nat} -> PolicySeq t Z
>   (::)  :  {t, n : Nat} -> Policy t -> PolicySeq (S t) n -> PolicySeq t (S n)

\noindent
Perhaps, the most important notion in the mathematical theory of optimal
control is that of \emph{value function}. The value function takes two
arguments: a policy sequence \cs{|ps|} for making \cs{|n|} decision steps starting
from decision step \cs{|t|} and an initial state \cs{|x : X t|}. It computes
the value of taking \cs{|n|} decision steps according to the policies \cs{|ps|}
when starting in \cs{|x|}:

%if False

> zero  :  Val

> (<++>) : {A : Type} -> (f, g : A -> Val) -> A -> Val
> f <++> g = \ a => f a <+> g a

%endif

> val : Functor M => {t, n : Nat} -> PolicySeq t n -> X t -> Val
> val {t}  Nil      x  =  zero
> val {t} (p :: ps) x  =  let y    =  p x in
>                         let mx'  =  next t x y in          
>                         meas (map (reward t x y <++> val ps) mx')

Notice that, independently of the initial state \cs{|x|}, the value of the
empty policy sequence is \cs{|zero|}. This is a problem-specific reference
value

< zero    :  Val

that has to be provided as part of the problem's specification.
%
The value of a policy sequence consisting of a first policy \cs{|p|} and of a
tail policy sequence \cs{|ps|} is defined inductively as the measure of an
\cs{|M|}-structure of \cs{|Val|} values. These values are obtained by first
computing the control \cs{|y|} dictated by \cs{|p|} in \cs{|x|}, the \cs{|M|}-structure of
possible next states \cs{|mx'|} dictated by \cs{|next|} and finally by adding
\cs{|reward t x y x'|} and \cs{|val ps x'|} for all \cs{|x'|} in \cs{|mx'|}. The result of
this functorial mapping is then measured with the problem-specific
measure \cs{|meas|} to obtain a result of type \cs{|Val|}. The function which is
mapped on \cs{|mx'|} is just a lifted version of \cs{|<+>|}, as one would expect:

< (<++>)  :  {A : Type} -> (f, g : A -> Val) -> A -> Val
< f <++> g = \ a => f a <+> g a

As shown in \citep{brede2021monadic}, \cs{|val ps x|} does indeed compute
the \cs{|meas|}-measure of the \cs{|<+>|}-sum of the \cs{|reward|}-rewards along the
possible trajectories starting at \cs{|x|} under \cs{|ps|} for sound choices of
\cs{|meas|}.
%
The advantage of the above formulation of \cs{|val|} \citep{bellman1957,
  bertsekas1995, puterman2014markov} is
that it can be exploited to compute policy sequences that are provably
optimal in the sense of

> OptPolicySeq  :  Functor M => {t, n : Nat} -> PolicySeq t n -> Type
> OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> (x : X t) -> val ps' x <= val ps x

Notice the universal quantification in the definition of \cs{|OptPolicySeq|}:
a policy sequence \cs{|ps|} is said to be optimal iff \cs{|val ps' x <= val ps x|}
for any \cs{|ps'|} and for any \cs{|x|}.
%
The generic, verified implementation of backward induction from
\citep{2017_Botta_Jansson_Ionescu} is a simple application of Bellman's
principle of optimality, often referred to as Bellman's equation
\citep{bellman1957}. It can be suitably formulated in terms of the
notion of \emph{optimal extension}. A policy \cs{|p : Policy t|} is an
optimal extension of a policy sequence \cs{|ps : Policy (S t) n|} if it
is the case that the value of \cs{|p :: ps|} is at least as good as the
value of \cs{|p' :: ps|} for any policy \cs{|p'|} and for any state
\cs{|x : X t|}:

> BestExt  :  Functor M => {t, n : Nat} -> PolicySeq (S t) n -> Policy t -> Type
> BestExt {t} ps p  =  (p' : Policy t) -> (x : X t) -> val (p' :: ps) x <= val (p :: ps) x

With this formalization of the notion of optimal extension, Bellman's
principle can then be formulated as

> Bellman  :  Functor M => {t, n : Nat} ->
>             (ps   :  PolicySeq (S t) n) -> OptPolicySeq ps ->
>             (p    :  Policy t)          -> BestExt ps p ->
>             OptPolicySeq (p :: ps)

In words: \emph{extending an optimal policy sequence with an optimal
extension (of that policy sequence) yields an optimal policy sequence}.
Another way of expressing the same principle is to say that prefixing
with optimal extensions preserves optimality.
%
Proving Bellman's optimality principle is almost straightforward and
crucially relies on \cs{|<=|} being reflexive and transitive (remember that
\cs{|<=|} is a total preorder).
%
With \cs{|Bellman|} and provided that we can compute best extensions of
arbitrary policy sequences

%
%% \REMARK{Nuria}{I think this sentence should suffice for the present
%%   paper and the following discussion of the proof could be omitted.}
%% \REMARK{Nicola}{I agree but I have given the proof sketch to introduce
%%   \cs{|plusMon|} and \cs{|measMon|}, the last two specification components that we
%%   have not discussed in the previous section. Perhaps we can avoid
%%   mentioning \cs{|plusMon|} and \cs{|measMon|} tout court, but this needs to be
%%   checked.}
%

%if False

The proof obligation is to show that

< val (p' :: ps') x <= val (p :: ps) x

for arbitrary \cs{|p'|}, \cs{|ps'|} and \cs{|x|} of suitable types. This is achieved by
transitivity of \cs{|<=|} on two sub proofs:

< val (p' :: ps') x <= val (p' :: ps) x

and

< val (p' :: ps) x <= val (p :: ps) x

The second inequality directly follows from the last argument of
Bellman, a proof that \cs{|BestExt ps p|}. 

The first inequality follows from the optimality of \cs{|ps|} (the second
argument of Bellman), reflexivity of \cs{|<=|} and from two crucial
\emph{monotonicity} properties:

> plusMon   :  {v1, v2, v3, v4 : Val} -> 
>              v1 <= v2 -> v3 <= v4 -> (v1 <+> v3) <= (v2 <+> v4)
>
> measMon   :  Functor M => {A : Type} -> 
>              (f, g : A -> Val) -> ((a : A) -> f a <= g a) ->
>              (ma : M A) -> meas (map f ma) <= meas (map g ma)

The second condition is a special case of the measure monotonicity
requirement originally formulated in \cite{ionescu2009} in
the framework of a theory of vulnerability and monadic dynamical
systems. It is a natural property that, among others, the expected value
measure and the worst (best) case measure do fulfill.

Like the reference value \cs{|zero|} discussed above, \cs{|plusMon|} and \cs{|measMon|
are specification components of the theory that we have not discussed in
the previous section.

We provide a proof of Bellman in appendix~\ref{appendix:Bellman}. As one
would expect, the proof crucially depends on the recursive definition of
|val|} discussed above.

%endif

%% The definition of \cs{|val|} and \cs{|Bellman|} are a formalization of Bellman's
%% equation as formulated in standard textbooks on optimal control theory
%% \cite{puterman2014markov}, \cite{bertsekas1995}.
%% With \cs{|Bellman|} and provided that we can compute best extensions of
%% arbitrary policy sequences

> bestExt      :  Functor M => {t, n : Nat} -> PolicySeq (S t) n -> Policy t

> bestExtSpec  :  Functor M => {t, n : Nat} -> 
>                 (ps : PolicySeq (S t) n) -> BestExt ps (bestExt ps)

\noindent
it is easy to derive a verified, generic implementation of backward
induction:

> bi  :   Functor M => (t : Nat) -> (n : Nat) -> PolicySeq t n
> bi t  Z     =  Nil
> bi t (S n)  =  let ps = bi (S t) n in bestExt ps :: ps

%if False

> nilOptPolicySeq  :  Functor M => OptPolicySeq Nil
> nilOptPolicySeq Nil x = reflexive lteTP zero

%endif

\noindent
For this implementation, a machine-checked proof that \cs{|bi t n|} is an
optimal policy sequence for any initial time \cs{|t|} and number of decision
steps \cs{|n|}:

> biLemma  :  Functor M => (t : Nat) -> (n : Nat) -> OptPolicySeq (bi t n)

\noindent
is a straightforward computation, see
\citep{2017_Botta_Jansson_Ionescu, brede2021monadic}.

%if False

> biLemma t  Z     =  nilOptPolicySeq
> biLemma t (S n)  =  Bellman ps ops p oep
>   where  ps   :  PolicySeq (S t) n
>          ps   =  bi (S t) n
>          ops  :  OptPolicySeq ps
>          ops  =  biLemma (S t) n
>          p    :  Policy t
>          p    =  bestExt ps
>          oep  :  BestExt ps p
>          oep  =  bestExtSpec ps

%endif

%
%%\REMARK{Nuria}{Maybe give a more informative name to \cs{|biLemma|?}
%
%% Nicola: we have called it \cs{|biLemma|} in all the publications we refer
%% the readers to, I do not see the need to give it another name here.
%

\subsection{Theory wrap-up} 
\label{subsection:wrap-up}

The components discussed in the last two sections are all what is needed
to define the measures of how much decisions matter that we have
discussed in the introduction. We introduce these measures in sections
\ref{section:responsibility} and \ref{section:generic}.
%
As discussed in \citep{brede2021monadic},
the \cite{2017_Botta_Jansson_Ionescu} theory is slightly more general
(but also more difficult to apply) than the one summarized above.
%
The price that we have to pay for the simplifications introduced here
are two additional requirements. First, controls have to be non-empty:

> notEmptyY : (t : Nat) -> (x : X t) -> Y t x

Second, the transition function is required to return non-empty
\cs{|M|} structures.
%% Also, in section \ref{subsection:solution_components}, we have not
%% discussed under which conditions one can implement optimal extensions of
%% arbitrary policy sequences. Readers interested in this question can find
%% detailed answers in \cite{2017_Botta_Jansson_Ionescu} and
%% \citep{brede2021monadic}.


%if False

* Extensions (optimal extension):

The verified, generic implementation of backward induction \cs{|bi|
naturally raises the question of under which conditions one can
implement 

< bestExt  :  Functor M => {t, n : Nat} -> 
             PolicySeq (S t) n -> Policy t
such that

< bestExtSpec  :  Functor M => {t, n : Nat} ->
<                (ps : PolicySeq (S t) n) -> BestExt ps (bestExt ps)

To this end, consider the function

> cval  :  Functor M => {t, n : Nat} -> 
>          PolicySeq (S t) n -> (x : X t) -> Y t x -> Val
> cval {t} ps x y  =  let mx' = next t x y in
>                     meas (map (reward t x y <++> val ps) mx')

By definition of \cs{|val|} and \cs{|cval|}, one has

  val (p :: ps) x  
    =
  meas (map (reward t x (p x) <++> val ps) (next t x (p x)))
    =  
  cval ps x (p x)

This suggests that, if we can maximize \cs{|cval|} that is, implement

> cvalmax     :  Functor M => {t, n : Nat} -> 
>                PolicySeq (S t) n -> (x : X t) -> Val

> cvalargmax  :  Functor M => {t, n : Nat} -> 
>                PolicySeq (S t) n -> (x : X t) -> Y t x

that fulfill

> cvalmaxSpec  :  Functor M => {t, n : Nat} -> 
>                 (ps : PolicySeq (S t) n) -> (x : X t) -> 
>                 (y : Y t x) -> cval ps x y <= cvalmax ps x

> cvalargmaxSpec  :  Functor M => {t, n : Nat} -> 
>                    (ps : PolicySeq (S t) n) -> (x  : X t) ->
>                    cvalmax ps x = cval ps x (cvalargmax ps x)

then we can implement optimal extensions of arbitrary policy
sequences. As it turns out, this intuition is correct. With

> bestExt = cvalargmax

, one has

> bestExtSpec {t} {n} ps p' x = s4 where
>   p     :  Policy t
>   p     =  bestExt ps
>   y     :  Y t x
>   y     =  p x
>   y'    :  Y t x
>   y'    =  p' x
>   s1    :  cval ps x y' <= cvalmax ps x
>   s1    =  cvalmaxSpec ps x y'
>   s2    :  cval ps x y' <= cval ps x (cvalargmax ps x)
>   s2    =  replace {P = \ z => (cval ps x y' <= z)} (cvalargmaxSpec ps x) s1
>   s3    :  cval ps x y' <= cval ps x y
>   s3    =  s2
>   s4    :  val (p' :: ps) x <= val (p :: ps) x
>   s4    =  s3

The observation that

< cvalmax     :  Functor M => {t, n : Nat} -> 
<                PolicySeq (S t) n -> (x : X t) -> Val

< cvalargmax  :  Functor M => {t, n : Nat} -> 
<                PolicySeq (S t) n -> (x : X t) -> Y t x

that fulfill

< cvalmaxSpec  :  Functor M => {t, n : Nat} -> 
<                 (ps : PolicySeq (S t) n) -> (x : X t) -> 
<                 (y : Y t x) -> cval ps x y <= cvalmax ps x

< cvalargmaxSpec  :  Functor M => {t, n : Nat} -> 
<                    (ps : PolicySeq (S t) n) -> (x  : X t) ->
<                    cvalmax ps x = cval ps x (cvalargmax ps x)

are sufficient to implement an optimal extension \cs{|bestExt|} that fulfills
|bestExtSpec|} naturally raises the question of what are necessary and
sufficient conditions for \cs{|cvalmax|} and \cs{|cvalargmax|}. 

Answering this question necessarily requires discussing properties of
|cval|} and goes well beyond the scope of formulating a theory of SDPs.
Here, we limit ourselves to remark that if \cs{|Y t x|} is finite

> finiteY : (t : Nat) -> (x : X t) -> Finite (Y t x)

one can implement \cs{|cvalmax|}, \cs{|cvalargmax|}, \cs{|cvalmaxSpec|} and
|cvalargmaxSpec|} by linear search:

> cardNotZero : (t : Nat) -> (x : X t) -> CardNotZ (finiteY t x)
> cardNotZero t x = cardNotZLemma (finiteY t x) (notEmptyY t x)

> cvalmax {t} ps x = Opt.Operations.max lteTP (finiteY t x) (cardNotZero t x) (cval ps x)

> cvalargmax {t} ps x = Opt.Operations.argmax lteTP (finiteY t x) (cardNotZero t x) (cval ps x)

> cvalmaxSpec {t} ps x = Opt.Operations.maxSpec lteTP (finiteY t x) (cardNotZero t x) (cval ps x)

> cvalargmaxSpec {t} ps x = Opt.Operations.argmaxSpec lteTP (finiteY t x) (cardNotZero t x) (cval ps x)


* Extensions (worst extension):

> worstExt  :  Functor M => {t, n : Nat} -> PolicySeq (S t) n -> Policy t

> cvalmin     :  Functor M => {t, n : Nat} -> 
>                PolicySeq (S t) n -> (x : X t) -> Val

> cvalargmin  :  Functor M => {t, n : Nat} -> 
>                PolicySeq (S t) n -> (x : X t) -> Y t x

> cvalminSpec  :  Functor M => {t, n : Nat} -> 
>                 (ps : PolicySeq (S t) n) -> (x : X t) -> 
>                 (y : Y t x) -> cvalmin ps x <= cval ps x y

> cvalargminSpec  :  Functor M => {t, n : Nat} -> 
>                    (ps : PolicySeq (S t) n) -> (x  : X t) ->
>                    cvalmin ps x = cval ps x (cvalargmin ps x)

> worstExt = cvalargmin

> cvalmin {t} ps x = Opt.Operations.min lteTP (finiteY t x) (cardNotZero t x) (cval ps x)

> cvalargmin {t} ps x = Opt.Operations.argmin lteTP (finiteY t x) (cardNotZero t x) (cval ps x)

> cvalminSpec {t} ps x = Opt.Operations.minSpec lteTP (finiteY t x) (cardNotZero t x) (cval ps x)

> cvalargminSpec {t} ps x = Opt.Operations.argminSpec lteTP (finiteY t x) (cardNotZero t x) (cval ps x)


* Extensions (section \ref{section:responsibility}):

Perhaps, some of these functions will become part of a theory extension
to a DSL for measuring how much decision matters.

> head  :  {t, n : Nat} -> PolicySeq t (S n) -> Policy t
> head (p :: ps) = p

> tail  :  {t, n : Nat} -> PolicySeq t (S n) -> PolicySeq (S t) n
> tail (p :: ps) = ps 



> data StateCtrlSeq  :  (t, n : Nat) -> Type where
>   Last  :  {t : Nat} -> X t -> StateCtrlSeq t (S Z)
>   (##)  :  {t, n : Nat} -> DPair (X t) (Y t) -> StateCtrlSeq (S t) (S n) -> StateCtrlSeq t (S (S n))


> trj  :  Monad M => {t, n : Nat} -> PolicySeq t n -> X t -> M (StateCtrlSeq t (S n))
> trj {t}  Nil      x  =  pure (Last x)
> trj {t} (p :: ps) x  =  let y   = p x in
>                         let mx' = next t x y in
>                         map ((MkDPair x y) ##) (mx' >>= trj ps)


* Extensions (section \ref{section:generic}):

%endif
