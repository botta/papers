% -*-Latex-*-

%if False

> module Introduction

%endif

\section{Introduction}
\label{section:introduction}

%
\begin{quote}
When a person performs or fails to perform a morally significant action,
we sometimes think that a particular kind of response is
warranted. Praise and blame are perhaps the most obvious forms this
reaction might take. For example, one who encounters a car accident may
be regarded as worthy of praise for having saved a child from inside the
burning car, or alternatively, one may be regarded as worthy of blame
for not having used one's mobile phone to call for help. To regard such
agents as worthy of one of these reactions is to regard them as
responsible for what they have done or left undone \citep{sep-moral-responsibility2014}.
\end{quote}
%

\noindent
The quote from ``The Stanford Encyclopedia of Philosophy'' (SEP)
provides a compelling account of what responsibility is about. The car
accident example is pointed because of two reasons.

First, because it rests on an implicit and widely accepted understanding
of what a person ``who encounters a car accident'' with a child ``inside
the burning car'' shall do. Namely, their best to rescue the kid.

Second, because what the person is regarded as worthy of praise or of
blame for having done (or left undone) are \emph{best} and \emph{worst}
actions with respect to the goal of rescuing the child: the agent can
expect little praise for having used the mobile phone to call for help
and possibly also little blame for not having managed to get the child
out of the burning car. By contrast, they can expect blame for not
having used the mobile phone to call for help.


\subsection{Responsibility in climate decisions}
\label{subsection:ruu}

In the context of climate policy, the measure by which praise and blame
shall be attributed to decisions is not always as clear as in the
\citep{sep-moral-responsibility2014} example. This is because of two
reasons.

We know that decisions that are taken (or delayed) now and in the next
decades, e.g., on greenhouse gas (GHG) emissions, are crucial for events
that unfold in the centuries and millennia to come, mainly because the
physical and chemical processes involved in reabsorbing atmospheric
$\mathrm{CO}_2$ are very slow \citep{pierrehumbert2010principles}.

We also know that current climate policies may lead future generations
to (attempt to) mitigate the negative effects of climate change (CC) by
adopting geo-engineering measures (for example, massive injections of
aerosols in the atmosphere \citep{esd-12-313-2021, esd-10-453-2019,
  Moreno-Cruz+Keith2012}) that can have severe collateral effects (for
instance, on agricultural yield, hydrological events, public
health \cite{EASTHAM2018}, ecosystems \cite{Zarnetskee1921854118} or
precipitation \cite{Bhowmick2021, acp-21-1287-2021})
or otherwise face enormous human and economic costs.

But, in contrast to decision problems in technical sciences and in
engineering, in which the \emph{goal} of decision making is typically
well understood, there is little agreement on how to value (and to
discount) the chances and the risks of climate change
\citep{shepherd2019}.

This is especially true when such risks and the associated potential
costs are related to events that unfold in hundreds or thousands of
years and thus very much depend on assumptions about the preferences of
future generations.

Because of these difficulties, most attempts at estimating the impact of
current and near term climate policies are based on comparisons of costs
and benefits over a time horizon of one or two hundred years
\citep{Nordhaus2018, Nordhaus12261, esd-10-453-2019, CARLINO202016593}
and, even so, are controversial \citep{pindyck2017, shepherd2019}.

In short: in climate policy one cannot rely on a widely accepted
understanding of what the goals of decision making are. Thus, specifying
such goals is not as straightforward as in the car accident example.

The second reason why attributing praise and blame to climate decisions
is not as straightforward as in the car accident example
\citep{sep-moral-responsibility2014} is uncertainty.
%
Can an agent be held responsible for (performing or for failing to
perform) actions that matter very little? What does it mean precisely
for decisions to matter?

In the scientific community but also in part of the civil society (think
of the ``Fridays for future'' movement), there is a strong concern that
decisions that are taken (or delayed) now will have severe consequences
on the options that will (not) be available to upcoming generations.

But what do we mean when we say that current decisions matter more than
decisions that will be taken by future generations? Are there systematic
ways to measure how much decisions matter when these have to be taken
under \emph{epistemic} but also political and social \emph{aleatoric}
\citep{shepherd2019} uncertainty? Is there a natural way of comparing
similar decisions at different points in time? To provide accountable
confidence that all the efforts that are associated with the actual
implementation of such decisions (often involving politically difficult
negotiations, changes in legislations, taxation and incentivations
schemes, not to mention technological research and development) are
(not) devoted to decisions that (do not) really matter?


\subsection{What this paper is about}
\label{subsection:about}

We propose a method for measuring how much decisions under uncertainty
matter and apply it to a stylized GHG emissions decision process.

The method is an application of the computational theory of policy
advice and avoidability originally proposed in
\citep{2017_Botta_Jansson_Ionescu}. This theory supports the
specification of time-discrete sequential decision problems
\citep{puterman2014markov,bertsekas1995} and the computation of verified
\emph{best} decisions under uncertainty. It is an extension of the
formal framework of vulnerability \citep{ionescu+al2009} and of the
notion of \emph{monadic} dynamical system originally introduced in
\citep{ionescu2009} and allows dealing with different kinds of
uncertainty in a logically consistent manner. The theory is formulated
in Idris \citep{idrisbook,idristutorial}, an implementation of type
theory \citep{ijb:ISoLA:2018}.

For the sake of providing a self-contained account of our method, we
summarize the elements of the theory \citep{2017_Botta_Jansson_Ionescu}
that we apply in this work in the next section. Readers familiar with
\citep{2017_Botta_Jansson_Ionescu} can skip section \ref{section:theory}
and jump directly to \ref{section:specification}.
%
The \citep{2017_Botta_Jansson_Ionescu} theory is formulated in Idris, a
dependently typed functional language \citep{idrisbook, idristutorial}.
Many climate scientists are well acquainted with imperative languages
but less so with functional, dependently typed languages. For these
readers, we provide a minimal introduction to the notation applied in
sections \ref{section:theory} to \ref{section:application} in appendix
\ref{section:notation}.

The method for measuring how much decisions under uncertainty matter is
based on the observation that many processes in which decisions have to
be taken sequentially and under uncertainty can be represented by finite
decision networks. We introduce finite decision networks formally in
section \ref{section:theory}. Intuitively, a finite decision network is
a network in which each decision yields a finite number of possible
outcomes.

Because the car accident example from
\citep{sep-moral-responsibility2014}, the stylized decision process
outlined in section \ref{subsection:stylized}, and many interesting
decision processes in climate policy can all be represented as finite
decision networks \citep{webster2008}, we can apply the theory of
section \ref{section:theory} to study such processes.

In particular we can apply the theory to compute a best and a
\emph{worst} decision at each node (decision step) of the network: The
idea is then to measure how much decisions matter by comparing the
values (for a specific decision-making goal) associated with such best
and worst decisions. If the values of best and worst decisions turn out
to be the same, then decisions at that decision step do not matter. By
contrast, the larger the difference between the value of best and
worst decisions, the more decisions do matter.

In section \ref{section:responsibility}, we formulate this simple
principle and define a measure of how much decisions matter for the
stylized decision process of section \ref{subsection:stylized}.  The
process is formally specified in section \ref{section:specification}.

In section \ref{section:generic}, we extend the specific measure of
section \ref{section:responsibility} to generic responsibility
measures. This is done by introducing a small domain-specific language
(DSL) for expressing decision-making goals (in the car accident example,
rescuing the child), measures of uncertainty and methods for computing
differences in the value of decisions.

These generic responsibility measures account for all the knowledge
which is encoded in a sequential decision process or network. They are
agnostic with respect to both decision makers and decision steps: how
much a decision matters does not depend on the aims or on the
preferences of the (real or hypothetical) decision maker; all decisions
are measured in the same way. The conditions ensure that responsibility
measurements are \emph{fair}. Here, "fair" is a technical notion and we
make no claims about fairness in any wider sense. In particular, this
technical notion shall not be confused with that of generational
fairness discussed in section \ref{section:conclusion}.

Before outlining the GHG emissions process that we will use to illustrate
our method for measuring how much decisions under uncertainty matter,
let us discuss a potential criticism.

We have explained that we measure how much decisions matter at a given
decision step (under uncertainty about the consequences of such
decisions both at that step and at future steps) by applying the theory
of section \ref{section:theory} to compute best and worst decisions.
%
What is the added value of measuring how much decisions matter for
policy making if we already know how to take best decisions? This is a
legitimate and important question to which we want to provide a first
answer right now.

Remember that, in order to obtain best (and worst) decisions for a
specific decision step one has to specify a goal of decision making. For
example, rescuing the child in the car accident example or, as we will
see in the next section, avoid long term climate change impacts or short
term economic downturns.

The value of best and worst decisions and thus how much decisions matter
will then typically be different for different goals. Best decisions
under a given goal might be suboptimal (or even worst) under another
goal. Decisions that matter a lot for a given goal might turn out to be
irrelevant for another one.

In section \ref{subsection:ruu} we have pointed out how difficult and
controversial it is to specify such goals in climate decision processes,
see also the discussion on the impossibility of ``value-free'' climate
science in \citep{shepherd2019}.  Thus, the added value of our measures
is that of providing a better understanding of
%
%the decision process at stake. More specifically of
%
how the importance of specific decisions depends on the (possibly
conflicting) goals of decision-making and also on the measure of
uncertainty (expected value, worst-case value) and on other aspects of
sequential decision processes discussed in section \ref{section:theory}

Our hope is that understanding that a specific climate policy (say,
pushing forward a ``green'' transition right now) may be crucial or
irrelevant depending on which measures of uncertainty and goals are put
forward for the decision process at stake will lead to a more rational
and collaborative approach, for example in climate negotiations.

Thus, besides proposing a novel approach to the problem of rational
choice and attribution of responsibility \citep{10.5555/1622487.1622491,
  10.1109/QEST.2006.9, halpern2014cause}, our work is a contribution to
pragmatic decision making under uncertainty with a specific focus on
climate decisions.

%%%

\subsection{A stylized decision process}
\label{subsection:stylized}

Consider a GHG emissions process in which now and for a few more decades,
humanity (taken here as a global decision maker) faces two options:

\begin{enumerate}
%
\item Start a ``green'' transition by reducing GHG emissions according
  to a ``safe'' corridor, for example, the one depicted at page 15,
  Figure SPM.3a of the IPCC Summary for Policymakers
  \citep{IPCC_SR15_summary}
  %%\REMARK{Patrik}{Use the 2021 report?}. Nicola: I am not sure we
  %% have something like that in the new IPCC report
%
\item Delay such transition.
%
\end{enumerate}

\noindent
In other words, assume that, over the time period between two subsequent
decisions (say, for concreteness, a decade), either a transition to a
nearly decarbonized global socio-economic system is started or nothing
happens. Further, assume that, once a transition has been started, it
cannot be halted or reversed by later decisions or events.
%
We consider this oversimplified situation only for the sake of clarity,
although it might well be that green transitions are in fact fast and
irreversible \citep{Otto2354}.

Selecting to start a green transition in a specific physical, social and
economic condition yields a different ``new'' condition at the next
decision step. Let's call one such condition a \emph{micro-state}.

The idea is that micro-states are detailed descriptions of physical,
social and economic observables. For example, a micro-state could encode
values of GHG concentrations in the atmosphere, carbon mass in the ocean
upper layer, global temperature deviations, frequency of extreme events,
values of economic growth indicators, measures of inequality, etc.
%
Even if we knew the ``current'' micro-state perfectly, the set of
possible micro-states at the next decision step (say, one decade later)
would still be extremely large, reflecting both the epistemic
uncertainties (imperfect knowledge) about the (physical, social and
economical) processes that unfold in the time between now and the next
decision step and the aleatoric uncertainty \citep{shepherd2019} of
those processes.

Descriptions of decision processes explicitly based on micro-states
would be both computationally intractable and, as discussed in detail in
section \ref{subsection:realistic}, methodologically questionable.
%
As in the the car accident example quoted at the opening of
this section, we avoid these shortcomings by considering only a small
number of sets (clusters, partitions) of micro-states. These
\emph{macro-states} (in the following, just states) consist of
micro-states in which:

\begin{itemize}
\item A green transition has been \emph{started} or \emph{delayed} (\cs{|S|}-states,
  \cs{|D|}-states).
\item The economic wealth is \emph{high} or \emph{low} (\cs{|H|}-states, \cs{|L|}-states).
\item The world is \emph{committed} or \emph{uncommitted} to severe CC impacts
  (\cs{|C|}-states, \cs{|U|}-states).
\end{itemize}

\noindent
In other words, we only distinguish between 8 possible states:
\cs{|DHU|}, \cs{|DHC|}, \cs{|DLU|}, \cs{|DLC|}, \cs{|SHU|}, \cs{|SHC|},
\cs{|SLU|} and \cs{|SLC|} where \cs{|DHU|} represent micro-states in
which a green transition has been \emph{delayed}, economic wealth is
\emph{high} and the world is \emph{uncommitted} to future severe CC
impacts. Similarly for \cs{|DHC|}, \cs{|DLU|}, etc.

Clearly, this is a very crude simplification. But it is useful to study
the impact of uncertainty on relevant climate decisions and sufficient
to illustrate our approach towards measuring how much decisions matter.
%
Also, notice that binary partitioning of micro-states is at the core of
the original notion of planetary boundaries \citep{ro06010m}, of the
topological classification proposed in \citep{heitzig16ab} and of the
social dilemmas discussed in \citep{1885-164074}.

The decision process starts in \cs{|DHU|}. In this state, a decision to start a
green transition can lead to any of the \cs{|DHU|} \dots \cs{|SLC|} states, albeit
\emph{with different probabilities}: the idea is that the probability of
reaching states in which the green transition has been started
(\cs{|S|}-states) is \emph{higher} than the probability of reaching \cs{|D|}-states,
in which the green transition has been delayed. Symmetrically, we assume
that the decision to delay the start of a green transition in \cs{|DHU|} is
more likely to yield \cs{|D|}-states than \cs{|S|}-states.

In other words, we assume our (global, idealized) decision maker to be
\emph{effective}, but only to a certain degree. This accounts for the
fact that, in practice, decisions are not always implemented, be this
because global coordination is necessarily imperfect, because global
players tend to be in competition and legislations tend to have large
inertia or perhaps because some other global challenge (a pandemic or an
economic downturn) has taken center stage. As demonstrated in
\citep{esd-9-525-2018}, limited effectiveness has a significant impact
on optimal GHG emissions policies. Thus, it would be inappropriate to
assume that decisions are always implemented with certainty.

Another essential trait of our stylized process is that decisions to
start a green transition, if implemented, are more likely to yield
states with a low level of economic wealth (\cs{|L|}-states) than states with
high economic wealth. This assumption reflects the fact that starting a
green transition requires more investments and costs than just moving to
states in which most of the work towards a globally decarbonized
society remains to be done.

Finally, we assume that the probability of entering states in which the
world is committed to severe CC impacts is higher in states in which a
green transition has not already been started as compared to states in
which a green transition has been started.
%
Also, as one would expect, delaying transitions to decarbonized
economies \emph{increases} the likelihood of entering states in which
the world is committed to severe CC impacts.

We give a complete formal specification of our stylized decision process
in section \ref{section:specification}. Before turning to section
\ref{section:theory}, let's look a bit more closely at the notion of
responsibility discussed so far.

%%%

\subsection{Clarifications, caveats and related work}
\label{subsection:prc}

The notion of responsibility illustrated by the car accident example
depends on a number of factors.

First and foremost, we have an entity capable of taking decision: the
``one who encounters a car accident''. In the stylized decision process
outlined in \ref{subsection:stylized}, we have referred to this entity
as to the \emph{decision maker}.

Second, we have situations like ``encountering a car accident'' or like
``the child being saved''. These are coarse, macroscopic descriptions of
initial, intermediate or final stages of a decision process that unfolds
in time.
%
In our stylized decision process, we have used the term \emph{state} to
denote such coarse descriptions or, more concretely, sets of possible
micro-states. We formalize the notion of state in section
\ref{section:theory}.

The third important element we have is \emph{options}. In the SEP
example, the decision maker (the ``one who encounters a car accident'')
may ``be regarded as worthy of praise'' or ``may be regarded as worthy
of blame'' for having or for not having used a mobile phone to call for
help: decision makers have to be capable of performing certain actions
(using a mobile phone to call for help, save the child) for being
``regarded as responsible for what they have done or left undone''.

In our stylized decision process we have maintained that, in the initial
state DHU, the decision maker is, up to a certain extent, capable of
starting a green transition or to delay it. In this state, they might be
%% TR: "for having or for not having" is referred to below, so this
%% exact wording should be used
held responsible for having or for not having started the transition.

Notice that the options available to the decision maker in a given state
typically depend on that state and, in general, also on the point in
time (decision step) at which that state has been obtained.

Also notice that, while the \emph{for} in ``for having or for not
having'' is relative to a decision taken, the praise or the blame and
therefore the extent to which the decision maker is regarded as
responsible, crucially depend on the consequences of that decision with
respect to a goal: the child being saved, the economic wealth being
high, the world not being committed to severe CC impacts.

A few remarks are in order here. First, notice that such a goal does not
need to relate to what the decision maker considers to be desirable or
worth pursuing in the decision process at stake.

Second, for a decision maker to be held responsible for a decision in a
given state, say $x$, the goal of decision making has to be specified in
terms of future states that are obtainable from $x$. If this is not the
case, the decisions to be taken in $x$ are not \emph{causally relevant}
\citep{10.1111/ecoj.12507} and any responsibility measure should return
a verdict of ``not responsible''.

Finally, a necessary condition for a decision maker to be held
responsible for a decision in a given state is that at least two choices
are available in that state, under the principle that one cannot be held
responsible if one has no choice.

We conclude this introduction with a few caveats. The first one is about
the notion of responsibility itself: there is a huge literature on the
problem of measuring (quantifying, attributing, etc.) responsibility.

Common approaches distinguish at least between \emph{ex-ante} and
\emph{ex-post} notions of responsibility \citep{heitzig2020degrees} and,
when more entities contribute to a decision, for example in voting
schemes or international agreements, between \emph{individual} and
\emph{collective} responsibility \citep{10.1111/ecoj.12507}.

In the context of law, notions of ex-post responsibility are crucial,
e.g., to quantify liability for harm. But for the kind of GHG emissions
decision processes exemplified by our stylized process and as a
guideline for decision making, ex-ante responsibility is the relevant
notion.

Another caveat is about the notion of stylized decision process
itself. We have introduced this notion in \citep{esd-9-525-2018} and we
will discuss it in more detail in section
\ref{subsection:realistic}. The notion is closely related with that of
\emph{storyline} put forward in \citep{shepherd2019} but there are also
important differences.
%
The storyline approach has been proposed to overcome the (essentially
unavoidable) ineffectiveness of predictions of climate change impacts at
regional scales. It maintains that, at such scales, questions of climate
risks (for given scenarios) need to be reframed ``from the ostensibly
objective prediction space into the explicitly subjective decision
space''.
%
The distinction between epistemic and aleatoric uncertainty and the
``identification of physically self-consistent, plausible pathways'' are
pivotal for such reframing and ``the mathematical framework of a causal
network'' is the key for ``reconciling storyline and probabilistic
approaches''.

The notion of stylized decision process accounts for the fact that at
the global scale ``climate decisions are not made on the basis of
climate change alone'', are rarely implemented with certainty and can
easily be sidetracked by other global challenges, as discussed in
section \ref{subsection:stylized}. As a consequence, questions of
climate policy need to be studied in ``the explicitly subjective
decision space'' at both the global and the local scale.

As in the storyline approach put forward in \citep{shepherd2019}, the
key for applying stylized decision processes is a mathematical
framework. In our case, this is provided by the theory
\citep{2017_Botta_Jansson_Ionescu}, and the causal networks proposed in
\citep{shepherd2019} are a special case of decision networks, see also
sections \ref{section:theory} and \ref{section:specification}.
%
To the best of our knowledge, \citep{2017_Botta_Jansson_Ionescu} is
still the only theory for computing optimal policies for decision making
under monadic uncertainty that has been \emph{verified}.
%
This means that the policies obtained with the theory can be
machine-checked to be optimal. The possibility of computing verified
optimal policies was one of the two main motivations (the other one
being the capability of enforcing transparency of assumptions) for
formulating the theory in a dependently typed language.
%
A consequence of this is that the best and the worst decisions that
define the responsibility measures proposed in our application are
provably best and the worst decisions.
%
We believe that providing this level of guarantees is crucial in climate
decision making: in contrast to policy advice in, e.g., engineering and
logistics, recommendations to decision makers in matters of climate
policy cannot undergo empirical verification.
%
Thus, in climate policy advice, the only guarantees that advisors can
provide to decision makers have to come from formal methods and verified
computations is the highest standard of correctness that science can
provide today.

A final caveat is about what this paper is not about. We develop a
formal method to understand which decisions under uncertainty matter
most and apply this method to a decision problem of \emph{global}
climate policy. Our aim is neither to recommend climate actions nor to
design specific mechanisms, e.g. to improve coordination and
collaboration between decision makers.
%
First and foremost, we aim at better understanding climate decision
making under uncertainty.

%% The price that we have to pay for the explicit construction of generic
%% measures of how much decisions matter is a rather long manuscript. The
%% fact that we also apply these measures to a concrete GHG emissions
%% process does not contribute to make this work shorter.

%% We have tried to compensate for this by relegating the more technical
%% parts of our construction and of the specification of the stylized GHG
%% process to the appendix.
