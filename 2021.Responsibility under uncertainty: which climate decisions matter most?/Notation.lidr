% -*-Latex-*-

%if False

> module Notation

> import Data.Vect

> %default total
> %access public export
> %auto_implicits off

> infixr 10 ^

> A : Type
> B : Type
> C : Type

> FALSE : Type
> FALSE = Void

> partial
> headL : {A : Type} -> List A -> A
> headL (a :: as) = a

%endif


\section{Functional notation}
\label{section:notation}

In sections \ref{section:specification} to \ref{section:application} we
apply some elements of the theory of policy advice and avoidability
\citep{2017_Botta_Jansson_Ionescu}. These are summarized in section
\ref{section:theory}. The \citep{2017_Botta_Jansson_Ionescu} theory is
formulated in Idris, a dependently typed functional language
\citep{idrisbook, idristutorial}. Many climate scientists are well
acquainted with imperative languages but less so with functional,
dependently typed languages. Here we provide a minimal introduction to
the notation of section \ref{section:theory} and to dependent types.

%%%

\subsection{Imperative, functional and dependently typed languages}

Somewhat simplified, an \emph{imperative} program is a sequence of
instructions of what a computing machine should do.
%
In contrast, a \emph{functional} program is a description of what the
machine should compute as a mathematical function from input to output.
%
Common to both paradigms is the ability to name and reuse patterns of
computation to enable concise and precise descriptions of algorithms.

Dependently typed languages like NuPRL, Coq, Agda, Idris or Lean support
implementing programs but also postulating axioms, building theories and
formulating program specifications. These are formal descriptions of
what programs are required to do.
%
Program specifications are crucial for verified programming. Verified
programs are programs that have been machine-checked (verified) to
fulfill a specification. They represent the highest correctness standard
currently achievable \citep{ijb:ISoLA:2018,
ionescujansson:LIPIcs:2013:3899}.


% Possible references:
% * http://www2.computer.org/cms/Computer.org/ComputingNow/homepage/mostread/MostRead-CS-FunctionalProgramming.pdf
% * http://blog.atos.net/blog/2012/06/07/the-rise-of-functional-programming/
% * http://mitpress.mit.edu/SICM/
% * https://www.fpcomplete.com/blog/2012/04/the-downfall-of-imperative-programming
% * http://en.wikipedia.org/wiki/Function_%28mathematics%29

%%%

\subsection{Expressions and their types}

At the core of all programming languages is a sublanguage of
\emph{expressions} like \cs{|1+2|}, \cs{|"Hello"|}, \cs{|[1,7,3,8]|}, etc.
%
In strongly typed languages like Idris each ``valid'' expression has a
\emph{type}, like \cs{|Nat|}, \cs{|String|}, \cs{|List Nat|}, etc.
%
The judgment \cs{|e : t|} states that the expression \cs{|e|} has type \cs{|t|}.
%
Most of the power of Idris comes from its type-checker which can check
these judgments for very complex expressions \cs{|e|} and types \cs{|t|}.
%
In the examples below we use a few arbitrary but fixed types \cs{|A|}, \cs{|B|} and
\cs{|C|}.

%%%

\subsection{Function application and currying}
\label{subsection:FAC}

In Idris (and several other functional languages like Haskell and
Agda) the notation for function application is juxtaposition.
%
You can think of it as an invisible infix operator binding more
strongly than any other operator.
%
Thus, \cs{|f x|} denotes the application of the function \cs{|f|} to the argument
\cs{|x|}. Parentheses are used as in mathematics to resolve operator
precedence like in \cs{|(2 + 3) * 4|} and to denote tuples like \cs{|(1, True,
'c')|}.
%
It is always possible to add extra parentheses, so \cs{|f(x)|} is also a
valid syntax for function application.

In mathematics, a function of $n>1$ arguments is often ``implicitly
converted'' to a function taking as arguments $n-$tuples. For example,
if \cs{|g|} takes one argument in \cs{|A|} and another one in \cs{|B|} and returns
values in \cs{|C|}, we write \cs{|g(x,y)|} to denote the application of \cs{|g|} to the
pair \cs{|(x,y) : (A,B)|} (in Idris, \cs{|(A,B)|} denotes the Cartesian product of
\cs{|A|} and \cs{|B|}) and say that \cs{|g|} has type \cs{|(A,B) -> C|}.

In functional notation we instead use nested function application and
write \cs{|(g x) y|} (which can also be written \cs{|g x y|} because function
application is left-associative) to denote the application of \cs{|g|} to \cs{|x
: A|} and \cs{|y : B|}.
%
Thus \cs{|g|} has type \cs{|A -> (B -> C)|} or simply \cs{|A -> B -> C|}, \cs{|g x|} has
type \cs{|B -> C|} and \cs{|g x y : C|}.
%
This is called the \emph{curried} form.
%
Infix operators like \cs{|(+) : Nat -> Nat -> Nat|} are, just as in
mathematics, a special case where a (binary) function can be written
between its first and second argument: \cs{|2 + 3 : Nat|}.

\subsection{Definitions, pattern matching and recursion}
\label{subsubsection:fundef}

The ability to name and reuse expressions is at the core of all
programming languages. In strongly typed functional languages, we can
name and reuse expressions as long as we provide their type.

> aNumber : Nat
> aNumber = 1738

\noindent
Any time \cs{|aNumber|} is used we can just substitute \cs{|1738|}. We can define
functions through lambda-expressions:

> aFun : Nat -> Nat
> aFun = \x  => 2*x+1

\noindent
or, equivalently \cs{|aFun x = 2*x+1|}. The latter form is useful when we want
to distinguish different cases by \emph{pattern matching}:

> (^) : Double -> Nat -> Double
> x ^ Z      = 1
> x ^ (S n)  = x * (x^n)

\noindent
The two cases (for zero and the successor of \cs{|n|}) can be seen as
equations we want to hold for the ``to the power of'' binary operator \cs{|(^)|}.
%
In addition to pattern matching, this example also introduces recursion:
the function being defined, \cs{|(^)|}, is applied to \cs{|(S n)|} on the left
hand side and to \cs{|n|} on the right hand side of the second equation.

%%%

\subsection{Partial application and higher order functions}
\label{subsubsection:currying}

If a function of two (or more) arguments, \cs{|g : A -> B -> C|}, is applied
to just one argument \cs{|x|} we obtain a function \cs{|g x : B -> C|} which is a
\emph{partially applied} version of \cs{|g|}.
%
Thus, we can view any function as a 1-argument function, possibly
returning a function.

We can also convert \cs{|g|} into \cs{|h : (A,B) -> C|} by pairing up the first
two arguments. More generally, we can convert any ($n$-argument)
function into a 1-argument function that takes as arguments $n$-tuples.
For binary functions this conversion can be done generically:

> uncurry : (A -> B -> C) -> ((A, B) -> C)
> uncurry f (a, b) = f a b

\noindent
The implementation is straightforward: \cs{|uncurry|} takes as input a
function \cs{|f|} which takes values of type \cs{|A|} to functions from \cs{|B|} to
\cs{|C|}. It returns a function that takes as input pairs of type \cs{|(A,B)|}. 
%**TODO: Perhaps explain notation confusion: the type A cross B is
%written (A,B)
% Nicola: done (2021-10-21)
This is our first example of a \emph{higher-order} function: a function
taking another function as a parameter. The opposite transformation is
also short and clean:

> curry :   ((A, B) -> C) -> (A -> B -> C)
> curry f a b = f (a, b)

\noindent
These examples are a bit abstract, so here is a more applied example:
%
Given a time-dependent reward function \cs{|reward : Nat -> A -> Double|} and
a parameter \cs{|rate : Double|} we construct a discounted reward function by
applying the higher-order function \cs{|discount|}:

> discount :  Double ->  (Nat -> A -> Double)  ->  (Nat  ->   A   ->  Double)
> discount    rate       reward                =   \t    =>   \x  =>  (rate^t) * (reward t x)

\subsection{Polymorphic functions and equality types}
\label{subsubsection:pfagp}

The types presented so far have been \emph{monomorphic}: using only
specific types like \cs{|Nat|}, \cs{|Double|} and the fixed types
\cs{|A|}, \cs{|B|}, and \cs{|C|}.
%
Many programs work generically for a large class of types. For example,
\cs{|discount|} works for any \cs{|A|} and \cs{|curry|} for any \cs{|A|}, \cs{|B|}, and \cs{|C|}.
%
A simpler example is the projection function \cs{|fst|} for extracting the
first component of a pair:

> fst :  {A, B : Type} ->  (A, B)  ->  A
> fst                      (x, y)  =   x

\noindent
The type of \cs{|fst|} depends on two \emph{type variables} \cs{|A|} and \cs{|B|}. Thus
\cs{|fst|} is in fact a three-argument function taking two types and a pair
and returning the first component of the pair.
%
The two first arguments are \emph{implicit} arguments which can be
inferred by the system in most use cases.
%
In section \ref{section:theory}, most functions are polymorphic, using a
combination of explicit and implicit type arguments.

%%%

%% \subsection{Equality}
%% \label{subsubsection:equality}

Dependently typed functional languages support reasoning about the
equality of expressions.
%
The claim that an expression \cs{|a : A|} is equal to an expression \cs{|b :B|} is
written simply \cs{|a = b|}.
%
The infix operator \cs{|(=)|} has type \cs{|A -> B -> Type|} and defines a whole
family of types: for every \cs{|a : A|}, and \cs{|b : B|} we have a type \cs{|a = b|}.
%
Almost all types in this family are empty (uninhabited, contain no
values) but a few contain one value written \cs{|Refl : a = a|}. Thus, a
value \cs{|p : a = b|} tells us that \cs{|a|} and \cs{|b|} are equal (and \cs{|p|} is a
proof of that fact).
%
Here are two examples of using equality types to specify properties
of multiplication:

> multUnitSpec   :  (y : Double)        -> 1*y = y
> multAssocSpec  :  (x, y, z : Double)  -> (x*y)*z = x*(y*z)

%%%

\subsection{Dependent types and data declarations}
\label{subsection:DT}

Many programming languages use types to make sure the code doesn't go
wrong, but dependently typed languages support types which depend on
values.
%
We have already seen some examples: \cs{|multUnitSpec|} is a function whose
return type \cs{|(1*y = y)|} depends on \cs{|y|}, a value of type \cs{|Double|}. The
equality type \cs{|x = y|} depends on the two values \cs{|x|} and \cs{|y|}.

We can take advantage of dependent types to specify requirements that
multiplication shall fulfil, as in the \cs{|multUnitSpec|} and
\cs{|multAssocSpec|} examples. We can also apply dependent types to restrict
the values of arguments, for example, to specify a square root function
that accepts only non-negative arguments.
%
Restricting sounds negative, but it allows to avoid nonsensical
combinations of values, which helps to eliminate whole classes of
software bugs.

%% \subsection{Datatypes}
%% \label{subsubsection:data}

Let's start with a non-dependent data declaration that introduces
natural numbers:

< data Nat : Type where
<   Z  : Nat
<   S  : Nat -> Nat

\noindent
This states that \cs{|Nat|} is a type and that values of type \cs{|Nat|} can be
constructed using \cs{|Z|} for zero and \cs{|S n|} for the successor of \cs{|n :
Nat|}. The same declaration can be written in a less verbose form

< data Nat = Z | S Nat

where the vertical bar separates the two \emph{data constructors} \cs{|Z|}
and \cs{|S|}. In this form, the types of \cs{|Nat|}, \cs{|Z|} and \cs{|S|} are implicit. We
apply this form of data declaration in section \ref{subsection:XY},
e.g., to define the data types \cs{|State|} and \cs{|StartDelay|}.


%% \subsection{Lists, vectors}
%% \label{subsubsection:vectors}

With \cs{|Nat|} and the syntax for \cs{|data|} declarations in place we move on to
the more complex example of lists of fixed-length:

< data Vect : Nat -> Type -> Type where
<   Nil   :  Vect Z a
<   (::)  :  (x : a) -> (xs : Vect n a) -> Vect (S n) a

\noindent
Thus, for any \cs{|n : Nat|} and \cs{|A : Type|}, values of type \cs{|Vect n
A|} are lists of length \cs{|n|} of elements of type \cs{|A|}. For example

> xs : Vect 3 Double
> xs = 0.1 :: 0.6 :: 0.4 :: Nil

\noindent
Idris also provides syntax extensions for defining vectors and lists of
variable length in square brackets notation:

> ys : Vect 4 Nat
> ys = [1,2,3,4]

> zs : List String
> zs = []

\noindent
A simple example of a vector based function is \cs{|head|} which extracts the
first element of a vector:

> head : {n : Nat} -> {A : Type} -> Vect (S n) A -> A
> head (x :: xs) = x

\noindent
Note that \cs{|head|} is only defined for non-empty vectors: vectors of
length \cs{|S n|} for some \cs{|n|}. By restricting the arguments of \cs{|head|}, we
make sure that the function is never applied to empty vectors, thus
eliminating a common source of errors.

In section \ref{section:theory} we use a data declaration similar to
\cs{|Vect|} to define a datatype \cs{|PolicySeq|} for sequences of policies of
fixed-length.

%%%

\subsection{Properties as types, specifications}
\label{subsection:PAT}

In programming, the type \cs{|Bool|} is often used to collect the two truth
values \cs{|False|} and \cs{|True|} and to implement run-time ``truth'' tests.
%
In dependently typed programming and constructive mathematics we can go
one step further and represent truth ``values'' at the type level.  Such
truths can then be type-checked at compile time and before a (possibly
incorrect) program is executed.

In much the same way as \cs{|(=) : A -> B -> Type|} represents equality of
expressions at the type level (remember section
\ref{subsubsection:pfagp}) and allows us to construct equality proofs
straightforwardly,

> p : 2 + 3 = 5
> p = Refl

\noindent
we can represent other binary relations through types. For example, we
can define a ``smaller or equal'' relation for natural numbers

> (<=) : Nat -> Nat -> Type

\noindent
or, as in section \ref{subsection:specification_components}, one that
compares values in \cs{|Val|}, the type used there to represent rewards. In
all cases the idea is the same: any inhabited type (any type for which
we can provide a value, like \cs{|Refl|} for equality) represents truth and
empty types represent falsity. An empty type, usually called \cs{|Void|}, is
easily defined through a \cs{|data|} declaration with no constructors

< data Void : Type where

and \cs{|(<=)|} can be defined in such a way that \cs{|7 <= 3|} is empty (thus, no
values of this type can be constructed) and values of type \cs{|3 <= 7|} can
be constructed easily, similarly to values of type \cs{|2 + 3 = 5|}.

The function \cs{|(<=)|} is an example of a binary \emph{predicate} on
natural numbers. Similarly, unary predicates on values of type \cs{|A|} can
be represented by functions \cs{|P : A -> Type|}.
%
Predicates are useful to specify properties of computations, as we
have already seen with \cs{|multUnitSpec|} and \cs{|multAssocSpec|}. Here is another
example: given a sorting function

> sort : {n : Nat} -> {A : Type} -> Vect n A -> Vect n A

and a predicate representing ``sortedness'':

> Sorted : {n : Nat} -> {A : Type} -> Vect n A -> Type

we can formulate the requirement that \cs{|sort|} shall return sorted
vectors:

> sortSpec : {n : Nat} -> (xs : Vect n A) -> Sorted (sort xs)

Any valid implementation of \cs{|sortSpec|} is then logically equivalent to a
proof that, for any vector \cs{|xs|}, \cs{|sort xs|} is sorted\footnote{An
  implementation of \cs{|sortSpec|} is not enough to guarantee that \cs{|sort|} is
  correct but is a first step in the right direction.}.  More generally,
any function of type \cs{|(x : A) -> P x|} (for any predicate \cs{|P : A ->
Type|}) is logically equivalent to a proof that \cs{|P x|} is non-empty for
every \cs{|x : A|}.
%
In section \ref{section:theory} we exploit this equivalence to posit
that the set of controls \cs{|Y t x|} associated with a state \cs{|x : X t|}
shall not be empty through the function \cs{|notEmptyY|}.

%%%

%if False

\subsection{Existential types}

We have seen that functions of type \cs{|(x : A) -> P x|} are logically
equivalent to proofs of (non-emptiness of) \cs{|P x|} for every \cs{|x :
A|}.

With dependent types we can also encode the notion ``there exists an \cs{|x
: A|} such that \cs{|P x|} holds''. What is captured is not a classical
existential quantifier but a constructive quantifier: we require both a piece of
evidence \cs{|x : A|} and a proof \cs{|px : P x|} that \cs{|P|} holds at \cs{|x|}:

%format ** = "\mathbin{*\!*}"
%{
%format Exists = "\Varid{Exists}"
% TODO: Currently lhs2TeX makes the keyword Exists come out as the
% $\exists$ symbol. That should probably be changed (at least for the
% preliminaries).

< data Exists  :  {A : Type} -> (A -> Type) -> Type where
<   Evidence   :  {A : Type} -> {P : A -> Type} ->
<                 (x : A) -> (px : P x) -> Exists P

In addition to the logical reading you can also see \cs{|Evidence x px|} as a
dependent pair in which the type of the second element \cs{|px|} depends on
the value \cs{|x|} of the first one.

%endif

%%%

\subsection{Programs, proofs, and totality}
\label{subsection:CH}

We have seen that, in dependently typed languages, properties can be
represented by types and proofs by values of these types.
%
The correspondence between functional notation and logic goes
deeper
% \TODO{Nuria, Patrik: Cite C-H?} closed 21-10-28
and we sum up the main results in
Table~\ref{fig:CurryHoward}:

\begin{table}[h]
\begin{center}
  \begin{tabular}{||l@@{\quad}||l||}
    \hline
    Functional notation (Idris)  & Logic
    \\ \hline
    \cs{|p : P|}  (\cs{|p|} is a
    program of type \cs{|P|})     & \cs{|p|} is a proof of \cs{|P|}
    \\ inhabited type        & provable proposition
    \\ empty type            & False
    \\ singleton type        & True
    \\ \cs{|P -> Q|}              & \cs{|P|} implies \cs{|Q|}
    \\ \cs{|Exists {A} P|}        & there exists a witness \cs{|x : A|} such that \cs{|P x|} holds
    \\ \cs{|(x : A) -> P x|}      & forall \cs{|x|} of type \cs{|A|}, \cs{|P x|} holds
    \\ \hline
  \end{tabular}
\end{center}
\vspace{2mm}
\caption{Propositions-as-Types and Proofs-as-Programs (``Curry-Howard'')
  correspondence
  relating dependent type theory and logic \citep{Howard69, DBLP:journals/cacm/Wadler15}.}
\label{fig:CurryHoward}
\end{table}

\noindent
When we embed logic in a dependently typed language, we have to require
all our functions to be total (otherwise the logic will be inconsistent).

A total function \cs{|f : A -> B|} is defined for all \cs{|x : A|}, whereas a
partial function is undefined for some \cs{|x : A|}. If partial functions
were allowed, we could use them to prove any theorem, including patently
false ones.
%
A simple example is the partial function \cs{|headL : List A -> A|}
which is undefined for empty lists. Using \cs{|headL|} we could easily
prove a false theorem (like |3 = 5|) by first building an empty list
of proofs, |[] : List (3 = 5)|, and then extracting the first element:

> surprise : 3 = 5
> surprise = headL []

%if False

> surprise2 : 3 = 5
> surprise2 = void (headL [])

%endif

A function may cover all cases, but still fail to be total. An extreme
example is the completely circular definition

> circular : Void
> circular = circular

\noindent
If we require functions to be total (which we do in sections
\ref{section:theory} to~\ref{section:application}), the totality checker
will warn about missing cases and potentially circular definitions.

%%%

%if False

\subsection{Type checking, correctness and literate programming}
\label{subsubsection:tcac}

Throughout this paper, we specify computations using properties-as-types
and apply the Idris type checker to verify that the computations satisfy
their specifications.

The whole paper is written as literate Idris programs, one program per
section. The programs have been processed with lhs2\TeX \citep{lhs2tex}
and with latexmk to generate a PostScript manuscript.

All files are publicly available at \citep{papers}. The program for this
section is in folder ``2021.Responsibility under uncertainty: which
climate decisions matter most?''.

%endif
