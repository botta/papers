% -*-Latex-*-

%if False

> module Generic

> import Data.So
> import Effects

> import Effect.Exception
> import Effect.StdIO

> import Basic.Predicates
> import FastSimpleProb.Measures
> import FastSimpleProb.Functor
> import NonNegDouble.NonNegDouble
> import NonNegDouble.Constants
> import NonNegDouble.BasicOperations
> import NonNegDouble.Operations
> import NonNegDouble.Predicates
> import NonNegDouble.Properties
> import NonNegDouble.LTEProperties
> import Double.Predicates
> import LocalEffect.Exception
> import LocalEffect.StdIO
> import Fun.Operations

> import Theory
> import Specification

> %default total
> %access public export
> %auto_implicits off

> infixr 5 <&&>
> infixr 4 <||>

> Subset : Type -> Type
> Subset X = X -> Bool

> Region : Type
> Region = (t : Nat) -> Subset (X t)

> elem : (t : Nat) -> X t -> Subset (X t) -> Bool
> elem t x xs = xs x

> data Goal : Type where
>   Exit    :  Region -> Goal
>   Enter   :  Region -> Goal
>   StayIn  :  Region -> Goal
>   Avoid   :  Region -> Goal
>   (<&&>)  :  Goal -> Goal -> Goal
>   (<||>)  :  Goal -> Goal -> Goal
>   Not     :  Goal -> Goal

> eval : Goal -> (t : Nat) -> (x : X t) -> Y t x -> X (S t) -> Bool 
> eval (Exit r)     t x y x'  =  let t' = S t in       elem t x (r t)   &&  not (elem t' x' (r t'))
> eval (Enter r)    t x y x'  =  let t' = S t in  not (elem t x (r t))  &&       elem t' x' (r t')
> eval (StayIn r)   t x y x'  =  let t' = S t in                                 elem t' x' (r t')
> eval (Avoid r)    t x y x'  =  let t' = S t in                            not (elem t' x' (r t'))
> eval (g <&&> g')  t x y x'  =  eval g t x y x' && eval g' t x y x'
> eval (g <||> g')  t x y x'  =  eval g t x y x' || eval g' t x y x'
> eval (Not g)      t x y x'  =  not (eval g t x y x')

> Theory.Val    =  NonNegDouble
> Theory.(<+>)  =  (+)
> Theory.zero   =  cast 0.0
> Theory.(<=)   =  LTE
> Theory.lteTP  =  totalPreorderLTE

> goal : Goal

> Theory.reward t x y x' = if eval goal t x y x'
>                          then cast 1.0
>                          else cast 0.0

%endif

\section{Generic goal functions and responsibility measures} 
\label{section:generic}

In the last section we have introduced a measure \cs{|mMeas|} of how much
decisions under uncertainty matter.
%
We have constructed \cs{|mMeas|} for the decision process of section
\ref{section:specification} in three steps and we have seen that, for
this problem, \cs{|mMeas|} fulfills the requirements for responsibility
measures put forward in the introduction.

Specifically, in S1-S3, we have introduced an ad-hoc definition of the
reward function \cs{|reward|} in terms of the (implicit) goal of avoiding \cs{|L|}-
(low economic wealth) and \cs{|C|}- (committed) states and we have defined
\cs{|mMeas|} in terms of the normalized difference between the value of two
policy sequences.
%% In computing such difference, we have exploited the fact that \cs{|Val|}, the
%% type of rewards, had been defined to be \cs{|NonNegDouble|} and that
%% non-negative, double precision floating point numbers can be naturally
%% embedded into double precision floating point numbers, see \cs{|toDouble|} in
%% the definition of \cs{|mMeas|}.

In this section we generalize this construction: we drop the ad-hoc
definition of \cs{|reward|} from section \ref{section:responsibility} and
introduce instead a small DSL to express goals explicitly.
%
The DSL is implemented as an extension of the theory from section
\ref{section:theory} and consists of two artifacts: an abstract syntax
and an interpretation function \cs{|eval|}. The reward function is then
defined generically in terms of the interpretation.

%

\subsection{A minimal DSL for specifying goals} 
\label{subsection:goals}

Remember the definition of \cs{|reward|} from S1 of section
\ref{section:responsibility}:

< Theory.reward t x y x'  =  if isCommitted (S t) x' || isDisrupted (S t) x' then (cast 0.0) else (cast 1.0)

%
%%\REMARK{Nuria}{Maybe suppress the implicit arguments in the visible text?}
%
%% Nicola: done (implicit args were a mistake)
%

\noindent
and that \cs{|reward t x y x'|} represents the reward associated with
reaching state \cs{|x'|} when taking decision \cs{|y|} in state \cs{|x|} at decision
step \cs{|t|}, rewards are non-negative double-precision floating point
numbers (\cs{|Val|} = \cs{|NonNegDouble|}) and the rules for adding and comparing
rewards are the canonical operations for this type.

In this formulation, the goal (avoiding states that are committed or
that have a low level of economic wealth) for which we measure how much
decisions matter is stated implicitly through the definition of
\cs{|reward|}.

Instead, we want to hide the definition of \cs{|reward|}. In the theory
of section \ref{section:theory}, this is the function that has to be
specified to express the goal of decision making. Implementing
\cs{|reward|} could be challenging for domain experts with little
computer science background. We want to give them a means to avoid the
implementation and at the same time the opportunity of putting forward
the goal of decision making transparently. This can be done through the
definition:

> goal = Avoid isCommitted <&&> Avoid isDisrupted

Here \cs{|Avoid|} is a function that maps Boolean predicates to goals. It is
the fourth constructor of the abstract syntax

< data Goal : Type where
<   Exit    :  Region -> Goal
<   Enter   :  Region -> Goal
<   StayIn  :  Region -> Goal
<   Avoid   :  Region -> Goal
<   (<&&>)  :  Goal -> Goal -> Goal
<   (<||>)  :  Goal -> Goal -> Goal
<   Not     :  Goal -> Goal

%
%% \REMARK{Nuria}{I added some format instructions to main.fmt, among
%%   others for $<\&\&>$ and $<\mid\mid>$, if you would like to modify them
%%   further.}
%
%% Nicola: thanks!
%
to specify goals for decision processes that are informed by notions of
sustainable development or management \citep{heitzig16ab,
  IPCC_SR15_summary}: such goals are typically phrased in terms of a
verb (\emph{avoid}, \emph{exit}, \emph{enter}, \emph{stay within}, etc.)
and of a \emph{region} (predicate, subset of states) that encode notions
of planetary boundaries or operational safety\footnote{for example, in
  \citep{heitzig16ab}, a partitioning of the state space into a
  \emph{sunny} region and its \emph{dark} complement is the starting
  point for the construction of a hierarchy of regions: shelters,
  glades, lakes, trenches and abysses, see figure 1 at page 7.}.
%
In our formalization, such regions are encoded by

< Region : Type
< Region = (t : Nat) -> Subset (X t)

where \cs{|Subset A|} is an alias for \cs{|A -> Bool|}. Notice the usage of the
conjunction \cs{|<&&>|} in the specification of \cs{|goal|}. Its semantics, like
the semantics of the other constructors of the syntax, is given by the
interpretation function

< eval : Goal -> (t : Nat) -> (x : X t) -> Y t x -> X (S t) -> Bool 
< eval (Exit r)     t x y x'  =  let t' = S t in       elem t x (r t)   &&  not (elem t' x' (r t'))
< eval (Enter r)    t x y x'  =  let t' = S t in  not (elem t x (r t))  &&       elem t' x' (r t')
< eval (StayIn r)   t x y x'  =  let t' = S t in                                 elem t' x' (r t')
< eval (Avoid r)    t x y x'  =  let t' = S t in                            not (elem t' x' (r t'))
< eval (g <&&> g')  t x y x'  =  eval g t x y x' && eval g' t x y x'
< eval (g <||> g')  t x y x'  =  eval g t x y x' || eval g' t x y x'
< eval (Not g)      t x y x'  =  not (eval g t x y x')

While the definition of \cs{|eval|} is almost straightforward\footnote{In
  this definition, \cs{|elem t|} takes a state \cs{|x : X t|} and a Boolean
  function on states (a subset of states) \cs{|s : Subset (X t)|} and
  applies \cs{|s|} to \cs{|x|}.}, domain experts do not need to be concerned with
it. They just apply the constructors of \cs{|Goal|} to specify the goal of
decision making like in the definition of \cs{|goal|} given above. The goal
for which we measure how much decisions matter is then fully transparent
and the rewards are a straightforward function of \cs{|eval goal|}:

< Theory.reward t x y x' = if eval goal t x y x' then cast 1.0 else cast 0.0


\subsection{Degrees of commitment, fuzzy predicates} 
\label{subsection:fuzzy}

In more realistic (as opposed to stylized, see section
\ref{subsection:realistic}) GHG emissions decision processes, states are
not necessarily either fully committed or fully uncommitted to severe
impacts from climate change and decision makers are confronted with many
degrees of commitment, possibly infinitely many.

A similar situation holds for other predicates on states, like being
\emph{vulnerable} (or \emph{adapted}) to climate change or for measures
of economic growth or welfare.
%
This raises the question of how to specify the goals of decision making
in decision processes in which predicates like \cs{|isCommitted|} do not
return Boolean values but, for example, values in $[0,1]$.
%
In this situation, a partitioning of the state space into regions is not
immediately available and the specification of goals requires an
extension both of the syntax \cs{|Goal|} for encoding goals and of the
interpretation function \cs{|eval|} associated with this syntax.

Discussing such extensions here would go well beyond the scope of this
paper. However, the problem of developing a DSL for expressing the goals
of decision making (and defining reward functions that are consistent
with such goals) for realistic decision processes is a crucial step
towards rationalizing decision making in climate policy advice and we
plan to tackle this problem in an upcoming work.
%%\TODO{Nuria: cite the DSL TiPES report.}.


\subsection{Some caveats}
\label{subsection:caveats}

%if False

* Consistency check with section \ref{section:responsibility}:

> mMeas : (t : Nat) -> (n : Nat) -> X t -> Double
> mMeas t  Z    x  =  0.0
> mMeas t (S m) x  =  let ps  =  bi (S t) m in
>                     let v   =  toDouble (val (bestExt ps :: ps) x) in
>                     let v'  =  toDouble (val (worstExt ps :: ps) x) in
>                     if v == 0 then v else (v - v') / v

> Theory.meas = expectedValue

%endif

With \cs{|mMeas|} defined as in section \ref{section:responsibility} and with
\cs{|goal : Goal|} specified as above, one can recover the results for the
decision process of section \ref{section:specification}.
%
Before we turn back to this process in the last section, let us
discuss a few aspects of the responsibility measures discussed so far.

One important trait of these measures is that they are obtained by
extending the decision process for which one wants to measure how much
decisions matter to a fully specified finite horizon sequential decision
problem.
%
In comparison to approaches like those proposed in
\citep{10.1109/QEST.2006.9}, \citep{10.5555/1622487.1622491} and, more
recently, \citep{heitzig2020degrees}, this approach has both advantages
and disadvantages.

From the conceptual point of view, the major advantages are simplicity
and straightforwardness: in contrast to models of causality like those
put forward in the works mentioned above, finite horizon sequential
decision problems are conceptually simple and well understood.
%
Also, for finite horizon sequential decision problems, we can
compute \emph{verified} best and worst policies. This guarantees that
the results obtained for a specific problem are a logical consequence of
the assumptions made for that problem and not of programming errors or
numerical errors.
%
Because all the assumptions underlying a specific problem are put
forward explicitly via specifications like

< goal = Avoid isCommitted <&&> Avoid isDisrupted,

the approach also guarantees high standards of transparency.
Simplicity and straightforwardness are also the main drawbacks of our
approach: we can only derive responsibility measures for decision
processes that can be naturally extended to finite horizon sequential
decision problems.

This is the case for the stylized GHG emissions decision process
discussed throughout our work and, indeed, for many interesting problems
in climate policy because, as pointed out in \citep{webster2008}:
%
\begin{quote}
Climate policy decisions are necessarily sequential decisions over time
under uncertainty, given the magnitude of uncertainty in both economic
and scientific processes, the decades-to-centuries time scale of the
phenomenon, and the ability to reduce uncertainty and revise decisions
along the way.
\end{quote}
%
But it is not immediately obvious how our approach could be applied to
measure how much decisions matter in situations in which collective
decisions emerge from a potentially large number of
individual decisions, e.g., mediated through certain widely accepted
mechanisms like majoritarian rules like in voting processes. 

Another important aspect of the measures of responsibility proposed in
this work is the comparison between verified best and what we called
``conditional worst'' decisions at the specific state at which we want
to measure responsibility. Remember that, in the definition of \cs{|mMeas|},
\cs{|v|} and \cs{|v'|} are \cs{|val (bestExt ps :: ps) x|} and \cs{|val (worstExt ps :: ps)
x|}, respectively. Here, \cs{|x : X t|} is a state at decision step \cs{|t|}, \cs{|ps|}
is a verified optimal sequence of policies for taking \cs{|n|} decisions
starting from step \cs{|t + 1|} and \cs{|n + 1|} is the decision horizon.

Due to the definition of \cs{|bestExt|}, generic backward induction and
\cs{|biLemma|} from section \ref{subsection:solution_components}, \cs{|bestExt ps
:: ps|} is an optimal policy sequence and \cs{|bestExt ps|} is an optimal
policy (a function from states to controls) at decision step
\cs{|t|}. Similarly \cs{|worstExt ps|} is a policy that guarantees

< val (  worstExt ps :: ps) x <= val (p :: ps) x
 
for all \cs{|x : X t|} and \cs{|p : Policy t|}. In other words, we compare ``best''
decision (given by \cs{|bestExt ps|}) and "worst" decision (given by \cs{|worstExt
ps|}) in \cs{|x|} \emph{conditional} to future decisions being best ones.

This is crucial because the difference between best and worst decisions
(and hence our estimates of how much decisions matter) at a given step
and in a given state would in general be different if we assumed that
future decisions are not taken optimally.

In the context of our decision problem, for example, we would
come up with a different measure of responsibility for ``current''
decisions if we assumed that future generations do not care about
avoiding negative impacts from climate change or economic downturns or,
equivalently, that they do care but do not act accordingly. If there are
reasons to believe that this is the case, the verified optimal policy
sequence \cs{|ps|} in the definition of \cs{|mMeas|} has to be replaced with one
which is consistent with such a belief. For example, if we believe that
the next generation will act more myopically (or more farsighted) than
for a horizon of \cs{|n|} decision steps, we have to compute \cs{|ps|}
accordingly. This can be done using the verified methods of the
\citep{2017_Botta_Jansson_Ionescu} theory.

Finally, we want to flag the role of the measure of uncertainty \cs{|meas|}
from section \ref{section:theory} in the definition of \cs{|val|} and thus of
\cs{|v|} and \cs{|v'|}. In all computations shown in this paper we have taken
\cs{|meas|} to be the \emph{expected value measure} but other measures of uncertainty
are conceivable and we refer interested readers to \citep{ionescu2009,
2017_Botta_Jansson_Ionescu} and \citep{brede2021monadic}.
