# On the Correctness of Monadic Backward Induction

This directory contains the sources of [1] and some supplementary material.

## Contents

- [paper](paper): The literate Idris sources of [1]. The sources can be
  compiled to pdf using `make` and type-checked using the command `idris
  --checkpkg paper.ipkg`.

- [code](code): A condensed version of the source code provided as
supplementary material to the paper, see [README](code/README.md).

- [lwtheory](lwtheory): The lightweight version of the BJI-framework
  discussed in [1], together with code for computing optimal
  extensions. Requires [2] to type-check, see [README](lwtheory/README.md).

The files have been type-checked with Idris 1.3.2, Idris 1.3.3-git:ae98085b8 and
IdrisLibs v1.3.3. If you encounter any issues, please get in touch with
Nuria Brede (nubrede@pik-potsdam.de) or Nicola Botta
(botta@pik-potsdam.de).


### Acknowledgment

This is [TiPES](https://www.tipes.dk/) contribution No 37.
This project has received funding from
the European Union’s Horizon 2020 research and innovation programme
under grant agreement No 820970.


### Timeline

* 2020-07-14: Submitted to the Journal of Functional Programming (JFP)
* 2020-11-02: Decision: Major revision
* 2021-02-23: Submitted revised manuscript (R1)
* 2021-05-26: Decision: Major revision
* 2021-07-22: Submitted revised manuscript (R2)
* 2021-08-28: Decision: Accepted with very minor revision
* 2021-09-01: Submitted revised manuscript (final)

---

[1] Nuria Brede and Nicola Botta. (2021)
    On the Correctness of Monadic Backward Induction.

[2] Nicola Botta. (2016-21)
    [IdrisLibs](https://gitlab.pik-potsdam.de/botta/IdrisLibs)


[3] Botta, N., Jansson, P., Ionescu, C. (2017).
    [Contributions to a computational theory of policy advice and avoidability](https://doi.org/10.1017/S0956796817000156). J. Funct. Program. 27:e23.
