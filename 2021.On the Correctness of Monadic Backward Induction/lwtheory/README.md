# Lightweight BJI-Framework

## Contents

- [LightweightTheory](LightweightTheory.lidr):
  This file contains the lightweight version of the BJI-framework discussed in [1], augmented with the necessary implementations for computing optimal extensions and using the framework's backward induction algorithm.
  This file needs the library from [2] to type-check.

## Type-checking

To type check `LightweightTheory.lidr`

- download IdrisLibs [2].

- enter `idris -i $IDRISLIBS --sourcepath $IDRISLIBS --allow-capitalized-pattern-variables LightweightTheory.lidr` to type-check and load the file into the Idris REPL
or

- enter `idris -i $IDRISLIBS --sourcepath $IDRISLIBS --allow-capitalized-pattern-variables --check LightweightTheory.lidr` to just type-check the file

where `IDRISLIBS` is the path of `IdrisLibs`.


## References

[1] Brede, N., Botta, N. (2021). On the Correctness of Monadic Backward Induction. *submitted to J. Funct. Program*.

[2] Botta, N. (2016-2021). [IdrisLibs](https://gitlab.pik-potsdam.de/botta/IdrisLibs).

[3] Botta, N., Jansson, P., Ionescu, C. (2017). Contributions to a computational theory of policy
advice and avoidability. J. Funct. Program. 27:e23.
