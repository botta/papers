> module ListMeasures

> import Syntax.PreorderReasoning

> import Equality

> import Framework
> import List

> %default total
> %auto_implicits off
> %access public export



--------------------------------------------------------------------------
In this file we show that for monoids |(Val, odot, neutr)| and measures 
that are monoid homomorphisms |meas = foldr odot neutr| 
the three conditions hold, if |<+>| distributes over |odot| on the left.
(Otherwise just |measPureSpec| and |measJoinSpec| hold.)
We also show that |measMonSpec| holds if |odot| is monotone with 
respect to |<=|.  
--------------------------------------------------------------------------


Define

> odot     :  Val -> Val -> Val

> neutr    :  Val

> odotList :  List Val -> Val

which can be written as |List|-fold

> odotList = foldr odot neutr 


and consider the following (partial) instance of the framework:

< Framework.M = List     -- imported from |List.lidr|

> Framework.meas   = odotList


We require |(Val, odot, neutr)| to be a monoid.

> odotNeutrRight     : (l : Val) ->  l `odot` neutr = l  

> odotNeutrLeft      : (r : Val) ->  neutr `odot` r = r
> 
> odotAssociative    : (l, v, r : Val) ->  l `odot` (v `odot` r) = (l `odot` v) `odot` r 

> oplusOdotDistrLeft : (n, l, r : Val) -> n <+> (l `odot` r) = n <+> l `odot` n <+> r
 

Then |odotList| is a monoid homomorphism and thus preserves the binary operation. 

> odotListAppend  :  (xs, xs' : List Val) -> 
>                    odotList (xs ++ xs') = ((odotList xs) `odot` (odotList xs'))
> odotListAppend [] xs'         = rewrite odotNeutrLeft (odotList xs') in Refl
> odotListAppend (x :: xs) xs'  = 
>       rewrite odotListAppend xs xs'                           in 
>       rewrite odotAssociative x (odotList xs) (odotList xs')  in Refl  


------------------------------------
All four measure requirements hold:
------------------------------------


measPureSpec:
-------------

> -- |meas . pure `ExtEq` id|
> Framework.measPureSpec a = odotNeutrRight a


measJoinSpec:
-------------

< measJoinSpec  : odotList . map odotList `ExtEq` odotList . concat

> Framework.measJoinSpec  []                 = Refl
> Framework.measJoinSpec ([] :: vss)         = 
>   rewrite odotNeutrLeft (odotList (map odotList vss))  in
>   rewrite measJoinSpec vss                             in 
>   Refl
> Framework.measJoinSpec ((v :: vs) :: vss)  = 
>   rewrite sym (odotAssociative v (odotList vs) (odotList (map odotList vss)))  in 
>   rewrite measJoinSpec vss                                                     in
>   rewrite sym (odotListAppend vs (concat vss))                                 in  
>   Refl  
>
> {--
>   -- names
>   let odot           = odot in
>   let odotL          = odotList in
>   -- proof contexts
>   let c             = odot v in
>   let c'            = \prf=> v `odot` (odotL vs `odot` prf) in
>   -- proof steps
>   let useOdotAssoc   = odotAssociative v (odotL vs) (odotL (map odotL vss)) in
>   let useIH         = measJoinSpec vss in
>   let useOdotAppend  = odotListAppend vs (concat vss) in
>   -------------------------------------------------------------------------
>   ( ( odotL . map odotL) ((v :: vs) :: vss) )             ={ Refl }=
>   
>   ( (v `odot` odotL vs) `odot` (odotL (map odotL vss)) )  ={sym useOdotAssoc}=
>   
>   ( v `odot` (odotL vs `odot` (odotL (map odotL vss))) )  ={cong {f=c'} useIH}=
>   
>   ( v `odot` (odotL vs `odot` (odotL (concat vss))) )     ={cong {f=c} (sym useOdotAppend)}=
>   
>   ( v `odot` (odotL (vs ++ concat vss)) )                 ={Refl}=
>   
>   ( (odotL . concat) ((v :: vs) :: vss) )                 QED

> --} 


measPlusSpec:
-------------

< measPlusSpec : (v : Val) -> (mv : List Val) -> NotEmpty mv ->
<               odotList . map ((<+>) v) mv = (((<+>) v) . odotList) mv
 
> Framework.measPlusSpec _ []         ne =  void ne 
> Framework.measPlusSpec v (x :: [])  _  =  rewrite (odotNeutrRight x)          in 
>                                           rewrite (odotNeutrRight (v <+> x))  in 
>                                           Refl
> Framework.measPlusSpec v (x :: (x' :: xs)) _ = 
>    rewrite measPlusSpec v (x' :: xs) ()                        in
>    rewrite sym (oplusOdotDistrLeft v x (odotList (x' :: xs)))  in
>    Refl 
> {-
>    -- proof steps
>    let useIH = measPlusSpec v (x' :: xs) ()                              in
>    let useOplusOdotDistr = oplusOdotDistrLeft v x (odotList (x' :: xs))  in
>    -------------------------------------------------------------------
> 
>    ( (meas . map (v <+>)) (x :: (x' :: xs)) )               ={ Refl }=
>        
>    ( odotList ((v <+> x) :: map (v <+>) (x' :: xs)) )       ={ Refl }=
>        
>    ( (v <+> x) `odot`  odotList (map (v <+>) (x' :: xs)) )  ={ cong useIH }=
>        
>    ( (v <+> x) `odot`  ((v <+>) . odotList) (x' :: xs) )    ={ sym useOplusOdotDistr }=
>        
>    ( v <+> (x `odot`  odotList (x' :: xs) ) )               ={ Refl }=
>        
>    ( ((v <+>) . odotList) (x :: (x' :: xs)) )               QED
> --}



measMonSpec:
------------

If |odot| is monotone with respect to |<=|, 

> odotMon   :  {a, b, c, d : Val} -> a <= b -> c <= d -> (a `odot` c) <= (b `odot` d)

we can moreover show 

< measMonSpec   :  {A : Type} -> (f, g : A -> Val) -> ((a : A) -> (f a) <= (g a)) ->
<                  (ma : M A) -> meas (map f ma) <= meas (map g ma)

> Framework.measMonSpec f g ee []           =  lteRefl
> Framework.measMonSpec f g faga (a :: as)  =  
>    let ltefaga = faga a                  in
>    let lteIH   = measMonSpec f g faga as in
>    odotMon ltefaga lteIH
    
