> module Equality
 
> %default total
> %auto_implicits off
> %access public export


> ExtEq : {A, B : Type} -> (f, g : A -> B) -> Type
> ExtEq {A} f g = (a : A) -> f a = g a
