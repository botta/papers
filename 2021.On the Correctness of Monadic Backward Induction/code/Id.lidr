> module Id

> import Syntax.PreorderReasoning

> import Equality
> import Framework

> %default total
> %auto_implicits off
> %access public export



We consider an instance of the framework for deterministic 
sequential decision problems (thus with degenerated uncertainty), 
using the |Id| monad. 
All the definitions below are evidently trivial in this case.


> Framework.M T    = T
> Framework.map    = id
> Framework.pure   = id  
> Framework.join   = id
 
> Framework.(>>=) ma f  =  f ma


< mapPresExtEq :  {A, B : Type} -> (f, g : A -> B) ->
<                 f `ExtEq` g -> map f `ExtEq` map g

> Framework.mapPresExtEq {A} {B} f g ee a  =  ee a
 

< NotEmpty    :  {A : Type} -> Id A -> Type

> Framework.NotEmpty a  =  Unit


< pureNotEmpty  :  {A : Type} -> (a : A) -> NotEmpty (pure a)

> Framework.pureNotEmpty a  =  () 


< mapPresNotEmpty  :  {A, B : Type} -> (f : A -> B) -> (ma : M A) -> 
<                     NotEmpty ma -> NotEmpty (map f ma)
 
> Framework.mapPresNotEmpty f a  _   =  () 


< bindPresNotEmpty  :  {A, B : Type} -> (f : A -> M B) -> (ma : M A) -> 
<                      NotEmpty ma -> -> ((a : A) -> NotEmpty (f a)) ->
<                      NotEmpty (ma >>= f)
 
> Framework.bindPresNotEmpty f a ne  _  = ()


We omit the proofs of the standard monad laws for |Id|.

