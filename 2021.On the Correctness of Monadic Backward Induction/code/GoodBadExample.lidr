> module GoodBadExample

> import Data.Fin

> import Equality

> import Framework
> import List

> %default total
> %auto_implicits off
> %access public export

The following example corresponds to the toy "climate" SDP described in section 2.1 of [1].

Two "states of the world":

> data StateOfTheWorld = Good | Bad

> Framework.X _ = StateOfTheWorld

> finiteState : {t : Nat} -> Framework.X t -> Fin 2
> finiteState Good  = FZ
> finiteState Bad   = FS FZ

Two "emission control options":

> data EmissionLevel = Low | High

> Framework.Y _ _ = EmissionLevel

> finiteControl : {t : Nat} -> {x : Framework.X t} -> Framework.Y t x -> Fin 2
> finiteControl Low   = FZ
> finiteControl High  = FS FZ

< Framework.M = List

> Framework.Val   = Nat
> Framework.zero  = 0
> Framework.(<+>) = (+)


> maxList :  List Nat -> Nat
> maxList []         = 0
> maxList (x :: xs)  = x `maximum` (maxList xs)

> minList :  List Nat -> Nat
> minList []         = 0
> minList (x :: [])  = x
> minList (x :: xs)  = x `minimum` (minList xs)


Comment/uncomment to try the two measures:

> Framework.meas = minList
> -- Framework.meas = maxList  
 
 
Transition function:

> Framework.next _t Good Low   = [Good]
> Framework.next _t Bad  High  = [Bad]
> Framework.next _t _x   _y    = [Good, Bad] 

Reward/ cost function: 

> Framework.reward _t _x Low  Good  = 3
> Framework.reward _t _x High Good  = 2
> Framework.reward _t _x Low  Bad   = 1
> Framework.reward _t _x High Bad   = 0


Constant policies:

> constH : (t : Nat) -> Framework.Policy t
> constH _ = const High

> constL : (t : Nat) -> Framework.Policy t
> constL _ = const Low

Other simple policies:

> react : (t : Nat) -> Framework.Policy t
> react _ Good = High 
> react _ Bad = Low

> maintain : (t : Nat) -> Framework.Policy t
> maintain _ Good = Low
> maintain _ Bad = High


Example policy sequences:


> ps : Framework.PolicySeq 0 2
> ps = (constL 0 :: (constH 1 :: Nil))

> ps' : Framework.PolicySeq 0 2
> ps' = (constH 0 :: (constL 1 :: Nil))



> ps1 : Framework.PolicySeq 0 3 
> ps1 = constH 0 :: (constL 1 :: (constH 2 :: Nil))
 
> ps2 : Framework.PolicySeq 0 3
> ps2 = constH 0 :: (constH 1 :: (react 2 :: Nil))
 
> ps3 : Framework.PolicySeq 0 5
> ps3 = constH 0 :: (maintain 1 :: (react 2 :: (constH 3 :: (react 4 :: Nil))))




Some test cases:
----------------

Initial state:

> x0 : Framework.X 0
> x0 = Good
> -- x0 = Bad

> test : Type
> test = val ps x0 = val' ps x0 


> test1 : Type
> test1 = val ps1 x0 = val' ps1 x0

> test2 : Type
> test2 = val ps2 x0 = val' ps2 x0

> test3 : Type
> test3 = val ps3 x0 = val' ps3 x0 


