> module SchedulingExample

> import Data.Fin

> import Equality

> import Framework
> import Id

> %default total
> %auto_implicits off
> %access public export


The following example is the deterministic SDP from [Bertsekas1995, ch.1]
described in section 2.2 of [1].

For this example we make use of the possible dependencies 
for state and control spaces in the BJI-framework.

As type of states at time steps $0 < t < 4$ 
we use could the canonical finite set of size |2 * t|:

< Framework.X (S (S (S (S _))))  =  Void
< Framework.X Z                  =  Unit
< Framework.X t                  =  Fin (2 * (S t))

but we stay more closely to the diagram in figure 1b and define

> data State1    = A | C
> data State2    = AB | AC | CA | CD  
> data State3    = ABC | ACB | ACD | CAB | CAD | CDA

> Framework.X Z              = Unit
> Framework.X (S Z)          = State1
> Framework.X (S (S Z))      = State2
> Framework.X (S (S (S Z)))  = State3
> Framework.X _ = Void


As |A| and |C| are already the names of constructors, 
we use |AA| etc. for possible actions: 

> data Act = AA | BB | CC | DD 

> data Ctrl : Act -> Type where
>   Do : (a : Act) ->  Ctrl a  

The type of controls at each decision step and state is defined by 

> Framework.Y Z          _   = Either (Ctrl AA) (Ctrl CC)
> Framework.Y (S Z)      A   = Either (Ctrl BB) (Ctrl CC)
> Framework.Y (S Z)      C   = Either (Ctrl AA) (Ctrl DD)
> Framework.Y (S (S Z))  AB  = Ctrl CC
> Framework.Y (S (S Z))  AC  = Either (Ctrl BB) (Ctrl DD)
> Framework.Y (S (S Z))  CA  = Either (Ctrl BB) (Ctrl DD)
> Framework.Y (S (S Z))  CD  = Ctrl AA
> Framework.Y _          _   = Void
  

The SDP is deterministic, thus

< Framework.M = Id


As values we use natural numbers with their familiar structure

> Framework.Val = Nat

> Framework.zero = 0

> Framework.(<+>) = (+)

> Framework.meas = id 
 

The transition function following fig. 1b is then defined by:

> Framework.next Z          ()  (Left  (Do AA))  = A
> Framework.next Z          ()  (Right (Do CC))  = C
> Framework.next (S Z)      A   (Left  (Do BB))  = AB
> Framework.next (S Z)      A   (Right (Do CC))  = AC
> Framework.next (S Z)      C   (Left  (Do AA))  = CA
> Framework.next (S Z)      C   (Right (Do DD))  = CD
> Framework.next (S (S Z))  AB  (Do CC)          = ABC
> Framework.next (S (S Z))  AC  (Left  (Do BB))  = ACB
> Framework.next (S (S Z))  AC  (Right (Do DD))  = ACD
> Framework.next (S (S Z))  CA  (Left  (Do BB))  = CAB
> Framework.next (S (S Z))  CA  (Right (Do DD))  = CAD
> Framework.next (S (S Z))  CD  (Do AA)          = CDA
> 
> Framework.next (S (S (S _))) _ _ impossible 


and the reward (or cost) function by: 

> Framework.reward Z          ()  (Left  (Do AA))  A    = 5
> Framework.reward Z          ()  (Right (Do CC))  C    = 3
> Framework.reward (S Z)      A   (Left  (Do BB))  AB   = 2
> Framework.reward (S Z)      A   (Right (Do CC))  AC   = 3
> Framework.reward (S Z)      C   (Left  (Do AA))  CA   = 4
> Framework.reward (S Z)      C   (Right (Do DD))  CD   = 6
> Framework.reward (S (S Z))  AB  (Do CC)          ABC  = 3
> Framework.reward (S (S Z))  AC  (Left  (Do BB))  ACB  = 4
> Framework.reward (S (S Z))  AC  (Right (Do DD))  ACD  = 6
> Framework.reward (S (S Z))  CA  (Left  (Do BB))  CAB  = 2
> Framework.reward (S (S Z))  CA  (Right (Do DD))  CAD  = 4
> Framework.reward (S (S Z))  CD  (Do AA)          CDA  = 3
> 
> Framework.reward Z          _   _                _    = 0
> Framework.reward (S Z)      _   _                _    = 0
> Framework.reward (S (S Z))  _   _                _    = 0
> Framework.reward (S (S (S _))) _ _ _ impossible 


Some example policies:

> p0A : Framework.Policy 0
> p0A () = Left (Do AA) 

> p0C : Framework.Policy 0
> p0C () = Right (Do CC) 

> p1 : Framework.Policy 1
> p1 A = Left (Do BB) 
> p1 C = Left (Do AA)

> p2 : Framework.Policy 2
> p2 AB = Do CC 
> p2 AC = Left (Do BB) 
> p2 CA = Left (Do BB) 
> p2 CD = Do AA 

> psA : Framework.PolicySeq 0 3
> psA = (p0A :: (p1 :: (p2 :: Nil)))

> psC : Framework.PolicySeq 0 3
> psC = (p0C :: (p1 :: (p2 :: Nil)))


> testA : Type
> testA = val psA () = val' psA ()  -- 10 = 10

> testC : Type
> testC = val psC () = val' psC ()  -- 9 = 9

