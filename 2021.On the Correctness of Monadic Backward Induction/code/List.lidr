> module List

> import Syntax.PreorderReasoning

> import Equality
> import Framework

> %default total
> %auto_implicits off
> %access public export



We consider an instance of the framework 
for non-deterministic sequential decision problems, using the |List| monad: 

> Framework.M                =  List

< Framework.map f []         =  []
< Framework.map f (a :: as)  =  f a :: map f as

> Framework.map f            =  foldr (\a => ((f a) ::)) [] 
  
> Framework.pure a           =  [a]
 
> Framework.join             =  concat
 
> Framework.(>>=) ma f       =  (join . map f) ma


First we need to make sure that |map| preserves extensional equality:

< mapPresExtEq :  {A, B : Type} -> (f, g : A -> B) ->
<                 f `ExtEq` g -> map f `ExtEq` map g

> Framework.mapPresExtEq {A} {B} f g ee []         =  Refl
> Framework.mapPresExtEq {A} {B} f g ee (a :: as)  =  rewrite ee a in
>                                                     rewrite mapPresExtEq f g ee as in
>                                                     Refl
 

Then we need to define when a list is non-empty:

< NotEmpty    :  {A : Type} -> List A -> Type

> Framework.NotEmpty  Nil       =  Void
> Framework.NotEmpty (a :: as)  =  Unit


We have to show that |pure| fulfills the non-emptiness requirement:

< pureNotEmpty  :  {A : Type} -> (a : A) -> NotEmpty (pure a)

> Framework.pureNotEmpty a  =  () 


And that |map| and |(>>=)| preserve non-emptiness:

< mapPresNotEmpty  :  {A, B : Type} -> (f : A -> B) -> (ma : M A) -> 
<                     NotEmpty ma -> NotEmpty (map f ma)

> Framework.mapPresNotEmpty f []         ne  =  void ne 
> Framework.mapPresNotEmpty f (a :: as)  _   =  () 


To show |bindPresNotEmptySpec| we first prove a lemma about preservation of 
non-emptiness by |++|:

> appendPresNotEmptyLeft  :  {A : Type} -> (as, as' : List A) -> NotEmpty as ->
>                            NotEmpty (as ++ as') 
> appendPresNotEmptyLeft []         _    ne  =  void ne              
> appendPresNotEmptyLeft as         []   ne  =  rewrite appendNilRightNeutral as in ne
> appendPresNotEmptyLeft (a :: as)  _    _   =  ()


Then 

< bindPresNotEmpty  :  {A, B : Type} -> (f : A -> M B) -> (ma : M A) -> 
<                      NotEmpty ma -> -> ((a : A) -> NotEmpty (f a)) ->
<                      NotEmpty (ma >>= f)
 
> Framework.bindPresNotEmpty f []         ne  _     = void ne
> Framework.bindPresNotEmpty f (a :: as)  _   nefa  = 
>     appendPresNotEmptyLeft (f a) (join (map f as)) (nefa a) 


We omit the proofs of the standard monad laws for |List|.

