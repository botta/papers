> module MaxList

> import Syntax.PreorderReasoning

> import Equality

> import Framework
> import List

> %default total
> %auto_implicits off
> %access public export



--------------------------------------------------------------------------
In this file we consider an example instance such that |meas| and |oplus| 
fulfill the requirements |measPureSpec|, |measJoinSpec| and |measPlusSpec|.
--------------------------------------------------------------------------


Define

> maxList :  List Nat -> Nat

< maxList []         = 0
< maxList (x :: xs)  = x `maximum` (maxList xs)

which is a functor-algebra for |List|:

> maxList = foldr maximum 0  


and consider the following (partial) instance of the framework:

< Framework.M = List     -- imported from |List.lidr|

> Framework.Val    = Nat
> Framework.zero   = 0
> Framework.(<+>)  = (+)

> Framework.meas   = maxList


We need |maximumAssociative| and |maximumZeroNLeft| from the standard library 
and the following lemmas:

> plusMaxDistrLeft : (n, l, r : Nat) -> n + (l `maximum` r) = n + l `maximum` n + r
> plusMaxDistrLeft Z l r      = Refl
> plusMaxDistrLeft (S n) l r  = rewrite plusMaxDistrLeft n l r in Refl
 

> maxListAppend  :  (xs, xs' : List Nat) -> 
>                   maxList (xs ++ xs') = ((maxList xs) `maximum` (maxList xs'))
> maxListAppend [] xs'         = Refl
> maxListAppend (x :: xs) xs'  = 
>       rewrite maxListAppend xs xs'                             in 
>       rewrite maximumAssociative x (maxList xs) (maxList xs')  in Refl  



------------------------------------------------------
All three measure requirements hold for this instance:
------------------------------------------------------


measPureSpec:
-------------

> -- |meas . pure `ExtEq` id|
> Framework.measPureSpec a = maximumZeroNLeft a


measJoinSpec:
-------------

< measJoinSpec  : maxList . map maxList `ExtEq` maxList . concat

> Framework.measJoinSpec []                  = Refl
> Framework.measJoinSpec ([] :: vss)         = rewrite measJoinSpec vss     in Refl
> Framework.measJoinSpec ((v :: vs) :: vss)  =
> --{
>   rewrite sym (maximumAssociative v (maxList vs) (maxList (map maxList vss)))  in 
>   rewrite measJoinSpec vss                                                     in
>   rewrite sym (maxListAppend vs (concat vss))                                  in  
>   Refl 
> --}
> 
> {-  -- names
>   let max           = maximum in
>   let maxL          = maxList in
>   -- proof contexts
>   let c             = max v in
>   let c'            = \prf=> v `max` (maxL vs `max` prf) in
>   -- proof steps
>   let useMaxAssoc   = maximumAssociative v (maxL vs) (maxL (map maxL vss)) in
>   let useIH         = measJoinSpec vss in
>   let useMaxAppend  = maxListAppend vs (concat vss) in
>   -------------------------------------------------------------------------
>   ( ( maxL . map maxL) ((v :: vs) :: vss) )          ={ Refl }=
>   
>   ( (v `max` maxL vs) `max` (maxL (map maxL vss)) )  ={sym useMaxAssoc}=
>   
>   ( v `max` (maxL vs `max` (maxL (map maxL vss))) )  ={cong {f=c'} useIH}=
>   
>   ( v `max` (maxL vs `max` (maxL (concat vss))) )    ={cong {f=c} (sym useMaxAppend)}=
>   
>   ( v `max` (maxL (vs ++ concat vss)) )              ={Refl}=
>   
>   ( (maxL . concat) ((v :: vs) :: vss) )             QED

> -} 


measPlusSpec:
-------------

< measPlusSpec : (v : Val) -> (mv : List Val) -> NotEmpty mv ->
<               maxList . map (v +) mv = ((v +) . maxList) mv
 
> Framework.measPlusSpec _ []         ne =  void ne 
> Framework.measPlusSpec v (x :: [])  _  =  rewrite (maximumZeroNLeft x)           in 
>                                           rewrite (maximumZeroNLeft (plus v x))  in 
>                                           Refl
> Framework.measPlusSpec v (x :: (x' :: xs)) _ =
>    rewrite measPlusSpec v (x' :: xs) ()                      in
>    rewrite sym (plusMaxDistrLeft v x (maxList (x' :: xs)))  in
>    Refl
> {-
>    -- proof steps
>    let useIH = measPlusSpec v (x' :: xs) ()                          in
>    let usePlusMaxDistr = plusMaxDistrLeft v x (maxList (x' :: xs))  in
>    -------------------------------------------------------------------
> 
>    ( (meas . map (v +)) (x :: (x' :: xs)) )               ={ Refl }=
>        
>    ( maxList ((v + x) :: map (v +) (x' :: xs)) )          ={ Refl }=
>        
>    ( (v + x) `maximum`  maxList (map (v +) (x' :: xs)) )  ={ cong useIH }=
>        
>    ( (v + x) `maximum`  ((v +) . maxList) (x' :: xs) )    ={ sym usePlusMaxDistr }=
>        
>    ( v + (x `maximum`  maxList (x' :: xs) ) )             ={ Refl }=
>        
>    ( ((v +) . maxList) (x :: (x' :: xs)) )         QED
> -}
