% -*-Latex-*-

%if False

> module Discussion

> import Framework

> %default total
> %auto_implicits off
> %access public export

> neutr : Val
>
> odot : Val -> Val -> Val


%endif


\section{Discussion}
\label{section:discussion}
%
In the last two sections we have seen what the three conditions mean 
for concrete examples and how they are used in the correctness proof.
In this section we take a step back and consider them from a more abstract 
point of view.

\paragraph*{Category-theoretical perspective.}\hspace{0.1cm}
Readers familiar with the theory of monads might have recognised that the
first two conditions ensure that |meas| is the structure map of a
monad algebra for |M| on |Val| and thus the pair |(Val, meas)| is an
object of
the Eilenberg-Moore category associated with the monad |M|. The third
condition requires the map |(v <+>)| to be an |M|-algebra homomorphism
-- a structure preserving map -- for arbitrary values |v|.

This perspective allows us to use existing knowledge about monad
algebras as a first criterion for choosing measures. For example, the
Eilenberg-Moore-algebras of the list monad are monoids -- this
implicitly played a role in the examples we considered
above. \cite{DBLP:journals/tcs/Jacobs11} shows that the algebras of
the distribution monad for probability distributions with finite
support correspond to convex sets. Interestingly, convex sets play an
important role in the theory of optimal control
\citep{bertsekas2003convex}.

\paragraph*{Measures for the list monad.} \hspace{0.1cm}
%
The knowledge that monoids are |List|-algebras suggests a generic
description of admissible measures for |M = List|:
Given a monoid |(Val, odot, b)|, we can prove that monoid
homomorphisms of the form |foldr odot b| fulfil the three conditions,
if |<+>| distributes over $\odot$ on the left. I.e. for |meas = foldr
odot b| the three conditions can be proven from

> odotNeutrRight      :  (l : Val)        ->  l `odot` neutr         =  l
> odotNeutrLeft       :  (r : Val)        ->  neutr `odot` r         =  r
> odotAssociative     :  (l, v, r : Val)  ->  l `odot` (v `odot` r)  =  (l `odot` v) `odot` r
> oplusOdotDistrLeft  :  (n, l, r : Val)  ->  n <+> (l `odot` r)     =  (n <+> l) `odot` (n <+> r)

Neutrality of |b| on the right is needed for |measPureSpec|,
while |measJoinSpec| follows from neutrality on the left and
the associativity of |odot|. The algebra morphism condition on |(v <+>)|
is provable from the distributivity of |<+>| over |odot| and again
neutrality of |b| on the right.
If moreover |odot| is monotone with respect to |<=|

> odotMon   :  {a, b, c, d : Val} -> a <= b -> c <= d -> (a `odot` c) <= (b `odot` d)

then we can also prove |measMonSpec| using the transitivity of |<=|.
%
The proofs are simple and can be found in the
supplementary material to this paper.
This also illustrates how the three abstract conditions follow from
more familiar algebraic properties.

\paragraph*{Mutual independence.}\hspace{0.1cm}
%
Although it does not seem surprising, it should be noted that the
three conditions are mutually independent. This can be concluded from
the counter-examples in Sec.~\ref{subsection:exAndCounterEx}: The
sum, the modified list maximum and the arithmetic average each fail
exactly one of the three conditions. 

\paragraph*{Sufficient vs.\ necessary.}\hspace{0.1cm}
%
The three conditions are sufficient to prove the extensional equality
of the functions |val| and |val'|. They are justified by their level
of generality and the fact that they hold for standard |measures| used
in control theory. However, we leave open the interesting question
whether these conditions are also necessary for the correctness of
monadic backward induction.

\paragraph*{Non-emptiness requirement.}\hspace{0.1cm}
%
Note that |mv| in the premises of |measPlusSpec| is required to be
non-empty. 
This condition arises from a pragmatic consideration.
As an example, let us again use the list monad with |Val=Nat| and |<+>
= +|. It is not hard to see that  
for any natural number |n| greater than 0 the equality
|meas (map (n +) []) = n + meas []| must fail.
So, if we wish to use the standard list data type instead of
defining a custom type of non-empty lists, the only way to prove
the base case of |measPlusSpec| is by contradiction with the
non-emptiness premise.

However, omitting the premise |mv : NotEmpty| would not prevent us
from generically proving the correctness result of
Section~\ref{section:valval} -- it would even simplify matters as it
would spare us reasoning about preservation of non-emptiness.
But it would implicitly restrict the class of monads that can be used
to instantiate |M|. For example, we have seen above, that
|measPlusSpec| is not provable for the empty list without the
non-emptiness premise and we would therefore need to resort to a custom 
type of non-empty lists instead. 

The price to pay for including the non-emptiness premise is
the additional condition |nextNotEmpty| on the transition function
|next| that was already stated in Sec.~\ref{subsection:wrap-up}.
Moreover, we have to postulate non-emptiness preservation laws for the
monad operations (Appendix~\ref{appendix:monadLaws}) and to prove an
additional lemma about the preservation of non-emptiness
(Appendix~\ref{appendix:lemmas}).
%
Conceptually, it might seem cleaner to omit the non-emptiness
condition: In this case, the remaining conditions would only concern
the interaction between the monad, the measure, the type of values and the
binary operation |<+>|. However, the non-emptiness preservation laws seem
less restrictive with respect to the monad. In particular, for our
above example of ordinary lists they hold (the relevant proofs can be
found in the supplementary material).
Thus we have opted for explicitly restricting the |next| function
instead of implicitly restricting the class of monads for which the
result of Sec.~\ref{section:valval} holds.
