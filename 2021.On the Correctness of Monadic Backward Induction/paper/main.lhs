% -*-Latex-*-
\RequirePackage{amsmath}

\documentclass{jfp}
%\documentclass{jfphack} % required to suppress line numbering for arxiv.org

\usepackage{amsmath}
\usepackage{url}
\usepackage{hyperref} % TODO: Warning: Height of page (\paperheight) is invalid (0.0pt), using 11in.
\usepackage{amssymb}
\usepackage{longtable}
\usepackage{tikz}
\usetikzlibrary{cd, automata, arrows, positioning, backgrounds}

\usepackage{subcaption}
\usepackage{natbib}

\usepackage[utf8]{inputenc}

\input{macros.TeX}
\input{PIKPalette}

\hypersetup{
  pdfauthor    = {Nuria Brede, Nicola Botta},
  pdftitle     = {Brede, Botta -- On the Correctness of Monadic Backward Induction},
  colorlinks   = true,             % Colours links instead of ugly boxes
  urlcolor     = PIKorangeDark3,   % Colour for external hyperlinks
  linkcolor    = PIKcyanDark3,     % Colour of internal links
  citecolor    = PIKgreenDark3     % Colour of citations
}


%% uncomment for debugging boxes
%\usepackage{lua-visual-debug}

%include main.fmt
%include polycode.fmt

\usepackage{enumitem}
\setlist[itemize,1]{leftmargin=10pt}
\setlist[enumerate,1]{leftmargin=10pt}


\begin{document}

\journaltitle{JFP}
\cpr{The Author(s),}
\doival{10.1017/xxxxx}

\lefttitle{Brede, Botta}
\righttitle{Correctness of Monadic BI}

\totalpg{\pageref{lastpage01}}
\jnlDoiYr{2021}

\title[Correctness of Monadic BI]{On the Correctness of\\ Monadic Backward Induction}

\begin{authgrp}
% double "@" in email to please lhs2tex
\author{NURIA BREDE}
\affiliation{University of Potsdam,
             Potsdam, Germany }\vspace{-1em}
\affiliation{Potsdam Institute for Climate Impact Research, 
             Potsdam, Germany\\[0.2em]
             (\email{brede@@uni-potsdam.de})
            % (\email{nuria.brede@@pik-potsdam.de})
            }
\author{NICOLA BOTTA}
\affiliation{Potsdam Institute for Climate Impact Research,
             Potsdam, Germany
           }\vspace{-1em}
\affiliation{Chalmers University of Technology, Göteborg, Sweden\\[0.2em]
             (\email{botta@@pik-potsdam.de}) }
\end{authgrp}


\begin{abstract}
  In control theory, to solve a finite-horizon sequential decision
  problem (SDP) commonly means to find a list of decision rules that
  result in an optimal expected total reward (or cost) when taking a
  given number of decision steps. SDPs
  are routinely solved using Bellman's backward induction.
  Textbook authors (e.g. Bertsekas or Puterman) typically give more or less formal
  proofs to show that the backward induction algorithm is correct as
  solution method for deterministic and stochastic SDPs.
  
  Botta, Jansson and Ionescu
  propose a generic framework for finite horizon, \emph{monadic} SDPs
  together with a monadic version of backward induction for
  solving such SDPs. In monadic SDPs, the monad captures a generic
  notion of uncertainty, while a generic measure function aggregates
  rewards.

  In the present paper we define a notion of correctness for monadic SDPs and identify
  three conditions that allow us to prove a correctness result for monadic
  backward induction that is comparable to textbook correctness proofs
  for ordinary backward induction.
  The conditions that we impose are fairly general and can be cast in
  category-theoretical terms using the notion of
  Eilenberg-Moore-algebra.
  They hold in familiar settings like those of
  deterministic or stochastic SDPs but we also give examples in which they fail.
  Our results show that backward induction can safely be employed for a
  broader class of SDPs than usually treated in textbooks. However, they also rule out
  certain instances that were considered admissible in the context of
  \bottaetal's generic framework.
  
  Our development is formalised in Idris as an extension
  of the \bottaetal framework and the sources are available as
  supplementary material.

\end{abstract}


% \begin{keywords}
%   verification,
%   dynamic programming,
%   value function,
%   correctness,
%   functional languages,
%   dependently-typed programming,
%   Idris
% \end{keywords}

\maketitle
\setlength\mathindent{0.5cm}
\renewcommand{\hscodestyle}{\small\setlength{\belowdisplayskip}{6pt plus 0pt minus 0pt}}

%include Introduction.lidr
%include MonadicSDP.lidr
%include Framework.lidr
%include Preparation.lidr
%include Conditions.lidr
%include Theorem.lidr
%include Discussion.lidr
%include Conclusion.lidr

%\pagebreak

\section*{Acknowledgements} \label{section:acknowledgements}

The work presented in this paper was motivated by a remark of Marina
Mart{\'i}nez Montero who raised the question of the equivalence between
|val| and |val'| (and, thus, of the correctness of the
\bottaetal framework) during an introduction to verified decision making
that the authors gave at UCL (Université catholique de Louvain) in
2019. We are especially thankful to Marina for that question!

We are grateful to Jeremy Gibbons, Christoph Kreitz, Patrik Jansson, Tim
Richter and to the JFP editors and reviewers, whose comments and
recommendations have lead to significant improvements of the original
manuscript.

A very special thanks goes to the anonymous reviewer who has suggested
both a more straightforward proof of the |val|-|val'| equality and,
crucially, weaker conditions on the measure function for the
result to hold. This warrants the applicability of the
\bottaetal framework for verified decision making to a wider class of
problems than our original conditions.

The work presented in this paper heavily relies on free software, among
others on Coq, Idris, Agda, GHC, git, vi, Emacs, \LaTeX\ and on the
FreeBSD and Debian GNU/Linux operating systems.  It is our pleasure to
thank all developers of these excellent products.
%
This is TiPES contribution No 37. This project has received funding from
the European Union’s Horizon 2020 research and innovation programme
under grant agreement No 820970 (TiPES
--Tipping Points in the Earth System, \citeyear{TiPES::Website}).

\subsection*{Conflicts of Interest}
None.


\bibliographystyle{jfplike}
\bibliography{references}

%include Appendix.lidr

\label{lastpage01}

\end{document}
