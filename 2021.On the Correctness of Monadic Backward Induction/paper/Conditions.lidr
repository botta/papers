% -*-Latex-*-

%if False

> module Conditions

> import Framework
> %hide A
 
> %default total
> %auto_implicits off
> %access public export


> Q : Type

> Prob : Type -> Type

> supp : {A : Type} -> Prob A -> List A

> prob : {A : Type} -> Prob A -> A -> Q


> neutr : Val
>
> odot : Val -> Val -> Val


%endif

\section{Correctness conditions}
\label{section:conditions}

We now formulate three conditions on combinations of the monad,
the measure function and the binary operation |<+>| that imply the
extensional equality of |val| and |val'|:


\setlist[itemize,1]{leftmargin=58pt}
\begin{itemize}

% measPureSpec

\item[{\bf Condition 1.}]
The measure needs to be left-inverse to |pure|:
\footnote{The symbol |`ExtEq`| denotes \emph{extensional} equality,
  see Appendix~\ref{appendix:monadLaws}}

< measPureSpec     :  meas . pure `ExtEq` id


\begin{center}
  \includegraphics{img/diag1.eps}
% \begin{tikzcd}[column sep=2cm]
% %
% Val       \arrow[d, "pure", swap] \arrow[dr, "id"] \\
% M Val     \arrow[r, "meas", swap]                        & Val\\
% %
% \end{tikzcd}
\end{center}

% measJoinSpec

\item[{\bf Condition 2.}]
  Applying the measure after |join| needs to be extensionally
equal to applying it after |map meas|:

< measJoinSpec  : meas . join `ExtEq` meas . map meas

\begin{center}
  \includegraphics{img/diag2.eps}
% \begin{tikzcd}[column sep=3cm]
% %
% M (M Val) \arrow[r, "map\ meas"] \arrow[d, "join", swap] & M Val \arrow[d, "meas" ] \\
% M Val     \arrow[r, "meas", swap]                        & Val\\
% %
% \end{tikzcd}
\end{center}

% measPlusSpec


\item[\bf{Condition 3.}]
For arbitrary |v: Val| and non-empty |mv: M Val| applying
the measure after mapping |(v <+>)| onto |mv| needs to be equal to
applying |(v <+>)| after the measure:

< measPlusSpec  :  (v : Val) -> (mv : M Val) -> (NotEmpty mv) ->
<                  (meas . map (v <+>)) mv = ((v <+>) . meas) mv

\begin{center}
  \includegraphics{img/diag3.eps}
% \begin{tikzcd}[column sep=3cm]
% %
% M Val \arrow[r, "map (v\, \oplus)"] \arrow[d, "meas", swap] & M Val \arrow[d, "meas" ] \\
% Val   \arrow[r, "(v\, \oplus)", swap]                       & Val\\
% %
% \end{tikzcd}
%
%\vspace{0.1cm}
\end{center}

\end{itemize}
\setlist[itemize,1]{leftmargin=10pt}

Essentially, these conditions assure that the measure is well-behaved
relative to the monad structure and the |<+>|-operation.
They arise by, again, generalising from the
standard example of stochastic SDPs with a probability monad,
the \emph{expected value} as measure and ordinary addition as |<+>|.
The first two conditions are lifting properties that allow to do
computations either in the monad or the underlying structure
with the same result.
The third condition is a distributivity law. For the computation of the
measured total reward it means that instead of adding the current reward to
the outcome of each trajectory and then measuring, one may as well first measure the
outcomes and then add the current reward.

\vspace{0.2cm}
To illustrate the conditions, let us consider a simple representation of
discrete probability distributions like in \citep{DBLP:journals/jfp/ErwigK06}.

> Dist :  Type -> Type
> Dist Omega  =  List (Omega, Double)

with

> distMap  :  {A, B : Type} -> (f : A -> B) -> Dist A -> Dist B 
> distMap f aps  =  [(f (fst ap), snd ap) | ap <- aps]

> distPure    :  {A : Type} -> A -> Dist A
> distPure a  =  [(a, 1.0)]

> distJoin : {A : Type} -> (Dist (Dist A)) -> Dist A
> distJoin apsps = concat [ [ (fst ap, snd ap * snd aps) | ap <- fst aps] | aps <- apsps]

|Val = Double| and as measure the expected value

> expected : Dist Double -> Double
> expected dps = sum [fst dp * snd dp | dp <- dps]

With |meas = expected| and |<+> = +|, we can now consider the three conditions from above.

\paragraph*{Condition 1.} \hspace{0.1cm}
The first condition |measPureSpec| holds since |1.0|
is neutral for |*|.

< expected . distPure a = a * 1.0 = a

The second and the third condition require some arithmetic reasoning, so
let us just consider them for two examples. 
Let |a, b, c, d| be variables of type |Double| and say we have distributions

%if False

> parameters (a, b, c, d : Double)

%endif

> dps1  :  Dist Double
> dps1  =  [(a, 0.5), (b, 0.3), (c, 0.2)]

> dps2  :  Dist Double
> dps2  =  [(a, 0.4), (d, 0.6)]

> dpdps  :  Dist (Dist Double)
> dpdps  = [(dps1, 0.1), (dps2, 0.9)] 


\paragraph*{Condition 2.} \hspace{0.1cm}
Then the second condition |measJoinSpec| instantiates to

< (expected . distJoin) dpdps = (expected . distMap expected) dpdps

This equality holds because of the standard properties of
addition and multiplication:

< (expected . distJoin) dpdps                                                                =
<
< expected [(a, 0.5 * 0.1), (b, 0.3 * 0.1), (c, 0.2 * 0.1), (a, 0.4 * 0.9), (d, 0.6 * 0.9)]  =
<
< (a * 0.5 * 0.1) + (b * 0.3 * 0.1) + (c * 0.2 * 0.1) + (a * 0.4 * 0.9) + (d * 0.6 * 0.9)    =
<
< ((a * 0.5 + b * 0.3 + c * 0.2) * 0.1 + (a * 0.4 + d * 0.6) * 0.9                 =
<
< expected [(a * 0.5 + b * 0.3 + c * 0.2, 0.1) , (a * 0.4 + d * 0.6, 0.9)]         =
<
< (expected . distMap expected) dpdps


\paragraph*{Condition 3.} \hspace{0.1cm}
For the third condition |measPlusSpec|, consider for some |v : Double|
the equation

< (expected . distMap (v +)) dps1 = ((v +) . expected) dps1

Again using the usual arithmetic laws for |+| and |*|, we can calculate

< expected (distMap (v +) [(a, 0.5), (b, 0.3), (c, 0.2)])            =
<
< (v + a) * 0.5 + (v + b) * 0.3 + (v + c) * 0.2                      =
<
< (v * 0.5 + a * 0.5) + (v * 0.3 + b * 0.3) + (v * 0.2 + c * 0.2)    =
<
< (v * 0.5 + v * 0.3 + v * 0.2) + (a * 0.5 + b * 0.3 + c * 0.2)  =
<
< v + expected [(a, 0.5), (b, 0.3), (c, 0.2)]

As we can see, an essential ingredient for the equality to hold is that
the mapped occurrences of
|(v +)| are weighted by the probabilities which add up to 1.
%

\vspace{0.2cm}
Note that in this example, we have glossed over problems that might arise
from the use of |Dist| to represent probability distributions.
\footnote{For the sake of simplicity,
  we do not address (important)
  conceptional questions concerning the representation
  of probability distributions or the problems caused by the use of
  floating point arithmetic in this example.
  Note however, that the chosen type |Prob| does e.g. neither
  enforce that the probabilities lie in the interval |[0,1]| nor
  that they add up to |1|. These properties would however
  be crucial for actual proofs.}
%
We will briefly address probability monads and the expected value
from a more abstract perspective in Subsection~\ref{subsection:impactMeas}.



\subsection{Examples and counter-examples}
\label{subsection:exAndCounterEx}

Besides the motivating example above, let us now consider some more
functions that have the correct type to serve as a measure, 
and that do or do not fulfil the three conditions.

Simple examples of admissible measures are the minimum (|minList| as
defined in Fig.~\ref{fig:example1Formal}) or maximum (|maxList = foldr
`maximum` 0|) of a list for |M = List| with |Nat| as type of values
and ordinary addition as |<+>|. It is straightforward to prove that
the conditions hold for these two measures and the proofs for
|maxList| are included in the supplementary material.

The function |length| is a very simple counter-example:
It has the right type for a list measure but fails all three
of the conditions.
%
As to other counter-examples, let us revisit the conditions one by one.
Throughout, we use |M = List| with |map = listMap|, |join = concat|
and |<+> = +|
(the canonical addition for the respective type of |Val|). 

\paragraph*{Condition~1.} \hspace{0.1cm}
%
We remain in the setting of
Ex.~\ref{subsection:example1SDPs} with |Val = Nat|,
and just vary the measure. Using a somewhat contrived
variation of |maxList|

> maxListVar  :  List Nat -> Nat
> maxListVar  =  foldr (\x, v => (x + 1 `maximum` v)) 0

with |meas = maxListVar| it suffices to consider
that for an arbitrary |n : Nat|

< (maxListVar . pure) n = maxListVar [n] = (n + 1) `maximum` 0 = n + 1 != n = id n

to see that now the condition |measPureSpec| fails.

\paragraph*{Condition~2.} \hspace{0.1cm}
%
To exhibit a measure that fails the condition |measJoinSpec|, we
switch to |Val = Double| and use the arithmetic average

> avg  :  List Double -> Double
> avg []  =  0.0
> avg ds  = sum ds / cast (length ds)

as measure |meas  = avg|. Taking a list of lists of different lengths
like [[1], [2, 3]] we have

< avg (concat [[1], [2, 3]])          =   avg [1, 2, 3]  = 2
<                                     !=
< avg (listMap avg [[1], [2, 3]])     =   avg [1, 2.5]   = 1.75

\paragraph*{Condition~3.} \hspace{0.1cm}
%
Let again |Val = Nat| to take another look at our counter-example
from the last section with |meas = sum|, the arithmetic sum of a list.
It does fulfil |measPureSpec| and |measJoinSpec|, the first by definition,
the second by structural induction using the associativity of |+|.
But it fails to fulfil
|measPlusSpec|. If the list has the form |a :: as|, we would have to
show the following equality for |measPlusSpec| to hold:

< (sum . listMap (v +)) (a :: as) = ((v +) . sum) (a :: as)

Clearly, if |v != 0| and |as != []| this equality cannot hold.
This is why in the last section the equality of |val| and |val'|
failed for |meas = sum|.\\
A similar failure would arise if we chose |meas = foldr (*) 1| instead,
as |+| does not distribute over |*|. But if we turned the situation
around by setting |<+> = *| and |meas = sum|, the condition
|measPlusSpec| would hold thanks to the usual arithmetic
distributivity law for |*| over |+|.

\vspace{0.2cm}
All of the measures considered in this subsection do fulfil the
|measMonSpec| condition imposed by the BJI-theory. This raises the
question how previously admissible measures are impacted by adding the
three new conditions to the framework.

\subsection{Impact on previously admissible measures}
\label{subsection:impactMeas}
%
As we have seen in Sec.~\ref{subsection:solution_components}, the
BJI-framework already requires measures to fulfil the monotonicity
condition

< measMonSpec  :  {A : Type} -> (f, g : A -> Val) -> ((a : A) -> (f a) <= (g a)) ->
<                 (ma : M A) -> meas (map f ma) <= meas (map g ma)

\bottaetal show that the arithmetic average (for |M = List|), the
worst-case measure (for |M = List| and for a probability monad |M = Prob|)
and the expected value measure (for |M = Prob|) all fulfil |measMonSpec|.
Thus, a natural question is whether these measures also fulfil the three
additional requirements.

\paragraph*{Expected value for probability distributions.} \hspace{0.1cm}
%
As already discussed, most applications of backward induction concern
stochastic SDPs where possible rewards are aggregated using the
expected value measure from probability theory, commonly denoted as |E|.

Essentially, for a numerical type |Q|, the
expected value of a probability distribution on |Q| is

> E  :  Num Q => Prob Q -> Q
> E pq = sum [q * prob pq q | q <- supp pq]

where |prob| and |supp| are generic functions that encode the notions of
\emph{probability} and of \emph{support} associated with a finite
probability distribution:

< prob : {A : Type} -> Prob A -> A -> Q
<
< supp : {A : Type} -> Prob A -> List A

For |pa| and |a| of suitable types, |prob pa a| represents the
probability of |a| according to |pa|. Similarly, |supp pa| returns a
list of those values whose probability is not zero in |pa|.
%
The probability function |prob| has to fulfil the axioms of
probability theory. In particular,

< sum [prob pa a | a <- supp pa] = 1

This condition implies that probability distributions cannot be empty, a
precondition of |measPlusSpec|. Putting forward minimal specifications
for |prob| and |supp| is not completely trivial but if the |+|-operation
associated with |Q| is commutative and associative, if |*|
distributes over |+| and if the |map| and |join| associated with
|Prob| -- for |f|, |a|, |b|, |pa| and |ppa| of suitable types --
fulfil the conservation law

< prob (map f pa) b = sum [prob pa a | a <- supp pa, f a == b]

and the total probability law

< prob (join ppa) a = sum [prob pa a * prob ppa pa | pa <- supp ppa]

 then the expected value fulfils |measPureSpec|, |measJoinSpec| and
 |measPlusSpec|.
%
 This is not surprising since -- as stated above -- this has been the
 guiding example for the generalisation to monadic SDPs and the formulation
 of the three conditions.
 
\paragraph*{Average and arithmetic sum.} \hspace{0.1cm}
%
As can already be concluded
from the corresponding counter-examples in the previous subsection,
neither the plain arithmetic average nor the arithmetic sum are
suited as measure when using the standard monad structure on
|List| to represent non-deterministic
uncertainty. We think this is an important observation, as the
average seems innocent enough to come to mind as a simple way
to represent uniformly distributed outcomes:
\emph{``The probability of each element can simply be inferred from the length of the list 
-- so why bother to explicitly deal with probabilities?''} 
Although our counter-example shows that this idea is flawed, the intuition
behind it can be employed to define an alternative, but less general monad
structure on lists by incorporating the averaging operation into the joining
of lists (i.e. by choosing |join = map avg|).
However, this only makes sense for types that are instances of the |Num|
and |Fractional| type classes, and naturality only holds for a restricted class of
functions (namely additive functions). As a consequence, this alternative structure
does not seem particularly useful for our current purpose either.

\paragraph*{Worst-case measures.} \hspace{0.1cm}
%
In many important applications in
climate impact research but also in portfolio management and sports,
decisions are taken as to minimise the consequences of worst case
outcomes. Depending on how ``worse'' is defined, the corresponding
measures might pick the maximum or
minimum from an |M|-structure of values. In the previous subsection we
considered an example in which the monad was |List|, the operation
|<+>| plain addition together with either |maxList| or |minList| as
measure. And indeed we can prove that for both measures the three
requirements hold (the proofs for |maxList| can be found in the
supplementary material). This gives us a useful notion of worst-case
measure that is admissible for monadic backward induction.

\vspace{0.2cm}
We can thus conclude that the new requirements hold for certain
familiar measures, but that they also rule out
  certain instances that were considered admissible in the BJI-framework.
%
Given the three conditions |measPureSpec|, |measJoinSpec|,
|measPlusSpec| hold, we can prove the
extensional equality of the functions |val| and |val'| generically.
This is what we will do in the next section.

