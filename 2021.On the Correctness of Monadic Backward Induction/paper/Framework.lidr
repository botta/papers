% -*-Latex-*-

%if False

> module Framework

> import Data.Vect

> %default total
> %auto_implicits off
> %access public export


> infixr 7 <++>

> M  :  Type -> Type

> map  :  {A, B : Type} -> (A -> B) -> M A -> M B

> ExtEq : {A, B : Type} -> (f, g : A -> B) -> Type
> ExtEq {A} f g = (a : A) -> f a = g a


> data TotalPreorder  :  {A : Type} -> (A -> A -> Type) -> Type where
>   MkTotalPreorder   :  {A : Type} ->
>                        (R : A -> A -> Type) ->
>                        (reflexive : (x : A) -> R x x) ->
>                        (transitive : (x : A) -> (y : A) -> (z : A) -> R x y -> R y z -> R x z) ->
>                        (totalPre : (x : A) -> (y : A) -> Either (R x y) (R y x)) ->
>                        TotalPreorder R

> reflexive  :  {A : Type} -> {R : A -> A -> Type} -> TotalPreorder R -> (x : A) -> R x x
> reflexive (MkTotalPreorder R reflTP _ _)  x = reflTP x

%endif


\section{The BJI-framework}
\label{section:framework}

The BJI-framework is a dependently typed formalisation of optimal
control theory for finite-horizon, discrete-time SDPs. It extends
mathematical formulations for stochastic
SDPs \citep{bertsekas1995, bertsekasShreve96, puterman2014markov}
to the general problem of optimal decision making under \emph{monadic}
uncertainty.

For monadic SDPs, the framework provides a
generic implementation of backward induction. It has been applied to
study the impact of uncertainties on optimal emission policies
\citep{esd-9-525-2018} and is currently used to investigate solar
radiation management problems under tipping point
uncertainty \citep{TiPES::Website}.

In a nutshell, the framework consists of two sets of components: one
for the \emph{specification} of an SDP and one for its \emph{solution}
with monadic backward induction.

\subsection{Problem specification components}
\label{subsection:specification_components}

In the following we discuss the components necessary to specify a
monadic SDP.\\
The first one is the monad |M|:

< M         :  Type -> Type

As discussed in the previous
section, |M| accounts for the uncertainties that affect the decision
problem. For our first example, we could for instance define |M| to be
|List|. For the second example it suffices to use |M = Id| as the
problem is deterministic.

Further, the BJI-framework supports the specification of the
\emph{states}, the \emph{controls} and the \emph{transition function} of
an SDP through

> X         :  (t : Nat) -> Type
> Y         :  (t : Nat) -> X t -> Type
> next      :  (t : Nat) -> (x : X t) -> Y t x -> M (X (S t))

The interpretation is that |X t| represents the states at decision step
|t|.\footnote{Note that in Idris, $S$ and $Z$ are the familiar
  constructors of the data type |Nat|.} In the first example of
Sec.~\ref{section:SDPs}, there are just two states
(|Good| and |Bad|) such that |X| is a constant family:

> data State  =  Good | Bad
> X _t        =  State

But in the second example the possible states depend on the decision
step |t|. Taking for example step |t = 2|, we could simply define

< data State2  =  AB | AC | CA | CD
< X  2         =  State2

% Remark: To make this type-check, we have to use |(S (S Z))|
%         instead of |2|:
% < X (S (S Z))  =  State2

Alternatively, we could employ type dependency in a more systematic way
to express that in Ex.~2 states are admissible sequences of
actions

> data Act = A | B | C | D

Recall that actions could require that another action was performed before,
no action was to be carried out twice and the problem was limited to
3 steps. These conditions might be captured by a type-valued predicate

> AdmissibleState  : {t : Nat} -> Vect t Act -> Type

and the type of states might then be expressed as dependent pair
of a vector of actions and a proof that it is admissible.

< X t = (as : Vect t Act ** AdmissibleState as)

Similarly, |Y t x| represents the controls available at decision step
|t| and in state |x| and |next t x y| represents the states that can
be obtained by selecting control |y| in state |x| at decision step
|t|. In our first example, the available controls remain constant over
time (|High| or |Low|) like the states, but for the second example,
the type dependency is relevant: e.g. we might define (again at step
|t=2|)

< data CtrlAC   =  B | D
< Y  2  AC      =  CtrlAC

% > Y (S (S Z)) AC  = CtrlAC

or more elegantly use dependent pairs to define the type of controls,
using the observation that an action is an
admissible control for some state represented by a vector of actions,
if adding the action to the vector results again in an admissible state :

> AdmissibleControl  :  {t : Nat} -> Vect t Act -> Act -> Type
> AdmissibleControl as a  =  AdmissibleState (a :: as)

< Y t x = (a : Act ** AdmissibleControl (fst x) a) 


Recall from Sec.~\ref{section:SDPs} that the monad, the states,
the controls and the next function together define a decision process.
In order to fully
specify a decision problem, one also has to define the rewards obtained
at each decision step and the operation that is used to add up rewards.
In the BJI-framework, this is done in terms of

> Val       :  Type
> reward    :  (t : Nat) -> (x : X t) -> Y t x -> X (S t) -> Val
> (<+>)     :  Val -> Val -> Val

Here, |Val| is the type of rewards and |reward t x y x'| is the reward
obtained by selecting control |y| in state |x| if the next state is
|x'|, an element of the state space at step |t + 1|.
Note that for deterministic problems it is unnecessary to parameterise
the reward function over the next state as it is unique and can thus be
obtained from the current state and control. But for non-deterministic
problems it is useful to be able to assign rewards depending on the
(uncertain) outcome of a transition.

A few remarks are at place here.

\begin{itemize}
%
\item In many applications, |Val| is a numerical type and the controls of the
  SDP represent resources (fuel, water, etc.) that come at a cost. In these
cases, the reward function encodes the costs and perhaps also the
benefits associated with a decision step. Often, the latter also depends
both on the current state |x| and on the next state |x'|. The
BJI-framework nicely copes with all these situations.
%
\item The operation |<+>| determines how rewards are added up. It
  could be a simple arithmetic operation, but it could also be defined
  in terms of problem-specific parameters, e.g.\ discount factors to
  give more weight to current rewards as compared to future rewards.
%
\item Mapping |reward t x y| onto |next t x y| (remember
that |M| is a monad and thus a functor) yields a value of type |M
Val|. These are the \emph{possible} rewards obtained by selecting
control |y| in state |x| at decision step |t|.
%
In mathematical theories of optimal control, the implicit assumption
often is that |Val| is equal to |Real| and that the |M|-structure is a
probability distribution on real numbers  which can be evaluated with
the \emph{expected value} measure.
%
However, in many practical applications, measuring uncertainty of rewards in
terms of the expected value is inadequate \citep{mercure2020risk}.
The BJI-framework therefore takes a generic approach and allows the
specification of SDPs in terms of problem-specific measures.
%
\end{itemize}
%
As just discussed, in SDPs with uncertainty a measure is required to
aggregate multiple possible rewards. The BJI-framework supports
the specification of the measure by:

> meas             :  M Val -> Val

In our first example we could use the minimum of a list as worst-case
measure, while in the second example the measure
would just be the identity (as the problem is deterministic).

Before we get to the solution components of the BJI-framework, one
more ingredient needs to be specified. In the next section we will
formalise a notion of optimality for which it is necessary to be able
to compare elements of |Val|.
%
The framework allows users to compare |Val|-values in terms of a problem
specific comparison operator:

> (<=)             :  Val -> Val -> Type

The operator |(<=)| is required to define a total preorder on |Val|.
%
%if false

> lteTP            :  TotalPreorder (<=)

%endif
%
In our two examples, we simply have:

> Val              =  Nat
> (<+>)            =  (+)
> (<=)             =  LTE

Three more ingredients are necessary to fully specify a monadic SDP,
but we defer discussing them to when they come
up in the next subsection.
For illustration, a formalisation of Ex.~\ref{subsection:example1SDPs} can be found in
Fig.~\ref{fig:example1Formal}. A formalisation of Ex.~\ref{subsection:example2SDPs} is
included in the supplementary material.


% -----------------------------------

% Figure: Formalisation of examples

% -----------------------------------


\begin{figure}

\centering
  \framebox{

    \parbox{\textwidth-0.5cm}{


      \scalebox{0.8}{

        \small
        \begin{tabular}{m{7.3cm} m{6.7cm}}

          \multicolumn{2}{l}{ \parbox[t]{13cm}{
          \textbf{Formalisation of Ex.~\ref{subsection:example1SDPs}:}\\

          We use the monoid and preorder structure on |Nat|, i.e.
          |Val = Nat|, |(<+>) = (+)|, |zero = 0|, |LTE = (<=)|.

          }
          }
          \\

          \parbox{6.9cm}{

< M = List

Measure:

< minList  :  List Nat -> Nat
< minList []         =  0
< minList (x :: [])  =  x
< minList (x :: xs)  =  x `minimum` xs
<
< meas = minList

States and Controls:

< data States    =  Good | Bad
< data Controls  =  High | Low
<
< X _t     =  States
< Y _t _x  =  Controls

          }
                    &

                      \parbox{6.5cm}{

                      Transition function:

< next _t Good  Low   =  [Good]
< next _t Bad   High  =  [Bad]
< next _t _x    _y    =  [Good, Bad]

Rewards:

< reward _t _x Low   Good  =  3
< reward _t _x High  Good  =  2
< reward _t _x Low   Bad   =  1
< reward _t _x High  Bad   =  0



          }
        \end{tabular}
      }

      \caption{A formalisation of Ex.~\ref{subsection:example1SDPs} from
        Sec.~\ref{section:SDPs}
        \label{fig:example1Formal}.
      }
    }
  }
\end{figure}




\subsection{Problem solution components}
\label{subsection:solution_components}

The second set of components of the BJI-framework is an extension of the
mathematical theory of optimal control for stochastic sequential
decision problems to monadic problems. Here, we provide a summary of the
theory. Motivation and full details can be found in
\citep{2014_Botta_et_al, 2017_Botta_Jansson_Ionescu, esd-9-525-2018}.
%

The theory formalises the notions of policy (decision
rule) from Sec.~\ref{section:SDPs} as

> Policy    :  (t : Nat) -> Type
> Policy t  =  (x : X t) -> Y t x

Policy sequences are then essentially vectors
of policies\footnote{The curly brackets in the types of |Nil| and
  |(::)| indicate that |t| and |n| are implicit arguments.}.

> data PolicySeq  :  (t, n : Nat) -> Type where
>   Nil   :  {t : Nat} -> PolicySeq t Z
>   (::)  :  {t, n : Nat} -> Policy t -> PolicySeq (S t) n -> PolicySeq t (S n)

Notice the role of the step (time) index |t| and of the length index |n|
in the constructors of policy sequences:
For a policy sequence to make sense, policies for taking decisions at
step |t| can only be prepended to policy sequences for taking \emph{first}
decisions at step |t + 1| and the operation yields
policy sequences for taking \emph{first} decisions at step |t|.
Thus the time index allows to ensure a consistency property of policy
sequences by construction.
As for plain vectors and lists, prepending a policy to a policy sequence of
length |n| yields a policy sequence of length |n + 1|.
Both the time and the
length index will be useful below: they allow to express that the backward
induction algorithm computes policy sequences starting at a specific time
and having a specific length depending on its inputs.   

The perhaps most important ingredient of backward induction is a
\emph{value function} that incrementally measures and adds up rewards.
For a given decision problem,
the value function takes two arguments: a policy sequence |ps| for making
|n| decision steps starting from decision step |t| and an initial state
|x : X t|. It computes the value of taking |n| decision steps
according to the policies |ps| when starting in |x|. In the
BJI-framework, the value function is defined as

%if False

> zero  :  Val

> (<++>)    :  {A : Type} -> (f, g : A -> Val) -> A -> Val
> f <++> g  =  \ a => f a <+> g a

%endif

> val  :  {t, n : Nat} -> PolicySeq t n -> X t -> Val
> val {t}  Nil      x  =  zero
> val {t} (p :: ps) x  =  let y    =  p x in
>                         let mx'  =  next t x y in
>                         meas (map (reward t x y <++> val ps) mx')

Notice that, independently of the initial state |x|, the value of the
empty policy sequence is |zero|.
This is a problem-specific reference
value

< zero    :  Val

that has to be provided as part of a problem
specification.\footnote{The name might
  suggest that |zero| is supposed to be a neutral element relative to
  |<+>|. However, this is not required by the framework.} It is one of
the specification components that we have not
discussed in Sec.~\ref{subsection:specification_components}.
%
The value of a policy sequence consisting of a first policy |p| and of a
tail policy sequence |ps| is defined inductively as the measure of an
|M|-structure of |Val|-values. These values are obtained by first
computing the control |y| dictated by the policy |p| in state |x| and
the |M|-structure of possible next states |mx'| dictated by the
transition function |next|. Then, for all |x'| in |mx'| the current reward 
|reward t x y x'| is added to the aggregated outcome for the tail policy sequence
|val ps x'| . Finally, the result of
this functorial mapping is aggregated with the problem-specific
measure |meas| to obtain a result of type |Val| as outcome
for the policy sequence |(p :: ps)|. The function which is
mapped onto |mx'| is just a lifted version of |<+>|:

< (<++>)  :  {A : Type} -> (f, g : A -> Val) -> A -> Val
< f <++> g = \ a => f a <+> g a

We will come back to the value function of the BJI-theory in
Sec.~\ref{section:preparation} where we will contrast it
with a function |val'| that, for a policy sequence |ps| and an initial state
|x|, computes the measure of the sum of the
rewards along all possible trajectories starting at |x| under
|ps| (the \emph{measured total reward} that we anticipated in
Sec.~\ref{section:SDPs}).
%
For the time being, though, we accept the notion of value of a policy
sequence as put forward in the BJI-theory and we show how the
definition of |val| can be employed to compute policy sequences that
are provably optimal in the sense of

> OptPolicySeq  :  {t, n : Nat} -> PolicySeq t n -> Type
> OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> (x : X t) -> val ps' x <= val ps x

Notice the universal quantification in this definition:
A policy sequence |ps| is defined to be optimal iff |val ps' x <= val
ps x| for any policy sequence |ps'| and for any state |x|.

The generic implementation of backward induction in the
BJI-framework is an application of \emph{Bellman's principle of
optimality} mentioned in Sec.~\ref{section:SDPs}.
In control theory textbooks, this principle is often
referred to as \emph{Bellman's equation}. It can be suitably
formulated in terms of the notion of \emph{optimal extension}. We say
that a policy |p : Policy t| is an optimal extension of a policy
sequence |ps : Policy (S t) n| if it is
the case that the value of |p :: ps| is at least as good as the value of
|p' :: ps| for any policy |p'| and for any state |x : X t|:

> OptExt  :  {t, n : Nat} -> PolicySeq (S t) n -> Policy t -> Type
> OptExt {t} ps p  =  (p' : Policy t) -> (x : X t) -> val (p' :: ps) x <= val (p :: ps) x

With the notion of optimal extension in place, Bellman's principle can
be formulated as

> Bellman  :  {t, n : Nat} ->
>             (ps   :  PolicySeq (S t) n) -> OptPolicySeq ps ->
>             (p    :  Policy t) -> OptExt ps p ->
>             OptPolicySeq (p :: ps)

In words: \emph{extending an optimal policy sequence with an optimal
extension (of that policy sequence) yields an optimal policy sequence}
or shorter \emph{prefixing with optimal extensions preserves
  optimality}.
%
Proving Bellman's optimality principle is almost straightforward and
relies on |<=| being reflexive and transitive and two
\emph{monotonicity} properties:

> plusMonSpec   :  {v1, v2, v3, v4 : Val} -> v1 <= v2 -> v3 <= v4 -> (v1 <+> v3) <= (v2 <+> v4)
>
> measMonSpec   :  {A : Type} -> (f, g : A -> Val) -> ((a : A) -> f a <= g a) ->
>                  (ma : M A) -> meas (map f ma) <= meas (map g ma)

The second condition is a special case of the measure monotonicity
requirement originally formulated by \cite{ionescu2009} in
the context of a theory of vulnerability and monadic dynamical
systems. It is a natural property and the expected value measure, the
worst (best) case measure and any sound statistical measure fulfil it.
%
Like the reference value |zero| discussed above, |plusMonSpec| and
|measMonSpec| are specification components of the BJI-framework that
we have not discussed in Sec.~\ref{subsection:specification_components}.
%
We provide a proof of |Bellman| in Appendix~\ref{appendix:Bellman}. As one
would expect, the proof makes essential use of the recursive definition of
the function |val| discussed above.
%
As a consequence, this precise definition of |val| plays a crucial
role for the verification of backward induction in
\citep{2017_Botta_Jansson_Ionescu}.

Apart from the increased level of generality, the
definition of |val| and |Bellman| are in fact just an
Idris formalisation of Bellman's equation as formulated in control
theory textbooks. With |Bellman| and provided that we can compute
optimal extensions of arbitrary policy sequences

> optExt      :  {t, n : Nat} -> PolicySeq (S t) n -> Policy t
>
> optExtSpec  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)

it is easy to derive an implementation of monadic backward
induction that computes provably optimal policy sequences with
respect to |val|: first, notice that the empty policy sequence is
optimal:

> nilOptPolicySeq  :  OptPolicySeq Nil
> nilOptPolicySeq Nil x = reflexive lteTP zero

This is the base case for constructing optimal policy sequences by
backward induction, starting from the empty policy sequence:

> bi  :   (t, n : Nat) -> PolicySeq t n
> bi t  Z     =  Nil
> bi t (S n)  =  let ps = bi (S t) n in optExt ps :: ps

That |bi| computes optimal policy sequences with respect to |val|
is then proved by induction on |n|, the input that determines the
length of the resulting policy sequence:

> biOptVal  :  (t, n : Nat) -> OptPolicySeq (bi t n)
> biOptVal t  Z     =  nilOptPolicySeq
> biOptVal t (S n)  =  Bellman ps ops p oep where
>   ps   :  PolicySeq (S t) n  ;;  ps   =  bi (S t) n
>   ops  :  OptPolicySeq ps    ;;  ops  =  biOptVal (S t) n
>   p    :  Policy t           ;;  p    =  optExt ps
>   oep  :  OptExt ps p        ;;  oep  =  optExtSpec ps

This is the verification result for |bi| of
\citep{2017_Botta_Jansson_Ionescu}.\footnote{Note that |biOptVal| is
  called |biLemma| in \citep{2017_Botta_Jansson_Ionescu}. We chose the
  new name to emphasise that |bi| computes optimal policy sequences
  with respect to |val|.}
%

\subsection{BJI-framework wrap-up.}
\label{subsection:wrap-up}

The specification and solution components discussed in the last two
sections are all we need to formulate precisely the problem of
correctness for monadic backward induction in the BJI-framework.
This is done in the next section.
Before turning to it, two further remarks are necessary:

\begin{itemize}

\item The theory proposed in \citep{2017_Botta_Jansson_Ionescu} is
slightly more general than the one summarised above. Here, policies are
just functions from states to controls:

< Policy  :  (t : Nat) -> Type
< Policy t = (x : X t) -> Y t x

By contrast, in \citep{2017_Botta_Jansson_Ionescu}, policies are indexed
over a number of decision steps |n|

< Policy  :  (t, n : Nat) -> Type
< Policy t Z      =  Unit
< Policy t (S m)  =  (x : X t) -> Reachable x -> Viable (S m) x -> GoodCtrl t x m

and their domain for |n > 0| is restricted to states that are
\emph{reachable} and \emph{viable} for |n| steps. This allows
to cope with states whose control set is empty and with transition
functions that return empty |M|-structures of next states.
(For a discussion of reachability and viability
see \citep[Sec.~3.7~and~3.8]{2017_Botta_Jansson_Ionescu}.)

This generality, however, comes at a cost: Compare e.g.\
the proof of Bellman's principle from the last
subsection with the corresponding proof in
\cite[Appendix B]{2017_Botta_Jansson_Ionescu}. The impact of the
reachability and viability
constraints on other parts of the theory is even more severe.

Here, we have decided to trade some generality for better readability
and opted for a simplified version of the original theory.
Still, for the generic backward induction algorithm we need to make
sure that it is possible to define
policy sequences of the length required for a specific SDP. 
This can e.g. be done by postulating
controls to be non-empty:

> notEmptyY  :  (t : Nat) -> (x : X t) -> Y t x

We also impose a non-emptiness requirement on
the transition function |next| that will be discussed in
Sec.~\ref{section:discussion}.
%
%if false

> NotEmpty  :  {A : Type} -> M A -> Type

%endif

> nextNotEmpty  :  {t : Nat} -> (x : X t) -> (y : Y t x) -> NotEmpty (next t x y)

\item In section \ref{subsection:solution_components}, we have not
discussed under which conditions one can implement optimal extensions of
arbitrary policy sequences. This is an interesting topic that is however
orthogonal to the purpose of the current paper.
For the same reason we have not addressed the question of how to make |bi|
more efficient by tabulation.
We briefly discuss the specification and implementation of optimal extensions
in the BJI-framework in Appendix~\ref{appendix:optimal_extension}.
We refer the reader interested in tabulation of |bi| to
\href{https://gitlab.pik-potsdam.de/botta/IdrisLibs/-/blob/master/SequentialDecisionProblems/TabBackwardsInduction.lidr}
{SequentialDecisionProblems.TabBackwardsInduction}
of \citep{botta20162018}.

\end{itemize}

\vfill
\pagebreak