% -*-Latex-*-

%if False

> module Preparation

> import Syntax.PreorderReasoning

> import Framework

> %default total
> %auto_implicits off
> %access public export

> %hide Prelude.Monad.(>>=)
> %hide Prelude.Monad.join
> %hide Prelude.Functor.map
> %hide Prelude.List.last
> %hide Prelude.List.head
> %hide Prelude.Stream.head
> %hide Applicative.pure

> infixr 7 ##

> val'Det  :  {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Val

> pure     :  {A : Type} -> A -> M A

> (>>=)    :  {A, B : Type} -> M A -> (A -> M B) -> M B

> join     :  {A : Type} -> M (M A) -> M A


> data Controls  =  High | Low
> Framework.Y _t _x  =  Controls

> Framework.M = List

%endif

\section{Correctness for monadic backward induction}
\label{section:preparation}

In this section we formally specify the notions of correctness for
monadic backward induction |bi| and the value function |val| of the
BJI-framework that we will study in the remainder of this paper.
%
We develop these notions as generic variants of the corresponding
notions for stochastic SDPs.

\subsection{Extension of the BJI-framework}
\label{subsection:frameworkExtension}

In the previous section, we have seen that a monadic SDP can be
specified in terms of nine components: |M|, |X|, |Y|,
|next|, |Val|, |zero|, |<+>|, |<=| and |reward|.

Given a policy sequence (optimal or not) and an initial state
for an SDP, we can compute the |M|-structure of possible trajectories
starting at that state:

> data StateCtrlSeq  :  (t, n : Nat) -> Type where
>   Last  :  {t : Nat} -> X t -> StateCtrlSeq t (S Z)
>   (##)  :  {t, n : Nat} -> (x : X t ** Y t x) -> StateCtrlSeq (S t) (S n) -> StateCtrlSeq t (S (S n))

> trj  :  {t, n : Nat} -> PolicySeq t n -> X t -> M (StateCtrlSeq t (S n))
> trj {t}  Nil      x  =  pure (Last x)
> trj {t} (p :: ps) x  =  let y   = p x in
>                         let mx' = next t x y in
>                         map ((x ** y) ##) (mx' >>= trj ps)

where we use |StateCtrlSeq| as type of trajectories. Essentially it is
a non-empty list of (dependent) state/control pairs, with the exception of the base case
which is a singleton just containing the last state reached.

Furthermore, we can compute the \emph{total reward} for a single
trajectory, i.e. its sum of rewards:

%if False

> head  :  {t, n : Nat} -> StateCtrlSeq t (S n) -> X t
> head (Last x)              =  x
> head ((x ** y) ## xys)  =  x

%endif

> sumR  :  {t, n : Nat} -> StateCtrlSeq t n -> Val
> sumR {t} (Last x)              =  zero
> sumR {t} ((x ** y) ## xys)  =  reward t x y (head xys) <+> sumR xys

where |head| is the helper function

< head  :  {t, n : Nat} -> StateCtrlSeq t (S n) -> X t
< head (Last x)              =  x
< head ((x ** y) ## xys)  =  x

By mapping |sumR| onto an |M|-structure of trajectories, we obtain an
|M|-structure containing the individual sums of rewards of the
trajectories. Now, using the measure function, we can compute the
generic analogue of the expected total reward for a policy sequence |ps|
and an initial state |x|:

> val'  :  {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> Val
> val' ps  =  meas . map sumR . trj ps

As anticipated in Sec.~\ref{section:SDPs} we call the value
computed by |val'| the \emph{measured total reward}. Recall that
solving a stochastic SDP commonly means finding a policy sequence that
maximises the \emph{expected total reward}. By analogy, we define that
solving a monadic SDP means to find a policy sequence that maximises
the \emph{measured total reward}. I.e. given |t| and |n|, the solution
of a monadic SDP is a sequence of |n| policies that maximises the measure
of the sum of rewards along all possible trajectories of length |n|
that are rooted in an initial state at step |t|. 

Again by analogy to the stochastic case, we define monadic
backward induction to be correct if, for a given SDP, the policy
sequence computed by |bi| is the solution to the SDP.
I.e., we consider |bi| to be correct if it meets the specification
%
%if False

> GenOptPolicySeq  :  {t, n : Nat} -> (PolicySeq t n -> X t -> Val) -> PolicySeq t n -> Type
> GenOptPolicySeq {t} {n} f ps  =  (ps' : PolicySeq t n) -> (x : X t) -> f ps' x <= f ps x

%endif

> biOptMeasTotalReward  :  (t, n : Nat) -> GenOptPolicySeq val' (bi t n)

where |GenOptPolicySeq| is a generalised version of the optimality
predicate |OptPolicySeq| from
Sec.~\ref{subsection:solution_components}. It now takes as an
additional parameter the function with respect to which the policy
sequence is to be optimal:

< GenOptPolicySeq  :  {t, n : Nat} ->  (PolicySeq t n -> X t -> Val) ->  PolicySeq t n -> Type
<
< GenOptPolicySeq {t} {n} f ps  =  (ps' : PolicySeq t n) -> (x : X t) -> f ps' x <= f ps x

As recapitulated in Sec.~\ref{subsection:solution_components},
\bottaetal have already shown that if |M| is a monad, |<=| a total
preorder and
|<+>| and |meas| fulfil two monotonicity conditions, then |bi t n|
yields an optimal policy sequence with respect to the value function
|val| in the sense that |val ps' x <= val (bi t n) x| for any
policy sequence |ps'| and initial state |x|, for arbitrary |t,
n : Nat|. Or, expressed using the generalised optimality predicate,
that the type

< GenOptPolicySeq {t} {n} val (bi t n)

is inhabited.
As seen in Sec.~\ref{subsection:solution_components}, the function
|val| measures and adds rewards incrementally. But does it always
compute the measured total reward like |val'|?
Modulo differences in the presentation \citet[Theorem
4.2.1]{puterman2014markov} suggests that for standard
stochastic SDPs, |val| and |val'| are extensionally equal, which in turn
allows the use of backward induction for solving these SDPs.
Generalising, we therefore consider |val| as
correct if it fulfils the specification

< valMeasTotalReward  :  {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> val ps x = val' ps x

If this equality held for the general monadic SDPs of the BJI-theory,
we could prove the correctness of |bi| as immediate corollary of
|valMeasTotalReward| and \bottaetal\!'s result |biOptVal|.
%
The statement |biOptMeasTotalReward| can be seen as a generic version
of textbook correctness statements for backward induction as solution
method for stochastic SDPs like \citep[prop.1.3.1]{bertsekas1995} or
\citep[Theorem~4.5.1.c]{puterman2014markov}.
By proving |valMeasTotalReward| we could therefore extend the
verification of \citep{2017_Botta_Jansson_Ionescu} and obtain a
stronger correctness result for monadic backward induction. 

\vspace{0.2cm}
Our main objective in the remainder of the paper is therefore to prove
that |valMeasTotalReward| holds.
%
But there is a problem.


\subsection{The problem with the BJI-value function}
\label{subsection:counterEx}
%
A closer look at |val| and |val'| reveals two quite different
computational patterns: applied to a policy sequence |ps| of length |n + 1|
and a state |x|, the function |val| directly evaluates |meas| on
the |M|-structure of rewards
corresponding to the possible next states after one step. This entails
further evaluations of |meas| for each possible next state.
%
By contrast, |val' ps x| entails only one evaluation of |meas|,
independently of the length of |ps|. The computation, however, builds up
an intermediate |M|-structure of state-control sequences. The elements of this
|M|-structure, the state-control sequences, are then consumed by |sumR|
and finally the |M|-structure of rewards is reduced by |meas|.

For illustration, let us revisit Ex.~\ref{subsection:example1SDPs} from
Sec.~\ref{section:SDPs} as formalised in
Fig.~\ref{fig:example1Formal}.
To do an example calculation with |val| and |val'| we first need a
concrete policy sequence as input.
The simplest two policies are the two constant policies:

> constH : (t : Nat) -> Policy t
> constH _t = const High
>
> constL : (t : Nat) -> Policy t
> constL _t = const Low

From these, we can define a policy sequence

> ps : PolicySeq 0 3
> ps = constH 0 :: (constL 1 :: (constH 2 :: Nil))

It is instructive to compute |val ps Good| and |val' ps
Good| by hand.  Recall that in this example, we have
|M = List| with |(>>=) = concatMap| and |Val = Nat| with |<+> = +|.
The measure |meas| thus needs to
have the type |List Nat -> Nat|. Without instantiating |meas| for the
moment, the computations roughly exhibit the structure

< val ps Good =  meas [2 + meas [3 + meas [2, 0]], 0 + meas [3 + meas [2, 0], 1 + meas [0]]]
<
< val' ps Good =  meas  [7, 5, 5, 3, 1]

and it is not ``obviously clear'' that |val| and |val'| are
extensionally equal without further knowledge about |meas|.

In the deterministic case, i.e. for |M = Id| and
|meas = id|, |val ps x| and |val' ps x| are indeed equal for all |ps|
and |x|, without imposing any further 
conditions (as we will see in Sec.~\ref{section:valval}).
For the stochastic case, \cite[Theorem
4.2.1]{puterman2014markov} suggests that the equality
should hold. But for the monadic case, no such result has been
established.
And as it turns out, in general the functions |val| and |val'| are not
unconditionally equal -- consider the following counter-example:
%
We continue in the setting of Ex.~\ref{subsection:example1SDPs}
from above, but now instantiate the measure to the plain arithmetic sum 

< meas = foldr (+) 0

% > Framework.meas = foldr (+) 0

This measure fulfils the monotonicity condition
(|measMonSpec|, Sec.~\ref{subsection:solution_components}) imposed by the
BJI-framework.
But if we instantiate the above computations with it, then we get |val
ps Good = 13| and |val' ps Good| = 21!
%
We thus see that the equality between |val| and |val'| cannot hold
unconditionally in the generic setting of the BJI-framework.
In the next section we therefore present conditions under which the
equality \emph{does} hold.
